<?php

/**
 * PCS Mexico
 *
 * Sistema de Control de Acceso
 *
 * @category   project
 * @package    Mail
 * @copyright  Copyright (c) 2007-2009 PCS Mexico (http://www.pcsmexico.com)
 * @author     Vicente Mendoza Moreno
 * @version    1.0
 */

/**
 * dependences
 */
require_once 'Swift/swift_required.php';

/**
 * Clase ZendMailer
 *
 * @category   project
 * @package    Project_Mail
 * @copyright  Copyright (c) 2007-2009 PCS Mexico (http://www.pcsmexico.com)
 * @author     Vicente Mendoza Moreno
 */
class ZendMailer extends Swift_Mailer 
{
    
    /**
     * Default Transport
     * @var unknown_type
     */
    protected static $defaultTransport;
    
    /**
     * Direccion
     * @var unknown_type
     */
    protected static $defaultFrom;
    
    /**
     * smarty 
     * @var ProjectSmarty
     */
    protected static $smarty = null;
    
    /**
     * Creates a new message.
     *
     * @param string|array $from    The from address
     * @param string|array $to      The recipient(s)
     * @param string       $subject The subject
     * @param string       $body    The body
     *
     * @return Swift_Message A Swift_Message instance
     */
    public function compose( $to = null, $subject = null, $body = null, $bodyType = 'text/html', $charset = 'iso-8859-1', $attachmentFile=NULL) 
    {
    	if(is_null($attachmentFile)){
	        return Swift_Message::newInstance()
		        ->setFrom( self::$defaultFrom )
		        ->setTo( $to )
		        ->setSubject( $subject )
		        ->setBody( $body, $bodyType, $charset);
    	}
    	else{
    		$attachment = Swift_Attachment::newInstance($attachmentFile, 'my-file.pdf', 'application/pdf'); 
	        return Swift_Message::newInstance()
		        ->setFrom( self::$defaultFrom )
		        ->setTo( $to )
		        ->setSubject( $subject )
		        ->setBody( $body, $bodyType, $charset)
		        ->attach($attachment);    		
    	}
    }
    
    /**
     * 
     * @param Swift_Transport $transport
     */
    public function __construct(Swift_Transport $transport = null)
    {
        if(null === $transport)
        {
            self::initialize();
            $transport = self::$defaultTransport;
        }
            
        parent::__construct($transport);
        Swift_Preferences::getInstance()->setCharset('iso-8859-1');
    }
    
    /**
     * 
     * @param Swift_Transport $transport
     */
    public static function newInstance(Swift_Transport $transport = null)
    {
        if(null === $transport)
        {
            self::initialize();
            $transport = self::$defaultTransport;
        }
            
        return parent::newInstance($transport);
    }
    
    /**
     * Sends a message.
     *
     * @param string|array $to      The recipient(s)
     * @param string       $subject The subject
     * @param string       $body    The body
     * @param string       $tpl
     * @param array        $parameters
     * @return int The number of sent emails
     */
    public function composeAndSend($to, $subject, $body, $tpl = null, $parameters = array(), $images = array(), $attachmentFiles=NULL) 
    {
        if (null != $tpl )
        {
            $request = new Zend_Controller_Request_Http();
        	self::$smarty->assign('host', $request->getServer('HTTP_HOST'));
            foreach ($parameters as $parameter => $value)
                 self::$smarty->assign($parameter, $value);
            foreach ($images as $image => $value)
                 self::$smarty->assign($image, $this->generateImageUrl($value));
            $body = self::$smarty->fetch($tpl);
        }
           
        return $this->send( $this->compose( $to, $subject, $body, 'text/html','iso-8859-1',$attachmentFiles) );
    }
    
    /**
     * Adjunta una imagen al cuerpo del mensaje
     * @param string $path la direccion de la imagen
     */
    private function generateImageUrl($path)
    {
        $this->request = new Zend_Controller_Request_Http();
        return 'http://' . $this->request->getServer('HTTP_HOST') . $this->request->getBaseUrl() . '/' . $path;
    }
    
    /**
     * Iniciliza el transport
     */
    public static function initialize()
    {
        require_once 'Zend/Registry.php';
        $registry = Zend_Registry::getInstance();
        
        $configMailer = $registry->get('config')->mailer;
        
        self::$smarty = $registry->smarty;
        
        self::$defaultFrom = $configMailer->fromMail;
        
        self::$defaultTransport = Swift_SmtpTransport::newInstance($configMailer->host, $configMailer->port, $configMailer->security)
	        ->setUsername($configMailer->user)
	        ->setPassword($configMailer->password);
    }

}