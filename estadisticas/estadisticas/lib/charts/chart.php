<?php
/**
 * PCS Mexico
 *
 * Sistema de Control de Acceso
 *
 * @category   Project
 * @package    Project_Report
 * @copyright  Copyright (c) 2007-2008 PCS Mexico (http://www.pcsmexico.com)
 * @version    1.0
 * @author     Mendoza Moreno Vicente
 */

/**
 * Clase MSChart para manejar las fusion chart
 *
 * @category   Project
 * @package    Project_Report
 * @copyright  Copyright (c) 2007-2008 PCS Mexico (http://www.pcsmexico.com)
 */
require_once 'lib/charts/XmlWriter.class.php';
class Chart
{
  
  /**
   * Constructor
   * @param string $caption
   * @param string $xAxisName
   * @param string $yAxisName
   * @param array $attributes
   */
  public function __construct($caption = '', $series = '', $attributes = null)
  {
		$array = array(
		    array('monkey', 'banana', 'Jim', 1),
		    array('hamster', 'apples', 'Kola', 2),
		    array('turtle', 'beans', 'Berty', 3),
		);
		$xml = new XmlWriter2();
		$xml->push('chart');
		foreach ($array as $animal) {
		    $xml->push('animal', array('species' => $animal[0]));
		    $xml->element('name', $animal[2], array('xid'=>$animal[3]));
		    $xml->element('food', $animal[1], array('xid'=>$animal[3]));
		    $xml->pop();
		}
		$xml->pop();
		return $xml->getXml(); 	    
  }
  

  
  /**
   * Manda el documento xml al explorador
   */
  public function toBrowser()
  {
    
    
  }
  
  
  
  
}