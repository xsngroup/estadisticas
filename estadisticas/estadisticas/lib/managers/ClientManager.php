<?php
require_once "application/models/catalogs/CompanyCatalog.php";
require_once "application/models/catalogs/StreamServerCatalog.php";
require_once 'lib/managers/ManagerException.php';


class ClientManager
{
	protected static $instance = null;
	private $companyCatalog;
	private $serverCatalog;
	private $idUser;
	private $company;
	private $servers;
	private $relations;
	private $lives;
	private $vods;
	
	protected function ClientManager()
	{
		$this->companyCatalog= CompanyCatalog::getInstance();
		$this->serverCatalog= StreamServerCatalog::getInstance();
		$this->idUser=$this->getUser()->getIdUser();
	}
	
 	public static function getInstance()
 	{
 		if (!isset(self::$instance))
        {
          self::$instance = new ClientManager();
        }
        return self::$instance;
 	}
 	
    /**
     * Obtiene la sesion del usuario
     * @return UserSession
     */
    private function getUser()
    {
    	require_once 'lib/security/UserSession.php';
    	return UserSession::getInstance();
    } 	
 	
 	public function getCompany()
 	{
 	    $this->company= $this->companyCatalog->getByUser($this->idUser)->getOne();

	    if(!$this->company instanceof Company){
        	return "No existe ningun cliente asociado";
        }
        
        return $this->company;
 	}
 	
 	public function getServers()
 	{
        $this->servers = $this->serverCatalog->getByCompany($this->company->getIdCompany());
        if($this->servers->isEmpty()){
        	return "No existe ningun servidor asociado";        	
        } 

        return $this->servers;
 	}
 	
 	public function getCompanyServerIds()
 	{
 		$criteria = new Criteria();
 		
 		$company = $this->getCompany();
 		if($company instanceof Company){  				 		
	 		$criteria->add('id_company',$company->getIdCompany(),Criteria::EQUAL);
	 		$servers = $this->getServers();
	 		foreach($servers as $server){
	 		   $criteria->add('id_server', $server->getIdServer(), Criteria::EQUAL);
	 		}
 		}
 		$this->relations = $this->companyCatalog->getCompanyStreamServerRelations($criteria);
 		
 		return $this->relations;
 	}
 	
 	public function getLiveStream()
 	{
 		require_once 'application/models/catalogs/FlashStreamCatalog.php';
 		
 		$criteria = new Criteria();
 		
 		$company = $this->getCompany();
 		
 		if($company instanceof Company){ 		 		
	 		$criteria->add('id_company',$company->getIdCompany(),Criteria::EQUAL);
	 		$servers = $this->getServers();
	 		
	 		foreach($servers as $server){
	 		   $criteria->add('id_server', $server->getIdServer(), Criteria::EQUAL);
	 		}
 		}
 		$this->relations = $this->companyCatalog->getCompanyStreamServerRelations($criteria);
 		
 		$ids= array();
 		
 		if(count($this->relations)>0){
 			foreach($this->relations as $item){
 				if(!is_null($item['id_stream_flash'])){
 					$ids[]=$item['id_stream_flash'];
 				}
 			}
 		}

 		$this->lives= FlashStreamCatalog::getInstance()->getByIds($ids);
 		
 		return $this->lives;
 	}
 	
 	public function getVodStream()
 	{
 		require_once 'application/models/catalogs/FlashVodCatalog.php';
 		
		$criteria = new Criteria();
 		
 		$company = $this->getCompany();
 		
 		
 		if($company instanceof Company){
 		$criteria->add('id_company',$company->getIdCompany(),Criteria::EQUAL);
 		$servers = $this->getServers();
 		
	 		foreach($servers as $server){
	 		   $criteria->add('id_server', $server->getIdServer(), Criteria::EQUAL);
	 		}
 		}
 		$this->relations = $this->companyCatalog->getCompanyStreamServerRelations($criteria);
 		
 		$ids= array();
 		
 		if(count($this->relations)>0){
 			foreach($this->relations as $item){
 				if(!is_null($item['id_vod_flash'])){
 					$ids[]=$item['id_vod_flash'];
 				}
 			}
 		}

 		$this->vods= FlashVodCatalog::getInstance()->getByIds($ids);
 		
 		return $this->vods; 		
 	}
}