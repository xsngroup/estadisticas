$(document).ready(function(){
	$('#coin-slider').coinslider({width:850,height:530,spw:12,sph:10,navigation: true});
	$('#coin-slider-2').coinslider({width:328,height:493,spw:6,sph:4,navigation: true});
	
	
	  var typeSecurity=2;	
	  $("#customeTheme").attr("autocomplete","off");
	  $("#customeTheme").formToWizard({ submitButton: 'generate' });
	
	  $('.preview').click(function(){	  	
			var url = "http://" + domain + "/xStream/public/auth/free/codeRef/" + codeRef;    
			 window.open(url);
			 return false;	
			//alert(domain+" "+codeRef);
		   });
		   $('.colorTheme').click(function(){
			      $.post(baseUrl + "/custome-theme/select-color", {
			    		color: $(this).val(),
			    		idCompany: $("#id_company").val()
			    	  }, function(data){
			    		if (data.length > 0) {
			    			$('#loadresponse').fadeIn('slow', function() {
			    				$('#loadresponse').show();
			    				$('#loadresponse').html("<p>"+data+"</p>");
			    				setTimeout(hideDiv ,3000);
			    			});			    			
			    		}
			    	  });      
			});

		   $('.typesidebar').click(function(){
			      $.post(baseUrl + "/custome-theme/select-sidebar", {
			    		sidebar: $(this).val(),
			    		idCompany: $("#id_company").val()
			    	  }, function(data){
			    		if (data.length > 0) {
			    			$('#loadresponse').fadeIn('slow', function() {
			    				$('#loadresponse').show();
			    				$('#loadresponse').html("<p>"+data+"</p>");
			    				setTimeout(hideDiv ,3000);
			    			});	
			    		}
			    	  });   
			      if($(this).val()==2){
			    	  $("#loadppt").show();
			      }
			      else{
			    	  $("#loadppt").hide();
			      }
			});
		   
		   $('#saveppt').click(function(){
			      $.post(baseUrl + "/custome-theme/upload-ppt", {
			    		signal:$("#nameppt").val(),
			    		idCompany: $("#id_company").val()
			    	  }, function(data){
			    		if (data.length > 0) {
			    			$('#loadresponse').fadeIn('slow', function() {
			    				$('#loadresponse').show();
			    				$('#loadresponse').html("<p>"+data+"</p>");
			    				$("#loadppt").hide();
			    				setTimeout(hideDiv ,3000);			    				
			    			});	
			    		}
			    	  });   			    	 
			});		   
		   
		   $('.typeinit').click(function(){
		   	      typeSecurity= $(this).val();
			      $.post(baseUrl + "/custome-theme/select-init", {
			    		init:typeSecurity,
			    		idCompany: $("#id_company").val()
			    	  }, function(data){
			    		if (data.length > 0) {
			    			$('#loadresponse').fadeIn('slow', function() {
			    				$('#loadresponse').show();
			    				$('#loadresponse').html("<p>"+data+"</p>");
			    				setTimeout(hideDiv ,3000);
			    			});	
			    		}
			    	  });  
			      if($(this).val()==1){
			    	  $("#loadsecure").show();
			      }
			      else{
			    	  $("#loadsecure").hide();
			      }			      
			});	
		   
		   $('.buttons').click(function(){
			   var buttons="";
			   var indice=0;
			   $('[name=buttons]:checked').each(function() {                   
			       if(indice==0){
			    	   buttons=$(this).val();
			       }
			       else{
			    	   buttons+='|'+$(this).val();
			       }
			       indice++;
			     });
			   //$('#loadresponse').html(buttons);
			     
			      $.post(baseUrl + "/custome-theme/select-buttons", {
			    		buttons: buttons,
			    		idCompany: $("#id_company").val()
			    	  }, function(data){
			    		if (data.length > 0) {
			    			$('#loadresponse').fadeIn('slow', function() {
			    				$('#loadresponse').show();
			    				$('#loadresponse').html("<p>"+data+"</p>");
			    				setTimeout(hideDiv ,3000);
			    			});	
			    		}
			    	  });
			    	        
			});			   	   
		   
			$(".validateCustom").validate({
				submitHandler :  function(form){
					if ($(form).hasClass('ajaxForm')) {
						$(form).find('input[type=text]').addClass('ac_loading');
						$(form).ajaxSubmit({
							success: ajaxForm.callback,
							clearForm: true,
							error: ajaxGeneric.errorCallback
						});
					}
					else {
						//alert("oks");
					      if(typeSecurity==1){
					    	  var usernameCbx=$("#usernameCbx").attr("checked");
					    	  var passwordCbx=$("#passwordCbx").attr("checked");
					    	  var number=parseInt($("#noAccess").val());
					    	 if((usernameCbx | passwordCbx) & number > 0)
					    	 {
					    		 form.submit();
					    		 //alert("si esta bien");
					    	 }
					    	 else{
					    		 alert("Debes seleccionar alguna opcion de acceso para generar y el numero debe ser mayor que: 0"); 
					    	 }
					      }	
					      else{
					    	     form.submit();
					      }
					}	
				}
			});	
			
		   $('.skinPcEvent').click(function(){
			   	if ($(this).val()==1) {
					$('#choiceColor').show();
				}
				else{				
					$('#choiceColor').hide();
				}
				
				switch(parseInt($(this).val())){
					case 1:
					//alert("uno");
					        $('#medidaLogo').html("Medida: 252 x 90 pixeles");
							$('#medidaBanner1').html("Medida: 728 x 90 pixeles");
							$('#medidaBanner2').html("Medida: 347 x 108 pixeles");
					        $('#imagesTheme1').show();
							$('#imagesTheme2').hide();
					  break;
					case 2:
					        $('#medidaLogo').html("Medida: 170 x 75 pixeles");
							$('#medidaBanner1').html("Medida: 720 x 165 pixeles");
							$('#medidaFondo').html("Medida: 1850 x 900 pixeles");					
					        $('#imagesTheme1').hide();
							$('#imagesTheme2').show();					
					  break;
					  
					case 3:
					        $('#medidaLogo').html("Medida: 200 x 75 pixeles");
							$('#medidaBanner1').html("Medida: 500 x 75 pixeles");
							$('#medidaFondo').html("Medida: 1600 x 1692 pixeles");					
					        $('#imagesTheme1').hide();
							$('#imagesTheme2').show();					
					  break;					  
				}
						   	
			      $.post(baseUrl + "/custome-theme/select-skin-pc", {
			    		idSkin: $(this).val(),
			    		idCompany: $("#id_company").val()
			    	  }, function(data){
			    		if (data.length > 0) {
			    			$('#loadresponse').fadeIn('slow', function() {
			    				$('#loadresponse').show();
			    				$('#loadresponse').html("<p>"+data+"</p>");
			    				setTimeout(hideDiv ,3000);
			    			});			    			
			    		}
			    	  });      
			});
		   
		   $('.skinMobileEvent').click(function(){
			      $.post(baseUrl + "/custome-theme/select-skin-mobile", {
			    		idSkin: $(this).val(),
			    		idCompany: $("#id_company").val()
			    	  }, function(data){
			    		if (data.length > 0) {
			    			$('#loadresponse').fadeIn('slow', function() {
			    				$('#loadresponse').show();
			    				$('#loadresponse').html("<p>"+data+"</p>");
			    				setTimeout(hideDiv ,3000);
			    			});			    			
			    		}
			    	  });      
			});		   
			
});	
ajaxForm = { callback : function(e)
			{
				$('.ac_loading').removeClass('ac_loading');
				var needOdd = $('#ajaxList tr:last-child').hasClass('odd') ? false : true;
				$('#ajaxList').append(e);
				if(needOdd)
					$('#ajaxList tr:last-child').addClass('odd');
			}}

ajaxGeneric = { errorCallback : function(e) { $('#contentPanel').prepend('<div class=error>'+e.responseText+'</div>'); }}

function hideDiv(){
	$('#loadresponse').fadeOut('slow', function() {
	    $('#loadresponse').hide();
	});
}