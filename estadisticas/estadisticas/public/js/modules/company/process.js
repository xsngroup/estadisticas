/**
 * 
 */
$(document).ready(function(){
	    $(".selectLive").change(function(){
		var combos = new Array();
		combos['id_server'] = "id_live";
		combos['id_live'] ="id_stream";
		var posicion = $(this).attr("name");
		var valor = $(this).val();
		    if(posicion == 'id_server' && valor!=0){
				$("#id_stream").html('	<option value="0" selected="selected">Selecciona un montaje...</option>');
		    } 		
			if(posicion == 'id_server' && valor==0){
				$("#id_live").html('	<option value="0" selected="selected">Selecciona un servidor...</option>');
				$("#id_stream").html('	<option value="0" selected="selected">Selecciona un montaje...</option>');
			}else{
				$("#"+combos[posicion]).html('<option selected="selected" value="0">Cargando...</option>')
				if(valor!="0" || posicion !='id_stream'){
					$.post(baseUrl +"/company/combos",{
					combo:$(this).attr("name"), // Nombre del combo
					id:$(this).val() // Valor seleccionado
					},function(data){
					   $("#"+combos[posicion]).html(data);																																
					  })												
				}
			}
	    });
		
	    $(".selectLiveHelix").change(function(){
		var combos = new Array();
		combos['id_server_helix1'] = "id_live_helix";
		combos['id_live_helix'] ="id_stream_helix";
		var posicion = $(this).attr("name");
		var valor = $(this).val();
	    if(posicion == 'id_server_helix1' && valor!=0){
			$("#id_stream_helix").html('	<option value="0" selected="selected">Selecciona un montaje...</option>');
	    } 		
			if(posicion == 'id_server_helix1' && valor==0){
				$("#id_live_helix").html('	<option value="0" selected="selected">Selecciona un servidor...</option>');
				$("#id_stream_helix").html('	<option value="0" selected="selected">Selecciona un montaje...</option>');
			}else{
				$("#"+combos[posicion]).html('<option selected="selected" value="0">Cargando...</option>')
				if(valor!="0" || posicion !='id_stream_helix'){
					$.post(baseUrl +"/company/combos",{
					combo:$(this).attr("name"), // Nombre del combo
					id:$(this).val() // Valor seleccionado
					},function(data){
					   $("#"+combos[posicion]).html(data);																																
					  })												
				}
			}
	    });		
	
	    $(".selectVod").change(function(){
		var combos = new Array();
		combos['id_server2'] = "id_vod";
		var posicion = $(this).attr("name");
		var valor = $(this).val()		
		if(posicion == 'id_server2' && valor==0){
			$("#id_vod").html('	<option value="0" selected="selected">Selecciona un servidor...</option>');
		}else{
			$("#"+combos[posicion]).html('<option selected="selected" value="0">Cargando...</option>')
			if(valor!="0" || posicion !='id_vod'){
				$.post(baseUrl +"/company/combos",{
									combo:$(this).attr("name"),
									id:$(this).val()
									},function(data){
													$("#"+combos[posicion]).html(data);																																	
													})												
			}
		}
	    });	
		
	    $(".selectVodHelix").change(function(){
		var combos = new Array();
		combos['id_server_helix2'] = "id_vod_helix";
		var posicion = $(this).attr("name");
		var valor = $(this).val()		
		if(posicion == 'id_server_helix2' && valor==0){
			$("#id_vod_helix").html('	<option value="0" selected="selected">Selecciona un servidor...</option>');
		}else{
			$("#"+combos[posicion]).html('<option selected="selected" value="0">Cargando...</option>')
			if(valor!="0" || posicion !='id_vod_helix'){
				$.post(baseUrl +"/company/combos",{
									combo:$(this).attr("name"),
									id:$(this).val()
									},function(data){
													$("#"+combos[posicion]).html(data);																																	
													})												
			}
		}
	    });			
	
	$('.typeRelation').click(function(){
	      if($(this).val()==1){
	    	  $(".loadLive").show();
			  $(".loadVod").hide();
	      }
	      else{
		  	$(".loadLive").hide();
	    	  $(".loadVod").show();
	      }	
	});
	
	$('.typeStreamServer').click(function(){
	      if($(this).val()==1){
	    	  $("#displayFlashOption").show();
			  $("#displayHelixOption").hide();
	      }
	      else{
		  	$("#displayFlashOption").hide();
	    	  $("#displayHelixOption").show();
	      }	
	});	
});	