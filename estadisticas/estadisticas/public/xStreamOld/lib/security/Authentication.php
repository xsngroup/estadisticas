<?php
/**
 * ##$BRAND_NAME$##
 *
 * ##$DESCRIPTION$##
 *
 * @category   Project
 * @package    Project_Security
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */

require_once 'lib/security/AuthException.php';
require_once 'application/models/catalogs/UserCatalog.php';
require_once 'application/models/catalogs/UserLogCatalog.php';
require_once 'application/models/catalogs/UsertempCatalog.php';
require_once 'application/models/catalogs/UsertempLogCatalog.php';

/**
 * Esta clase funge como el actor que realiza la autenticacion del usuario
 *
 * @category   project
 * @package    Project_Security
 * @copyright  ##$COPYRIGHT$##
 */
class Authentication
{

    /**
     * Metodo que autentica si existe el usuario y la contrase�a en la base de datos de lo contrario tira una excepcion.
     * ademas si existe guarda una variable en sesion la cual es util para saber si el usuario ya ha sido autenticado.
     * @param string $username Nombre de usuario
     * @param string $password Password
     * @return User $user
     * @throws Exception Si no existen coincidencias en el usuario/password
     */
    public static function authenticate($username, $password)
    {
    	$request = new Zend_Controller_Request_Http();
        try
        {
            $criteria = new Criteria();
            $criteria->add(Usertemp::USERNAME, $username, Criteria::EQUAL);
            $criteria->add(Usertemp::PASSWORD, $password, Criteria::EQUAL, Criteria::PASSWORD);                      
            $criteria->setLimit(1);
            $userTempCatalog=UsertempCatalog::getInstance();
            $user = $userTempCatalog->getByCriteria($criteria)->getOne();
            
            if(!($user instanceof Usertemp))
                throw new AuthException("Usuario y/o clave inv�lidos");            
                
            if($user->getStatus() == 0)
                throw new AuthException("Usuario desactivado");
            
			
            $date=$date = date('Y-m-d H:i:s');
        	$userLog = UsertempLogFactory::create( UsertempLog::LOGIN, $request->getServer("REMOTE_ADDR"),  $date, 'Inicio de sesi�n '.$user->getUsername(), $user->getIdUsertemp());
          
            return $user;
        }
        catch(AuthException $e)
        {
			if($username != null)
			{
				$criteria = new Criteria();
			    $criteria->add(Usertemp::USERNAME, $username, Criteria::EQUAL);			    
			    $criteria->setLimit(1);
			    $user = UsertempCatalog::getInstance()->getByCriteria($criteria)->getOne();
			    if($user instanceof Usertemp)
			    {
			    	$date=$date = date('Y-m-d H:i:s');
			        $userLog = UsertempLogFactory::create(UsertempLog::FAILED_LOGIN, $request->getServer("REMOTE_ADDR"),  $date, 'Fall�, inicio de sesi�n',$user->getIdUsertemp());
			        UsertempLogCatalog::getInstance()->create($userLog);
			    }
			}
			throw $e;
        }
    }
    
    public static function authenticateUserTemp($username=null, $password, $code=null)
    {
    	$request = new Zend_Controller_Request_Http();
        try
        {
            $criteria = new Criteria();
            //$criteria->setMode(Criteria::INCLUSIVE);
            //$criteria->add(Usertemp::USERNAME, $username, Criteria::EQUAL);
            $criteria->add(Usertemp::PASSWORD, $password, Criteria::EQUAL);              
            $criteria->setLimit(1);
            
            $userTempCatalog=UsertempCatalog::getInstance();
            $user = $userTempCatalog->getByCriteria($criteria)->getOne();
            
            if(!($user instanceof Usertemp))
                throw new AuthException("Usuario y/o clave inv�lidos");
                
            if($user->getStatus() == 2)
                throw new AuthException("Usuario desactivado");	
           // if($user->getStatus() == 3)   
             //    throw new AuthException("Usuario actualmente logueado"); 
                
            /*
           
            $user->setPassword($password);
            */
            //$user->setUsername($username);
            //$user->setCode($code);     
            //$user->setStatus(Usertemp::$Status['Bussy']);    
            //$userTempCatalog->update($user);
            
            
            $date=$date = date('Y-m-d H:i:s');
			$flag=0;
        	$userLog = UsertempLogFactory::create( UsertempLog::LOGIN, $request->getServer("REMOTE_ADDR"),  $date, 'Inicio de sesi�n '.$user->getUsername(), $user->getIdUsertemp());
            //UsertempLogCatalog::getInstance()->create($userLog);
			$fechaone=$user->getDateCreate();
			$userGet=UsertempLogCatalog::getInstance()->getById($user->getIdUsertemp(),false);
			  
			 $oldArray     = explode(" ", $date.'');
             $oldSubArray  = explode("-", $oldArray[0]);
		    $oldSubArray2=explode(":",$oldArray[1]);
            $timestamp1   = mktime($oldSubArray2[0],$oldSubArray2[1],$oldSubArray2[2], $oldSubArray[1], $oldSubArray[2], $oldSubArray[0]);
            $newArray     = explode(" ", $fechaone);
           $newSubArray  = explode("-", $newArray[0]);
		  $newSubArray2= explode(":",$newArray[1]);
          $timestamp2   = mktime($newSubArray2[0],$newSubArray2[1],$newSubArray2[2], $newSubArray[1], $newSubArray[2], $newSubArray[0]);
          $segundos_dif = $timestamp1 - $timestamp2;
			if($segundos_dif<=(3*60)){
			throw new AuthException("La contrase�a ya ha sido utilizada"); 
			}else{
			$flag=1;
			}
			if(($userGet==null && $user->getIdCompany()==9)|| $flag==1){
			UsertempLogCatalog::getInstance()->create($userLog);
			}else{
			throw new AuthException("La contrase�a ya ha sido utilizada"); 
			}
            return $user;
        }
        catch(AuthException $e)
        {
			if($username != null)
			{
				$criteria = new Criteria();
			    $criteria->add(Usertemp::USERNAME, $username, Criteria::EQUAL);			    
			    $criteria->setLimit(1);
                $userTempCatalog=UsertempCatalog::getInstance();
                $user = $userTempCatalog->getByCriteria($criteria)->getOne();
			    if($user instanceof Usertemp)
			    {
			    	$date=$date = date('Y-m-d H:i:s');
			        $userLog = UsertempLogFactory::create(UsertempLog::FAILED_LOGIN, $request->getServer("REMOTE_ADDR"),  $date, 'Fall�, inicio de sesi�n',$user->getIdUsertemp());
			        UsertempLogCatalog::getInstance()->create($userLog);
			    }
			}
			throw $e;
        }
    }    
    
    public static function authenticateFree($username)
    {
        	$request = new Zend_Controller_Request_Http();
        try
        {
            $criteria = new Criteria();
            //$criteria->setMode(Criteria::INCLUSIVE);
            $criteria->add(Usertemp::USERNAME, $username, Criteria::EQUAL);              
            $criteria->setLimit(1);
            
            $userTempCatalog=UsertempCatalog::getInstance();
            $user = $userTempCatalog->getByCriteria($criteria)->getOne();
            
            if(!($user instanceof Usertemp))
                throw new AuthException("Usuario y/o clave inv�lidos");
                
            if($user->getStatus() == 2)
                throw new AuthException("Usuario desactivado");
                                
            /*
            $user->setUsername($username);
            $user->setPassword($password);
            
            $user->setStatus(Usertemp::$Status['Bussy']);    
            $userTempCatalog->update($user);
            */
            
            $date=$date = date('Y-m-d H:i:s');
        	$userLog = UsertempLogFactory::create( UsertempLog::LOGIN, $request->getServer("REMOTE_ADDR"),  $date, 'Inicio de sesi�n '.$user->getUsername(), $user->getIdUsertemp());
            UsertempLogCatalog::getInstance()->create($userLog);
            return $user;
        }
        catch(AuthException $e)
        {
			if($username != null)
			{
				$criteria = new Criteria();
			    $criteria->add(Usertemp::USERNAME, $username, Criteria::EQUAL);			    
			    $criteria->setLimit(1);
                $userTempCatalog=UsertempCatalog::getInstance();
                $user = $userTempCatalog->getByCriteria($criteria)->getOne();
			    if($user instanceof Usertemp)
			    {
			    	$date=$date = date('Y-m-d H:i:s');
			        $userLog = UsertempLogFactory::create(UsertempLog::FAILED_LOGIN, $request->getServer("REMOTE_ADDR"),  $date, 'Fall�, inicio de sesi�n',$user->getIdUsertemp());
			        UsertempLogCatalog::getInstance()->create($userLog);
			    }
			}
			throw $e;
        }
    }    
    
    /**
     * 
     * @param string $username
     * @param string $password
     * @param int $idAccessRole
     * @return $user
     */
    public static function authenticateAs($username, $password, $idAccessRole)
    {
    	$user = self::authenticate($username,$password);
    	if($user->getIdAccessRole() != $idAccessRole)
    		throw new AuthException('El usuario no tiene permisos suficientes');
        return $user;
    }
    
}


