<?php

/**
 * ##$BRAND_NAME$##
 *
 * ##$DESCRIPTION$##
 *
 * @category   Lib
 * @package    Lib_Security
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */

/**
 * Zend_Session_Namespace
 */
require_once 'Zend/Session/Namespace.php';

/**
 * Clase para manejar la session del usuario
 *
 * @category   Lib
 * @package    Lib_Security
 * @copyright  ##$COPYRIGHT$##
 */
 class UserSession 
 {
 	
 	/** 
 	 * Instancia de UserSession
 	 * @staticvar UserSession $instance
 	 */
 	protected static $instance = null;
 	
 	/**
 	 * Variable para prevenir colisiones de sesiones 
 	 * @var string
 	 */
 	protected $fingerprint = null;
 	
 	/**
 	 * Constructor
 	 */
 	protected function UserSession()
 	{
 	    $registry = Zend_Registry::getInstance();
 	    $this->fingerprint = 'zf_' . md5($registry->config->report->title . '@' . $registry->config->database->params->dbname);
 	}
 	
 	/**
 	 * Singleton Obtiene una instancia
 	 * @return UserSession
 	 */
 	public static function getInstance()
 	{
 		if (!isset(self::$instance))
        {
          self::$instance = new UserSession();
        }
        return self::$instance;
 	}
 	
 	/**
 	 * Guarda un atributo en la sesion del usuario
 	 * @param string $name
 	 * @param mixed $value
 	 * @param string $ns Namespace 
 	 */
    public function setAttribute($name, $value, $ns = 'commonTheme')
 	{
 	    $session = new Zend_Session_Namespace($this->fingerprint . '@' . $ns);
 	    $session->{$name} = $value;
 	}
 	
 	/**
 	 * Obtiene un atributo de la sesion del usuario
 	 * @param string $name
 	 * @param mixed $default
 	 * @param string $ns Namespace
 	 * @return mixed
 	 */
 	public function getAttribute($name, $default = null, $ns = 'commonTheme')
 	{
 	    $session = new Zend_Session_Namespace($this->fingerprint . '@' . $ns);
 	    return isset($session->{$name}) ? $session->{$name} : $default;
 	}
 	
 	/**
 	 * Pregunta si existe un atributo en la sesion
 	 * @param string $name
 	 * @param string $ns Namespace
 	 * @return bool
 	 */
 	public function hasAttribute($name, $ns = 'commonTheme')
 	{
 	    $session = new Zend_Session_Namespace($this->fingerprint . '@' . $ns);
 	    return isset($session->{$name});
 	}
 	
 	/**
 	 * Elimina un atributo de la sesion del usuario
 	 * @param string $name
 	 * @param string $ns
 	 */
 	public function removeAttribute($name, $ns = 'commonTheme')
 	{
 	    $session = new Zend_Session_Namespace($this->fingerprint . '@' . $ns);
 	    unset($session->{$name});
 	}
 	
 	/**
 	 * Agrega una variable en sesion que se destruye al ser recuperada
 	 * @param string $name
 	 * @param mixed $value
 	 */
 	public function setFlash($name, $value)
 	{
 	    $this->setAttribute($name, $value, 'flashTheme');
 	}
 	
 	/**
 	 * Obtiene un variable flash y la destruye inmediatamente
 	 * @param string $name
 	 * @param mixed $default
 	 * @return mixed
 	 */
 	public function getFlash($name, $default = null)
 	{
 	    $flash = $this->getAttribute($name, $default, 'flashTheme');
 	    $this->removeAttribute($name, 'flashTheme');
 	    return $flash;
 	}
 	
 	/**
 	 * proxy Obtiene el IdUser
 	 * @return int
 	 */
 	public function getIdUser()
 	{
 	    return $this->getBean()->getIdUser();
 	}
 	
 	/**
 	 * proxy Obtiene el idPerson
 	 * @return int
 	 */
 	public function getIdPerson()
 	{
 	    return $this->getBean()->getIdPerson();
 	}
 	
 	/**
 	 * proxy Obtiene el username
 	 * @return string
 	 */
 	public function getUsername()
 	{
 	    return $this->getBean()->getUsername();
 	}
 	
 	/**
 	 * Retorna el Bean del Usuario
 	 * @return User
 	 */
 	public function getBean()
 	{
 	    require_once 'application/models/beans/User.php';
 	    return $this->getAttribute('userTheme', null, 'beanTheme');
 	}
 	
 	/**
 	 * Asignar el Bean para el usuario que esta en session
 	 * @param User $user
 	 */
 	public function setBean(User $user)
 	{
 	    $this->setAttribute('userTheme', $user, 'beanTheme');
 	}

 	
  	/**
 	 * proxy Obtiene el IdUsertemp
 	 * @return int
 	 */
 	public function getIdUserTemp()
 	{
 	    return $this->getBeanTemp()->getIdUsertemp();
 	} 	
 	
  	/**
 	 * proxy Obtiene el usernametemp
 	 * @return string
 	 */
 	public function getUsernameTemp()
 	{
 	    return $this->getBeanTemp()->getUsername();
 	} 	
 	
  	/**
 	 * proxy Obtiene el idcompany
 	 * @return string
 	 */
 	public function getIdCompanyTemp()
 	{
 	    return $this->getBeanTemp()->getIdCompany();
 	} 	
 	
  	/**
 	 * Retorna el Bean del Usuariotemp
 	 * @return Usertemp
 	 */
 	public function getBeanTemp()
 	{
 	    require_once 'application/models/beans/Usertemp.php';
 	    return $this->getAttribute('usertemp', null, 'beanTheme');
 	}
 	
 	/**
 	 * Asignar el Bean para el usuariotemp que esta en session
 	 * @param Usertemp $user
 	 */
 	public function setBeanTemp(Usertemp $usertemp)
 	{
 	    $this->setAttribute('usertemp', $usertemp, 'beanTheme');
 	} 	
 	
  	/**
 	 * Retorna el flag
 	 * @return flag
 	 */
 	public function getFlag()
 	{
 	    return $this->getAttribute('flagTheme', 0);
 	}
 	
 	/**
 	 * Asignar el flag
 	 * @param flag int
 	 */
 	public function setFlag($flag)
 	{
 	    $this->setAttribute('flagTheme', $flag);
 	} 	
 	
 	/**
 	 * Asigna el Bean de AccessRole en la sesi�n
 	 * @param AccessRole $accessRole
 	 */
 	public function setAccessRole(AccessRole $accessRole)
 	{
 		$this->setAttribute('accessroleTheme',$accessRole,'accessroleTheme');
 	}
 	
    /**
     * Retorna el Grupo de Usuario al que pertenece el Usuario
     * @return AccessRole
     */
    public function getAccessRole()
    {
        require_once 'application/models/beans/AccessRole.php';
        return $this->getAttribute('accessroleTheme', null, 'accessroleTheme');
    }
    
  	/**
 	 * Retorna el CustomeTheme
 	 * @return CustomeTheme
 	 */
 	public function getCustomerTheme()
 	{
 	    return $this->getAttribute('customerTheme', null);
 	}
 	
 	/**
 	 * Asignar el CustomeTheme
 	 * @param CustomeTheme $CustomeTheme
 	 */
 	public function setCustomerTheme(CustomeTheme $customerTheme)
 	{
 	    $this->setAttribute('customerTheme', $customerTheme);
 	}     
 	
 	/**
 	 * Destruye la sesion del usuario
 	 */
 	public function shutdown()
 	{
 	    require_once 'Zend/Session.php';
 	    Zend_Session::start();
 	    Zend_Session::destroy();
 	}
 	
 }