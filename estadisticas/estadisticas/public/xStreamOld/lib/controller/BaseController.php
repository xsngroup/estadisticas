<?php
/**
 * ##$BRAND_NAME$##
 *
 * ##$DESCRIPTION$##
 *
 * @category   Project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */

/**
 * Dependencias
 */
require_once 'application/models/catalogs/AccessRoleCatalog.php';
require_once 'application/models/catalogs/CompanyCatalog.php';
require_once 'application/models/catalogs/CustomeThemeCatalog.php';
require_once 'application/models/catalogs/FlashStreamCatalog.php';
require_once 'application/models/catalogs/StreamServerCatalog.php';
require_once 'application/models/catalogs/FlashVodCatalog.php';
require_once 'application/models/catalogs/LogoCatalog.php';
require_once 'application/models/catalogs/BannerFullCatalog.php';
require_once 'application/models/catalogs/BannerMiniCatalog.php';
require_once 'application/models/catalogs/PptCatalog.php';
require_once 'application/models/catalogs/PptJpgCatalog.php';
require_once 'application/models/catalogs/ViewerCatalog.php';
require_once 'lib/security/Authentication.php';
require_once 'lib/security/AuthException.php';
require_once 'lib/security/ManagerAcl.php';
require_once 'lib/menu/Menu.php';
require_once 'Zend/Translate.php';                
require_once 'Zend/Controller/Action.php';
require_once 'Zend/Date.php';

/**
 * Clase abstracta de la que extenderan nuestros controladores, para agrupar instrucciones comunes
 *
 * @category   project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 */
abstract class BaseController extends Zend_Controller_Action
{

    /**
     * Menu
     * @var Menu $menu
     */
    public $menu;

    /**
     * ZendSmarty
     * @var ZendSmarty
     */
    public $view;
    
    /**
     * Locale
     * 
     * @var Zend_Locale $locale
     */
    public $locale;

    /**
     * Zend_Registry
     *
     * @var Zend_Registry
     */
    protected $registry = null;

     /**
     * l10n
     * @var Zend_Translate $locale
     */
    public $l10n;
    
    /**
     * Class constructor
     *
     *
     * @param Zend_Controller_Request_Abstract $request
     * @param Zend_Controller_Response_Abstract $response
     * @param array $invokeArgs Any additional invocation arguments
     * @return void
     */
    public function BaseController(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
    {
    	if($request->isXmlHttpRequest())
    	{
    		$response->setHeader('content-type','application/x-www-form-urlencoded; charset=iso-8859-1',true);
    	}
    	parent::__construct($request, $response, $invokeArgs);
    }
    
    /**
     * Funcion Init que inicializa los valores similares para todos las acciones del controlador
     */
    public function init()
    {
        if($this->getUser()->getBean() == null && $this->getUser()->getBeanTemp() == null)
          $this->_redirect('auth/view-login');
          
        $managerAcl = ManagerAcl::getInstance();
        #if (!$managerAcl->getAcl()->isAllowed($this->getUser()->getAccessRole()->getIdAccessRole(), $this->getRequest()->getControllerName().'/'.$this->getRequest()->getActionName()) )
        #    throw new AuthException('Acceso Restringido');

    	$this->l10n = new Zend_Translate('gettext', 'data/locale/base.mo', 'es');
        $this->view->l10n = $this->l10n;
        
        $this->locale = $this->getRegistry()->get('Zend_Locale');

        $companyCatalog= CompanyCatalog::getInstance();
        $serverCatalog= StreamServerCatalog::getInstance();
        
        if($this->getUser()->getBeanTemp() == null){
        $id= $this->getUser()->getIdUser();                
        $company = $companyCatalog->getByUser($id)->getOne();       
        }
        else{
        $company = $companyCatalog->getById($this->getUser()->getBeanTemp()->getIdCompany());	
        }
        if($company instanceof Company){           	
        	
        	$customThemeCatalog=CustomeThemeCatalog::getInstance();
        	$flashStreamCatalog=FlashStreamCatalog::getInstance();
        	$flashVodCatalog=FlashVodCatalog::getInstance();
        	
        	$customTheme = $customThemeCatalog->getByIdCompany($company->getIdCompany())->getOne();
        	
        	$this->getUser()->setCustomerTheme($customTheme);
        	
        	$criteria= new Criteria();
        	$criteria->add('id_company',$company->getIdCompany(),Criteria::EQUAL);
        	$relations=$companyCatalog->getCompanyStreamServerRelations($criteria);
        	
        	$ids= array();
        	$idsVod= array();
        	$streamCol=null;
        	$vodCol=null;
        	if(count($relations)>0){        		
        		foreach($relations as $item){
        			if(!is_null($item['id_stream_flash'])){
        			  $ids[]= $item['id_stream_flash'];
        			}
        			if(!is_null($item['id_vod_flash'])){
        			  $idsVod[]= $item['id_vod_flash'];
        			}
        		}
        		$streamCol= $flashStreamCatalog->getByIds($ids);
        		$vodCol = $flashVodCatalog->getByIds($idsVod);
        		if(!$vodCol->isEmpty()){
        		  foreach($vodCol as $vod){
        		  	if($vod instanceof FlashVod){
        		  		$prefix=$vod->getPrefix();
        		  		$nameVideo=$vod->getNameVideo(); 
        		  		$servidor=$serverCatalog->getById($vod->getIdServer());
        		  		
        		  		$destino="";
        		  		if(!is_null($prefix)&&!empty($prefix)){
        		  			$destino=$prefix.'/'.$nameVideo;
        		  		}
        		  		else{
        		  			$destino=$nameVideo;
        		  		}
        		  		//echo 'rtmp://'.$servidor->getUrl().'/'.$destino;
        		  		if($company->getNameCompany()!="Amipci"){
							if (!file_exists('D:\\images\\galleryVod\\vod'.$nameVideo.'.jpg')){
	        			      exec('C:\\ffmpeg\\ffmpeg -itsoffset -4 -i rtmp://'.$servidor->getUrl().'/vod/'.$destino.' -vcodec mjpeg -vframes 1 -an -f rawvideo -s 80x65 D:\\images\\galleryVod\\vod'.$nameVideo.'.jpg');							
							}       		  		
        		  		}
        		  	}
        		  }
        		}
        	}
        	

        	$buttons=explode('|',$customTheme->getButtons());
        	
        	$buttonChoice= array();
        	//print_r($buttons);
        	$activeChat=false;
        	$activeComment=false;
        	foreach($buttons as $button){
        		switch (true){
        			case ($button == "find"):        				
        				      $buttonChoice[]=htmlentities("<a href='javascript:void(0);' id='searchIcon' title='Search'><img src='".$this->getRequest()->getBaseUrl()."/images/template/theme/gray/search.png' alt='search'></a>");
        				break;
        			case ($button == "chat"):
        				      $activeChat=true;        				
        				      $buttonChoice[]=htmlentities("<a href='javascript:void(0);' id='chatIcon' title='Chat'><img src='".$this->getRequest()->getBaseUrl()."/images/template/theme/gray/chat2.png' alt='Chat'></a>");
        				break;
        			case ($button == "facebook"):        				
        				      $buttonChoice[]=htmlentities("<a href='javascript:void(0);' id='faceIcon' title='Facebook'><img src='".$this->getRequest()->getBaseUrl()."/images/template/theme/gray/social_facebook.png' alt='Facebook'></a>");
        				break;
        			case ($button == "twitter"):        				
        				      $buttonChoice[]=htmlentities("<a href='javascript:void(0);' id='twettIcon' title='Twitter'><img src='".$this->getRequest()->getBaseUrl()."/images/template/theme/gray/social_twitter.png' alt='Twitter'></a>");
        				break; 
        			case ($button == "comment"):
        				      $activeComment=true;
        				      $buttonChoice[]=htmlentities("<a href='#top_arrow' title='Comentarios'><img src='".$this->getRequest()->getBaseUrl()."/images/template/theme/gray/social_email.png' alt='Comentarios'></a>");        				      
        				break;       				        				        				
        		}
        	}
        	
        	
        	
        	//print_r($buttonChoice);
        	
        	$this->getUser()->setAttribute('streamSession', $streamCol);		
        	$this->getUser()->setAttribute('vodSession', $vodCol);

        	if($this->getUser()->getBeanTemp() != null){
	        	$idUserTemp=$this->getUser()->getIdUserTemp();
	        	
	            $indice=0;
		        foreach($streamCol as $stream){
		        	if($indice==0){
		        	 $idStream = $stream->getIdStream();
		        	}
		        	else{
		        		break;
		        	}
		        	$indice++;
		        }        	
	        	/*
		        $viewerCatalog = ViewerCatalog::getInstance();
		        $viewer = $viewerCatalog->getById($idUserTemp);
	        	$viewerCatalog->linkToFlashStream($viewer->getIdViewer(), $idStream);
	        	*/
        	}
        	
        	$this->view->activeComment=$activeComment;
        	$this->view->activeChat=$activeChat;
        	$this->view->streamLive=$streamCol;
        	$this->view->streamVod=$vodCol;
        	$this->view->colorTheme=CustomeTheme::$themeLabel[$customTheme->getColorTheme()];
        	$this->view->rolUser = $this->getUser()->getAccessRole()->getName();
        	$this->view->buttonsChoice=$buttonChoice;
        	$this->view->customTheme=$customTheme;
        	$this->view->flag=$this->getUser()->getFlag();
        	$logoRes=LogoCatalog::getInstance()->getByCustomeTheme($customTheme->getIdCustomeTheme(),CustomeTheme::$Status['Active'])->getOne();
        	$bannerFullRes=BannerFullCatalog::getInstance()->getByCustomeTheme($customTheme->getIdCustomeTheme(),CustomeTheme::$Status['Active'])->getOne();
        	$bannerMiniRes=BannerMiniCatalog::getInstance()->getByCustomeTheme($customTheme->getIdCustomeTheme(),CustomeTheme::$Status['Active'])->getOne();
        	$ppts=PptCatalog::getInstance()->getByCustomeTheme($customTheme->getIdCustomeTheme(), CustomeTheme::$Status['Active']);
        	
        	($logoRes instanceof Logo)?$this->view->logo=$logoRes->getName():$this->view->logo=null;        	        	
        	($bannerFullRes instanceof BannerFull)?$this->view->bannerFull=$bannerFullRes->getName():$this->view->bannerFull=null;
        	($bannerMiniRes instanceof BannerMini)?$this->view->bannerMini=$bannerMiniRes->getName():$this->view->bannerMini=null;
        	if(!$ppts->isEmpty()){
        		
        		$pptJpgCatalog= PptJpgCatalog::getInstance();
        		$ppt2Jpg=array();
        		$indPpt=0;
        		
        		foreach ($ppts as $ppt){
        			
        			$criteria = new Criteria();
        			$criteria->add('id_ppt', $ppt->getIdPpt(), Criteria::EQUAL);
        		
        			$respond= $pptJpgCatalog->countByCriteria($criteria);
        			if(!is_null($respond)&&!empty($respond)){
        				$ppt2Jpg[$ppt->getIdPpt()]=$respond;
        			}
        			else{
        				$ppt2Jpg[$ppt->getIdPpt()]=0;
        			}
        			
        			if($indPpt==0){
        				$this->view->idPpt=$ppt->getIdPpt();
        				$this->view->namePpt=$ppt->getName();
        				$this->view->numPpt=$ppt2Jpg[$ppt->getIdPpt()];
        				$this->view->idStreamPpt=$idStream;
        				$indPpt++;
        			}
        			
        			$this->view->idPpt=$ppt->getIdPpt();
        			//$this->view->nameSignalDesktop=$ppt->getTitle();
        		}
        		
        		
        		$this->view->ppt2jpg=$ppt2Jpg;
        		$this->view->ppts=$ppts;
        	}
        	else{
        		$this->view->ppts=null;
        	}
        	($ppt instanceof Ppt)?$this->view->ppt=$ppt:$this->view->ppt=null;
        }
        
        
        $server = $serverCatalog->getByCompany($company->getIdCompany())->getOne();
         
        $this->view->company=$company;        
        $this->view->server=$server->getUrl();
        
        
        $menu = Menu::getInstance();
        $menu->setAcl($managerAcl->getAcl());
        $menu->setRole($this->getUser()->getAccessRole()->getIdAccessRole());
        $menu->build();
        $this->view->menu = $menu->render($menu->getUserMenu());
        $this->view->menuFooter=$menu->getUserMenu();
        
        $this->toView();
    }
    
    /**
     * Variable globales a smarty
     */
    private function toView()
    {
    	$this->view->systemUser = $this->getUser()->getBean();
        $this->view->systemAccessRole = $this->getUser()->getAccessRole();
    	
        // Si se envian parametros que los pase a la vista
        if($this->getRequest()->isPost())
            $this->view->post = $this->getRequest()->getParams();
            
        $this->view->controller = $this->getRequest()->getControllerName();
        $this->view->action = $this->getRequest()->getActionName();
        $this->view->baseUrl = $this->getRequest()->getBaseUrl();        
        $this->view->ok = $this->getFlash('ok');
        $this->view->error = $this->getFlash('error');
        $this->view->notice = $this->getFlash('notice');
        $this->view->warning = $this->getFlash('warning');
        $this->view->note = $this->getFlash('note');
        
        $this->view->contentTitle = $this->getRegistry()->config->appSettings->titulo; 
        $this->view->systemTitle =  $this->getRegistry()->config->appSettings->titulo;
    }

    /**
     * Guarda una variable "flash", una variable que al ser utilizada es destruida
     * @param string $varName
     * @param int|string $value
     */
    protected function setFlash($varName, $value)
    {
        $this->getUser()->setFlash($varName, $value);
    }

    /**
     * Obtiene una variable "flash" y al ser leida esta se destruye
     * @param string $varName
     * @return int|string $value
     */
    protected function getFlash($varName, $default = null)
    {
    	return $this->getUser()->getFlash($varName, $default);
    }
    
    /**
     * Pone el titulo de la pagina
     * @param string $title
     */
    protected function setTitle($title)
    {
    	$this->view->contentTitle = $this->l10n->_($title);
    }
    
    /**
     * Obtiene la sesion del usuario
     * @return UserSession
     */
    public function getUser()
    {
    	require_once 'lib/security/UserSession.php';
    	return UserSession::getInstance();
    }
    
    /**
     * Obtiene la Lista de control de accesso
     * @return Zend_Acl
     */
    public function getAcl()
    {
    	return ManagerAcl::getInstance()->getAcl();
    }
    
    /**
     * No render
     */
    protected function noRender()
    {
    	$this->getHelper('viewRenderer')->setNoRender();
    }
    
    /**
     * Obtiene el registry
     * @return Zend_Registry
     */
    protected function getRegistry()
    {
    	if(null == $this->registry)
    	   $this->registry = Zend_Registry::getInstance();
    	return $this->registry;
    }
}














