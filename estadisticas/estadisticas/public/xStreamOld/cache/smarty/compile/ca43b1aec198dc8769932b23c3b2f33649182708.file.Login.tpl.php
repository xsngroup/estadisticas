<?php /* Smarty version 3.0rc1, created on 2012-12-26 17:57:57
         compiled from "application/views\auth/Login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1367250db8f0514eb06-18061924%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ca43b1aec198dc8769932b23c3b2f33649182708' => 
    array (
      0 => 'application/views\\auth/Login.tpl',
      1 => 1356565594,
    ),
  ),
  'nocache_hash' => '1367250db8f0514eb06-18061924',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/js/jquery/vanadium.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/js/modules/auth/login.js"></script>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/css//style.css" type="text/css" />
<style>
#sidebar {
float: right;
position: relative;
font-family: Arial;
font-size:12px;
}
.sidebar-home { 
width: 300px;
}
.sidebar-home h3 {
background: url(<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/gray/widget_header_large.png) no-repeat;
width:302px;
}
#sidebar h3 {
float: left;
height: 21px;
padding: 8px 0 0 10px;
font-size: 13px;
margin:0 0 0 0;
}

.widget {
border-left: 1px solid #eee;
border-right: 1px solid #eee;
border-bottom: 1px solid #eee;
background-color: #F5F4F4;
margin-top:20px;
}
.widget > div {
padding: 15px;
}

.textwidget {
text-align: center;
line-height: 19px;

}

 input, textarea, select, input[type="submit"], input[type="reset"], .widget .item-avatar img {
background: #f7f7f7 url(../../images/template/theme/gray/gradient.png) repeat-x;
border: 1px solid #d6d6d6;
}
input[type="submit"], input[type="reset"] {
color: #808080;
}

input[type="button"]:hover, input[type="submit"]:hover, input[type="reset"]:hover {
background: #d6d6d6;
}

input, textarea, select {
    color: #808080;
}
input, textarea, select {
    -moz-border-radius: 5px 5px 5px 5px;
    color: #808080;
    font-family: Arial;
    font-size: 13px;
    padding: 5px;
}
</style>
<div id="loginFormContainer">
<div id="sidebar" class="sidebar-home">
 <h3><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Acceso a streaming');?>
</h3>
 <div class="widget">
  <div class="textwidget">
	<?php if ($_smarty_tpl->getVariable('errorMessage')->value){?>
		<div class="error"><?php echo $_smarty_tpl->getVariable('errorMessage')->value;?>
</div>
	<?php }?>
	<input type="radio" name="typeLogin" class="typeLogin" value="1" checked="checked" /><label>Visitante</label>
	<input type="radio" name="typeLogin" class="typeLogin" value="2" /><label>Administrador</label><br/>
	<div id="loadVisit">
	<form action="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/auth/login-temp" method="post" id="loginForm">
	<!-- 
	<p>
		<label for="username"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Usuario');?>
:</label><br/>
		<input type="text" name="username" id="username" value="<?php echo $_smarty_tpl->getVariable('post')->value['username'];?>
"/>
	</p>
	-->
	<p>
		<label for="username"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('C�digo de Cliente');?>
:</label><br/>
		<input type="text" name="code" id="code" value="<?php echo $_smarty_tpl->getVariable('post')->value['username'];?>
"/>
	</p>	
	<p>
		<label for="password"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Contrase�a');?>
:</label><br/>
		<input type="password" name="password" id="password" value=""/>
	</p>
	<p>
		<label for="password"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Nombre');?>
:</label><br/>
		<input type="text" name="name" id="name" value=""/>
	</p>
	<p>
		<label for="password"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Email');?>
:</label><br/>
		<input type="text" name="email" id="email" value="" class="required"/>
	</p>
	<p>
	   <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('No. Asistentes');?>
:</label>
	   <select name="viewers" id="viewers">
	     <?php  $_smarty_tpl->tpl_vars['number'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('numberviewers')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['number']->key => $_smarty_tpl->tpl_vars['number']->value){
?>
	     <option value="<?php echo $_smarty_tpl->tpl_vars['number']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['number']->value;?>
</option>
	     <?php }} ?>
	   </select>
	</p>	
	<p>
		<input type="submit" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Iniciar Sesi�n');?>
"/>
	</p>
	</form>
	</div>
	<div id="loadAdmin" style="display:none;">
	<form action="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/auth/login" method="post" id="loginForm">
	<p>
		<label for="username"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Usuario');?>
:</label><br/>
		<input type="text" name="username" id="username" value="<?php echo $_smarty_tpl->getVariable('post')->value['username'];?>
"/>
	</p>
	<p>
		<label for="password"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Contrase�a');?>
:</label><br/>
		<input type="password" name="password" id="password" value=""/>
	</p>
	<p>
		<input type="submit" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Iniciar Sesi�n');?>
"/>
	</p>
	</form>
	</div>	
  </div>
</div>
</div>
</div>