<?php /* Smarty version 3.0rc1, created on 2013-05-14 11:55:44
         compiled from "application/views\layout/Layout.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1138551926c90215511-59672253%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '890976972ef327e98e43b5ac17bd5dd8dce54019' => 
    array (
      0 => 'application/views\\layout/Layout.tpl',
      1 => 1368550459,
    ),
  ),
  'nocache_hash' => '1138551926c90215511-59672253',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_icon')) include 'lib/smarty/plugins\function.icon.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/> 
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	<title><?php echo $_smarty_tpl->getVariable('systemTitle')->value;?>
 | <?php echo $_smarty_tpl->getVariable('contentTitle')->value;?>
</title>
 
 
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/css/<?php echo $_smarty_tpl->getVariable('colorTheme')->value;?>
/reset.css" type="text/css" />	
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/css/<?php echo $_smarty_tpl->getVariable('colorTheme')->value;?>
/style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/css/<?php echo $_smarty_tpl->getVariable('colorTheme')->value;?>
/prettyPhoto.css" type="text/css" />    
    <?php if ($_smarty_tpl->getVariable('colorTheme')->value=='gray'){?>
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/css/<?php echo $_smarty_tpl->getVariable('colorTheme')->value;?>
/style-light.css" type="text/css" />	
	<?php }?>
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/css/style.css" type="text/css" />	
	<!--[if IE]><link href="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/css/style-ie.css" rel="stylesheet" type="text/css" /><![endif]-->
	
	<?php $_template = new Smarty_Internal_Template("layout/Scripts.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

	
</head>

<body >

<div class="contentHelp" id="contentHelp" style="display:none;">
<p><b>Si presentas algun problema para ver correctamente el evento prueba lo siguiente:</b><a href="javascript:void(0);" id="closeHelp"><?php echo smarty_function_icon(array('src'=>'close','class'=>'closeHelp','title'=>'Cerrar'),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></p>
<ul>
  <li>- El sitio se visualiza correctamente en: <b>IE 8+</b>, <b>Firefox 3.6+</b>, <b>Chrome 11+</b>.</li>
  <li>- Tener instalado el plugin de Flash Player 10.2 � superior.</li>
  <li>- Tener una conexi�n a internet m�nima de 256Kbps.</li>
  <li>- Si no tienes instalado el Flash Player autom�ticamente se te solicitar� instalarlo.</li>  
  <li>- Dependiendo del navegador puede variar la forma de instalaci�n del Flash Player.</li>
  <li>- P�gina oficinal de Flash Player: <b><a href="http://get.adobe.com/es/flashplayer/">http://get.adobe.com/es/flashplayer/</a></b></li>
  <li>- Si tu conexi�n de internet falla, se puede detener el flujo de la trasmisi�n � diapositivas.</li>
  <li>- Si presentas problemas para ver la transmisi�n puedes recargar la p�gina completamente.</li>
  <li>- Debes tener habilitado el javascript en tu navegador para visualizar correctamente el sitio.</li>
</ul>
</div>
<div id="contentGral">
<a href="#top_arrow"></a> 
<!--Begin Page Wrapper-->

<div id="page-wrapper">
  <?php if ($_smarty_tpl->getVariable('flag')->value!=1){?>
  <div id="login">
     <div><?php if ($_smarty_tpl->getVariable('systemUser')->value){?><a href="#"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Bienvenido');?>
 <strong><?php echo $_smarty_tpl->getVariable('systemUser')->value->getUsername();?>
</strong> (<?php echo $_smarty_tpl->getVariable('systemAccessRole')->value->getName();?>
)</a> <span>|</span>   <a href="javascript:void(0);" class="linkHelp" id="openHelp" title="Ayuda">Ayuda<?php echo smarty_function_icon(array('src'=>'hblue','class'=>'help','title'=>'Ayuda'),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a>  
      <span> | </span><a href="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/auth/logout" title="Cerrar sesi�n"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Cerrar Sesi�n');?>
<?php echo smarty_function_icon(array('src'=>'logout','class'=>'help','title'=>'Ayuda'),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a><?php }?></div>
  
		
		

		
	
  </div>	
  <div class="clear"></div>
  <?php }else{ ?>
  <div id="login">
     <div>
        <a href="javascript:void(0);" class="linkHelp" id="openHelp" title="Ayuda">Ayuda<?php echo smarty_function_icon(array('src'=>'hblue','class'=>'help','title'=>'Ayuda'),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a>  
     </div>
  </div>	
  <div class="clear"></div>  
  <?php }?>
<!--End Page Wrapper-->

<!--Begin Header Top-->
<?php if ($_smarty_tpl->getVariable('systemUser')->value){?>
<script type="text/javascript">
var idClientG="<?php echo $_smarty_tpl->getVariable('company')->value->getIdCompany();?>
";
var nameClient="<?php echo $_smarty_tpl->getVariable('company')->value->getNameCompany();?>
";
var idPpt="<?php echo $_smarty_tpl->getVariable('idPpt')->value;?>
";
var namePpt="<?php echo $_smarty_tpl->getVariable('namePpt')->value;?>
";
var numPpt="<?php echo $_smarty_tpl->getVariable('numPpt')->value;?>
";
var idStreamG="<?php echo $_smarty_tpl->getVariable('idStreamPpt')->value;?>
";
var idCustomThemeG="<?php echo $_smarty_tpl->getVariable('customTheme')->value->getIdCustomeTheme();?>
";
</script>
<?php if ($_smarty_tpl->getVariable('rolUser')->value=="Administrador"||$_smarty_tpl->getVariable('customTheme')->value->getTypeGallery()==2){?>
<style type="text/css">
    #slider {
	  height: auto!important;
    }
</style>
<?php }?>
<div id="header-top">
	<a href=""> 
	<div id="logo" class="logo-default" style="background: url('<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/files/logos/<?php echo $_smarty_tpl->getVariable('logo')->value;?>
') no-repeat  !important;"></div>
	</a>
    
 
  <div id="nav">
  <img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/files/banners/fullBanners/<?php echo $_smarty_tpl->getVariable('bannerFull')->value;?>
" alt="" />
  <!-- <?php $_template = new Smarty_Internal_Template('layout/Menu.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
 -->
  </div>
</div>
<?php }?>
<!--End Header Top-->
<div id="loadDisplay">
<?php if ($_smarty_tpl->getVariable('systemUser')->value){?>

<!--Begin Slider-->
<div id="slider-top"></div>
<div class="nivoSlider" style="position: relative; background:#000 ;" id="slider">		
<div <?php if ($_smarty_tpl->getVariable('rolUser')->value=="Administrador"&&$_smarty_tpl->getVariable('customTheme')->value->getTypeGallery()==2){?>
  class="nivo-controlNav redi2"
<?php }else{ ?>
  <?php if ($_smarty_tpl->getVariable('customTheme')->value->getTypeGallery()==2){?>
     class="nivo-controlNav redi3"
  <?php }else{ ?>
     class="nivo-controlNav"
  <?php }?>   
<?php }?> 
   id="nivoLoad">
	<?php if ($_smarty_tpl->getVariable('customTheme')->value->getTypeGallery()==1){?>
		<?php $_smarty_tpl->assign("indicador",0,null,null);?>
		<div class="previewStream">
		<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('streamLive')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
?>
		<a href=<?php echo $_smarty_tpl->getVariable('item')->value->getName();?>
 id="top_arrow"></a>		
		<?php if ($_smarty_tpl->getVariable('indicador')->value<=2){?>
		     <?php if ($_smarty_tpl->getVariable('company')->value->getIdCompany()==52||$_smarty_tpl->getVariable('company')->value->getIdCompany()==55||$_smarty_tpl->getVariable('company')->value->getIdCompany()==63){?>
				<?php if ($_smarty_tpl->getVariable('company')->value->getIdCompany()==12){?>
				<a class="nivo-control" alt="0" onclick="document.location.reload(true)">
				<?php }else{ ?>
				<a class="nivo-control" alt="0" onclick="loadVideo('rtmplive','<?php echo $_smarty_tpl->getVariable('item')->value->getName();?>
');">
				<?php }?>
				
			 <?php }else{ ?>
				<?php if ($_smarty_tpl->getVariable('company')->value->getIdCompany()==12){?>
				<a class="nivo-control" alt="0" onclick="document.location.reload(true)">
				<?php }else{ ?>
				<a class="nivo-control" alt="0" onclick="loadVideo('live','<?php echo $_smarty_tpl->getVariable('item')->value->getName();?>
');">			 
				<?php }?>
			 <?php }?> 			
				<!-- <a class="nivo-control" alt="0" onclick="loadVideo('live','<?php echo $_smarty_tpl->getVariable('item')->value->getName();?>
');"> -->
					<img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/files/galleryLive/live.png" alt="">
					<div class="slider-thumb-caption">
					<strong><?php echo $_smarty_tpl->getVariable('item')->value->getTitle();?>
</strong>
					<p>
					  <?php echo $_smarty_tpl->getVariable('item')->value->getDescription();?>

					</p>
					</div>
				</a>
		<?php $_smarty_tpl->assign('indicador',$_smarty_tpl->getVariable('indicador')->value+1,null,null);?>
		<?php }?>
		<?php }} else { ?>
		<!-- <center style="padding-top:5px;"><span >No hay contenido para mostrar</span></center> -->
			<?php $_template = new Smarty_Internal_Template("ajax-event/listVodStream.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

		<?php } ?>
		</div>
		<div class="miniBanner"><img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/files/banners/miniBanners/<?php echo $_smarty_tpl->getVariable('bannerMini')->value;?>
" class="bannerImage"/></div>		
	<?php }?>
	<?php if ($_smarty_tpl->getVariable('customTheme')->value->getTypeGallery()==2&!is_null($_smarty_tpl->getVariable('ppts')->value)){?>	
		 <!-- { html_entity_decode( $ppt->getName() ) } -->
		 <!-- <?php echo $_smarty_tpl->getVariable('rolUser')->value;?>
 -->
		    <?php $_smarty_tpl->assign("indicadorX",0,null,null);?>
		 	<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('streamLive')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
?>
				<?php if ($_smarty_tpl->getVariable('indicadorX')->value==0){?>
					<input type="hidden" id="idStr" value="<?php echo $_smarty_tpl->getVariable('item')->value->getIdStream();?>
"></input>
					<?php $_smarty_tpl->assign('indicadorX',$_smarty_tpl->getVariable('indicadorX')->value+1,null,null);?>
				<?php }?>
			<?php }} ?>
		 <?php if ($_smarty_tpl->getVariable('rolUser')->value=="Administrador"){?>
		    <div class="previewStream">
		    <?php  $_smarty_tpl->tpl_vars['ppt'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('ppts')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['ppt']->key => $_smarty_tpl->tpl_vars['ppt']->value){
?>
					<a class="nivo-control" alt="0" onclick="">
						<img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/files/galleryLive/ppt.png" alt="" style="padding-right: 0px !important; width: auto !important;">
						<div class="slider-thumb-caption">
						<strong><?php echo $_smarty_tpl->getVariable('ppt')->value->getTitle();?>
</strong>
						<p class="namePpt" id="namePpt-<?php echo $_smarty_tpl->getVariable('ppt')->value->getIdPpt();?>
-<?php echo $_smarty_tpl->getVariable('ppt2jpg')->value[$_smarty_tpl->getVariable('ppt')->value->getIdPpt()];?>
">
						  <?php echo $_smarty_tpl->getVariable('ppt')->value->getName();?>

						</p>
						</div>
					</a>	    
		    <?php }} else { ?>
		     <center style="padding-top:5px;"><span >No hay presentaciones para mostrar</span></center>
		    <?php } ?>
			</div>
		 <?php }else{ ?>
		 <!-- 
          <?php  $_smarty_tpl->tpl_vars['ppt'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('ppts')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['ppt']->key => $_smarty_tpl->tpl_vars['ppt']->value){
?>
          <?php echo html_entity_decode($_smarty_tpl->getVariable('ppt')->value->getCode());?>
		
		    <?php }} else { ?>
		     <center style="padding-top:5px;"><span >No hay presentaciones para mostrar</span></center>
		    <?php } ?>     
          -->
				<?php $_smarty_tpl->assign("indicador",0,null,null);?>
				<?php $_smarty_tpl->assign("tituloEvt",'',null,null);?>
				<?php $_smarty_tpl->assign("descriEvt",'',null,null);?>
					<div id="loadVideoPlayer">
					<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('streamLive')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
?>
					<?php if ($_smarty_tpl->getVariable('indicador')->value==0){?>
					<?php $_smarty_tpl->assign('tituloEvt',$_smarty_tpl->getVariable('item')->value->getTitle(),null,null);?>
					<?php $_smarty_tpl->assign('descriEvt',$_smarty_tpl->getVariable('item')->value->getDescription(),null,null);?>
					<input type="hidden" id="idStr" value="<?php echo $_smarty_tpl->getVariable('item')->value->getIdStream();?>
"></input>
					<script type='text/javascript' src='<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/js/swfobject.js'></script>
					<div id="mediaspace"  style="padding-bottom: 20px;"></div>
					<script type='text/javascript'>
										var so = new SWFObject('<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/files/player.swf','ply','333','244','9');
											so.addParam('allowfullscreen','false');
											so.addParam('allowscriptaccess','always');
											so.addParam('wmode','opaque');
											if(<?php echo $_smarty_tpl->getVariable('company')->value->getIdCompany();?>
==12){
											so.addVariable('file','test.mp4');
											so.addVariable('streamer','rtmp://69.170.130.194/rtmplive/live/');
											}else{
											so.addVariable('file','<?php echo $_smarty_tpl->getVariable('item')->value->getName();?>
');
											//so.addVariable('file','970');
											//so.addVariable('streamer','rtmp://<?php echo $_smarty_tpl->getVariable('ipServer')->value;?>
/live/');
											so.addVariable('streamer','rtmp://<?php echo $_smarty_tpl->getVariable('server')->value;?>
/live/');
											}
											so.addVariable("autostart","true");
											so.write('mediaspace');
					
					</script>
					<?php $_smarty_tpl->assign('indicador',$_smarty_tpl->getVariable('indicador')->value+1,null,null);?>
					<?php }?>
					<?php }} ?>
					</div>	
					<br/>
					<br/>
					<!-- 
					<p>
					<label style="padding-left:5px;font-weight: bold;">Fecha:</label>{}
					</p>
					-->     		
					<p>
					<label style="padding-left:5px;font-weight: bold;">T�tulo: </label><?php echo $_smarty_tpl->getVariable('tituloEvt')->value;?>

					</p>
					<p>
					<label style="padding-left:5px;font-weight: bold;">Orador: </label><?php echo $_smarty_tpl->getVariable('descriEvt')->value;?>

					</p>
		 <?php }?>   
	<?php }?>						
</div>

<!-- end sidebar gallery -->
<!-- init load video or ppts -->
<?php if ($_smarty_tpl->getVariable('rolUser')->value=="Administrador"&&$_smarty_tpl->getVariable('customTheme')->value->getTypeGallery()==2){?>
<style>
#slider {
	background:none !important;
}
</style>
<div id="pptSyncDiv">
<div class="notice" style="z-index: 900; position: absolute; width: 600px; margin-left: 6%;"><p>Si presenta problemas para seguir correctamente el evento favor de recargar la p�gina para resolverlo.<?php echo smarty_function_icon(array('src'=>'close','class'=>'close','title'=>'Cerrar'),$_smarty_tpl->smarty,$_smarty_tpl);?>
</p></div>
        <script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/js/modules/layout/script.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/plugins/pptSync/history/history.css" />
        <script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/plugins/pptSync/history/history.js"></script>
        <!-- END Browser History required section -->  
		    
        <script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/plugins/pptSync/swfobject.js"></script>
	    <script type="text/javascript">
	            <!-- For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. --> 
	            var swfVersionStr = "10.0.0";
	            <!-- To use express install, set to playerProductInstall.swf, otherwise the empty string. -->
	            var xiSwfUrlStr = "<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/plugins/pptSync/playerProductInstall.swf";
	            var flashvars = {};
	            var params = {};
	            params.wmode="transparent";
	            params.quality = "high";
	            params.bgcolor = "#ffffff";
	            params.allowscriptaccess = "sameDomain";
	            params.allowfullscreen = "true";
	            var attributes = {};
	            attributes.id = "syncPpt";
	            attributes.name = "syncPpt";
	            attributes.align = "middle";
	            swfobject.embedSWF(
	                "<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/plugins/pptSync/syncPpt.swf", "flashContent", 
	                "757px", "480px", 
	                swfVersionStr, xiSwfUrlStr, 
	                flashvars, params, attributes);
				<!-- JavaScript enabled so display the flashContent div in case it is not replaced with a swf object. -->
				swfobject.createCSS("#flashContent", "display:block;text-align:left;");
	        </script>        
	        <div id="flashContent" style="display: none;">
	        	<p>
		        	To view this page ensure that Adobe Flash Player version 
					10.0.0 or greater is installed. 
				</p>
				<script type="text/javascript"> 
					var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://"); 
					document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
									+ pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
				</script> 
	        </div>	
</div>	                
<?php }else{ ?>
<?php if ($_smarty_tpl->getVariable('customTheme')->value->getTypeGallery()==2){?>
<style>
#slider {
	background:none !important;
}
</style>

<div id="pptClientDiv">
<div class="notice" style="z-index: 900; position: absolute; width: 600px; margin-left: 4px;"><p>Si presenta problemas para seguir correctamente el evento favor de recargar la p�gina para resolverlo.<?php echo smarty_function_icon(array('src'=>'close','class'=>'close','title'=>'Cerrar'),$_smarty_tpl->smarty,$_smarty_tpl);?>
</p></div>
		        <script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/js/modules/layout/script.js"></script>
		        <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/plugins/pptClient/history/history.css" />
		        <script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/plugins/pptClient/history/history.js"></script>
		        <!-- END Browser History required section -->  
				    
		        <script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/plugins/pptClient/swfobject.js"></script>
			    <script type="text/javascript">
			            <!-- For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. --> 
			            var swfVersionStr = "10.0.0";
			            <!-- To use express install, set to playerProductInstall.swf, otherwise the empty string. -->
			            var xiSwfUrlStr = "<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/plugins/pptClient/playerProductInstall.swf";
			            var flashvars = {};
			            var params = {};
			            params.wmode="transparent";
			            params.quality = "high";
			            params.bgcolor = "#ffffff";
			            params.allowscriptaccess = "sameDomain";
			            params.allowfullscreen = "true";
			            var attributes = {};
			            attributes.id = "clientPpt";
			            attributes.name = "clientPpt";
			            attributes.align = "middle";
			            swfobject.embedSWF(
			                "<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/plugins/pptClient/clientPpt.swf", "flashContent", 
			                "640px", "480px", 
			                swfVersionStr, xiSwfUrlStr, 
			                flashvars, params, attributes);
						<!-- JavaScript enabled so display the flashContent div in case it is not replaced with a swf object. -->
						swfobject.createCSS("#flashContent", "display:block;text-align:left;");
			        </script>        
			        <div id="flashContent" style="display: none;">
			        	<p>
				        	To view this page ensure that Adobe Flash Player version 
							10.0.0 or greater is installed. 
						</p>
						<script type="text/javascript"> 
							var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://"); 
							document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
											+ pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
						</script> 
			        </div>     		
     		</div>
<?php }else{ ?>     		
   <?php $_smarty_tpl->assign("indicador",0,null,null);?>
	<div id="loadVideoPlayer">
	<!-- parche para el eventito de ciudadano 3.0 -->
	<?php if ($_smarty_tpl->getVariable('company')->value->getIdCompany()!=17){?>
	<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('streamLive')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
?>
	<?php if ($_smarty_tpl->getVariable('indicador')->value==0){?>
	<input type="hidden" id="idStr" value="<?php echo $_smarty_tpl->getVariable('item')->value->getIdStream();?>
"></input>
	<!--<a href="#" class="video-link">Live Stream: <?php echo $_smarty_tpl->getVariable('item')->value->getTitle();?>
</a>-->
	<script type='text/javascript' src='<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/js/swfobject.js'></script>
	<div id='mediaspace'></div>
	
	
	
	<?php if ($_smarty_tpl->getVariable('company')->value->getIdCompany()==12){?>
	<iframe src="http://clientes.xsn.com.mx/avon3.html" name="Avon" height="344px" width="630px" frameborder="0" scrolling="no">Avon</iframe>

	<?php }else{ ?>
	<script type='text/javascript'>
						var so = new SWFObject('<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/files/player.swf','ply','630','344','9','#');
							so.addParam('allowfullscreen','true');
							so.addParam('allowscriptaccess','always');
							so.addParam('wmode','opaque');
							<?php if ($_smarty_tpl->getVariable('company')->value->getIdCompany()==52||$_smarty_tpl->getVariable('company')->value->getIdCompany()==55||$_smarty_tpl->getVariable('company')->value->getIdCompany()==26||$_smarty_tpl->getVariable('company')->value->getIdCompany()==63){?>
							so.addVariable('file','<?php echo $_smarty_tpl->getVariable('item')->value->getName();?>
');
							//so.addVariable('file','970');
							//so.addVariable('streamer','rtmp://<?php echo $_smarty_tpl->getVariable('ipServer')->value;?>
/live/');
							so.addVariable('streamer','rtmp://<?php echo $_smarty_tpl->getVariable('server')->value;?>
:1936/rtmplive/');							
							<?php }else{ ?>
							so.addVariable('file','<?php echo $_smarty_tpl->getVariable('item')->value->getName();?>
');
							//so.addVariable('file','970');
							//so.addVariable('streamer','rtmp://<?php echo $_smarty_tpl->getVariable('ipServer')->value;?>
/live/');
							so.addVariable('streamer','rtmp://<?php echo $_smarty_tpl->getVariable('server')->value;?>
/live/');
							<?php }?>
							so.addVariable("autostart","true");
							so.write('mediaspace');
								</script>
	<?php }?>

	<?php $_smarty_tpl->assign('indicador',$_smarty_tpl->getVariable('indicador')->value+1,null,null);?>
	<?php }?>
	<?php }} ?>
	<?php }else{ ?>
		<!--<a href="#" class="video-link">Vod Stream: amipcicongreso1</a>-->
		<script type='text/javascript' src='<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/js/swfobject.js'></script>
		<div id='mediaspace'></div>
		<script type='text/javascript'>
							var so = new SWFObject('<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/files/player.swf','ply','630','344','9','#');
								so.addParam('allowfullscreen','true');
								so.addParam('allowscriptaccess','always');
								so.addParam('wmode','opaque');
								so.addVariable('file','amipcicongreso1');
								//so.addVariable('file','970');
								//so.addVariable('streamer','rtmp://<?php echo $_smarty_tpl->getVariable('ipServer')->value;?>
/live/');
								so.addVariable('streamer','rtmp://69.170.128.2/vod/');
								so.addVariable("autostart","true");
								so.write('mediaspace');
		
		</script>	
	<?php }?>
	</div>	
	 
<?php }?>	 
<?php }?>
										
</div>		
			

<!--End Slider-->	
	<!--End Slider-->
	
	<!--Begin Header Bottom-->
	<div id="header-bottom">
	
		<div id="display-options">
		<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="http://www.xsn.com.mx" layout="button_count" show_faces="false" width="100" font="arial"></fb:like>
        <a href="javascript:void(0);" id="shared"><img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/gray/share.png" alt="compartir" /></a> 
             
<!-- 
<a name="fb_share"></a> 
<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" 
        type="text/javascript">
</script>
 -->
		</div>
	
				
		<!--Begin Social Icons-->
		<?php if ($_smarty_tpl->getVariable('customTheme')->value->getTypeGallery()==1&&$_smarty_tpl->getVariable('company')->value->getIdCompany()!=17){?>
			<div class="streamButtons">
				<?php if ($_smarty_tpl->getVariable('company')->value->getIdCompany()==12){?>
			<!--  <input type="button" class="live-stream" id="liveIcon"  title="Live Stream"  onclick="document.location.reload(true)" />-->
			 <!-- <input type="button" class="vod-stream" id="vodIcon"  title="Video On Demand Stream"  />-->
				<?php }else{ ?>
				  <input type="button" class="live-stream" id="liveIcon"  title="Live Stream"  onclick="document.location.reload(true)" />
			  <input type="button" class="vod-stream" id="vodIcon"  title="Video On Demand Stream"  />
			
				<?php }?>
		 	</div>	
		<?php }?> 	
		<div id="social-icons">
		<?php if ($_smarty_tpl->getVariable('customTheme')->value->getTypeGallery()==1){?>
			<!--                 		                
			<a href="javascript:void(0);" id="searchIcon" title="Search"><img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/gray/search.png" alt="search"></a>	
			<a href="javascript:void(0);" id="chatIcon" title="Chat"><img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/gray/chat2.png" alt="Chat"></a>													
			<a href="javascript:void(0);" id="faceIcon" title="Facebook"><img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/gray/social_facebook.png" alt="Facebook"></a>			
			<a href="javascript:void(0);" id="twettIcon" title="Twitter"><img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/gray/social_twitter.png" alt="Twitter"></a>									
		    -->
		<?php }?> 
		<?php if ($_smarty_tpl->getVariable('customTheme')->value->getTypeGallery()==2){?>
			<!-- <input type="button" class="live-stream" id="liveIcon"  title="Live Stream"  /> -->
			<!-- <input type="button" class="ppt" id="liveIcon"  title="Presentaciones"  /> -->
			<!-- <input type="button" class="vod-stream" id="vodIcon"  title="Video On Demand Stream"  /> -->
			
					                		                
			<!--  <a href="javascript:void(0);" id="chatIcon" title="Chat"><img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/gray/chat2.png" alt="Chat"></a>-->													
		<?php }?> 
			  <?php  $_smarty_tpl->tpl_vars['button'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('buttonsChoice')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['button']->key => $_smarty_tpl->tpl_vars['button']->value){
?>
			       <?php echo html_entity_decode($_smarty_tpl->tpl_vars['button']->value);?>

			  <?php }} ?>				              
		</div>
		<!--End Social Icons-->
	
	</div>
	<!--End Header Bottom-->
<?php }?>
</div>

<!-- End loadDisplay -->

<!-- begin content float -->
<div id="contentShare" style="display: none;">

  <a href="javascript:void(0);" id="closeShare" class="icon-right"><?php echo smarty_function_icon(array('src'=>'close','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Cerrar')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a>
  <h6 class="shareH">Comparte este sitio en tus redes sociales:</h6>
<div>
<div style="float: left;  margin-left: 18px;">
 <a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
</div>
<div style="float:right;  margin-right: 30px;"> 
<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like layout="button_count" show_faces="true" width="80" action="recommend"></fb:like>
</div>

</div>

</div>
<!-- end content float -->

<!--Begin Content Wrapper-->
<div id="content-wrapper">
   <!--  <div id="baseUrl"><?php if ($_smarty_tpl->getVariable('systemUser')->value){?><div class="subHeader"><div><em><?php echo $_smarty_tpl->getVariable('systemTitle')->value;?>
 &raquo; <?php echo $_smarty_tpl->getVariable('contentTitle')->value;?>
</em></div></div><?php }?></div><br/> -->
  
  <div id="contentPanel" class="contentPanel">
          	     <?php $_template = new Smarty_Internal_Template("layout/Messages.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

          	     <?php echo $_smarty_tpl->getVariable('contentPlaceHolder')->value;?>
  
  </div>
</div>
<!--End Content Wrapper-->	

<div class="clear"></div>
<?php $_template = new Smarty_Internal_Template("layout/Modals.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

<?php if ($_smarty_tpl->getVariable('systemUser')->value){?>
<!--Begin Footer Widgets-->
<!-- 
<div id="footer-widgets">
			
			
					<div class="footer-widget-outer footer-fourth">
				<div class="footer-widget-inner" id="categories-5"><h3>Categories</h3>		<ul>
	<li class="cat-item cat-item-17"><a title="View all posts filed under Category 1" href="http://ghostpool.com/wordpress/reviewit/category/category-1/">Category 1</a>
</li>
	<li class="cat-item cat-item-4"><a title="View all posts filed under Category 2" href="http://ghostpool.com/wordpress/reviewit/category/category-2/">Category 2</a>
</li>
	<li class="cat-item cat-item-5"><a title="View all posts filed under Category 3" href="http://ghostpool.com/wordpress/reviewit/category/category-3/">Category 3</a>
</li>
	<li class="cat-item cat-item-6"><a title="View all posts filed under Category 4" href="http://ghostpool.com/wordpress/reviewit/category/category-4/">Category 4</a>
</li>
		</ul>
</div>			</div>
			
					<div class="footer-widget-outer footer-fourth">
				<div class="footer-widget-inner" id="meta-3"><h3>Meta</h3>			<ul>
						<li><a href="http://ghostpool.com/wordpress/reviewit/wp-login.php">Log in</a></li>
			<li><a title="Syndicate this site using RSS 2.0" href="http://ghostpool.com/wordpress/reviewit/feed/">Entries <abbr title="Really Simple Syndication">RSS</abbr></a></li>
			<li><a title="The latest comments to all posts in RSS" href="http://ghostpool.com/wordpress/reviewit/comments/feed/">Comments <abbr title="Really Simple Syndication">RSS</abbr></a></li>
			<li><a title="Powered by WordPress, state-of-the-art semantic personal publishing platform." href="http://wordpress.org/">WordPress.org</a></li>
						</ul>
</div>			</div>
				
					<div class="footer-widget-outer footer-fourth">
				<div class="footer-widget-inner" id="archives-3"><h3>Archives</h3>		<ul>
			<li><a title="June 2010" href="http://ghostpool.com/wordpress/reviewit/2010/06/">June 2010</a></li>
	<li><a title="March 2010" href="http://ghostpool.com/wordpress/reviewit/2010/03/">March 2010</a></li>
	<li><a title="January 2010" href="http://ghostpool.com/wordpress/reviewit/2010/01/">January 2010</a></li>
		</ul>
</div>			</div>
				
					<div class="footer-widget-outer footer-fourth">
				<div class="footer-widget-inner" id="tag_cloud-3"><h3>Tags</h3><div><a style="font-size: 8pt;" title="1 topic" class="tag-link-7" href="http://ghostpool.com/wordpress/reviewit/tag/images1/">images1</a>
<a style="font-size: 18.5pt;" title="3 topics" class="tag-link-8" href="http://ghostpool.com/wordpress/reviewit/tag/news1/">news1</a>
<a style="font-size: 22pt;" title="4 topics" class="tag-link-9" href="http://ghostpool.com/wordpress/reviewit/tag/news2/">news2</a>
<a style="font-size: 14.3pt;" title="2 topics" class="tag-link-10" href="http://ghostpool.com/wordpress/reviewit/tag/news3/">news3</a>
<a style="font-size: 14.3pt;" title="2 topics" class="tag-link-11" href="http://ghostpool.com/wordpress/reviewit/tag/preview1/">preview1</a>
<a style="font-size: 14.3pt;" title="2 topics" class="tag-link-12" href="http://ghostpool.com/wordpress/reviewit/tag/preview2/">preview2</a>
<a style="font-size: 8pt;" title="1 topic" class="tag-link-13" href="http://ghostpool.com/wordpress/reviewit/tag/preview3/">preview3</a>
<a style="font-size: 8pt;" title="1 topic" class="tag-link-14" href="http://ghostpool.com/wordpress/reviewit/tag/videos1/">videos1</a></div>
</div>			</div>
				
	</div>
	-->
<!--End Footer Widgets-->


<div class="clear"></div>

<!--Begin Footer-->
<div id="footer">

	<div id="footer-content">

	  <div id="copyright">
		<!--Copyright � 2010. Designed and coded by Xsn Group.	-->
	  </div>	
	</div>
	
	<a href="#" id="top_arrow"></a>		
</div>
<?php }?>
<!--End Footer-->
</div>
<!--End Page Wrap-->
</div>

<script>

	function validatePassDate()
{
if (window.XMLHttpRequest)
  {
  xmlhttp=new XMLHttpRequest();
  }
else
  {
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.open("GET","http://xsn-stat.cloudapp.net/xStreamOld/public/index/Validate?us="+<?php echo $_smarty_tpl->getVariable('systemUser')->value->getIdUser();?>
,true);
xmlhttp.send();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	
    }
  }
 
}
setInterval("validatePassDate()",180000);
validatePassDate();
</script>


		
		
</body>


</html>
