var controlChat=0;

$(document).ready( function()
{
	intervalChat=setInterval( "timerChat()" ,15000);
	/**
	 * Boton chat
	 */
	$('#buttonChat').click(function(){
		
		var msgNew=$("#msgChat").val();
		$("#msgChat").attr('value','');
		//alert(msgNew);
		$.post(baseUrl + "/ajax-event/send-msg", {
			msg: msgNew
		}, function(data){
			if (data.length > 0) {	
				 if(controlChat>12){
					 $('ul#chatText li:first').remove(); 
				 }
				$('#chatText').append(data);
				controlChat++;
			}
		});			
	});
});	

function timerChat(){
		//$('#loadStats').html("cargando...");
		
		//$("#chatText li").remove();
		$.getJSON(baseUrl + "/ajax-event/get-msg",{option: "0"}, function(json){				
		   
		//recorremos todas las filas del resultado del proceso que obtenemos en Json

			$.each(json, function(i,item){
				
				var block;
				var filter;
				if(rolUser=="Administrador")
				{
					filter = item.username+": "+item.msg;
					block="<a href='javascript:void(0)' class='block' title='Bloquear comentario' onclick='blockComment("+item.idChat+",\""+filter+"\")'></a>";
				}
				else{
					block="";
				}				
				
				var flag=0;
				var lista = "<li>"+block+"<b>"+item.username+": </b>"+item.msg+"</li>";
				var textLista = item.username+": "+item.msg;
				 $('#chatText li').each(function(){
				 	//alert($(this).text());
					 if($(this).text()==textLista){
					 	flag=1;
					 }							
				 });
				 //alert(flag);
				 if (flag == 0) {
					 if(controlChat>12){
						 $('ul#chatText li:first').remove(); 
					 }
				 	$('#chatText').append(lista);
				 	controlChat++;
				 }				
			});
		});		
}


function blockComment(id, filter){	
	$.post(baseUrl + "/ajax-event/block-msg", {
		id: id
	}, function(data){
		if (data.length > 0) {				
			//$('#chatText').append(data);
			 $('#chatText li').each(function(){
				 	//alert($(this).text());
					 if($(this).text()==filter){
					 	$(this).remove();
					 }							
				 });			
			alert("Se bloqueo correctamente el comentario: <br/>"+filter);
		}
	});		
}