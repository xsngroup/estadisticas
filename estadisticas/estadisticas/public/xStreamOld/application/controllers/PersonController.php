<?php
/**
 * ##$BRAND_NAME$##
 *
 * ##$DESCRIPTION$##
 *
 * @category   Project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */

/**
 * BaseController
 */
require_once "lib/controller/BaseController.php";


/**
 * Clase IndexController que representa el controller para los codigos postales
 *
 * @category   Project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 */
class PersonController extends BaseController
{
    /**
     * regresa la información de un codigo postal
     */
    public function getJsonByCurpAction()
    {
    	try
        {
            require_once 'Zend/Json.php';
            $curp = $this->getRequest()->getParam('value');
            $persons = PersonCatalog::getInstance()->getByCurp($curp)->prepareToJson();
            die(Zend_Json::encode($persons));
        }
        catch(Exception $e)
        {
            throw new Exception("Error al buscar a la persona por CURP\n".$e->getMessage());
        }
    }
    

    
    
}




