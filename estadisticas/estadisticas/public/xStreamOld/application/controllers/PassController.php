<?php
/**
 * ##$BRAND_NAME$##
 *
 * ##$DESCRIPTION$##
 *
 * @category   Project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */

/**
 * UserLogCatalog
 */
require_once 'application/models/catalogs/UserLogCatalog.php';

/**
 * BaseController
 */
require_once 'lib/controller/BaseController.php';

/**
 * AccessRoleCatalog
 */
require_once 'application/models/catalogs/AccessRoleCatalog.php';

require_once 'application/models/catalogs/CompanyCatalog.php';

/**
 * Clase AuthController que representa el controlador para las acciones de login/logout
 *
 * @category   project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 */
class PassController extends BaseController
{

    /**
     * Sobrecargamos el metodo init
     */
    public function init()
    {
        $registry = Zend_Registry::getInstance();
        $this->view->moduleName = $this->getRequest()->getModuleName();
        $this->view->controllerName = $this->getRequest()->getControllerName();
        $this->view->actionName = $this->getRequest()->getActionName();
        $this->view->baseUrl = $this->getRequest()->getBaseUrl();
        $this->view->systemTitle = utf8_decode($registry->config->appSettings->titulo);
        $this->view->l10n = new Zend_Translate('gettext', 'data/locale/base.mo', 'es');
    }
    
    /**
     * 
     */
    public function configAction()
    {
    	require_once 'application/models/catalogs/SecurityActionCatalog.php';
    	require_once 'application/models/catalogs/SecurityControllerCatalog.php';
        parent::init();
        $this->setTitle('Configuración de Facultades');
        $this->view->accessRoles = AccessRoleCatalog::getInstance()->getActives(new Criteria());
        $actionsCriteria = new Criteria();
        $actionsCriteria->add(SecurityAction::SYSTEM,2,Criteria::EQUAL);
        $this->view->actions = SecurityActionCatalog::getInstance()->getByCriteria($actionsCriteria);
        $controllersCriteria = new Criteria();
        $controllersCriteria->add(SecurityController::SYSTEM,2,Criteria::EQUAL);
        $this->view->controllers = SecurityControllerCatalog::getInstance()->getByCriteria($controllersCriteria);
        $this->view->permissions = AccessRoleCatalog::getInstance()->getAllPermissions();
    }    
	public function errorAction(){
		
	}
	public function validateAction(){
		
	}
}
