<?php
/**
 * ##$BRAND_NAME$##
 *
 * ##$DESCRIPTION$##
 *
 * @category   Project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */

require_once "lib/controller/BaseController.php";


/**
 * Clase IndexController que representa el controller para la ruta default
 *
 * @category   Project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 */
class IndexController extends BaseController
{
    /**
     * Pantalla para despues del inicio de sesi�n
     */
    public function indexAction()
    {

       
        $this->view->contentTitle = 'Inicio';
        $this->view->setTpl('Home');
    }
    
	public function fullPptAction()
	{
		$this->view->setLayoutFile(false);
		$idStream = $this->getRequest()->getParam('idStream');
		$this->view->idStream=$idStream;
		$this->view->setTpl('FullPpt');
	}
	
	 public function validateAction()
    {

       
       
    }
}


