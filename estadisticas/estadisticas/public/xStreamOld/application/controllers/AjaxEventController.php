<?php
require_once 'lib/controller/BaseController.php';
require_once 'application/models/catalogs/ChatCatalog.php';
class AjaxEventController extends BaseController
{
	public function loadVideoAction()
	{
	   $servidorIp=null;
	   $type = $this->getRequest()->getParam('type');
	   $video = $this->getRequest()->getParam('video');
	   $port = $this->getRequest()->getParam('port');
	   $servidorIp = $this->getRequest()->getParam('server',null);
	   
	   if(!empty($servidorIp)&&!is_null($servidorIp)){
	   	 $this->view->serverIp=$servidorIp;
	   }
	   else{
	   	 $this->view->serverIp=null;
	   }
	   
	   $this->view->type=$type;
	   $this->view->port=$port;
	   $this->view->video=$video;
	   $this->view->setTpl('loadPlayer');
	   $this->view->setLayoutFile(false);	
	}
	public function liveAction()
	{
	   $this->view->setTpl('listLiveStream');
	   $this->view->setLayoutFile(false);
	}
	public function vodAction()
	{
		require_once 'application/models/catalogs/StreamServerCatalog.php';
		$this->view->serverStreaming=StreamServerCatalog::getInstance()->getActives()->toCombo();
	   $this->view->setTpl('listVodStream');
	   $this->view->setLayoutFile(false);
	}	

	public function searchAction()
	{
	   $this->view->setTpl('searchForm');
	   $this->view->setLayoutFile(false);
	}	
	
	public function chatAction()
	{
	   $this->view->setTpl('chatXsn');
	   $this->view->setLayoutFile(false);
	}

	public function faceAction()
	{
	   $this->view->setTpl('facebook');
	   $this->view->setLayoutFile(false);
	}

	public function twettAction()
	{
	   $this->view->setTpl('twitter');
	   $this->view->setLayoutFile(false);
	}	
	
	public function sendMsgAction()
	{
		require_once 'application/models/catalogs/ViewerCatalog.php';
		
		$this->view->setLayoutFile(false);
        $this->_helper->viewRenderer->setNoRender();
        
        $streams=$this->getUser()->getAttribute('streamSession', null);
        $vods=$this->getUser()->getAttribute('vodSession', null);
        
        //var_dump($streams);
        
        $idUserTemp = $this->getUser()->getIdUserTemp();
        $username=$this->getUser()->getUsername();   
        $msg = $this->getRequest()->getParam('msg');
        
        /********************Filtro de palabras*************************/
		$filtros = array("cabron", "pendejo", "estupido", "idiota", "maricon", "huevon", "puto", "marica", "putito",
		"bitch", "zorra", "pirix", "gay", "joto", "prosti", "pu�al", "marro", "parchar", "coger", "homosexual", "marica",
		"pinche", "jodido", "verga", "verch", "co�o", "mames", "mamar", "puta", "madre", "puta madre", "chingada", "chingada madre",
		"mallate", "imbecil", "Puto", "Puta");
				
		foreach($filtros as $key=> $value){
		$msg = str_replace("$value", "****", $msg);
		}
		//echo "$text";        
        /**************************************************************/
        
        
        $date=$date = date('Y-m-d H:i:s');
        $viewer = ViewerCatalog::getInstance()->getByIdUsertemp($idUserTemp)->getOne();
        
        if(!($viewer instanceof Viewer)){
        	$this->setFlash("error","No existe ning�n identificador de visitante asociado");
        	$this->_redirect('auth/logout');
        }
        
        $indice=0;
        foreach($streams as $stream){
        	if($indice==0){
        	 $idStream = $stream->getIdStream();
        	}
        	else{
        		break;
        	}
        	$indice++;
        }
        $chatCatalog= ChatCatalog::getInstance();
        $chat = new Chat();
        $chat->setTimestamp($date);
        $chat->setUsername($username);
        $chat->setMsg($msg);
        $chat->setStatus(Chat::$Status['Active']);
        $chat->setIdViewer($viewer->getIdViewer());
        $chatCatalog->create($chat);
        
        //aqui habria que linkear las relaciones con los streamings
        $chatCatalog->linkToFlashStream($chat->getIdChat(),$idStream);
        
        echo "<li><b>".$username.": </b>".$msg."</li>";
	}
	
	public function getMsgAction()
	{
		require_once 'Zend/Json.php';
		$this->view->setLayoutFile(false);
        $this->_helper->viewRenderer->setNoRender();

        $streams=$this->getUser()->getAttribute('streamSession', null);

	    $indice=0;
        foreach($streams as $stream){
        	if($indice==0){
        	 $idStream = $stream->getIdStream();
        	}
        	else{
        		break;
        	}
        	$indice++;
        }        
        $chatCatalog=ChatCatalog::getInstance();
        $criteria = new Criteria();
        $criteria->add('id_stream', $idStream, Criteria::EQUAL);
        $relations = $chatCatalog->getChatFlashStreamRelations($criteria);
        //print_r($relations);
        if(count($relations)>0){
	        $criteria = new Criteria();	 
	        $criteriaUno = new Criteria();
	        $criteriaDos = new Criteria();
	        $criteriaDos->setMode(Criteria::INCLUSIVE);             	
        	foreach($relations as $rel){
        		//echo $rel['id_chat'];
        		$criteriaDos->add('id_chat', $rel['id_chat'], Criteria::EQUAL);
        	}
        	 
            $criteriaUno->add('status', Chat::$Status['Active'], Criteria::EQUAL);
            $criteria->addAnd($criteriaDos,$criteriaUno);
	        $criteria->addDescendingOrderByColumn('timestamp');
	        $criteria->setLimit(10);
	        
	        $chats = $chatCatalog->getByCriteria($criteria);
	
	        //var_dump($chats);
	        foreach ($chats as $chat){
	        	$idChat[]=$chat->getIdChat();
	        	$username[]=$chat->getUsername();
	        	$msg[]=$chat->getMsg();
	        }
	        
	        $lenght= count($username);
	        for ($i=$lenght-1; $i>=0; $i--){
	           //$lista.= 
			   $array[] = array(
			         'idChat' => $idChat[$i],
			         'username' => $username[$i],
			         'msg' => $msg[$i]
			   );             
	        }	
	        
	        //echo $lista;
	        die(Zend_Json::encode($array)); 
        } 
        else{
		   $array[] = array(
		         'idChat' => "",
		         'username' => "",
		         'msg' => ""
		   );         	
        	die(Zend_Json::encode($array)); 
        }
	}
	
	public function blockMsgAction()
	{
		$this->view->setLayoutFile(false);
        $this->_helper->viewRenderer->setNoRender();
		$id = $this->getRequest()->getParam('id');
		$chatCatalog=ChatCatalog::getInstance();
		$chat= $chatCatalog->getById($id);
		$chatCatalog->deactivate($chat);
	}
	
	public function loadPptAction()
	{
	   $this->view->setTpl('loadPpt');
	   $this->view->setLayoutFile(false);
	}

	public function sendQuestionAction()
	{
		require_once 'application/models/catalogs/EmailCatalog.php';
		
		$this->view->setLayoutFile(false);
        $this->_helper->viewRenderer->setNoRender();		
		
		$customerTheme=$this->getUser()->getCustomerTheme();
		
		$email = EmailCatalog::getInstance()->getByCompany($customerTheme->getIdCompany())->getOne();
		//var_dump($email);
		
		if($email instanceof Email)
		{
		   $name = urldecode($this->getRequest()->getParam('name'));
		   $mail = $this->getRequest()->getParam('mail');
		   $question = urldecode($this->getRequest()->getParam('question'));
		   $answer = urldecode($this->getRequest()->getParam('answer'));
		   
		$header="From:".$mail."\n ";

        $header.="Comentarios del evento\n";
        
        $header.="Content-type: text/html; charset=iso-8859-1";
        
        $today = date("F j, Y, g:i a"); 

        $msj=$today."<br/><br/>Nombre: <b>".$name."</b>";
        $msj.="<br/><br/>Correo: <b>".$mail."</b>";
        $msj.="<br/><br/>Preguntas: <b>".$question."</b>";
        $msj.="<br/><br/>Comentarios: <b>".$answer."</b>";
        
        if($this->getUser()->getBeanTemp()->getIdCompany() == 24){
        	
        $msj.="<br/><br/><b>Calzada de Tlalpan 4502 </b>";
		$msj.="<br/><br/><b>Col. Secci�n XVI,Delegaci�n Tlalpan, C.P. 14080 </b>";
		$msj.="<br/><br/><b>M�xico, D.F.</b>";
        	
        	
        }else{
        $msj.="<br/><br/><b>Montesito #38 Oficina 31 </b>";
		$msj.="<br/><br/><b>Col. Napoles, C.P. 03810,Benito Ju�rez</b>";
		$msj.="<br/><br/><b>M�xico, D.F.</b>";
        }
		   
		    
	       //$contentMail="Nombre: ".$name."\n Correo: ".$mail."\n Preguntas: ".$question."\n Comentarios: ".$answer."\n";
	    			
	       //$infoXsn=$contentMail."\n\nXSN GROUP\n+52 (55) 5533 6226\nR�o Mississippi No. 58, Col. Cuauht�moc\n";
	    			
					//comentarios@xsn.com.mx
	       mail($email->getEmail(),"Comentarios del evento",$msj,$header);	
	       echo "Tus comentarios fueron enviados satisfactoriamente...";   		
		}
		else{
		   echo "No existe ninguna direccion de correo asociada a la plantilla...";   
		}		

	}	
	/*
	public function setJpgAction()
	{
		require_once 'application/models/catalogs/PptJpgCatalog.php';
		$this->view->setLayoutFile(false);
        $this->_helper->viewRenderer->setNoRender();		
		$idJpg = $this->getRequest()->getParam('idJpg');
		$idStream = $this->getRequest()->getParam('idStream');
		//$action = $this->getRequest()->getParam('action');
		$action=1;
		
		$pptJpgCatalog= PptJpgCatalog::getInstance();
		$timestamp=$date = date('Y-m-d H:i:s');
		$pptJpgCatalog->linkToStreamVod($idJpg, $idStream, $timestamp, $action);
	}
	
	public function getJpgAction()
	{
		require_once 'Zend/Json.php';
		require_once 'application/models/catalogs/PptJpgCatalog.php';
		
		$this->view->setLayoutFile(false);
        $this->_helper->viewRenderer->setNoRender();
        $idStream = $this->getRequest()->getParam('idStream');
        
        $pptJpgCatalog= PptJpgCatalog::getInstance();
        $criteria = new Criteria();
        $criteria->add('id_stream', $idStream, Criteria::EQUAL);
        $criteria->addDescendingOrderByColumn('timestamp');
        $criteria->setLimit(1);
        
        $respond=$pptJpgCatalog->getPptJpgStreamVodRelations($criteria);
        
        
        if(count($respond)>0){
        foreach($respond as $item){
        	$jpg= $pptJpgCatalog->getById($item['id_jpg']);
        }
        

		$array[] = array(
		         'nameJpg' => $jpg->getName(),
		   );             
        }
        else{
        	$array=array();
        }
        
        //echo $lista;
        die(Zend_Json::encode($array));         
	}*/
	
	
	public function vodPptAction(){
		require_once 'application/models/catalogs/FlashVodCatalog.php';
		require_once 'application/models/catalogs/PptJpgCatalog.php';
		
		$idVod = (int) $this->getRequest()->getParam('id');
		
		$flashVodCatalog=FlashVodCatalog::getInstance();
		
		$flashVod=$flashVodCatalog->getById($idVod);
		
		
		if($flashVod instanceof FlashVod){
			$video=null;
			$prefix=$flashVod->getPrefix();
			$name=$flashVod->getNameVideo();
			
			if(!is_null($prefix)){
				$video=$prefix.'/'.$name;
			}
			else{
			    $video=$name;	
			}
			
			$PptJpgCatalog = PptJpgCatalog::getInstance();
			$criteria = new Criteria();
            $criteria->add('id_vod_flash', $flashVod->getIdVod(), Criteria::EQUAL);
		    $pptJpgFlashVod = $PptJpgCatalog->getPptJpgStreamVodRelations($criteria);
	        $dataArray = array();
	        $indice=0;
	        $tempDateTime=null;
	        $segundoTemp=0;
	        foreach($pptJpgFlashVod as $rs){
               if($indice==0){
               	    $tempDateTime=$rs['timestamp'];
					$jpg=$PptJpgCatalog->getById($rs['id_jpg']);      	
		            $dataArray[] = '1+'.$jpg->getName();
		            $segundoTemp=0;
               }
               else{
					//$birth = new DateTime('2011-04-11 10:49:14'); 
					//$today = new DateTime('2011-04-11 10:59:14'); 
					$birth = new DateTime($tempDateTime); 
					$today = new DateTime($rs['timestamp']); 					
					$diff = $birth->diff($today);
					
					$nuevaHr=$diff->format('%h-%i-%s');
					//echo $nuevaHr."<br/>";
					
					$temp= explode('-',$nuevaHr);
					
					$hrToSg=(int)$temp[0]*3600;
					
					$minToSg=(int)$temp[1]*60;
					
					//echo $hrToSg+$minToSg+$temp[2];
					$sumaSegundos=$segundoTemp+($hrToSg+$minToSg+$temp[2]);
					$jpg=$PptJpgCatalog->getById($rs['id_jpg']);      	
		            $dataArray[] = $sumaSegundos.'+'.$jpg->getName();	 
		            $tempDateTime=$rs['timestamp'];  
		            $segundoTemp=$sumaSegundos;            	
               }
               $indice++;
	        }
		}
		
		$this->view->flashVod=$flashVod;
		$this->view->video=$video;
		$this->view->arrayData=$dataArray;
		$this->view->setTpl('vodPpt');
	}
}
?>