<?php
/**
 * ##$BRAND_NAME$##
 *
 * ##$DESCRIPTION$##
 *
 * @category   Project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */

/**
 * UserLogCatalog
 */
require_once 'application/models/catalogs/UserLogCatalog.php';

/**
 * BaseController
 */
require_once 'lib/controller/BaseController.php';

/**
 * AccessRoleCatalog
 */
require_once 'application/models/catalogs/AccessRoleCatalog.php';

require_once 'application/models/catalogs/CompanyCatalog.php';

/**
 * Clase AuthController que representa el controlador para las acciones de login/logout
 *
 * @category   project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 */
class AuthController extends BaseController
{

    /**
     * Sobrecargamos el metodo init
     */
    public function init()
    {
        $registry = Zend_Registry::getInstance();
        $this->view->moduleName = $this->getRequest()->getModuleName();
        $this->view->controllerName = $this->getRequest()->getControllerName();
        $this->view->actionName = $this->getRequest()->getActionName();
        $this->view->baseUrl = $this->getRequest()->getBaseUrl();
        $this->view->systemTitle = utf8_decode($registry->config->appSettings->titulo);
        $this->view->l10n = new Zend_Translate('gettext', 'data/locale/base.mo', 'es');
    }
    
    /**
     * 
     */
    public function configAction()
    {
    	require_once 'application/models/catalogs/SecurityActionCatalog.php';
    	require_once 'application/models/catalogs/SecurityControllerCatalog.php';
        parent::init();
        $this->setTitle('Configuraci�n de Facultades');
        $this->view->accessRoles = AccessRoleCatalog::getInstance()->getActives(new Criteria());
        $actionsCriteria = new Criteria();
        $actionsCriteria->add(SecurityAction::SYSTEM,2,Criteria::EQUAL);
        $this->view->actions = SecurityActionCatalog::getInstance()->getByCriteria($actionsCriteria);
        $controllersCriteria = new Criteria();
        $controllersCriteria->add(SecurityController::SYSTEM,2,Criteria::EQUAL);
        $this->view->controllers = SecurityControllerCatalog::getInstance()->getByCriteria($controllersCriteria);
        $this->view->permissions = AccessRoleCatalog::getInstance()->getAllPermissions();
    }
    
    /**
     * Agrega/Elimina un permiso en la base de datos de facultades
     */
    public function setPermissionAction()
    {
        require_once 'lib/security/SecurityInspector.php';
        parent::init();
        $securityInspector = new SecurityInspector();
        $securityInspector->setPermission($this->getRequest()->getParam('value'),$this->getRequest()->getParam('idAction'),$this->getRequest()->getParam('idAccessRole'));
        die($this->getRequest()->getParam('value'));
    }
    
    /**
     * Inspecciona Todos los controllers y las actions, las agrega a la base de datos, y eso
     */
    public function inspectAction()
    {
    	require_once 'lib/security/SecurityInspector.php';
    	parent::init();
    	$securityInspector = new SecurityInspector();
    	$securityInspector->analizeWorkspace();
    	$this->setFlash('ok','Workspace analizado');
    	$this->_redirect('auth/config');
    }
    
    /**
     * Accion para la pantalla de Login
     */
    public function viewLoginAction()
    {
    	$this->view->companys = CompanyCatalog::getInstance()->getByCriteria()->toCombo();
        $this->view->contentTitle = 'Iniciar Sesi�n';
        
        $viewers= array();
        for($i=1; $i<=50; $i++){
        	$viewers[]=$i;
        }
        $this->view->numberviewers=$viewers;
        $this->view->setTpl('Login');
    }

    /*
     * Inicio de Sesi�n
     */
    public function loginAction()
    {
    	require_once 'application/models/catalogs/UsertempCatalog.php';
        $username = trim($this->getRequest()->getParam('username'));
        $password = trim($this->getRequest()->getParam('password'));
        try
        {
            if($username == null || $password == null)
                throw new AuthException('Debe especificar Usuario y contrase�a');
           $userTemp = Authentication::authenticate($username, $password);                      
           
           $user = new User();
           $user->setIdUser($userTemp->getIdUsertemp());
           $user->setUsername($userTemp->getUsername());
           
           $this->getUser()->setBeanTemp($userTemp);
           $this->getUser()->setBean($user);
           $this->getUser()->setAccessRole(AccessRoleCatalog::getInstance()->getById($userTemp->getIdAccessRole()));
           
           $this->_redirect('/');
        }
        catch(AuthException $e)
        {
	        $viewers= array();
	        for($i=1; $i<=50; $i++){
	        	$viewers[]=$i;
	        }
	        $this->view->numberviewers=$viewers;        	
            $this->view->errorMessage = $e->getMessage();
            $this->view->contentTitle = 'Iniciar Sesi�n';
        }
    }
    
    public function loginTempAction()
    {
    	require_once 'application/models/catalogs/EmailCatalog.php';
    	require_once 'application/models/catalogs/ViewerCatalog.php';
    	
        $username = trim($this->getRequest()->getParam('username'));
        $password = trim($this->getRequest()->getParam('password'));
        $code = trim($this->getRequest()->getParam('code'));
        $name = $this->getRequest()->getParam('name');
        $email = $this->getRequest()->getParam('email');
        $numberViewers = $this->getRequest()->getParam('viewers');
        $date=$date = date('Y-m-d H:i:s');
        
        try
        {
            if($username == null && $password == null)
                throw new AuthException('Debe especificar Usuario y contrase�a');
                
		    if($email == null)
		         throw new AuthException('Debe especificar un correo electr�nico');                
           $userTemp = Authentication::authenticateUserTemp($name, $password, $code);
           
           $emailCatalog = EmailCatalog::getInstance();
           
           $criteria= new Criteria();
           $criteria->add('email', $email, Criteria::EQUAL);
           
           $emailRes=$emailCatalog->getByCriteria($criteria);
           
           if($emailRes instanceof Email){
           	 $idEmail=$emailRes->getIdEmail();
           }
           else{
           	   $emailObj = EmailFactory::createEmail($email);
               $emailCatalog->create($emailObj);
               $idEmail=$emailObj->getIdEmail();  
           }
           

           
           $viewerCatalog = ViewerCatalog::getInstance();
           $viewerObj = ViewerFactory::create($name, $date, $userTemp->getIdUsertemp(), $numberViewers);
           $viewerCatalog->create($viewerObj);
           
           $viewerCatalog->linkToEmail($viewerObj->getIdViewer(), $idEmail);
           
           $user = new User();
           $user->setIdUser($userTemp->getIdUsertemp());
           $user->setUsername($userTemp->getUsername());
           
           $this->getUser()->setBeanTemp($userTemp);
           $this->getUser()->setBean($user);
           $this->getUser()->setAccessRole(AccessRoleCatalog::getInstance()->getById($userTemp->getIdAccessRole()));
           $this->_redirect('/');
        }
        catch(AuthException $e)
        {
	        $viewers= array();
	        for($i=1; $i<=50; $i++){
	        	$viewers[]=$i;
	        }
	        $this->view->numberviewers=$viewers;          	
            $this->view->errorMessage = $e->getMessage();
            $this->view->contentTitle = 'Iniciar Sesi�n';
            $this->view->setTpl('Login');
        }
    }    
    /*
     * Inicio de Sesi�n plantilla
     */
    public function freeAction()
    {
        $username = base64_decode($this->getRequest()->getParam('codeRef'));
		
        try
        {
            if($username == null )
                throw new AuthException('No esta activa la plantilla');
			
           if(!is_null($this->getUser()->getBean())){
           	//$this->getUser()->shutdown();
              session_unset();
           }     
                
           $userTemp = Authentication::authenticateFree($username);
           $user = new User();
           $user->setIdUser($userTemp->getIdUsertemp());
           $user->setUsername($userTemp->getUsername());
          
           $this->getUser()->setBeanTemp($userTemp);
           $this->getUser()->setBean($user);
           $this->getUser()->setFlag(1);
           $this->getUser()->setAccessRole(AccessRoleCatalog::getInstance()->getById($userTemp->getIdAccessRole()));
           // if(true )
               //throw new AuthException('No esta activa la plantillaasasd'.$this->getUser()->);
		   $this->_redirect('/');
        }
        catch(AuthException $e)
        {
	        $viewers= array();
	        for($i=1; $i<=50; $i++){
	        	$viewers[]=$i;
	        }
	        $this->view->numberviewers=$viewers;          	
            $this->view->errorMessage = $e->getMessage();
            $this->view->contentTitle = 'Iniciar Sesi�n';
            $this->view->setTpl('Login');
        }
    }    
    
    /**
     * Accion que hace logout.
     */
    public function logoutAction()
    {
    	$this->getUser()->shutdown();
    	$this->_redirect('/');
    }
    
    /**
     * Acci�n para autorizar descuentos
     * @return unknown_type
     */
    public function autorizedAction()
    {
    	$username = $this->getRequest()->getParam('username');
        $password = $this->getRequest()->getParam('password');
        try
        {
           if($username == null || $password == null)
                throw new AuthException('Debe especificar Usuario y contrase�a');
           $user = Authentication::authenticateAs($username, $password, AccessRole::$AccessRoles['Gerente']); 
           $this->getUser()->setAttribute('discounts', true, 'authorized');
        }
        catch(AuthException $e)
        {
            throw new Exception($e->getMessage());
        }
        $this->view->setLayoutFile(false);
        
        die();
    }
	public function passErrorAction(){
		$this->view->contentTitle='Error';
	}
}
