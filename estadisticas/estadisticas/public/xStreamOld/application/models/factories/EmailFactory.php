<?php
/**
 * ##$BRAND_NAME$## 
 *
 * ##$DESCRIPTION$##
 *
 * @category   Project
 * @package    Project_Models
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */

/**
 * Email
 */
require_once 'application/models/beans/Email.php';

/**
 * Clase EmailFactory
 *
 * @category   Project
 * @package    Project_Models
 * @subpackage Project_Models_Factories
 * @copyright  ##$COPYRIGHT$##
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */
class EmailFactory
{
    /**
     * Instancia un nuevo objeto Email
     * @param int $idPerson 
     * @param string $email 
     * @return Email Objeto Email
     */
    public static function createEmail($email)
    {
        $newEmail = new Email();
        $newEmail->setEmail($email);
        return $newEmail;
    }

    /**
     * Método que construye un objeto Email y lo rellena con la información del rowset
     * @param array $fields El arreglo que devolvió el objeto Zend_Db despues del fetch
     * @return Email 
     */
    public static function createFromArray($fields)
    {
        $newEmail = new Email();
        $newEmail->setIdEmail($fields['id_email']);
        $newEmail->setEmail($fields['email']);
        return $newEmail;
    }
}



