<?php
/**
 * Xsn
 *
 * Xsn
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */
/**
 * Dependences
 */
require_once "application/models/beans/Usertemp.php";

/**
 * Clase UsertempFactory
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_factories
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx) 
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     <zetta> & <chentepixtol>
 * @version    1.0.2 SVN: $Revision$
 */
class UsertempFactory
{

   /**
    * Create a new Usertemp instance
    * @param string $username
    * @param string $password
    * @param int $status
    * @param int $idAccessRole
    * @return Usertemp
    */
   public static function create($username, $password, $code, $status, $dateExpirate, $dateCreate, $idAccessRole, $idCompany)
   {
      $newUsertemp = new Usertemp();
      $newUsertemp->setUsername($username);
      $newUsertemp->setPassword($password);
      $newUsertemp->setCode($code);
      $newUsertemp->setStatus($status);
      $newUsertemp->setDateExpirate($dateExpirate);
      $newUsertemp->setDateCreate($dateCreate);
      $newUsertemp->setIdAccessRole($idAccessRole);
      $newUsertemp->setIdCompany($idCompany);
      return $newUsertemp;
   }
   
    /**
     * Método que construye un objeto Usertemp y lo rellena con la información del rowset
     * @param array $fields El arreglo que devolvió el objeto Zend_Db despues del fetch
     * @return Usertemp 
     */
    public static function createFromArray($fields)
    {
        $newUsertemp = new Usertemp();
        $newUsertemp->setIdUsertemp($fields['id_usertemp']);
        $newUsertemp->setUsername($fields['username']);
        $newUsertemp->setPassword($fields['password']);
        $newUsertemp->setCode($fields['code']);
        $newUsertemp->setStatus($fields['status']);
        $newUsertemp->setDateExpirate($fields['date_expiration']);
        $newUsertemp->setDateCreate($fields['date_create']);        
        $newUsertemp->setIdAccessRole($fields['id_access_role']);
        $newUsertemp->setIdCompany($fields['id_company']);
        return $newUsertemp;
    }
   
}
