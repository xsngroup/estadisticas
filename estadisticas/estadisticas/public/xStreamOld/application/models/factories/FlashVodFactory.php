<?php
/**
 * Xsn
 *
 * Monitor stream servers
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */
/**
 * Dependences
 */
require_once "application/models/beans/FlashVod.php";

/**
 * Clase FlashVodFactory
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_factories
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx) 
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     <zetta> & <chentepixtol>
 * @version    1.0.2 SVN: $Revision$
 */
class FlashVodFactory
{

   /**
    * Create a new FlashVod instance
    * @param string $prefix
    * @param string $nameVideo
    * @param string $titleVideo
    * @param string $descriptionVideo
    * @param int $idServer
    * @return FlashVod
    */
   public static function create($prefix, $nameVideo, $titleVideo, $descriptionVideo, $idServer, $pptSync)
   {
      $newFlashVod = new FlashVod();
      $newFlashVod->setPrefix($prefix);
      $newFlashVod->setNameVideo($nameVideo);
      $newFlashVod->setTitleVideo($titleVideo);
      $newFlashVod->setDescriptionVideo($descriptionVideo);
      $newFlashVod->setIdServer($idServer);
      $newFlashVod->setPptSync($pptSync);
      return $newFlashVod;
   }
   
    /**
     * Método que construye un objeto FlashVod y lo rellena con la información del rowset
     * @param array $fields El arreglo que devolvió el objeto Zend_Db despues del fetch
     * @return FlashVod 
     */
    public static function createFromArray($fields)
    {
        $newFlashVod = new FlashVod();
        $newFlashVod->setIdVod($fields['id_vod']);
        $newFlashVod->setPrefix($fields['prefix']);
        $newFlashVod->setNameVideo($fields['name_video']);
        $newFlashVod->setTitleVideo($fields['title_video']);
        $newFlashVod->setDescriptionVideo($fields['description_video']);
        $newFlashVod->setIdServer($fields['id_server']);
        $newFlashVod->setPptSync($fields['ppt_sync']);
        return $newFlashVod;
    }
   
}
