<?php
/**
 * ##$BRAND_NAME$## 
 *
 * ##$DESCRIPTION$##
 *
 * @category   Project
 * @package    Project_Models
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */

/**
 * Clase Email
 *
 * @category   Project
 * @package    Project_Models
 * @subpackage Project_Models_Beans
 * @copyright  ##$COPYRIGHT$##
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */
class Email
{
    /**
     * Constante que contiene el nombre de la tabla
     * @static TABLENAME
     */
    const TABLENAME = "xsn_common_emails";
    const TABLENAME_EMAIL_COMPANY = 'xsn_core_companys_emails';
    const TABLENAME_EMAIL_VIEWER = 'xsn_core_viewers_emails';    
    const ID_EMAIL = "xsn_common_emails.id_email";
    const EMAIL = "xsn_common_emails.email";

    /**
     * $idEmail
     * 
     * @var int $idEmail
     */
    private $idEmail;


    /**
     * $email
     * 
     * @var string $email
     */
    private $email;

    /**
     * Set the idEmail value
     * 
     * @param int $idEmail
     */
    public function setIdEmail($idEmail)
    {
        $this->idEmail = $idEmail;
    }

    /**
     * Return the idEmail value
     * 
     * @return int
     */
    public function getIdEmail()
    {
        return $this->idEmail;
    }


    /**
     * Set the email value
     * 
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Return the email value
     * 
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }


}

