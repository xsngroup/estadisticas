<?php
/**
 * Xsn
 *
 * Xsn
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */

/**
 * Clase PptJpg
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_beans
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx) 
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     <zetta> & <chentepixtol>
 * @version    1.0.2 SVN: $Revision$
 */
class PptJpg
{
    /**
     * Constante que contiene el nombre de la tabla 
     * @static TABLENAME
     */
    const TABLENAME = "xsn_core_ppt_jpg";
    const TABLENAME_PPT_JPG_FLASH_VOD = 'xsn_core_jpg_stream';

    /**
     * Constantes para los nombres de los campos
     */
    const ID_JPG = "xsn_core_ppt_jpg.id_jpg";
    const NAME = "xsn_core_ppt_jpg.name";
    const ID_PPT = "xsn_core_ppt_jpg.id_ppt";
    

    /**
     * $idJpg 
     * 
     * @var int $idJpg
     */
    private $idJpg;
    

    /**
     * $name 
     * 
     * @var string $name
     */
    private $name;
    

    /**
     * $idPpt 
     * 
     * @var int $idPpt
     */
    private $idPpt;

    /**
     * Set the idJpg value
     * 
     * @param int idJpg
     * @return PptJpg $pptJpg
     */
    public function setIdJpg($idJpg)
    {
        $this->idJpg = $idJpg;
        return $this;
    }

    /**
     * Return the idJpg value
     * 
     * @return int
     */
    public function getIdJpg()
    {
        return $this->idJpg;
    }

    /**
     * Set the name value
     * 
     * @param string name
     * @return PptJpg $pptJpg
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Return the name value
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the idPpt value
     * 
     * @param int idPpt
     * @return PptJpg $pptJpg
     */
    public function setIdPpt($idPpt)
    {
        $this->idPpt = $idPpt;
        return $this;
    }

    /**
     * Return the idPpt value
     * 
     * @return int
     */
    public function getIdPpt()
    {
        return $this->idPpt;
    }

}
