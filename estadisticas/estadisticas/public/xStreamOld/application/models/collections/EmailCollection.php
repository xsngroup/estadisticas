<?php
/**
 * ##$BRAND_NAME$## 
 *
 * ##$DESCRIPTION$##
 *
 * @category   Project
 * @package    Project_Models
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */

/**
 * Clase EmailCollection que representa una collecci�n de objetos Email
 *
 * @category   Project
 * @package    Project_Models
 * @subpackage Project_Models_Collections
 * @copyright  ##$COPYRIGHT$##
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */
class EmailCollection extends ArrayIterator
{
	/**
     * Appends the value
     * @param Email $email
     */
    public function append($email)
    {
        parent::append($email);
    }

    /**
     * Return current array entry
     * @return Email
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * Return current array entry and
     * move to next entry
     * @return Email
     */
    public function read()
    {
        $email = $this->current();
        $this->next();
        return $email;
    }

    /**
     * Get the first array entry
     * if exists or null if not
     * @return Email|null
     */
    public function getOne()
    {
        if ($this->count() > 0)
        {
            $this->seek(0);
            return $this->current();
        } else
            return null;
    }
    
    /**
     * Genera un arreglo a partir de la colecci�n
     * @return Array
     */
    public function toArray()
    {
    	$array = array();
    	while($this->valid())
    	{
    		$object = $this->read();
    		$array[$object->getIdEmail()] = $object->getEmail();
    	}
    	return $array;
    }
}




