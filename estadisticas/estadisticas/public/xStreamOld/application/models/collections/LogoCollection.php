<?php
/**
 * Xsn
 *
 * Xsn
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */


require_once "lib/utils/Parser.php";

/**
 * Clase LogoCollection que representa una collección de objetos Logo
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_collections
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     zetta & chentepixtol
 * @version    1.0.2 SVN: $Revision$
 */
class LogoCollection extends ArrayIterator
{

    /**
     * @var Parser
     */
    private $parser;
    
    /**
     * Constructor
     * @param array $array
     * @return void
     */
    public function __construct($array = array())
    {
        $this->parser = new Parser('Logo');
        parent::__construct($array);
    }

    /**
     * Appends the value
     * @param Logo $logo
     */
    public function append($logo)
    {
        parent::offsetSet($logo->getIdLogo(), $logo);
        $this->rewind();
    }

    /**
     * Return current array entry
     * @return Logo
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * Return current array entry and 
     * move to next entry
     * @return Logo 
     */
    public function read()
    {
        $logo = $this->current();
        $this->next();
        return $logo;
    }

    /**
     * Get the first array entry
     * if exists or null if not 
     * @return Logo|null 
     */
    public function getOne()
    {
        if ($this->count() > 0)
        {
            $this->seek(0);
            return $this->current();
        } else
            return null;
    }
    
    /**
     * Contains one object with $idLogo
     * @param  int $idLogo
     * @return boolean
     */
    public function contains($idLogo)
    {
        return parent::offsetExists($idLogo);
    }
    
    /**
     * Remove one object with $idLogo
     * @param  int $idLogo
     */
    public function remove($idLogo)
    {
        if( $this->contains($idLogo) )
            $this->offsetUnset($idLogo);
    }
    
    /**
     * Merge two Collections
     * @param LogoCollection $logoCollection
     * @return void
     */
    public function merge(LogoCollection $logoCollection)
    {
        $logoCollection->rewind();
        while($logoCollection->valid())
        {
            $logo = $logoCollection->read();
            if( !$this->contains( $logo->getIdLogo() ) )
                $this->append($logo);
        }
        $logoCollection->rewind();
    }
    
    /**
     * Diff two Collections
     * @param LogoCollection $logoCollection
     * @return void
     */
    public function diff(LogoCollection $logoCollection)
    {
        $logoCollection->rewind();
        while($logoCollection->valid())
        {
            $logo = $logoCollection->read();
            if( $this->contains( $logo->getIdLogo() ) )
                $this->remove($logo->getIdLogo());     
        }
        $logoCollection->rewind();
    }
    
    /**
     * Intersect two Collections
     * @param LogoCollection $logoCollection
     * @return LogoCollection
     */
    public function intersect(LogoCollection $logoCollection)
    {
        $newlogoCollection = LogoCollection();
        $logoCollection->rewind();
        while($logoCollection->valid())
        {
            $logo = $logoCollection->read();
            if( $this->contains( $logo->getIdLogo() ) )
                $newlogoCollection->append($logo);
        }
        $logoCollection->rewind();
        return $newlogoCollection;
    }
    
    /**
     * Retrieve the array with primary keys 
     * @return array
     */
    public function getPrimaryKeys()
    {
        return array_keys($this->getArrayCopy());
    }
    
    /**
     * Retrieve the Logo with primary key  
     * @param  int $idLogo
     * @return Logo
     */
    public function getByPK($idLogo)
    {
        return $this->contains($idLogo) ? $this[$idLogo] : null;
    }
  
    /**
     * Transforma una collection a un array
     * @return array
     */
    public function toArray()
    {
        $array = array();
        while ($this->valid())
        {
            $logo = $this->read();
            $this->parser->changeBean($logo);
            $array[$logo->getIdLogo()] = $this->parser->toArray();
        }
        $this->rewind();
        return $array;
    }
    
    /**
     * Crea un array asociativo de $key => $value a partir de las constantes de un bean
     * @param string $ckey
     * @param string $cvalue
     * @return array
     */
    public function toKeyValueArray($ckey, $cvalue)
    {
        $array = array();
        while ($this->valid())
        {
            $logo = $this->read();
            $this->parser->changeBean($logo);
            $array += $this->parser->toKeyValueArray($ckey, $cvalue);
        }
        $this->rewind();
        return $array;
    }
    
    /**
     * Retrieve the parser object
     * @return Parser
     */
    public function getParser()
    {
        return $this->parser;
    }
    
    /**
     * Is Empty
     * @return boolean
     */
    public function isEmpty()
    {
        return $this->count() == 0;
    }
  
  
}

