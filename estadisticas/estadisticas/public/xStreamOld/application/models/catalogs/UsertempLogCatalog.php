<?php
/**
 * Xsn
 *
 * Xsn
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */

/**
 * Dependences
 */
require_once "lib/db/Catalog.php";
require_once "application/models/beans/UsertempLog.php";
require_once "application/models/exceptions/UsertempLogException.php";
require_once "application/models/collections/UsertempLogCollection.php";
require_once "application/models/factories/UsertempLogFactory.php";

/**
 * Singleton UsertempLogCatalog Class
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_catalogs
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     zetta & chentepixtol
 * @version    1.0.2 SVN: $Revision$
 */
class UsertempLogCatalog extends Catalog
{

    /**
     * Singleton Instance
     * @var UsertempLogCatalog
     */
    static protected $instance = null;


    /**
     * Método para obtener la instancia del catálogo
     * @return UsertempLogCatalog
     */
    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
          self::$instance = new self();
        }
        return self::$instance;
    }
    
    /**
     * Constructor de la clase UsertempLogCatalog
     * @return UsertempLogCatalog
     */
    protected function UsertempLogCatalog()
    {
        parent::Catalog();
    }

    /**
     * Metodo para agregar un UsertempLog a la base de datos
     * @param UsertempLog $usertempLog Objeto UsertempLog
     */
    public function create($usertempLog)
    {
        if(!($usertempLog instanceof UsertempLog))
            throw new UsertempLogException("passed parameter isn't a UsertempLog instance");
        try
        {
            $data = array(
                'event_type' => $usertempLog->getEventType(),
                'ip' => $usertempLog->getIp(),
                'timestamp' => $usertempLog->getTimestamp(),
                'note' => $usertempLog->getTimestamp(),
                'id_usertemp' => $usertempLog->getIdUsertemp(),
            );
            $data = array_filter($data, 'Catalog::notNull');
            $this->db->insert(UsertempLog::TABLENAME, $data);
            $usertempLog->setIdUsertempLog($this->db->lastInsertId());
        }
        catch(Exception $e)
        {
            throw new UsertempLogException("The UsertempLog can't be saved \n" . $e->getMessage());
        }
    }

    /**
     * Metodo para Obtener los datos de un objeto por su llave primaria
     * @param int $idUsertempLog
     * @param boolean $throw
     * @return UsertempLog|null
     */
    public function getById($idUsertempLog, $throw = false)
    {
        try
        {
            $criteria = new Criteria();
            $criteria->add(UsertempLog::ID_USERTEMP_LOG, $idUsertempLog, Criteria::EQUAL);
            $newUsertempLog = $this->getByCriteria($criteria)->getOne();
        }
        catch(Exception $e)
        {
            throw new UsertempLogException("Can't obtain the UsertempLog \n" . $e->getMessage());
        }
        if($throw && null == $newUsertempLog)
            throw new UsertempLogException("The UsertempLog at $idUsertempLog not exists ");
        return $newUsertempLog;
    }
    
    /**
     * Metodo para Obtener una colección de objetos por varios ids
     * @param array $ids
     * @return UsertempLogCollection
     */
    public function getByIds(array $ids)
    {
        if(null == $ids) return new UsertempLogCollection();
        try
        {
            $criteria = new Criteria();
            $criteria->add(UsertempLog::ID_USERTEMP_LOG, $ids, Criteria::IN);
            $usertempLogCollection = $this->getByCriteria($criteria);
        }
        catch(Exception $e)
        {
            throw new UsertempLogException("UsertempLogCollection can't be populated\n" . $e->getMessage());
        }
        return $usertempLogCollection;
    }

    /**
     * Metodo para actualizar un UsertempLog
     * @param UsertempLog $usertempLog 
     */
    public function update($usertempLog)
    {
        if(!($usertempLog instanceof UsertempLog))
            throw new UsertempLogException("passed parameter isn't a UsertempLog instance");
        try
        {
            $where[] = "id_usertemp_log = '{$usertempLog->getIdUsertempLog()}'";
            $data = array(
                'event_type' => $usertempLog->getEventType(),
                'ip' => $usertempLog->getIp(),
                'timestamp' => $usertempLog->getTimestamp(),
                'note' => $usertempLog->getTimestamp(),
                'id_usertemp' => $usertempLog->getIdUsertemp(),
            );
            $data = array_filter($data, 'Catalog::notNull');
            $this->db->update(UsertempLog::TABLENAME, $data, $where);
        }
        catch(Exception $e)
        {
            throw new UsertempLogException("The UsertempLog can't be updated \n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para guardar un usertempLog
     * @param UsertempLog $usertempLog
     */	
    public function save($usertempLog)
    {
        if(!($usertempLog instanceof UsertempLog))
            throw new UsertempLogException("passed parameter isn't a UsertempLog instance");
        if(null != $usertempLog->getIdUsertempLog())
            $this->update($usertempLog);
        else
            $this->create($usertempLog);
    }

    /**
     * Metodo para eliminar un usertempLog
     * @param UsertempLog $usertempLog
     */
    public function delete($usertempLog)
    {
        if(!($usertempLog instanceof UsertempLog))
            throw new UsertempLogException("passed parameter isn't a UsertempLog instance");
        $this->deleteById($usertempLog->getIdUsertempLog());
    }

    /**
     * Metodo para eliminar un UsertempLog a partir de su Id
     * @param int $idUsertempLog
     */
    public function deleteById($idUsertempLog)
    {
        try
        {
            $where = array($this->db->quoteInto('id_usertemp_log = ?', $idUsertempLog));
            $this->db->delete(UsertempLog::TABLENAME, $where);
        }
        catch(Exception $e)
        {
            throw new UsertempLogException("The UsertempLog can't be deleted\n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para eliminar varios UsertempLog a partir de su Id
     * @param array $ids
     */
    public function deleteByIds(array $ids)
    {
        try
        {
            $criteria = new Criteria();
            $criteria->add(UsertempLog::ID_USERTEMP_LOG, $ids, Criteria::IN);
            $this->db->delete(UsertempLog::TABLENAME, array($criteria->createSql()));
        }
        catch(Exception $e)
        {
            throw new UsertempLogException("Can't delete that\n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para Obtener todos los ids en un arreglo
     * @return array
     */
    public function retrieveAllIds()
    {
        return $this->getIdsByCriteria(new Criteria());
    }

    /**
     * Metodo para obtener todos los id de UsertempLog por criterio
     * @param Criteria $criteria
     * @return array Array con todos los id de UsertempLog que encajen en la busqueda
     */
    public function getIdsByCriteria(Criteria $criteria = null)
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        return $this->getCustomFieldByCriteria(UsertempLog::ID_USERTEMP_LOG, $criteria);
    }

    /**
     * Metodo para obtener un campo en particular de un UsertempLog dado un criterio
     * @param string $field
     * @param Criteria $criteria
     * @param $distinct
     * @return array Array con el campo de los objetos UsertempLog que encajen en la busqueda
     */
    public function getCustomFieldByCriteria($field, Criteria $criteria = null, $distinct = false)
    { 
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $distinct = $distinct ? 'DISTINCT' : '';
        try
        {
            $sql = "SELECT {$distinct} {$field}
                    FROM ".UsertempLog::TABLENAME."
                    WHERE  " . $criteria->createSql();
            $result = $this->db->fetchCol($sql);
        } catch(Zend_Db_Exception $e)
        {
            throw new UsertempLogException("No se pudieron obtener los fields de objetos UsertempLog\n" . $e->getMessage());
        }
        return $result;
    }

    /**
     * Metodo que regresa una coleccion de objetos UsertempLog 
     * dependiendo del criterio establecido
     * @param Criteria $criteria
     * @return UsertempLogCollection $usertempLogCollection
     */
    public function getByCriteria(Criteria $criteria = null)
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $this->db->setFetchMode(Zend_Db::FETCH_ASSOC);
        try 
        {
            $sql = "SELECT * FROM ".UsertempLog::TABLENAME."
                    WHERE " . $criteria->createSql();
            $usertempLogCollection = new UsertempLogCollection();
            foreach ($this->db->fetchAll($sql) as $result){
                $usertempLogCollection->append($this->getUsertempLogInstance($result));
            }
        }
        catch(Zend_Db_Exception $e)
        {
            throw new UsertempLogException("Cant obtain UsertempLogCollection\n" . $e->getMessage());
        }
        return $usertempLogCollection;
    }
    
    /**
     * Metodo que cuenta UsertempLog 
     * dependiendo del criterio establecido
     * @param Criteria $criteria
     * @param string $field
     * @return int $count
     */
    public function countByCriteria(Criteria $criteria = null, $field = 'id_usertemp_log')
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        try 
        {
            $sql = "SELECT COUNT( $field ) FROM ".UsertempLog::TABLENAME."
                    WHERE " . $criteria->createSql();   
            $count = $this->db->fetchOne($sql);
        }
        catch(Zend_Db_Exception $e)
        {
            throw new UsertempLogException("Cant obtain the count \n" . $e->getMessage());
        }
        return $count;
    }
    
    /**
     * Método que construye un objeto UsertempLog y lo rellena con la información del rowset
     * @param array $result El arreglo que devolvió el objeto Zend_Db despues del fetch
     * @return UsertempLog 
     */
    private function getUsertempLogInstance($result)
    {
        return UsertempLogFactory::createFromArray($result);
    }
  
    /**
     * Obtiene un UsertempLogCollection  dependiendo del idUsertemp
     * @param int $idUsertemp  
     * @return UsertempLogCollection 
     */
    public function getByIdUsertemp($idUsertemp)
    {
        $criteria = new Criteria();
        $criteria->add(UsertempLog::ID_USERTEMP, $idUsertemp, Criteria::EQUAL);
        $usertempLogCollection = $this->getByCriteria($criteria);
        return $usertempLogCollection;
    }


} 
 
