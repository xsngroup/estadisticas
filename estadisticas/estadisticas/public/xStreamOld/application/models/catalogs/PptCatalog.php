<?php
/**
 * Xsn
 *
 * Xsn
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */

/**
 * Dependences
 */
require_once "lib/db/Catalog.php";
require_once "application/models/beans/Ppt.php";
require_once "application/models/exceptions/PptException.php";
require_once "application/models/collections/PptCollection.php";
require_once "application/models/factories/PptFactory.php";

/**
 * Singleton PptCatalog Class
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_catalogs
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     zetta & chentepixtol
 * @version    1.0.2 SVN: $Revision$
 */
class PptCatalog extends Catalog
{

    /**
     * Singleton Instance
     * @var PptCatalog
     */
    static protected $instance = null;


    /**
     * Método para obtener la instancia del catálogo
     * @return PptCatalog
     */
    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
          self::$instance = new self();
        }
        return self::$instance;
    }
    
    /**
     * Constructor de la clase PptCatalog
     * @return PptCatalog
     */
    protected function PptCatalog()
    {
        parent::Catalog();
    }

    /**
     * Metodo para agregar un Ppt a la base de datos
     * @param Ppt $ppt Objeto Ppt
     */
    public function create($ppt)
    {
        if(!($ppt instanceof Ppt))
            throw new PptException("passed parameter isn't a Ppt instance");
        try
        {
            $data = array(
                'title' => $ppt->getTitle(),
                'name' => $ppt->getName(),  
                'code' => $ppt->getCode(), 
            );
            $data = array_filter($data, 'Catalog::notNull');
            $this->db->insert(Ppt::TABLENAME, $data);
            $ppt->setIdPpt($this->db->lastInsertId());
        }
        catch(Exception $e)
        {
            throw new PptException("The Ppt can't be saved \n" . $e->getMessage());
        }
    }

    /**
     * Metodo para Obtener los datos de un objeto por su llave primaria
     * @param int $idPpt
     * @param boolean $throw
     * @return Ppt|null
     */
    public function getById($idPpt, $throw = false)
    {
        try
        {
            $criteria = new Criteria();
            $criteria->add(Ppt::ID_PPT, $idPpt, Criteria::EQUAL);
            $newPpt = $this->getByCriteria($criteria)->getOne();
        }
        catch(Exception $e)
        {
            throw new PptException("Can't obtain the Ppt \n" . $e->getMessage());
        }
        if($throw && null == $newPpt)
            throw new PptException("The Ppt at $idPpt not exists ");
        return $newPpt;
    }
    
    /**
     * Metodo para Obtener una colección de objetos por varios ids
     * @param array $ids
     * @return PptCollection
     */
    public function getByIds(array $ids)
    {
        if(null == $ids) return new PptCollection();
        try
        {
            $criteria = new Criteria();
            $criteria->add(Ppt::ID_PPT, $ids, Criteria::IN);
            $pptCollection = $this->getByCriteria($criteria);
        }
        catch(Exception $e)
        {
            throw new PptException("PptCollection can't be populated\n" . $e->getMessage());
        }
        return $pptCollection;
    }

    /**
     * Metodo para actualizar un Ppt
     * @param Ppt $ppt 
     */
    public function update($ppt)
    {
        if(!($ppt instanceof Ppt))
            throw new PptException("passed parameter isn't a Ppt instance");
        try
        {
            $where[] = "id_ppt = '{$ppt->getIdPpt()}'";
            $data = array(
                'title' => $ppt->getTitle(),
                'name' => $ppt->getName(),
                'code' => $ppt->getCode(), 
            );
            $data = array_filter($data, 'Catalog::notNull');
            $this->db->update(Ppt::TABLENAME, $data, $where);
        }
        catch(Exception $e)
        {
            throw new PptException("The Ppt can't be updated \n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para guardar un ppt
     * @param Ppt $ppt
     */	
    public function save($ppt)
    {
        if(!($ppt instanceof Ppt))
            throw new PptException("passed parameter isn't a Ppt instance");
        if(null != $ppt->getIdPpt())
            $this->update($ppt);
        else
            $this->create($ppt);
    }

    /**
     * Metodo para eliminar un ppt
     * @param Ppt $ppt
     */
    public function delete($ppt)
    {
        if(!($ppt instanceof Ppt))
            throw new PptException("passed parameter isn't a Ppt instance");
        $this->deleteById($ppt->getIdPpt());
    }

    /**
     * Metodo para eliminar un Ppt a partir de su Id
     * @param int $idPpt
     */
    public function deleteById($idPpt)
    {
        try
        {
            $where = array($this->db->quoteInto('id_ppt = ?', $idPpt));
            $this->db->delete(Ppt::TABLENAME, $where);
        }
        catch(Exception $e)
        {
            throw new PptException("The Ppt can't be deleted\n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para eliminar varios Ppt a partir de su Id
     * @param array $ids
     */
    public function deleteByIds(array $ids)
    {
        try
        {
            $criteria = new Criteria();
            $criteria->add(Ppt::ID_PPT, $ids, Criteria::IN);
            $this->db->delete(Ppt::TABLENAME, array($criteria->createSql()));
        }
        catch(Exception $e)
        {
            throw new PptException("Can't delete that\n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para Obtener todos los ids en un arreglo
     * @return array
     */
    public function retrieveAllIds()
    {
        return $this->getIdsByCriteria(new Criteria());
    }

    /**
     * Metodo para obtener todos los id de Ppt por criterio
     * @param Criteria $criteria
     * @return array Array con todos los id de Ppt que encajen en la busqueda
     */
    public function getIdsByCriteria(Criteria $criteria = null)
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        return $this->getCustomFieldByCriteria(Ppt::ID_PPT, $criteria);
    }

    /**
     * Metodo para obtener un campo en particular de un Ppt dado un criterio
     * @param string $field
     * @param Criteria $criteria
     * @param $distinct
     * @return array Array con el campo de los objetos Ppt que encajen en la busqueda
     */
    public function getCustomFieldByCriteria($field, Criteria $criteria = null, $distinct = false)
    { 
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $distinct = $distinct ? 'DISTINCT' : '';
        try
        {
            $sql = "SELECT {$distinct} {$field}
                    FROM ".Ppt::TABLENAME."
                    WHERE  " . $criteria->createSql();
            $result = $this->db->fetchCol($sql);
        } catch(Zend_Db_Exception $e)
        {
            throw new PptException("No se pudieron obtener los fields de objetos Ppt\n" . $e->getMessage());
        }
        return $result;
    }

    /**
     * Metodo que regresa una coleccion de objetos Ppt 
     * dependiendo del criterio establecido
     * @param Criteria $criteria
     * @return PptCollection $pptCollection
     */
    public function getByCriteria(Criteria $criteria = null)
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $this->db->setFetchMode(Zend_Db::FETCH_ASSOC);
        try 
        {
            $sql = "SELECT * FROM ".Ppt::TABLENAME."
                    WHERE " . $criteria->createSql();
            $pptCollection = new PptCollection();
            foreach ($this->db->fetchAll($sql) as $result){
                $pptCollection->append($this->getPptInstance($result));
            }
        }
        catch(Zend_Db_Exception $e)
        {
            throw new PptException("Cant obtain PptCollection\n" . $e->getMessage());
        }
        return $pptCollection;
    }
    
    /**
     * Metodo que cuenta Ppt 
     * dependiendo del criterio establecido
     * @param Criteria $criteria
     * @param string $field
     * @return int $count
     */
    public function countByCriteria(Criteria $criteria = null, $field = 'id_ppt')
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        try 
        {
            $sql = "SELECT COUNT( $field ) FROM ".Ppt::TABLENAME."
                    WHERE " . $criteria->createSql();   
            $count = $this->db->fetchOne($sql);
        }
        catch(Zend_Db_Exception $e)
        {
            throw new PptException("Cant obtain the count \n" . $e->getMessage());
        }
        return $count;
    }
    
    /**
     * Método que construye un objeto Ppt y lo rellena con la información del rowset
     * @param array $result El arreglo que devolvió el objeto Zend_Db despues del fetch
     * @return Ppt 
     */
    private function getPptInstance($result)
    {
        return PptFactory::createFromArray($result);
    }

    /**
     * Link a Ppt to CustomeTheme
     * @param int $idPpt
     * @param int $idCustomeTheme
     * @param  $status
     */
    public function linkToCustomeTheme($idPpt, $idCustomeTheme, $status)
    {
        try
        {
            $this->unlinkFromCustomeTheme($idPpt, $idCustomeTheme);
            $data = array(
                'id_ppt' => $idPpt,
                'id_custome_theme' => $idCustomeTheme,
                'status' => $status,
            );
            $this->db->insert(Ppt::TABLENAME_PPT_CUSTOME_THEME, $data);
        }
        catch(Exception $e)
        {
            throw new PptException("Can't link Ppt to CustomeTheme\n" . $e->getMessage());
        }
    }

    /**
     * Unlink a Ppt from CustomeTheme
     * @param int $idPpt
     * @param int $idCustomeTheme
     */
    public function unlinkFromCustomeTheme($idPpt, $idCustomeTheme)
    {
        try
        {
            $where = array(
                $this->db->quoteInto('id_ppt = ?', $idPpt),
                $this->db->quoteInto('id_custome_theme = ?', $idCustomeTheme),
            );
            $this->db->delete(Ppt::TABLENAME_PPT_CUSTOME_THEME, $where);
        }
        catch(Exception $e)
        {
            throw new PptException("Can't unlink Ppt to CustomeTheme\n" . $e->getMessage());
        }
    }

    /**
     * Unlink all CustomeTheme relations
     * @param int $idPpt
     * @param  $status
     */
    public function unlinkAllCustomeThemeRelations($idPpt, $status = null)
    {
        try
        {
            $where = array(
                $this->db->quoteInto('id_ppt = ?', $idPpt),
            );
            if(null != $status) $where[] = $this->db->quoteInto('status = ?', $status);
            $this->db->delete(Ppt::TABLENAME_PPT_CUSTOME_THEME, $where);
        }
        catch(Exception $e)
        {
            throw new PptException("Can't unlink all CustomeTheme relations \n" . $e->getMessage());
        }
    }

    /**
     * Get Ppt - CustomeTheme relations by Criteria
     * @param Criteria $criteria
     * @return array
     */
    public function getPptCustomeThemeRelations(Criteria $criteria = null)
    { 
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $this->db->setFetchMode(Zend_Db::FETCH_ASSOC);
        try
        {
           $sql = "SELECT * FROM ". Ppt::TABLENAME_PPT_CUSTOME_THEME ."
                   WHERE  " . $criteria->createSql();
           $result = $this->db->fetchAll($sql);
        } catch(Exception $e)
        {
           throw new PptException("Can't obtain relations by criteria\n" . $e->getMessage());
        }
        return $result;
    }

    /**
     * Get PptCollection by CustomeTheme
     * @param int $idCustomeTheme
     * @param [|array] $status
     * @return PptCollection
     */
    public function getByCustomeTheme($idCustomeTheme, $status = null)
    {
        $criteria = new Criteria();
        $criteria->add('id_custome_theme', $idCustomeTheme, Criteria::EQUAL);
        if(null != $status) $criteria->add('status', $status, is_array($status) ? Criteria::IN : Criteria::EQUAL);
        $pptCustomeTheme = $this->getPptCustomeThemeRelations($criteria);
        $ids = array();
        foreach($pptCustomeTheme as $rs){
            $ids[] = $rs['id_ppt'];
        }
        return $this->getByIds($ids);
    }


} 
 
