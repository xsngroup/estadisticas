<div class="center">
    <h3>{$l10n->_('Listado de Usuarios')}</h3>
    <hr/>
	<table class="center">
		<caption>{$l10n->_('Lista de Usuarios')}</caption>
		<thead>
			<tr>
				<td>#</td>
				<td>{$l10n->_('Username')}</td>
				<td>{$l10n->_('Nombre')}</td>
				<td>{$l10n->_('Grupo')}</td>
				<td colspan="2">{$l10n->_('Acciones')}</td>
			</tr>
		</thead>
		<tbody id="ajaxList">
			{foreach $users as $user}
				<tr class="{$user@iteration|even}">
					<td>{$user->getIdUser()}</td>
					<td>{$user->getUsername()}</td>
					<td>{$user->getFullName()}</td>
					<td>{$accessRoles[$user->getIdAccessRole()]}</td>
					<td><a href="{url action=edit id=$user->getIdUser()}">{icon src=pencil class=tip title=Editar}</a></td>
					<td><a href="{url action=delete id=$user->getIdUser()}" class="confirm">{icon src=delete class=tip title=Borrar}</a></td>
				</tr>
			{/foreach}
		</tbody>
	</table>
</div>
