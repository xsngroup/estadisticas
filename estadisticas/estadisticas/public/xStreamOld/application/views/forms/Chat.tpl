<tr>
    <th>{$i18n->_('id_chat')}</th>
    <td># {$post['id_chat']}<input type="hidden" name="id_chat" id="id_chat" value="{$post['id_chat']}" /></td>
</tr>
<tr>
    <th>{$i18n->_('timestamp')}</th>
    <td><input type="text" name="timestamp" id="timestamp" value="{$post['timestamp']}" class="datePicker dateISO required" /></td>
</tr>
<tr>
    <th>{$i18n->_('msg')}</th>
    <td><textarea name="msg" id="msg" class="">{$post['msg']}</textarea></td>
</tr>
<tr>
    <th>{$i18n->_('status')}</th>
    <td><input type="text" name="status" id="status" value="{$post['status']}" class="number" /></td>
</tr>

<!--
$idChat = $this->getRequest()->getParam('id_chat');
$timestamp = $this->getRequest()->getParam('timestamp');
$msg = $this->getRequest()->getParam('msg');
$status = $this->getRequest()->getParam('status');
-->

<!--
$post = array(
    'id_chat' => $chat->getIdChat(),
    'timestamp' => $chat->getTimestamp(),
    'msg' => $chat->getMsg(),
    'status' => $chat->getStatus(),
);
-->
