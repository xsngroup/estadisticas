<tr>
    <th>{$i18n->_('id_custome_theme')}</th>
    <td># {$post['id_custome_theme']}<input type="hidden" name="id_custome_theme" id="id_custome_theme" value="{$post['id_custome_theme']}" /></td>
</tr>
<tr>
    <th>{$i18n->_('id_company')}</th>
    <td>{html_options name=id_company id=id_company options=$Companys selected=$post['id_company'] }</td>
</tr>
<tr>
    <th>{$i18n->_('url_logo')}</th>
    <td><input type="text" name="url_logo" id="url_logo" value="{$post['url_logo']}" class="" /></td>
</tr>
<tr>
    <th>{$i18n->_('url_banner')}</th>
    <td><input type="text" name="url_banner" id="url_banner" value="{$post['url_banner']}" class="" /></td>
</tr>
<tr>
    <th>{$i18n->_('color_theme')}</th>
    <td><input type="text" name="color_theme" id="color_theme" value="{$post['color_theme']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('type_gallery')}</th>
    <td><input type="text" name="type_gallery" id="type_gallery" value="{$post['type_gallery']}" class="number" /></td>
</tr>

<!--
$idCustomeTheme = $this->getRequest()->getParam('id_custome_theme');
$urlLogo = $this->getRequest()->getParam('url_logo');
$urlBanner = $this->getRequest()->getParam('url_banner');
$colorTheme = $this->getRequest()->getParam('color_theme');
$typeGallery = $this->getRequest()->getParam('type_gallery');
$idCompany = $this->getRequest()->getParam('id_company');
-->

<!--
$post = array(
    'id_custome_theme' => $customeTheme->getIdCustomeTheme(),
    'url_logo' => $customeTheme->getUrlLogo(),
    'url_banner' => $customeTheme->getUrlBanner(),
    'color_theme' => $customeTheme->getColorTheme(),
    'type_gallery' => $customeTheme->getTypeGallery(),
    'id_company' => $customeTheme->getIdCompany(),
);
-->
