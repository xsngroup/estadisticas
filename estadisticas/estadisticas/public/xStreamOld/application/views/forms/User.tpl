<tr>
    <th>{$i18n->_('id_user')}</th>
    <td># {$post['id_user']}<input type="hidden" name="id_user" id="id_user" value="{$post['id_user']}" /></td>
</tr>
<tr>
    <th>{$i18n->_('id_access_role')}</th>
    <td>{html_options name=id_access_role id=id_access_role options=$AccessRoles selected=$post['id_access_role'] }</td>
</tr>
<tr>
    <th>{$i18n->_('id_person')}</th>
    <td>{html_options name=id_person id=id_person options=$Persons selected=$post['id_person'] }</td>
</tr>
<tr>
    <th>{$i18n->_('username')}</th>
    <td><input type="text" name="username" id="username" value="{$post['username']}" class=" required" /></td>
</tr>
<tr>
    <th>{$i18n->_('password')}</th>
    <td><input type="text" name="password" id="password" value="{$post['password']}" class=" required" /></td>
</tr>
<tr>
    <th>{$i18n->_('status')}</th>
    <td><input type="text" name="status" id="status" value="{$post['status']}" class="number required" /></td>
</tr>
<tr>
    <th>{$i18n->_('system')}</th>
    <td><input type="text" name="system" id="system" value="{$post['system']}" class="number required" /></td>
</tr>

<!--
$idUser = $this->getRequest()->getParam('id_user');
$username = $this->getRequest()->getParam('username');
$password = $this->getRequest()->getParam('password');
$status = $this->getRequest()->getParam('status');
$idAccessRole = $this->getRequest()->getParam('id_access_role');
$idPerson = $this->getRequest()->getParam('id_person');
$system = $this->getRequest()->getParam('system');
-->

<!--
$post = array(
    'id_user' => $user->getIdUser(),
    'username' => $user->getUsername(),
    'password' => $user->getPassword(),
    'status' => $user->getStatus(),
    'id_access_role' => $user->getIdAccessRole(),
    'id_person' => $user->getIdPerson(),
    'system' => $user->getSystem(),
);
-->
