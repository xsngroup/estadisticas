        <script type="text/javascript" src="{$baseUrl}/js/modules/layout/script.js"></script>
        <link rel="stylesheet" type="text/css" href="{$baseUrl}/plugins/pptSync/history/history.css" />
        <script type="text/javascript" src="{$baseUrl}/plugins/pptSync/history/history.js"></script>
        <!-- END Browser History required section -->  
		    
        <script type="text/javascript" src="{$baseUrl}/plugins/pptSync/swfobject.js"></script>
	    <script type="text/javascript">
	            <!-- For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. --> 
	            var swfVersionStr = "10.0.0";
	            <!-- To use express install, set to playerProductInstall.swf, otherwise the empty string. -->
	            var xiSwfUrlStr = "{$baseUrl}/plugins/pptSync/playerProductInstall.swf";
	            var flashvars = {};
	            var params = {};
	            params.quality = "high";
	            params.bgcolor = "#ffffff";
	            params.allowscriptaccess = "sameDomain";
	            params.allowfullscreen = "true";
	            var attributes = {};
	            attributes.id = "syncPpt";
	            attributes.name = "syncPpt";
	            attributes.align = "middle";
	            swfobject.embedSWF(
	                "{$baseUrl}/plugins/pptSync/syncPpt.swf", "flashContent", 
	                "757px", "480px", 
	                swfVersionStr, xiSwfUrlStr, 
	                flashvars, params, attributes);
				<!-- JavaScript enabled so display the flashContent div in case it is not replaced with a swf object. -->
				swfobject.createCSS("#flashContent", "display:block;text-align:left;");
	        </script>        
	        <div id="flashContent" style="display: none;">

	        </div>