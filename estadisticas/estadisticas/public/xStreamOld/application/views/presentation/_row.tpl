_row.tpl

<tr>
    <td>{$presentation->getIdPresentation()}</td>
    <td>{$presentation->getName()}</td>
    <td>{$presentation->getStatus()}</td>
    <td><a href="{url action=edit idPresentation=$presentation->getIdPresentation()}">{icon src=pencil class=tip title=$i18n->_('Edit')}</a></td>
    <td><a href="{url action=delete idPresentation=$presentation->getIdPresentation()}" class="confirm">{icon src=delete class=tip title=$i18n->_('Delete')}</a></td>
</tr>
