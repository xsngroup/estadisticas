<form action="{url action=create}" method="post" class="validate ajaxForm">
<table class="center">
    <caption>{$i18n->_('CustomeTheme')}</caption>
    <tfoot>
        <tr>
            <td colspan="2"><input type="submit" value="{$i18n->_('Save')}" /></td>
        </tr>
    </tfoot>
    <tbody>
        {include file='customeTheme/Form.tpl'}
    </tbody>
</table>
</form>
<hr/>


<table class="center">
    <caption>{$i18n->_('List')}</caption>
    <thead>
        <tr>
            <td>{$i18n->_('IdCustomeTheme')}</td>
            <td>{$i18n->_('UrlLogo')}</td>
            <td>{$i18n->_('UrlBanner')}</td>
            <td>{$i18n->_('ColorTheme')}</td>
            <td>{$i18n->_('TypeGallery')}</td>
            <td>{$i18n->_('IdCompany')}</td>
            <td colspan="2">{$i18n->_('Actions')}</td>
        </tr>
    </thead>
    <tbody id="ajaxList">
        {foreach $customeThemes as $customeTheme}
            <tr class="{$customeTheme@iteration|odd}">
                <td>{$customeTheme->getIdCustomeTheme()}</td>
                <td>{$customeTheme->getUrlLogo()}</td>
                <td>{$customeTheme->getUrlBanner()}</td>
                <td>{$customeTheme->getColorTheme()}</td>
                <td>{$customeTheme->getTypeGallery()}</td>
                <td>{$customeTheme->getIdCompany()}</td>
                <td><a href="{url action=edit idCustomeTheme=$customeTheme->getIdCustomeTheme()}">{icon src=pencil class=tip title=$i18n->_('Edit')}</a></td>
                <td><a href="{url action=delete idCustomeTheme=$customeTheme->getIdCustomeTheme()}" class="confirm">{icon src=delete class=tip title=$i18n->_('Delete')}</a></td>
            </tr>
        {/foreach}
    </tbody>
</table>

