_row.tpl

<tr>
    <td>{$customeTheme->getIdCustomeTheme()}</td>
    <td>{$customeTheme->getUrlLogo()}</td>
    <td>{$customeTheme->getUrlBanner()}</td>
    <td>{$customeTheme->getColorTheme()}</td>
    <td>{$customeTheme->getTypeGallery()}</td>
    <td>{$customeTheme->getIdCompany()}</td>
    <td><a href="{url action=edit idCustomeTheme=$customeTheme->getIdCustomeTheme()}">{icon src=pencil class=tip title=$i18n->_('Edit')}</a></td>
    <td><a href="{url action=delete idCustomeTheme=$customeTheme->getIdCustomeTheme()}" class="confirm">{icon src=delete class=tip title=$i18n->_('Delete')}</a></td>
</tr>
