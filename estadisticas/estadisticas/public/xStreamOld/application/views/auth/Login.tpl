<script type="text/javascript" src="{$baseUrl}/js/jquery/vanadium.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/modules/auth/login.js"></script>
<link rel="stylesheet" href="{$baseUrl}/css//style.css" type="text/css" />
<style>
#sidebar {
float: right;
position: relative;
font-family: Arial;
font-size:12px;
}
.sidebar-home { 
width: 300px;
}
.sidebar-home h3 {
background: url({$baseUrl}/images/template/theme/gray/widget_header_large.png) no-repeat;
width:302px;
}
#sidebar h3 {
float: left;
height: 21px;
padding: 8px 0 0 10px;
font-size: 13px;
margin:0 0 0 0;
}

.widget {
border-left: 1px solid #eee;
border-right: 1px solid #eee;
border-bottom: 1px solid #eee;
background-color: #F5F4F4;
margin-top:20px;
}
.widget > div {
padding: 15px;
}

.textwidget {
text-align: center;
line-height: 19px;

}

 input, textarea, select, input[type="submit"], input[type="reset"], .widget .item-avatar img {
background: #f7f7f7 url(../../images/template/theme/gray/gradient.png) repeat-x;
border: 1px solid #d6d6d6;
}
input[type="submit"], input[type="reset"] {
color: #808080;
}

input[type="button"]:hover, input[type="submit"]:hover, input[type="reset"]:hover {
background: #d6d6d6;
}

input, textarea, select {
    color: #808080;
}
input, textarea, select {
    -moz-border-radius: 5px 5px 5px 5px;
    color: #808080;
    font-family: Arial;
    font-size: 13px;
    padding: 5px;
}
</style>
<div id="loginFormContainer">
<div id="sidebar" class="sidebar-home">
 <h3>{$l10n->_('Acceso a streaming')}</h3>
 <div class="widget">
  <div class="textwidget">
	{if $errorMessage}
		<div class="error">{$errorMessage}</div>
	{/if}
	<input type="radio" name="typeLogin" class="typeLogin" value="1" checked="checked" /><label>Visitante</label>
	<input type="radio" name="typeLogin" class="typeLogin" value="2" /><label>Administrador</label><br/>
	<div id="loadVisit">
	<form action="{$baseUrl}/auth/login-temp" method="post" id="loginForm">
	<!-- 
	<p>
		<label for="username">{$l10n->_('Usuario')}:</label><br/>
		<input type="text" name="username" id="username" value="{$post.username}"/>
	</p>
	-->
	<p>
		<label for="username">{$l10n->_('C�digo de Cliente')}:</label><br/>
		<input type="text" name="code" id="code" value="{$post.username}"/>
	</p>	
	<p>
		<label for="password">{$l10n->_('Contrase�a')}:</label><br/>
		<input type="password" name="password" id="password" value=""/>
	</p>
	<p>
		<label for="password">{$l10n->_('Nombre')}:</label><br/>
		<input type="text" name="name" id="name" value=""/>
	</p>
	<p>
		<label for="password">{$l10n->_('Email')}:</label><br/>
		<input type="text" name="email" id="email" value="" class="required"/>
	</p>
	<p>
	   <label>{$l10n->_('No. Asistentes')}:</label>
	   <select name="viewers" id="viewers">
	     {foreach $numberviewers as $number}
	     <option value="{$number}">{$number}</option>
	     {/foreach}
	   </select>
	</p>	
	<p>
		<input type="submit" value="{$l10n->_('Iniciar Sesi�n')}"/>
	</p>
	</form>
	</div>
	<div id="loadAdmin" style="display:none;">
	<form action="{$baseUrl}/auth/login" method="post" id="loginForm">
	<p>
		<label for="username">{$l10n->_('Usuario')}:</label><br/>
		<input type="text" name="username" id="username" value="{$post.username}"/>
	</p>
	<p>
		<label for="password">{$l10n->_('Contrase�a')}:</label><br/>
		<input type="password" name="password" id="password" value=""/>
	</p>
	<p>
		<input type="submit" value="{$l10n->_('Iniciar Sesi�n')}"/>
	</p>
	</form>
	</div>	
  </div>
</div>
</div>
</div>