<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/> 
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	<title>{$systemTitle} | {$contentTitle}</title>
 
 
	<link rel="stylesheet" href="{$baseUrl}/css/{$colorTheme}/reset.css" type="text/css" />	
	<link rel="stylesheet" href="{$baseUrl}/css/{$colorTheme}/style.css" type="text/css" />
	<link rel="stylesheet" href="{$baseUrl}/css/{$colorTheme}/prettyPhoto.css" type="text/css" />    
    {if $colorTheme=='gray'}
	<link rel="stylesheet" href="{$baseUrl}/css/{$colorTheme}/style-light.css" type="text/css" />	
	{/if}
	<link rel="stylesheet" href="{$baseUrl}/css/style.css" type="text/css" />	
	<!--[if IE]><link href="{$baseUrl}/css/style-ie.css" rel="stylesheet" type="text/css" /><![endif]-->
	{include file="layout/Scripts.tpl"}
<style type="text/css">
      
      .slide-out-div {
          padding: 20px;
          width: 360px;
         
		  z-index:100;
		  background:#EAEAEA;
      }      
	   .handle {
         
          width: 100%;
   
      }  
      </style>
      
	    
<script src="http://tab-slide-out.googlecode.com/files/jquery.tabSlideOut.v1.3.js"></script>

    <script type="text/javascript">
    $(function(){
        $('.slide-out-div').tabSlideOut({
            tabHandle: '.handle',                     //class of the element that will become your tab
            pathToTabImage: 'images/contact_tab.gif', //path to the image for the tab //Optionally can be set using css
            imageHeight: '24px',                     //height of tab image           //Optionally can be set using css
            imageWidth: '400px',                       //width of tab image            //Optionally can be set using css
            tabLocation: 'bottom',                      //side of screen where tab lives, top, right, bottom, or left
            speed: 300,                               //speed of animation
            action: 'click',                          //options: 'click' or 'hover', action to trigger animation
            topPos: '200px',                          //position from the top/ use if tabLocation is left or right
            leftPos: '20px',                          //position from left/ use if tabLocation is bottom or top
            fixedPosition: false                      //options: true makes it stick(fixed position) on scroll
        });

    });

    </script>
	
</head>
<body>

  <div class="slide-out-div">
            <a class="handle" href="http://link-for-non-js-users.html">Content</a>
            <link href="http://plantilla.xsn.com.mx/plugins/chatXsnMini/history/history.css" type="text/css" rel="stylesheet">
       <object width="347" height="235" align="middle" type="application/x-shockwave-flash" id="chatXsnMini" name="chatXsnMini" data="http://plantilla.xsn.com.mx/plugins/chatXsnMini/gray/Main.swf"><param name="wmode" value="transparent"><param name="quality" value="high"><param name="bgcolor" value="#ffffff"><param name="allowscriptaccess" value="sameDomain"><param name="allowfullscreen" value="true"></object>
        </div>
		
<div class="contentHelp" id="contentHelp" style="display:none;">
<p><b>Si presentas algun problema para ver correctamente el evento prueba lo siguiente:</b><a href="javascript:void(0);" id="closeHelp">{icon src=close class=closeHelp title=Cerrar}</a></p>
<ul>
  <li>- El sitio se visualiza correctamente en: <b>IE 8+</b>, <b>Firefox 3.6+</b>, <b>Chrome 11+</b>.</li>
  <li>- Tener instalado el plugin de Flash Player 10.2 � superior.</li>
  <li>- Tener una conexi�n a internet m�nima de 256Kbps.</li>
  <li>- Si no tienes instalado el Flash Player autom�ticamente se te solicitar� instalarlo.</li>  
  <li>- Dependiendo del navegador puede variar la forma de instalaci�n del Flash Player.</li>
  <li>- P�gina oficinal de Flash Player: <b><a href="http://get.adobe.com/es/flashplayer/">http://get.adobe.com/es/flashplayer/</a></b></li>
  <li>- Si tu conexi�n de internet falla, se puede detener el flujo de la trasmisi�n � diapositivas.</li>
  <li>- Si presentas problemas para ver la transmisi�n puedes recargar la p�gina completamente.</li>
  <li>- Debes tener habilitado el javascript en tu navegador para visualizar correctamente el sitio.</li>
</ul>
</div>
<div id="contentGral">
<a href="#top_arrow"></a> 
<!--Begin Page Wrapper-->

<div id="page-wrapper">
  {if $flag!=1}
  <div id="login">
     <div>{if $systemUser}<a href="#">{$l10n->_('Bienvenido')} <strong>{$systemUser->getUsername()}</strong> ({$systemAccessRole->getName()})</a> <span>|</span>   <a href="javascript:void(0);" class="linkHelp" id="openHelp" title="Ayuda">Ayuda{icon src=hblue class=help title=Ayuda}</a>  
      <span> | </span><a href="{$baseUrl}/auth/logout" title="Cerrar sesi�n">{$l10n->_('Cerrar Sesi�n')}{icon src=logout class=help title=Ayuda}</a>{/if}</div>
  </div>	
  <div class="clear"></div>
  {else}
  <div id="login">
     <div>
        <a href="javascript:void(0);" class="linkHelp" id="openHelp" title="Ayuda">Ayuda{icon src=hblue class=help title=Ayuda}</a>  
     </div>
  </div>	
  <div class="clear"></div>  
  {/if}
<!--End Page Wrapper-->

<!--Begin Header Top-->
{if $systemUser}
<script type="text/javascript">
var idClientG="{$company->getIdCompany()}";
var nameClient="{$company->getNameCompany()}";
var idPpt="{$idPpt}";
var nameSignalDesktop="{$nameSignalDesktop}";
//var namePpt="{$namePpt}";
//var numPpt="{$numPpt}";
var idStreamG="{$idStreamPpt}";
var idCustomThemeG="{$customTheme->getIdCustomeTheme()}";
</script>
{if $rolUser=="Administrador" || $customTheme->getTypeGallery()==2}
<style type="text/css">
    #slider {
	  height: auto!important;
    }
</style>
{/if}
<div id="header-top">

	<a href=""> 
	<div id="logo" class="logo-default" style="background: url('{$baseUrl}/files/logos/{$logo}') no-repeat  !important;"></div>
	</a>
    
 
  <div id="nav">
  <img src="{$baseUrl}/files/banners/fullBanners/{$bannerFull}" alt="" />
  <!-- {include file='layout/Menu.tpl'} -->
  </div>
</div>
{/if}
<!--End Header Top-->
<div id="loadDisplay">
{if $systemUser}

<!--Begin Slider-->
<div id="slider-top"></div>
<div class="nivoSlider" style="position: relative; background:#000 ;" id="slider">		
<div {if $rolUser=="Administrador" && $customTheme->getTypeGallery()==2}
  class="nivo-controlNav redi2"
{else}
  {if $customTheme->getTypeGallery()==2}
     class="nivo-controlNav redi3"
  {else}
     class="nivo-controlNav"
  {/if}   
{/if} 
   id="nivoLoad">
	{if $customTheme->getTypeGallery()==1}
		{assign var="indicador" value=0}
		<div class="previewStream">
		{foreach $streamLive as $item}
		{if $indicador<=2}
		     {if $company->getIdCompany()==52 || $company->getIdCompany()==55  || $company->getIdCompany()==63}
				<a class="nivo-control" alt="0" onclick="loadVideo('rtmplive','{$item->getName()}');">
			 {else}
				<a class="nivo-control" alt="0" onclick="loadVideo('live','{$item->getName()}');">			 
			 {/if} 			
				<!-- <a class="nivo-control" alt="0" onclick="loadVideo('live','{$item->getName()}');"> -->
					<img src="{$baseUrl}/files/galleryLive/live.png" alt="">
					<div class="slider-thumb-caption">
					<strong>{$item->getTitle()}</strong>
					<p>
					  {$item->getDescription()}
					</p>
					</div>
				</a>
		{$indicador=$indicador+1}
		{/if}
		{foreachelse}
		<!-- <center style="padding-top:5px;"><span >No hay contenido para mostrar</span></center> -->
			{include file="ajax-event/listVodStream.tpl"}
		{/foreach}
		</div>
		<div class="miniBanner"><img src="{$baseUrl}/files/banners/miniBanners/{$bannerMini}" class="bannerImage"/></div>		
	{/if}
	{if $customTheme->getTypeGallery()==2 & !is_null($nameSignalDesktop)}	
		 <!-- { html_entity_decode( $ppt->getName() ) } -->
		 <!-- {$rolUser} -->
		    {assign var="indicadorX" value=0}
		 	{foreach $streamLive as $item}
				{if $indicadorX==0}
					<input type="hidden" id="idStr" value="{$item->getIdStream()}"></input>
					{$indicadorX=$indicadorX+1}
				{/if}
			{/foreach}
		 {if $rolUser=="Administrador"}
		    <div class="previewStream">
		    {foreach $ppts as $ppt}
					<a class="nivo-control" alt="0" onclick="">
						<img src="{$baseUrl}/files/galleryLive/ppt.png" alt="" style="padding-right: 0px !important; width: auto !important;">
						<div class="slider-thumb-caption">
						<strong>{$ppt->getTitle()}</strong>
						<p class="namePpt" id="namePpt-{$ppt->getIdPpt()}-{$ppt2jpg[$ppt->getIdPpt()]}">
						  {$ppt->getName()}
						</p>
						</div>
					</a>	    
		    {foreachelse}
		     <center style="padding-top:5px;"><span >No hay presentaciones para mostrar</span></center>
		    {/foreach}
			</div>
		 {else}
		 <!-- 
          {foreach $ppts as $ppt}
          {html_entity_decode($ppt->getCode())}		
		    {foreachelse}
		     <center style="padding-top:5px;"><span >No hay presentaciones para mostrar</span></center>
		    {/foreach}     
          -->
				{assign var="indicador" value=0}
				{assign var="tituloEvt" value=""}
				{assign var="descriEvt" value=""}
					<div id="loadVideoPlayer">
					{foreach $streamLive as $item}
					{if $indicador==0}
					{$tituloEvt=$item->getTitle()}
					{$descriEvt=$item->getDescription()}
					<input type="hidden" id="idStr" value="{$item->getIdStream()}"></input>
					<script type='text/javascript' src='{$baseUrl}/js/swfobject.js'></script>
					<div id="mediaspace"  style="padding-bottom: 20px;"></div>
					<script type='text/javascript'>
										var so = new SWFObject('{$baseUrl}/files/player.swf','ply','333','244','9');
											so.addParam('allowfullscreen','false');
											so.addParam('allowscriptaccess','always');
											so.addParam('wmode','opaque');
											so.addVariable('file','{$item->getName()}');
											//so.addVariable('file','970');
											//so.addVariable('streamer','rtmp://{$ipServer}/live/');
											so.addVariable('streamer','rtmp://{$server}/live/');
											so.addVariable("autostart","true");
											so.write('mediaspace');
					
					</script>
					{$indicador=$indicador+1}
					{/if}
					{/foreach}
					</div>	
					<br/>
					<br/>
					<!-- 
					<p>
					<label style="padding-left:5px;font-weight: bold;">Fecha:</label>{}
					</p>
					-->     		
					<p>
					<label style="padding-left:5px;font-weight: bold;">Titulo:</label>{$tituloEvt}
					</p>
					<p>
					<label style="padding-left:5px;font-weight: bold;">Descripci&oacute;n:</label>{$descriEvt}
					</p>
		 {/if}   
	{/if}						
</div>

<!-- end sidebar gallery -->
<!-- init load video or ppts -->
{if $rolUser=="Administrador" && $customTheme->getTypeGallery()==2}
<style>
#slider {
	background:none !important;
}
</style>
<div id="pptSyncDiv">
<div class="notice" style="z-index: 900; position: absolute; width: 600px; margin-left: 6%;"><p>Si presenta problemas para seguir correctamente el evento favor de recargar la p�gina para resolverlo.{icon src=close class=close title=Cerrar}</p></div>
        <script type="text/javascript" src="{$baseUrl}/js/modules/layout/script.js"></script>
        <link rel="stylesheet" type="text/css" href="{$baseUrl}/plugins/pptSync/history/history.css" />
        <script type="text/javascript" src="{$baseUrl}/plugins/pptSync/history/history.js"></script>
        <!-- END Browser History required section -->  
		    
        <script type="text/javascript" src="{$baseUrl}/plugins/pptSync/swfobject.js"></script>
	    <script type="text/javascript">
	            <!-- For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. --> 
	            var swfVersionStr = "10.0.0";
	            <!-- To use express install, set to playerProductInstall.swf, otherwise the empty string. -->
	            var xiSwfUrlStr = "{$baseUrl}/plugins/pptSync/playerProductInstall.swf";
	            var flashvars = {};
	            var params = {};
	            params.wmode="transparent";
	            params.quality = "high";
	            params.bgcolor = "#ffffff";
	            params.allowscriptaccess = "sameDomain";
	            params.allowfullscreen = "true";
	            var attributes = {};
	            attributes.id = "syncPpt";
	            attributes.name = "syncPpt";
	            attributes.align = "middle";
	            swfobject.embedSWF(
	                "{$baseUrl}/plugins/pptSync/syncPpt.swf", "flashContent", 
	                "757px", "480px", 
	                swfVersionStr, xiSwfUrlStr, 
	                flashvars, params, attributes);
				<!-- JavaScript enabled so display the flashContent div in case it is not replaced with a swf object. -->
				swfobject.createCSS("#flashContent", "display:block;text-align:left;");
	        </script>        
	        <div id="flashContent" style="display: none;">
	        	<p>
		        	To view this page ensure that Adobe Flash Player version 
					10.0.0 or greater is installed. 
				</p>
				<script type="text/javascript"> 
					var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://"); 
					document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
									+ pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
				</script> 
	        </div>	
</div>	                
{else}
{if $customTheme->getTypeGallery()==2}
<style>
#slider {
	background:none !important;
}
</style>
<div id="pptClientDiv">
<div class="notice" style="z-index: 900; position: absolute; width: 600px; margin-left: 4px;"><p>Si presenta problemas para seguir correctamente el evento favor de recargar la p�gina para resolverlo.{icon src=close class=close title=Cerrar}</p></div>
		        <script type="text/javascript" src="{$baseUrl}/js/modules/layout/script.js"></script>
		        <link rel="stylesheet" type="text/css" href="{$baseUrl}/plugins/desktopShared/history/history.css" />
		        <script type="text/javascript" src="{$baseUrl}/plugins/desktopShared/history/history.js"></script>
		        <!-- END Browser History required section -->  
				    
		        <script type="text/javascript" src="{$baseUrl}/plugins/desktopShared/swfobject.js"></script>
			    <script type="text/javascript">
			            <!-- For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. --> 
			            var swfVersionStr = "10.0.0";
			            <!-- To use express install, set to playerProductInstall.swf, otherwise the empty string. -->
			            var xiSwfUrlStr = "{$baseUrl}/plugins/desktopShared/playerProductInstall.swf";
			            var flashvars = {};
			            var params = {};
			            params.wmode="transparent";
			            params.quality = "high";
			            params.bgcolor = "#ffffff";
			            params.allowscriptaccess = "sameDomain";
			            params.allowfullscreen = "true";
			            var attributes = {};
			            attributes.id = "desktopShared";
			            attributes.name = "desktopShared";
			            attributes.align = "middle";
			            swfobject.embedSWF(
			                "{$baseUrl}/plugins/desktopShared/desktopSharedClient.swf", "flashContent", 
			                "640px", "480px", 
			                swfVersionStr, xiSwfUrlStr, 
			                flashvars, params, attributes);
						<!-- JavaScript enabled so display the flashContent div in case it is not replaced with a swf object. -->
						swfobject.createCSS("#flashContent", "display:block;text-align:left;");
			        </script>        
			        <div id="flashContent" style="display: none;">
			        	<p>
				        	To view this page ensure that Adobe Flash Player version 
							10.0.0 or greater is installed. 
						</p>
						<script type="text/javascript"> 
							var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://"); 
							document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
											+ pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
						</script> 
			        </div>     		
     		</div>
{else}     		
   {assign var="indicador" value=0}
	<div id="loadVideoPlayer">
	<!-- parche para el eventito de ciudadano 3.0 -->
	{if $company->getIdCompany()!=17}
	{foreach $streamLive as $item}
	{if $indicador==0}
	<input type="hidden" id="idStr" value="{$item->getIdStream()}"></input>
	<a href="#" class="video-link">Live Stream: {$item->getTitle()}</a>
	<script type='text/javascript' src='{$baseUrl}/js/swfobject.js'></script>
	<div id='mediaspace'></div>
	<script type='text/javascript'>
						var so = new SWFObject('{$baseUrl}/files/player.swf','ply','630','344','9','#');
							so.addParam('allowfullscreen','true');
							so.addParam('allowscriptaccess','always');
							so.addParam('wmode','opaque');
							{if $company->getIdCompany()==52 || $company->getIdCompany()==55|| $company->getIdCompany()==26|| $company->getIdCompany()==63}
							so.addVariable('file','{$item->getName()}');
							//so.addVariable('file','970');
							//so.addVariable('streamer','rtmp://{$ipServer}/live/');
							so.addVariable('streamer','rtmp://{$server}:1936/rtmplive/');							
							{else}
							so.addVariable('file','{$item->getName()}');
							//so.addVariable('file','970');
							//so.addVariable('streamer','rtmp://{$ipServer}/live/');
							so.addVariable('streamer','rtmp://{$server}/live/');
							{/if}
							so.addVariable("autostart","true");
							so.write('mediaspace');
	
	</script>
	{$indicador=$indicador+1}
	{/if}
	{/foreach}
	{else}
		<a href="#" class="video-link">Vod Stream: amipcicongreso1</a>
		<script type='text/javascript' src='{$baseUrl}/js/swfobject.js'></script>
		<div id='mediaspace'></div>
		<script type='text/javascript'>
							var so = new SWFObject('{$baseUrl}/files/player.swf','ply','630','344','9','#');
								so.addParam('allowfullscreen','true');
								so.addParam('allowscriptaccess','always');
								so.addParam('wmode','opaque');
								so.addVariable('file','amipcicongreso1');
								//so.addVariable('file','970');
								//so.addVariable('streamer','rtmp://{$ipServer}/live/');
								so.addVariable('streamer','rtmp://69.170.128.2/vod/');
								so.addVariable("autostart","true");
								so.write('mediaspace');
		
		</script>	
	{/if}
	</div>	
	 
{/if}	 
{/if}
										
</div>		
			

<!--End Slider-->	
	<!--End Slider-->
	
	<!--Begin Header Bottom-->
	<div id="header-bottom">
	
		<div id="display-options">
		<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="http://www.xsn.com.mx" layout="button_count" show_faces="false" width="100" font="arial"></fb:like>
        <a href="javascript:void(0);" id="shared"><img src="{$baseUrl}/images/template/theme/gray/share.png" alt="compartir" /></a> 
             
<!-- 
<a name="fb_share"></a> 
<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" 
        type="text/javascript">
</script>
 -->
		</div>
	
				
		<!--Begin Social Icons-->
		{if $customTheme->getTypeGallery()==1&&$company->getIdCompany()!=17}
			<div class="streamButtons">
			  <input type="button" class="live-stream" id="liveIcon"  title="Live Stream"  />
			  <input type="button" class="vod-stream" id="vodIcon"  title="Video On Demand Stream"  />		
		 	</div>	
		{/if} 	
		<div id="social-icons">
		{if $customTheme->getTypeGallery()==1}
			<!--                 		                
			<a href="javascript:void(0);" id="searchIcon" title="Search"><img src="{$baseUrl}/images/template/theme/gray/search.png" alt="search"></a>	
			<a href="javascript:void(0);" id="chatIcon" title="Chat"><img src="{$baseUrl}/images/template/theme/gray/chat2.png" alt="Chat"></a>													
			<a href="javascript:void(0);" id="faceIcon" title="Facebook"><img src="{$baseUrl}/images/template/theme/gray/social_facebook.png" alt="Facebook"></a>			
			<a href="javascript:void(0);" id="twettIcon" title="Twitter"><img src="{$baseUrl}/images/template/theme/gray/social_twitter.png" alt="Twitter"></a>									
		    -->
		{/if} 
		{if $customTheme->getTypeGallery()==2}
			<!-- <input type="button" class="live-stream" id="liveIcon"  title="Live Stream"  /> -->
			<!-- <input type="button" class="ppt" id="liveIcon"  title="Presentaciones"  /> -->
			<!-- <input type="button" class="vod-stream" id="vodIcon"  title="Video On Demand Stream"  /> -->
			
					                		                
			<!--  <a href="javascript:void(0);" id="chatIcon" title="Chat"><img src="{$baseUrl}/images/template/theme/gray/chat2.png" alt="Chat"></a>-->													
		{/if} 
			  {foreach $buttonsChoice as $button}
			       {html_entity_decode($button)}
			  {/foreach}				              
		</div>
		<!--End Social Icons-->
	
	</div>
	<!--End Header Bottom-->
{/if}
</div>

<!-- End loadDisplay -->

<!-- begin content float -->
<div id="contentShare" style="display: none;">
  <a href="javascript:void(0);" id="closeShare" class="icon-right">{icon src=close class=tip title=$l10n->_('Cerrar')}</a>
  <h6 class="shareH">Comparte este sitio en tus redes sociales:</h6>
<div>
<div style="float: left;  margin-left: 18px;">
 <a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
</div>
<div style="float:right;  margin-right: 30px;"> 
<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like layout="button_count" show_faces="true" width="80" action="recommend"></fb:like>
</div>

</div>

</div>
<!-- end content float -->

<!--Begin Content Wrapper-->
<div id="content-wrapper">
   <!--  <div id="baseUrl">{if $systemUser}<div class="subHeader"><div><em>{$systemTitle} &raquo; {$contentTitle}</em></div></div>{/if}</div><br/> -->
  
  <div id="contentPanel" class="contentPanel">
          	     {include file="layout/Messages.tpl"}
          	     {$contentPlaceHolder}  
  </div>
</div>
<!--End Content Wrapper-->	

<div class="clear"></div>
{include file="layout/Modals.tpl"}
{if $systemUser}
<!--Begin Footer Widgets-->
<!-- 
<div id="footer-widgets">
			
			
					<div class="footer-widget-outer footer-fourth">
				<div class="footer-widget-inner" id="categories-5"><h3>Categories</h3>		<ul>
	<li class="cat-item cat-item-17"><a title="View all posts filed under Category 1" href="http://ghostpool.com/wordpress/reviewit/category/category-1/">Category 1</a>
</li>
	<li class="cat-item cat-item-4"><a title="View all posts filed under Category 2" href="http://ghostpool.com/wordpress/reviewit/category/category-2/">Category 2</a>
</li>
	<li class="cat-item cat-item-5"><a title="View all posts filed under Category 3" href="http://ghostpool.com/wordpress/reviewit/category/category-3/">Category 3</a>
</li>
	<li class="cat-item cat-item-6"><a title="View all posts filed under Category 4" href="http://ghostpool.com/wordpress/reviewit/category/category-4/">Category 4</a>
</li>
		</ul>
</div>			</div>
			
					<div class="footer-widget-outer footer-fourth">
				<div class="footer-widget-inner" id="meta-3"><h3>Meta</h3>			<ul>
						<li><a href="http://ghostpool.com/wordpress/reviewit/wp-login.php">Log in</a></li>
			<li><a title="Syndicate this site using RSS 2.0" href="http://ghostpool.com/wordpress/reviewit/feed/">Entries <abbr title="Really Simple Syndication">RSS</abbr></a></li>
			<li><a title="The latest comments to all posts in RSS" href="http://ghostpool.com/wordpress/reviewit/comments/feed/">Comments <abbr title="Really Simple Syndication">RSS</abbr></a></li>
			<li><a title="Powered by WordPress, state-of-the-art semantic personal publishing platform." href="http://wordpress.org/">WordPress.org</a></li>
						</ul>
</div>			</div>
				
					<div class="footer-widget-outer footer-fourth">
				<div class="footer-widget-inner" id="archives-3"><h3>Archives</h3>		<ul>
			<li><a title="June 2010" href="http://ghostpool.com/wordpress/reviewit/2010/06/">June 2010</a></li>
	<li><a title="March 2010" href="http://ghostpool.com/wordpress/reviewit/2010/03/">March 2010</a></li>
	<li><a title="January 2010" href="http://ghostpool.com/wordpress/reviewit/2010/01/">January 2010</a></li>
		</ul>
</div>			</div>
				
					<div class="footer-widget-outer footer-fourth">
				<div class="footer-widget-inner" id="tag_cloud-3"><h3>Tags</h3><div><a style="font-size: 8pt;" title="1 topic" class="tag-link-7" href="http://ghostpool.com/wordpress/reviewit/tag/images1/">images1</a>
<a style="font-size: 18.5pt;" title="3 topics" class="tag-link-8" href="http://ghostpool.com/wordpress/reviewit/tag/news1/">news1</a>
<a style="font-size: 22pt;" title="4 topics" class="tag-link-9" href="http://ghostpool.com/wordpress/reviewit/tag/news2/">news2</a>
<a style="font-size: 14.3pt;" title="2 topics" class="tag-link-10" href="http://ghostpool.com/wordpress/reviewit/tag/news3/">news3</a>
<a style="font-size: 14.3pt;" title="2 topics" class="tag-link-11" href="http://ghostpool.com/wordpress/reviewit/tag/preview1/">preview1</a>
<a style="font-size: 14.3pt;" title="2 topics" class="tag-link-12" href="http://ghostpool.com/wordpress/reviewit/tag/preview2/">preview2</a>
<a style="font-size: 8pt;" title="1 topic" class="tag-link-13" href="http://ghostpool.com/wordpress/reviewit/tag/preview3/">preview3</a>
<a style="font-size: 8pt;" title="1 topic" class="tag-link-14" href="http://ghostpool.com/wordpress/reviewit/tag/videos1/">videos1</a></div>
</div>			</div>
				
	</div>
	-->
<!--End Footer Widgets-->


<div class="clear"></div>

<!--Begin Footer-->
<div id="footer">

	<div id="footer-content">

	  <div id="copyright">
		Copyright � 2010. Designed and coded by Xsn Group.	
	  </div>	
	</div>
	
	<a href="#" id="top_arrow"></a>		
</div>
{/if}
<!--End Footer-->
</div>
<!--End Page Wrap-->
</div>
</body>
</html>
