<?php
/**
 * Xsn
 *
 * upload video to converter multiple formats
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-2011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */


require_once "lib/utils/Parser.php";

/**
 * Clase CompanyLogCollection que representa una collección de objetos CompanyLog
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_collections
 * @copyright  Copyright (c) 2010-2011 Xsn Group (http://www.xsn.com.mx)
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     zetta & chentepixtol
 * @version    1.0.2 SVN: $Revision$
 */
class CompanyLogCollection extends ArrayIterator
{

    /**
     * @var Parser
     */
    private $parser;
    
    /**
     * Constructor
     * @param array $array
     * @return void
     */
    public function __construct($array = array())
    {
        $this->parser = new Parser('CompanyLog');
        parent::__construct($array);
    }

    /**
     * Appends the value
     * @param CompanyLog $companyLog
     */
    public function append($companyLog)
    {
        parent::offsetSet($companyLog->getIdCompanyLog(), $companyLog);
        $this->rewind();
    }

    /**
     * Return current array entry
     * @return CompanyLog
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * Return current array entry and 
     * move to next entry
     * @return CompanyLog 
     */
    public function read()
    {
        $companyLog = $this->current();
        $this->next();
        return $companyLog;
    }

    /**
     * Get the first array entry
     * if exists or null if not 
     * @return CompanyLog|null 
     */
    public function getOne()
    {
        if ($this->count() > 0)
        {
            $this->seek(0);
            return $this->current();
        } else
            return null;
    }
    
    /**
     * Contains one object with $idCompanyLog
     * @param  int $idCompanyLog
     * @return boolean
     */
    public function contains($idCompanyLog)
    {
        return parent::offsetExists($idCompanyLog);
    }
    
    /**
     * Remove one object with $idCompanyLog
     * @param  int $idCompanyLog
     */
    public function remove($idCompanyLog)
    {
        if( $this->contains($idCompanyLog) )
            $this->offsetUnset($idCompanyLog);
    }
    
    /**
     * Merge two Collections
     * @param CompanyLogCollection $companyLogCollection
     * @return void
     */
    public function merge(CompanyLogCollection $companyLogCollection)
    {
        $companyLogCollection->rewind();
        while($companyLogCollection->valid())
        {
            $companyLog = $companyLogCollection->read();
            if( !$this->contains( $companyLog->getIdCompanyLog() ) )
                $this->append($companyLog);
        }
        $companyLogCollection->rewind();
    }
    
    /**
     * Diff two Collections
     * @param CompanyLogCollection $companyLogCollection
     * @return void
     */
    public function diff(CompanyLogCollection $companyLogCollection)
    {
        $companyLogCollection->rewind();
        while($companyLogCollection->valid())
        {
            $companyLog = $companyLogCollection->read();
            if( $this->contains( $companyLog->getIdCompanyLog() ) )
                $this->remove($companyLog->getIdCompanyLog());     
        }
        $companyLogCollection->rewind();
    }
    
    /**
     * Intersect two Collections
     * @param CompanyLogCollection $companyLogCollection
     * @return CompanyLogCollection
     */
    public function intersect(CompanyLogCollection $companyLogCollection)
    {
        $newcompanyLogCollection = CompanyLogCollection();
        $companyLogCollection->rewind();
        while($companyLogCollection->valid())
        {
            $companyLog = $companyLogCollection->read();
            if( $this->contains( $companyLog->getIdCompanyLog() ) )
                $newcompanyLogCollection->append($companyLog);
        }
        $companyLogCollection->rewind();
        return $newcompanyLogCollection;
    }
    
    /**
     * Retrieve the array with primary keys 
     * @return array
     */
    public function getPrimaryKeys()
    {
        return array_keys($this->getArrayCopy());
    }
    
    /**
     * Retrieve the CompanyLog with primary key  
     * @param  int $idCompanyLog
     * @return CompanyLog
     */
    public function getByPK($idCompanyLog)
    {
        return $this->contains($idCompanyLog) ? $this[$idCompanyLog] : null;
    }
  
    /**
     * Transforma una collection a un array
     * @return array
     */
    public function toArray()
    {
        $array = array();
        while ($this->valid())
        {
            $companyLog = $this->read();
            $this->parser->changeBean($companyLog);
            $array[$companyLog->getIdCompanyLog()] = $this->parser->toArray();
        }
        $this->rewind();
        return $array;
    }
    
    /**
     * Crea un array asociativo de $key => $value a partir de las constantes de un bean
     * @param string $ckey
     * @param string $cvalue
     * @return array
     */
    public function toKeyValueArray($ckey, $cvalue)
    {
        $array = array();
        while ($this->valid())
        {
            $companyLog = $this->read();
            $this->parser->changeBean($companyLog);
            $array += $this->parser->toKeyValueArray($ckey, $cvalue);
        }
        $this->rewind();
        return $array;
    }
    
    /**
     * Retrieve the parser object
     * @return Parser
     */
    public function getParser()
    {
        return $this->parser;
    }
    
    /**
     * Is Empty
     * @return boolean
     */
    public function isEmpty()
    {
        return $this->count() == 0;
    }
  
  
}

