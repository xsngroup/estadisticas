<?php
/**
 * Xsn
 *
 * Xsn
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */

/**
 * Dependences
 */
require_once "lib/db/Catalog.php";
require_once "application/models/beans/Logo.php";
require_once "application/models/exceptions/LogoException.php";
require_once "application/models/collections/LogoCollection.php";
require_once "application/models/factories/LogoFactory.php";

/**
 * Singleton LogoCatalog Class
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_catalogs
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     zetta & chentepixtol
 * @version    1.0.2 SVN: $Revision$
 */
class LogoCatalog extends Catalog
{

    /**
     * Singleton Instance
     * @var LogoCatalog
     */
    static protected $instance = null;


    /**
     * Método para obtener la instancia del catálogo
     * @return LogoCatalog
     */
    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
          self::$instance = new self();
        }
        return self::$instance;
    }
    
    /**
     * Constructor de la clase LogoCatalog
     * @return LogoCatalog
     */
    protected function LogoCatalog()
    {
        parent::Catalog();
    }

    /**
     * Metodo para agregar un Logo a la base de datos
     * @param Logo $logo Objeto Logo
     */
    public function create($logo)
    {
        if(!($logo instanceof Logo))
            throw new LogoException("passed parameter isn't a Logo instance");
        try
        {
            $data = array(
                'name' => $logo->getName(),
            );
            $data = array_filter($data, 'Catalog::notNull');
            $this->db->insert(Logo::TABLENAME, $data);
            $logo->setIdLogo($this->db->lastInsertId());
        }
        catch(Exception $e)
        {
            throw new LogoException("The Logo can't be saved \n" . $e->getMessage());
        }
    }

    /**
     * Metodo para Obtener los datos de un objeto por su llave primaria
     * @param int $idLogo
     * @param boolean $throw
     * @return Logo|null
     */
    public function getById($idLogo, $throw = false)
    {
        try
        {
            $criteria = new Criteria();
            $criteria->add(Logo::ID_LOGO, $idLogo, Criteria::EQUAL);
            $newLogo = $this->getByCriteria($criteria)->getOne();
        }
        catch(Exception $e)
        {
            throw new LogoException("Can't obtain the Logo \n" . $e->getMessage());
        }
        if($throw && null == $newLogo)
            throw new LogoException("The Logo at $idLogo not exists ");
        return $newLogo;
    }
    
    /**
     * Metodo para Obtener una colección de objetos por varios ids
     * @param array $ids
     * @return LogoCollection
     */
    public function getByIds(array $ids)
    {
        if(null == $ids) return new LogoCollection();
        try
        {
            $criteria = new Criteria();
            $criteria->add(Logo::ID_LOGO, $ids, Criteria::IN);
            $logoCollection = $this->getByCriteria($criteria);
        }
        catch(Exception $e)
        {
            throw new LogoException("LogoCollection can't be populated\n" . $e->getMessage());
        }
        return $logoCollection;
    }

    /**
     * Metodo para actualizar un Logo
     * @param Logo $logo 
     */
    public function update($logo)
    {
        if(!($logo instanceof Logo))
            throw new LogoException("passed parameter isn't a Logo instance");
        try
        {
            $where[] = "id_logo = '{$logo->getIdLogo()}'";
            $data = array(
                'name' => $logo->getName(),
            );
            $data = array_filter($data, 'Catalog::notNull');
            $this->db->update(Logo::TABLENAME, $data, $where);
        }
        catch(Exception $e)
        {
            throw new LogoException("The Logo can't be updated \n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para guardar un logo
     * @param Logo $logo
     */	
    public function save($logo)
    {
        if(!($logo instanceof Logo))
            throw new LogoException("passed parameter isn't a Logo instance");
        if(null != $logo->getIdLogo())
            $this->update($logo);
        else
            $this->create($logo);
    }

    /**
     * Metodo para eliminar un logo
     * @param Logo $logo
     */
    public function delete($logo)
    {
        if(!($logo instanceof Logo))
            throw new LogoException("passed parameter isn't a Logo instance");
        $this->deleteById($logo->getIdLogo());
    }

    /**
     * Metodo para eliminar un Logo a partir de su Id
     * @param int $idLogo
     */
    public function deleteById($idLogo)
    {
        try
        {
            $where = array($this->db->quoteInto('id_logo = ?', $idLogo));
            $this->db->delete(Logo::TABLENAME, $where);
        }
        catch(Exception $e)
        {
            throw new LogoException("The Logo can't be deleted\n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para eliminar varios Logo a partir de su Id
     * @param array $ids
     */
    public function deleteByIds(array $ids)
    {
        try
        {
            $criteria = new Criteria();
            $criteria->add(Logo::ID_LOGO, $ids, Criteria::IN);
            $this->db->delete(Logo::TABLENAME, array($criteria->createSql()));
        }
        catch(Exception $e)
        {
            throw new LogoException("Can't delete that\n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para Obtener todos los ids en un arreglo
     * @return array
     */
    public function retrieveAllIds()
    {
        return $this->getIdsByCriteria(new Criteria());
    }

    /**
     * Metodo para obtener todos los id de Logo por criterio
     * @param Criteria $criteria
     * @return array Array con todos los id de Logo que encajen en la busqueda
     */
    public function getIdsByCriteria(Criteria $criteria = null)
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        return $this->getCustomFieldByCriteria(Logo::ID_LOGO, $criteria);
    }

    /**
     * Metodo para obtener un campo en particular de un Logo dado un criterio
     * @param string $field
     * @param Criteria $criteria
     * @param $distinct
     * @return array Array con el campo de los objetos Logo que encajen en la busqueda
     */
    public function getCustomFieldByCriteria($field, Criteria $criteria = null, $distinct = false)
    { 
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $distinct = $distinct ? 'DISTINCT' : '';
        try
        {
            $sql = "SELECT {$distinct} {$field}
                    FROM ".Logo::TABLENAME."
                    WHERE  " . $criteria->createSql();
            $result = $this->db->fetchCol($sql);
        } catch(Zend_Db_Exception $e)
        {
            throw new LogoException("No se pudieron obtener los fields de objetos Logo\n" . $e->getMessage());
        }
        return $result;
    }

    /**
     * Metodo que regresa una coleccion de objetos Logo 
     * dependiendo del criterio establecido
     * @param Criteria $criteria
     * @return LogoCollection $logoCollection
     */
    public function getByCriteria(Criteria $criteria = null)
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $this->db->setFetchMode(Zend_Db::FETCH_ASSOC);
        try 
        {
            $sql = "SELECT * FROM ".Logo::TABLENAME."
                    WHERE " . $criteria->createSql();
            $logoCollection = new LogoCollection();
            foreach ($this->db->fetchAll($sql) as $result){
                $logoCollection->append($this->getLogoInstance($result));
            }
        }
        catch(Zend_Db_Exception $e)
        {
            throw new LogoException("Cant obtain LogoCollection\n" . $e->getMessage());
        }
        return $logoCollection;
    }
    
    /**
     * Metodo que cuenta Logo 
     * dependiendo del criterio establecido
     * @param Criteria $criteria
     * @param string $field
     * @return int $count
     */
    public function countByCriteria(Criteria $criteria = null, $field = 'id_logo')
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        try 
        {
            $sql = "SELECT COUNT( $field ) FROM ".Logo::TABLENAME."
                    WHERE " . $criteria->createSql();   
            $count = $this->db->fetchOne($sql);
        }
        catch(Zend_Db_Exception $e)
        {
            throw new LogoException("Cant obtain the count \n" . $e->getMessage());
        }
        return $count;
    }
    
    /**
     * Método que construye un objeto Logo y lo rellena con la información del rowset
     * @param array $result El arreglo que devolvió el objeto Zend_Db despues del fetch
     * @return Logo 
     */
    private function getLogoInstance($result)
    {
        return LogoFactory::createFromArray($result);
    }

    /**
     * Link a Logo to CustomeTheme
     * @param int $idLogo
     * @param int $idCustomeTheme
     * @param  $status
     */
    public function linkToCustomeTheme($idLogo, $idCustomeTheme, $status)
    {
        try
        {
            $this->unlinkAllCustomeThemeRelations($idCustomeTheme);
            $data = array(
                'id_logo' => $idLogo,
                'id_custome_theme' => $idCustomeTheme,
                'status' => $status,
            );
            $this->db->insert(Logo::TABLENAME_LOGO_CUSTOME_THEME, $data);
        }
        catch(Exception $e)
        {
            throw new LogoException("Can't link Logo to CustomeTheme\n" . $e->getMessage());
        }
    }

    /**
     * Unlink a Logo from CustomeTheme
     * @param int $idLogo
     * @param int $idCustomeTheme
     */
    public function unlinkFromCustomeTheme($idLogo, $idCustomeTheme)
    {
        try
        {
            $where = array(
                $this->db->quoteInto('id_logo = ?', $idLogo),
                $this->db->quoteInto('id_custome_theme = ?', $idCustomeTheme),
            );
            $this->db->delete(Logo::TABLENAME_LOGO_CUSTOME_THEME, $where);
        }
        catch(Exception $e)
        {
            throw new LogoException("Can't unlink Logo to CustomeTheme\n" . $e->getMessage());
        }
    }

    /**
     * Unlink all CustomeTheme relations
     * @param int $idCustomTheme
     * @param  $status
     */
    public function unlinkAllCustomeThemeRelations($idCustomTheme, $status = null)
    {
        try
        {
            $where = array(
                $this->db->quoteInto('id_custome_theme = ?', $idCustomTheme),
            );
            if(null != $status) $where[] = $this->db->quoteInto('status = ?', $status);
            //$this->db->delete(Logo::TABLENAME_LOGO_CUSTOME_THEME, $where);
            $data = array(
                'status' => Logo::$Status['Inactive'],
            );
            $data = array_filter($data, 'Catalog::notNull');
            $this->db->update(Logo::TABLENAME_LOGO_CUSTOME_THEME, $data, $where);            
            
        }
        catch(Exception $e)
        {
            throw new LogoException("Can't unlink all CustomeTheme relations \n" . $e->getMessage());
        }
    }

    /**
     * Get Logo - CustomeTheme relations by Criteria
     * @param Criteria $criteria
     * @return array
     */
    public function getLogoCustomeThemeRelations(Criteria $criteria = null)
    { 
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $this->db->setFetchMode(Zend_Db::FETCH_ASSOC);
        try
        {
           $sql = "SELECT * FROM ". Logo::TABLENAME_LOGO_CUSTOME_THEME ."
                   WHERE  " . $criteria->createSql();
           $result = $this->db->fetchAll($sql);
        } catch(Exception $e)
        {
           throw new LogoException("Can't obtain relations by criteria\n" . $e->getMessage());
        }
        return $result;
    }

    /**
     * Get LogoCollection by CustomeTheme
     * @param int $idCustomeTheme
     * @param [|array] $status
     * @return LogoCollection
     */
    public function getByCustomeTheme($idCustomeTheme, $status = null)
    {
        $criteria = new Criteria();
        $criteria->add('id_custome_theme', $idCustomeTheme, Criteria::EQUAL);
        if(null != $status) $criteria->add('status', $status, is_array($status) ? Criteria::IN : Criteria::EQUAL);
        $logoCustomeTheme = $this->getLogoCustomeThemeRelations($criteria);
        $ids = array();
        foreach($logoCustomeTheme as $rs){
            $ids[] = $rs['id_logo'];
        }
        return $this->getByIds($ids);
    }


} 
 
