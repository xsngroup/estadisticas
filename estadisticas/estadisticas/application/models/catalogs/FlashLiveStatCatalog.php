<?php
/**
 * Xsn
 *
 * Xsn Monitor Stream Servers
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-2011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */

/**
 * Dependences
 */
require_once "lib/db/Catalog.php";
require_once "application/models/beans/FlashLiveStat.php";
require_once "application/models/exceptions/FlashLiveStatException.php";
require_once "application/models/collections/FlashLiveStatCollection.php";
require_once "application/models/factories/FlashLiveStatFactory.php";

/**
 * Singleton FlashLiveStatCatalog Class
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_catalogs
 * @copyright  Copyright (c) 2010-2011 Xsn Group (http://www.xsn.com.mx)
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     zetta & chentepixtol
 * @version    1.0.2 SVN: $Revision$
 */
class FlashLiveStatCatalog extends Catalog
{

    /**
     * Singleton Instance
     * @var FlashLiveStatCatalog
     */
    static protected $instance = null;


    /**
     * Método para obtener la instancia del catálogo
     * @return FlashLiveStatCatalog
     */
    public static function getInstance($selectDb=0)
    {
        if (!isset(self::$instance))
        {
          self::$instance = new self($selectDb);
        }
        return self::$instance;
    }
    
    /**
     * Constructor de la clase FlashLiveStatCatalog
     * @return FlashLiveStatCatalog
     */
    protected function FlashLiveStatCatalog($selectDb=0)
    {
        if($selectDb<1){
    		parent::Catalog();
    	}
    	else{
    		$this->Catalog();
    	} 
    }

    /**
     * Metodo para agregar un FlashLiveStat a la base de datos
     * @param FlashLiveStat $flashLiveStat Objeto FlashLiveStat
     */
    public function create($flashLiveStat)
    {
        if(!($flashLiveStat instanceof FlashLiveStat))
            throw new FlashLiveStatException("passed parameter isn't a FlashLiveStat instance");
        try
        {
            $data = array(
                'timestamp' => $flashLiveStat->getTimestamp(),
                'publish_name' => $flashLiveStat->getPublishName(),
                'type' => $flashLiveStat->getType(),
                'client' => $flashLiveStat->getClient(),
                'publish_time' => $flashLiveStat->getPublishTime(),
                'id_live' => $flashLiveStat->getIdLive(),
            );
            $data = array_filter($data, 'Catalog::notNull');
            $this->db->insert(FlashLiveStat::TABLENAME, $data);
            $flashLiveStat->setIdLiveStat($this->db->lastInsertId());
        }
        catch(Exception $e)
        {
            throw new FlashLiveStatException("The FlashLiveStat can't be saved \n" . $e->getMessage());
        }
    }

    /**
     * Metodo para Obtener los datos de un objeto por su llave primaria
     * @param int $idLiveStat
     * @param boolean $throw
     * @return FlashLiveStat|null
     */
    public function getById($idLiveStat, $throw = false)
    {
        try
        {
            $criteria = new Criteria();
            $criteria->add(FlashLiveStat::ID_LIVE_STAT, $idLiveStat, Criteria::EQUAL);
            $newFlashLiveStat = $this->getByCriteria($criteria)->getOne();
        }
        catch(Exception $e)
        {
            throw new FlashLiveStatException("Can't obtain the FlashLiveStat \n" . $e->getMessage());
        }
        if($throw && null == $newFlashLiveStat)
            throw new FlashLiveStatException("The FlashLiveStat at $idLiveStat not exists ");
        return $newFlashLiveStat;
    }
    
    /**
     * Metodo para Obtener una colección de objetos por varios ids
     * @param array $ids
     * @return FlashLiveStatCollection
     */
    public function getByIds(array $ids)
    {
        if(null == $ids) return new FlashLiveStatCollection();
        try
        {
            $criteria = new Criteria();
            $criteria->add(FlashLiveStat::ID_LIVE_STAT, $ids, Criteria::IN);
            $flashLiveStatCollection = $this->getByCriteria($criteria);
        }
        catch(Exception $e)
        {
            throw new FlashLiveStatException("FlashLiveStatCollection can't be populated\n" . $e->getMessage());
        }
        return $flashLiveStatCollection;
    }

    /**
     * Metodo para actualizar un FlashLiveStat
     * @param FlashLiveStat $flashLiveStat 
     */
    public function update($flashLiveStat)
    {
        if(!($flashLiveStat instanceof FlashLiveStat))
            throw new FlashLiveStatException("passed parameter isn't a FlashLiveStat instance");
        try
        {
            $where[] = "id_live_stat = '{$flashLiveStat->getIdLiveStat()}'";
            $data = array(
                'timestamp' => $flashLiveStat->getTimestamp(),                
                'type' => $flashLiveStat->getType(),
                'client' => $flashLiveStat->getClient(),
                'publish_time' => $flashLiveStat->getPublishTime(),
                'connects' => $flashLiveStat->getConnects(),            
                'id_stream' => $flashLiveStat->getIdStream(),
            );
            $data = array_filter($data, 'Catalog::notNull');
            $this->db->update(FlashLiveStat::TABLENAME, $data, $where);
        }
        catch(Exception $e)
        {
            throw new FlashLiveStatException("The FlashLiveStat can't be updated \n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para guardar un flashLiveStat
     * @param FlashLiveStat $flashLiveStat
     */	
    public function save($flashLiveStat)
    {
        if(!($flashLiveStat instanceof FlashLiveStat))
            throw new FlashLiveStatException("passed parameter isn't a FlashLiveStat instance");
        if(null != $flashLiveStat->getIdLiveStat())
            $this->update($flashLiveStat);
        else
            $this->create($flashLiveStat);
    }

    /**
     * Metodo para eliminar un flashLiveStat
     * @param FlashLiveStat $flashLiveStat
     */
    public function delete($flashLiveStat)
    {
        if(!($flashLiveStat instanceof FlashLiveStat))
            throw new FlashLiveStatException("passed parameter isn't a FlashLiveStat instance");
        $this->deleteById($flashLiveStat->getIdLiveStat());
    }

    /**
     * Metodo para eliminar un FlashLiveStat a partir de su Id
     * @param int $idLiveStat
     */
    public function deleteById($idLiveStat)
    {
        try
        {
            $where = array($this->db->quoteInto('id_live_stat = ?', $idLiveStat));
            $this->db->delete(FlashLiveStat::TABLENAME, $where);
        }
        catch(Exception $e)
        {
            throw new FlashLiveStatException("The FlashLiveStat can't be deleted\n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para eliminar varios FlashLiveStat a partir de su Id
     * @param array $ids
     */
    public function deleteByIds(array $ids)
    {
        try
        {
            $criteria = new Criteria();
            $criteria->add(FlashLiveStat::ID_LIVE_STAT, $ids, Criteria::IN);
            $this->db->delete(FlashLiveStat::TABLENAME, array($criteria->createSql()));
        }
        catch(Exception $e)
        {
            throw new FlashLiveStatException("Can't delete that\n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para Obtener todos los ids en un arreglo
     * @return array
     */
    public function retrieveAllIds()
    {
        return $this->getIdsByCriteria(new Criteria());
    }

    /**
     * Metodo para obtener todos los id de FlashLiveStat por criterio
     * @param Criteria $criteria
     * @return array Array con todos los id de FlashLiveStat que encajen en la busqueda
     */
    public function getIdsByCriteria(Criteria $criteria = null)
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        return $this->getCustomFieldByCriteria(FlashLiveStat::ID_LIVE_STAT, $criteria);
    }

    /**
     * Metodo para obtener un campo en particular de un FlashLiveStat dado un criterio
     * @param string $field
     * @param Criteria $criteria
     * @param $distinct
     * @return array Array con el campo de los objetos FlashLiveStat que encajen en la busqueda
     */
    public function getCustomFieldByCriteria($field, Criteria $criteria = null, $distinct = false)
    { 
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $distinct = $distinct ? 'DISTINCT' : '';
        try
        {
            $sql = "SELECT {$distinct} {$field}
                    FROM ".FlashLiveStat::TABLENAME."
                    WHERE  " . $criteria->createSql();
            $result = $this->db->fetchCol($sql);
        } catch(Zend_Db_Exception $e)
        {
            throw new FlashLiveStatException("No se pudieron obtener los fields de objetos FlashLiveStat\n" . $e->getMessage());
        }
        return $result;
    }

    /**
     * Metodo que regresa una coleccion de objetos FlashLiveStat 
     * dependiendo del criterio establecido
     * @param Criteria $criteria
     * @return FlashLiveStatCollection $flashLiveStatCollection
     */
    public function getByCriteria(Criteria $criteria = null)
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $this->db->setFetchMode(Zend_Db::FETCH_ASSOC);
        try 
        {
            $sql = "SELECT * FROM ".FlashLiveStat::TABLENAME."
                    WHERE " . $criteria->createSql();
            $flashLiveStatCollection = new FlashLiveStatCollection();
            foreach ($this->db->fetchAll($sql) as $result){
                $flashLiveStatCollection->append($this->getFlashLiveStatInstance($result));
            }
        }
        catch(Zend_Db_Exception $e)
        {
            throw new FlashLiveStatException("Cant obtain FlashLiveStatCollection\n" . $e->getMessage());
        }
        return $flashLiveStatCollection;
    }
    
    /**
     * Metodo que cuenta FlashLiveStat 
     * dependiendo del criterio establecido
     * @param Criteria $criteria
     * @param string $field
     * @return int $count
     */
    public function countByCriteria(Criteria $criteria = null, $field = 'id_live_stat')
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        try 
        {
            $sql = "SELECT COUNT( $field ) FROM ".FlashLiveStat::TABLENAME."
                    WHERE " . $criteria->createSql();   
            $count = $this->db->fetchOne($sql);
        }
        catch(Zend_Db_Exception $e)
        {
            throw new FlashLiveStatException("Cant obtain the count \n" . $e->getMessage());
        }
        return $count;
    }
    
    /**
     * Método que construye un objeto FlashLiveStat y lo rellena con la información del rowset
     * @param array $result El arreglo que devolvió el objeto Zend_Db despues del fetch
     * @return FlashLiveStat 
     */
    private function getFlashLiveStatInstance($result)
    {
        return FlashLiveStatFactory::createFromArray($result);
    }
  
    /**
     * Obtiene un FlashLiveStatCollection  dependiendo del idLive
     * @param int $idLive  
     * @return FlashLiveStatCollection 
     */
    public function getByIdLive($idLive)
    {
        $criteria = new Criteria();
        $criteria->add(FlashLiveStat::ID_LIVE, $idLive, Criteria::EQUAL);
        $flashLiveStatCollection = $this->getByCriteria($criteria);
        return $flashLiveStatCollection;
    }

    /**
     * Obtiene un FlashLiveStatCollection  dependiendo del idStream
     * @param int $idStream  
     * @return FlashLiveStatCollection 
     */
    public function getByIdStream($idStream)
    {
        $criteria = new Criteria();
        $criteria->add(FlashLiveStat::ID_STREAM, $idStream, Criteria::EQUAL);
        $flashLiveStatCollection = $this->getByCriteria($criteria);
        return $flashLiveStatCollection;
    }

	/* (non-PHPdoc)
     * @see Catalog::Catalog()
     */
    public function Catalog ()
    {
        // TODO Auto-generated method stub
	        parent::setDatePart("YYYY-MM-dd hh:mm:ss");
	    	parent::setDb(DBAO::Database(1));         	
    }    
} 
 
