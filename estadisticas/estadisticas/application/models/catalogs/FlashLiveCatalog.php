<?php
/**
 * Xsn
 *
 * Xsn Monitor Stream Servers
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-2011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */

/**
 * Dependences
 */
require_once "lib/db/Catalog.php";
require_once "application/models/beans/FlashLive.php";
require_once "application/models/exceptions/FlashLiveException.php";
require_once "application/models/collections/FlashLiveCollection.php";
require_once "application/models/factories/FlashLiveFactory.php";

/**
 * Singleton FlashLiveCatalog Class
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_catalogs
 * @copyright  Copyright (c) 2010-2011 Xsn Group (http://www.xsn.com.mx)
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     zetta & chentepixtol
 * @version    1.0.2 SVN: $Revision$
 */
class FlashLiveCatalog extends Catalog
{

    /**
     * Singleton Instance
     * @var FlashLiveCatalog
     */
    static protected $instance = null;


    /**
     * Método para obtener la instancia del catálogo
     * @return FlashLiveCatalog
     */
    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
          self::$instance = new self();
        }
        return self::$instance;
    }
    
    /**
     * Constructor de la clase FlashLiveCatalog
     * @return FlashLiveCatalog
     */
    protected function FlashLiveCatalog()
    {
        parent::Catalog();
    }

    /**
     * Metodo para agregar un FlashLive a la base de datos
     * @param FlashLive $flashLive Objeto FlashLive
     */
    public function create($flashLive)
    {
        if(!($flashLive instanceof FlashLive))
            throw new FlashLiveException("passed parameter isn't a FlashLive instance");
        try
        {
            $data = array(
                'name' => $flashLive->getName(),
                'id_server' => $flashLive->getIdServer(),
            );
            $data = array_filter($data, 'Catalog::notNull');
            $this->db->insert(FlashLive::TABLENAME, $data);
            $flashLive->setIdLive($this->db->lastInsertId());
        }
        catch(Exception $e)
        {
            throw new FlashLiveException("The FlashLive can't be saved \n" . $e->getMessage());
        }
    }

    /**
     * Metodo para Obtener los datos de un objeto por su llave primaria
     * @param int $idLive
     * @param boolean $throw
     * @return FlashLive|null
     */
    public function getById($idLive, $throw = false)
    {
        try
        {
            $criteria = new Criteria();
            $criteria->add(FlashLive::ID_LIVE, $idLive, Criteria::EQUAL);
            $newFlashLive = $this->getByCriteria($criteria)->getOne();
        }
        catch(Exception $e)
        {
            throw new FlashLiveException("Can't obtain the FlashLive \n" . $e->getMessage());
        }
        if($throw && null == $newFlashLive)
            throw new FlashLiveException("The FlashLive at $idLive not exists ");
        return $newFlashLive;
    }
    
    /**
     * Metodo para Obtener una colección de objetos por varios ids
     * @param array $ids
     * @return FlashLiveCollection
     */
    public function getByIds(array $ids)
    {
        if(null == $ids) return new FlashLiveCollection();
        try
        {
            $criteria = new Criteria();
            $criteria->add(FlashLive::ID_LIVE, $ids, Criteria::IN);
            $flashLiveCollection = $this->getByCriteria($criteria);
        }
        catch(Exception $e)
        {
            throw new FlashLiveException("FlashLiveCollection can't be populated\n" . $e->getMessage());
        }
        return $flashLiveCollection;
    }

    /**
     * Metodo para actualizar un FlashLive
     * @param FlashLive $flashLive 
     */
    public function update($flashLive)
    {
        if(!($flashLive instanceof FlashLive))
            throw new FlashLiveException("passed parameter isn't a FlashLive instance");
        try
        {
            $where[] = "id_live = '{$flashLive->getIdLive()}'";
            $data = array(
                'name' => $flashLive->getName(),
                'id_server' => $flashLive->getIdServer(),
            );
            $data = array_filter($data, 'Catalog::notNull');
            $this->db->update(FlashLive::TABLENAME, $data, $where);
        }
        catch(Exception $e)
        {
            throw new FlashLiveException("The FlashLive can't be updated \n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para guardar un flashLive
     * @param FlashLive $flashLive
     */	
    public function save($flashLive)
    {
        if(!($flashLive instanceof FlashLive))
            throw new FlashLiveException("passed parameter isn't a FlashLive instance");
        if(null != $flashLive->getIdLive())
            $this->update($flashLive);
        else
            $this->create($flashLive);
    }

    /**
     * Metodo para eliminar un flashLive
     * @param FlashLive $flashLive
     */
    public function delete($flashLive)
    {
        if(!($flashLive instanceof FlashLive))
            throw new FlashLiveException("passed parameter isn't a FlashLive instance");
        $this->deleteById($flashLive->getIdLive());
    }

    /**
     * Metodo para eliminar un FlashLive a partir de su Id
     * @param int $idLive
     */
    public function deleteById($idLive)
    {
        try
        {
            $where = array($this->db->quoteInto('id_live = ?', $idLive));
            $this->db->delete(FlashLive::TABLENAME, $where);
        }
        catch(Exception $e)
        {
            throw new FlashLiveException("The FlashLive can't be deleted\n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para eliminar varios FlashLive a partir de su Id
     * @param array $ids
     */
    public function deleteByIds(array $ids)
    {
        try
        {
            $criteria = new Criteria();
            $criteria->add(FlashLive::ID_LIVE, $ids, Criteria::IN);
            $this->db->delete(FlashLive::TABLENAME, array($criteria->createSql()));
        }
        catch(Exception $e)
        {
            throw new FlashLiveException("Can't delete that\n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para Obtener todos los ids en un arreglo
     * @return array
     */
    public function retrieveAllIds()
    {
        return $this->getIdsByCriteria(new Criteria());
    }

    /**
     * Metodo para obtener todos los id de FlashLive por criterio
     * @param Criteria $criteria
     * @return array Array con todos los id de FlashLive que encajen en la busqueda
     */
    public function getIdsByCriteria(Criteria $criteria = null)
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        return $this->getCustomFieldByCriteria(FlashLive::ID_LIVE, $criteria);
    }

    /**
     * Metodo para obtener un campo en particular de un FlashLive dado un criterio
     * @param string $field
     * @param Criteria $criteria
     * @param $distinct
     * @return array Array con el campo de los objetos FlashLive que encajen en la busqueda
     */
    public function getCustomFieldByCriteria($field, Criteria $criteria = null, $distinct = false)
    { 
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $distinct = $distinct ? 'DISTINCT' : '';
        try
        {
            $sql = "SELECT {$distinct} {$field}
                    FROM ".FlashLive::TABLENAME."
                    WHERE  " . $criteria->createSql();
            $result = $this->db->fetchCol($sql);
        } catch(Zend_Db_Exception $e)
        {
            throw new FlashLiveException("No se pudieron obtener los fields de objetos FlashLive\n" . $e->getMessage());
        }
        return $result;
    }

    /**
     * Metodo que regresa una coleccion de objetos FlashLive 
     * dependiendo del criterio establecido
     * @param Criteria $criteria
     * @return FlashLiveCollection $flashLiveCollection
     */
    public function getByCriteria(Criteria $criteria = null)
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $this->db->setFetchMode(Zend_Db::FETCH_ASSOC);
        try 
        {
            $sql = "SELECT * FROM ".FlashLive::TABLENAME."
                    WHERE " . $criteria->createSql();
            $flashLiveCollection = new FlashLiveCollection();
            foreach ($this->db->fetchAll($sql) as $result){
                $flashLiveCollection->append($this->getFlashLiveInstance($result));
            }
        }
        catch(Zend_Db_Exception $e)
        {
            throw new FlashLiveException("Cant obtain FlashLiveCollection\n" . $e->getMessage());
        }
        return $flashLiveCollection;
    }
    
    /**
     * Metodo que cuenta FlashLive 
     * dependiendo del criterio establecido
     * @param Criteria $criteria
     * @param string $field
     * @return int $count
     */
    public function countByCriteria(Criteria $criteria = null, $field = 'id_live')
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        try 
        {
            $sql = "SELECT COUNT( $field ) FROM ".FlashLive::TABLENAME."
                    WHERE " . $criteria->createSql();   
            $count = $this->db->fetchOne($sql);
        }
        catch(Zend_Db_Exception $e)
        {
            throw new FlashLiveException("Cant obtain the count \n" . $e->getMessage());
        }
        return $count;
    }
    
    /**
     * Método que construye un objeto FlashLive y lo rellena con la información del rowset
     * @param array $result El arreglo que devolvió el objeto Zend_Db despues del fetch
     * @return FlashLive 
     */
    private function getFlashLiveInstance($result)
    {
        return FlashLiveFactory::createFromArray($result);
    }
  
    /**
     * Obtiene un FlashLiveCollection  dependiendo del idServer
     * @param int $idServer  
     * @return FlashLiveCollection 
     */
    public function getByIdServer($idServer)
    {
        $criteria = new Criteria();
        $criteria->add(FlashLive::ID_SERVER, $idServer, Criteria::EQUAL);
        $flashLiveCollection = $this->getByCriteria($criteria);
        return $flashLiveCollection;
    }


    /**
     * Link a FlashLive to FlashStream
     * @param int $idLive
     * @param int $idStream
     */
    public function linkToFlashStream($idLive, $idStream)
    {
        try
        {
            $this->unlinkFromFlashStream($idLive, $idStream);
            $data = array(
                'id_live' => $idLive,
                'id_stream' => $idStream,
            );
            $this->db->insert(FlashLive::TABLENAME_FLASH_LIVE_FLASH_STREAM, $data);
        }
        catch(Exception $e)
        {
            throw new FlashLiveException("Can't link FlashLive to FlashStream\n" . $e->getMessage());
        }
    }

    /**
     * Unlink a FlashLive from FlashStream
     * @param int $idLive
     * @param int $idStream
     */
    public function unlinkFromFlashStream($idLive, $idStream)
    {
        try
        {
            $where = array(
                $this->db->quoteInto('id_live = ?', $idLive),
                $this->db->quoteInto('id_stream = ?', $idStream),
            );
            $this->db->delete(FlashLive::TABLENAME_FLASH_LIVE_FLASH_STREAM, $where);
        }
        catch(Exception $e)
        {
            throw new FlashLiveException("Can't unlink FlashLive to FlashStream\n" . $e->getMessage());
        }
    }

    /**
     * Unlink all FlashStream relations
     * @param int $idLive
     */
    public function unlinkAllFlashStreamRelations($idLive)
    {
        try
        {
            $where = array(
                $this->db->quoteInto('id_live = ?', $idLive),
            );
            $this->db->delete(FlashLive::TABLENAME_FLASH_LIVE_FLASH_STREAM, $where);
        }
        catch(Exception $e)
        {
            throw new FlashLiveException("Can't unlink all FlashStream relations \n" . $e->getMessage());
        }
    }

    /**
     * Get FlashLive - FlashStream relations by Criteria
     * @param Criteria $criteria
     * @return array
     */
    public function getFlashLiveFlashStreamRelations(Criteria $criteria = null)
    { 
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $this->db->setFetchMode(Zend_Db::FETCH_ASSOC);
        try
        {
           $sql = "SELECT * FROM ". FlashLive::TABLENAME_FLASH_LIVE_FLASH_STREAM ."
                   WHERE  " . $criteria->createSql();
           $result = $this->db->fetchAll($sql);
        } catch(Exception $e)
        {
           throw new FlashLiveException("Can't obtain relations by criteria\n" . $e->getMessage());
        }
        return $result;
    }

    /**
     * Get FlashLiveCollection by FlashStream
     * @param int $idStream
     * @return FlashLiveCollection
     */
    public function getByFlashStream($idStream)
    {
        $criteria = new Criteria();
        $criteria->add('id_stream', $idStream, Criteria::EQUAL);
        $flashLiveFlashStream = $this->getFlashLiveFlashStreamRelations($criteria);
        $ids = array();
        foreach($flashLiveFlashStream as $rs){
            $ids[] = $rs['id_live'];
        }
        return $this->getByIds($ids);
    }
    
} 
 
