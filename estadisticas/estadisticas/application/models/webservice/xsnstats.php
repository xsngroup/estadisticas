<?php
class xsnstats
{
	 /**
	 * TestConnect Method
	 * 
	 * @return String
	 */	
	public function testConnect($params)
	{
		return "test";
	}	
	
	 /**
	 * Get Validation Method
	 * 
	 * @param String $username
	 * 
	 * @param String $password
	 * 
	 * @return Array
	 */		
	public function validate($username,$password)
	{
		require_once 'lib/security/Authentication.php';
		require_once 'application/models/beans/User.php';
		require_once 'application/models/catalogs/CompanyCatalog.php';
		/*
		if($username=="irgg"&&$password==123){
           return "correcto";			
		}
		else{
           return "fallo";			
		}	
		*/
        $respond = array();
        try
        {
            if($username == null || $password == null)
               return 'Debe especificar Usuario y contrase�a';
           $user = Authentication::authenticate($username, $password);

           if($user instanceof User)
           {
         	
           	   $company = CompanyCatalog::getInstance()->getByUser($user->getIdUser())->getOne();
           	   
           	   $userUtf8= array();
           	   if($company instanceof Company){
           	   	 $userUtf8['idCompany']=$company->getIdCompany();
           	   }
           	   else{
           	   	 $userUtf8['idCompany']="nonCompany";
           	   }
	           $respond['msg']="correcto";
	          
	           $userUtf8['IdUser']=$user->getIdUser();
	           $userUtf8['Name']=utf8_encode($user->getName());
	           $userUtf8['Username']=utf8_encode($user->getUsername());
	           $userUtf8['LastName']=utf8_encode($user->getLastName());
	           $respond['obj']=$userUtf8;           	
           	   return $respond;
           }
           else{
           	    $respond['msg']="fallo";
           	    return $respond;
           }
        }
        catch(AuthException $e)
        {
           	    $respond['msg']="fallo".utf8_encode($e->getMessage());
           	    return $respond;            
        }		
	}

	 /**
	 * Get LiveStats Method
	 * 
	 * @param Int $idCompany
	 * 
	 * @return Array
	 */		
	public function getLiveStats($idCompany)
	{
		require_once 'application/models/catalogs/FlashLiveStatCatalog.php';
		require_once 'application/models/catalogs/FlashStreamCatalog.php';
		require_once 'application/models/catalogs/CompanyCatalog.php';
		
		$criteria = new Criteria();
		$criteria->add('id_company', $idCompany, Criteria::EQUAL);
		$rel=CompanyCatalog::getInstance()->getCompanyStreamServerRelations($criteria);

		$statArray = array();
		if(count($rel)>0){
			foreach($rel as $item){
				if(!is_null($item['id_stream_flash'])){
					$criteria = new Criteria();
					$criteria->add('id_stream', $item['id_stream_flash'], Criteria::EQUAL);
					$criteria->addDescendingOrderByColumn('timestamp');
					$criteria->setLimit(1);
					$stat=FlashLiveStatCatalog::getInstance()->getByCriteria($criteria)->getOne();
					
					if($stat instanceof FlashLiveStat){
						$stream=FlashStreamCatalog::getInstance()->getById($stat->getIdStream());
						$statArray['timestamp']=	$stat->getTimestamp();
						$statArray['type'] = $stat->getType();
						$statArray['connects'] = $stat->getConnects();	
						$statArray['stream'] = $stream->getName();
						$statArray['idStream'] = $stream->getIdStream();				
					}
				}
			}
			if(!is_null($statArray['stream']))
			{
				$statArray['msg']=utf8_encode("correcto");
			}
			else{
				$statArray['msg']=utf8_encode("No se encontr� informaci�n de alguna se�al � video");				
			}
			return $statArray; 
		}
		else{
			$statArray['msg']=utf8_encode("No existen se�ales � videos asociados a tu cuenta");
			return $statArray; 
		}
		
	}
	
	 /**
	 * Get HistLive Method
	 * 
	 * @param Int $idStream
	 * @param String $fecFrom
	 * @param String $fecTo
	 * @param String $hrFrom
	 * @param String $hrTo
	 * 
	 * @return Array
	 */		
	public function getHistLive($idStream,$fecFrom,$fecTo,$hrFrom,$hrTo)
	{
		
		require_once 'application/models/catalogs/FlashLiveStatCatalog.php';
		
        $dateStart = $fecFrom;
        $dateEnd = $fecTo;       
        $initHour = $hrFrom;
        $finishHour = $hrTo;
       	             
        
        $dates = array();
        
        if((!is_null($dateStart) &!empty($dateStart))&&(!is_null($dateEnd) &!empty($dateEnd))){
        	 $dates[] = $dateStart.' '.$initHour.':00';
        	 $dates[] = $dateEnd.' '.$finishHour.':00';
        }
        else{
	        if (! is_null($dateStart) &! empty($dateStart)) {
	            $dates[] = $dateStart;
	        }
	        if (! is_null($dateEnd) &! empty($dateEnd)) {
	            $dates[] = $dateEnd;
	        }    
        }       
        
        
        $totalViews=0;
        
        if($idStream!=0){        	
        		$criteria = new Criteria();
        		$criteria->add('id_stream', $idStream, Criteria::EQUAL);        		     		
        		
        		$criteria->add('`timestamp`', $dates, Criteria::BETWEEN);
        		
        		$liveStat= FlashLiveStatCatalog::getInstance()->getByCriteria($criteria); 
        		$histStat = array();       		
                if(!$liveStat->isEmpty()){
                	$indice=0;                	
					$i=0;                	                	
                	$totalCon=0;
                	//$var= "Fecha,Conectados\n";
                    $tempvar=0;
                    $histStat['msg']="correcto";
                    $contentStat= array(); 
                	foreach($liveStat as $data){
                		$conteo = array(); 
                		if($i==0){
                			$conteo['timestamp']=$data->getTimestamp();
                			$conteo['concurrentes']=$data->getConnects();
                			$conteo['unicos']=$data->getConnects();
                			$conteo['nuevos']=$data->getConnects();
                			$conteo['total']=$data->getConnects();
	                		//$var.= $data->getTimestamp().",".$data->getConnects().",".$data->getConnects().",".$data->getConnects().",".$data->getConnects()."\n";              		
	                		$totalCon+=$data->getConnects();
	                		$tempvar=$data->getConnects();
	                		$totalViews=$data->getConnects();
                		}
                		else{
                			if($data->getConnects()>$tempvar){
                				$newConnects=$data->getConnects()-$tempvar;
                				$totalViews+=$newConnects;
	                		    //$var.= $data->getTimestamp().",".$data->getConnects().",".$tempvar.",".$newConnects.",".$totalViews."\n";              		
	                			$conteo['timestamp']=$data->getTimestamp();
	                			$conteo['concurrentes']=$data->getConnects();
	                			$conteo['unicos']=$tempvar;
	                			$conteo['nuevos']=$newConnects; 
	                			$conteo['total']=$totalViews;               				
	                		    $tempvar=$data->getConnects();
                			}
                			else{
	                		    //$var.= $data->getTimestamp().",".$data->getConnects().",".$data->getConnects().",0".",".$totalViews."\n";              		
	                			$conteo['timestamp']=$data->getTimestamp();
	                			$conteo['concurrentes']=$data->getConnects();
	                			$conteo['unicos']=$data->getConnects();
	                			$conteo['nuevos']=0; 
	                			$conteo['total']=$totalViews;                  				
	                		    $tempvar=$data->getConnects();                				
                			}
                		}
                		$contentStat[]=$conteo;
		                $i++;		                
                	}
                	
                	$histStat['stats']=$contentStat;
                	
                	return $histStat;

                	//print $var;
                    //die();
                }
                else{
                	$histStat['msg']="No existen datos para mostrar..."; 
                	return $histStat;
                }
        }		
	}	
	
}
?>