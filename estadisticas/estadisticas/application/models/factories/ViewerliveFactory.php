<?php
/**
 * Xsn
 *
 * Xsn
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */
/**
 * Dependences
 */
require_once "application/models/beans/Viewerlive.php";

/**
 * Clase ViewerliveFactory
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_factories
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx) 
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     <zetta> & <chentepixtol>
 * @version    1.0.2 SVN: $Revision$
 */
class ViewerliveFactory
{

   /**
    * Create a new Viewerlive instance
    * @param string $timestamp
    * @param int $status
    * @param int $idStream
    * @param string $ipviewer
    * @return Viewerlive
    */
   public static function create($timestamp, $status, $idStream, $ipviewer)
   {
      $newViewerlive = new Viewerlive();
      $newViewerlive
          ->setTimestamp($timestamp)
          ->setStatus($status)
          ->setIdStream($idStream)
          ->setIpviewer($ipviewer)
      ;
      return $newViewerlive;
   }
   
    /**
     * Método que construye un objeto Viewerlive y lo rellena con la información del rowset
     * @param array $fields El arreglo que devolvió el objeto Zend_Db despues del fetch
     * @return Viewerlive 
     */
    public static function createFromArray($fields)
    {
        $newViewerlive = new Viewerlive();
        $newViewerlive->setTimestamp($fields['timestamp']);
        $newViewerlive->setStatus($fields['status']);
        $newViewerlive->setIdStream($fields['id_stream']);
        $newViewerlive->setIpviewer($fields['ipviewer']);
        return $newViewerlive;
    }
   
}
