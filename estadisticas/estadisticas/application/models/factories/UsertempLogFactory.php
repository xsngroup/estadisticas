<?php
/**
 * Xsn
 *
 * Xsn
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */
/**
 * Dependences
 */
require_once "application/models/beans/UsertempLog.php";

/**
 * Clase UsertempLogFactory
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_factories
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx) 
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     <zetta> & <chentepixtol>
 * @version    1.0.2 SVN: $Revision$
 */
class UsertempLogFactory
{

   /**
    * Create a new UsertempLog instance
    * @param int $eventType
    * @param string $ip
    * @param datetime $timestamp
    * @param string $note
    * @param int $idUsertemp
    * @return UsertempLog
    */
   public static function create($eventType, $ip, $timestamp, $note, $idUsertemp)
   {
      throw new Exception('Factory Deprecated');
      $newUsertempLog = new UsertempLog();
      $newUsertempLog
          ->setEventType($eventType)
          ->setIp($ip)
          ->setTimestamp($timestamp)
          ->setNote($note)
          ->setIdUsertemp($idUsertemp)
      ;
      return $newUsertempLog;
   }
   
    /**
     * Método que construye un objeto UsertempLog y lo rellena con la información del rowset
     * @param array $fields El arreglo que devolvió el objeto Zend_Db despues del fetch
     * @return UsertempLog 
     */
    public static function createFromArray($fields)
    {
        $newUsertempLog = new UsertempLog();
        $newUsertempLog->setIdUsertempLog($fields['id_usertemp_log']);
        $newUsertempLog->setEventType($fields['event_type']);
        $newUsertempLog->setIp($fields['ip']);
        $newUsertempLog->setTimestamp($fields['timestamp']);
        $newUsertempLog->setNote($fields['note']);
        $newUsertempLog->setIdUsertemp($fields['id_usertemp']);
        return $newUsertempLog;
    }
   
}
