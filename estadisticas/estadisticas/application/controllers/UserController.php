<?php
/**
 * ##$BRAND_NAME$##
 *
 * ##$DESCRIPTION$##
 *
 * @category   Project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */

/**
 * BaseController
 */
require_once "lib/controller/BaseController.php";
require_once 'lib/managers/UserManager.php';


/**
 * Clase UserController que representa el controlador del objeto User
 *
 * @category   Project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 */
class UserController extends BaseController
{
	
    /**
     * Listado de access Usuarios
     */
    public function indexAction()
    {
        $this->_forward('list');
    }
    
    /**
     * Pantalla de listado de bancos
     */
    public function listAction()
    {
    	$this->view->users = UserCatalog::getInstance()->getActives();
    	$this->view->accessRoles = AccessRoleCatalog::getInstance()->getByCriteria()->toCombo();
    	$this->setTitle('Lista de Usuarios');
    }
        
    /**
     * Formulario para crear un nuevo usuario
     *
     */
    public function newAction()
    {
    	$this->setLists();
    	$this->view->setTpl('Form');
    	$this->view->isNew = true;
    	$this->setTitle('Guardar nuevo Usuario');
    }
    
    /**
     * Acci�n que recibe los par�metros de un nuevo banco
     */
    public function createAction()
    {
    	try 
    	{
    	   UserManager::getInstance()->create($this->getRequest(), $this->getUser()->getIdUser());
    	   $this->setFlash('ok','Se guard� correctamente el usuario');
    	   $this->_redirect('user');
    	} catch (ManagerException $e)
    	{
    		$this->setTitle('Guardar nuevo usuario (corregir problemas)');
    		$this->view->error = $this->l10n->_('No se puede guardar el usuario, ') . $e->getMessage();
    		$this->view->isNew = true;
    		$this->view->setTpl('Form');
    		$this->setLists();
    	}
    }
    
    /**
     * Pantalla de edici�n
     */
    public function editAction()
    {
    	try {
	    	$this->view->setTpl('Form');
	        $this->setLists();
	        $this->view->post = UserManager::getInstance()->getInfoForm($this->getRequest()->getParam('id'));
	        $this->setTitle('Editar Usuario');
    	} catch (ManagerException $e)
    	{
    		$this->setFlash('error', $e->getMessage());
    		$this->_redirect('user');
    	}
    }
    
    /**
     * Acci�n que recibe los par�metros de la edici�n de un usuario
     */
    public function updateAction()
    {
    	try
    	{
    	   UserManager::getInstance()->update($this->getRequest()->getParam('id'), $this->getRequest(), $this->getUser()->getIdUser());
    	   $this->setFlash('ok','Se edit� correctamente el usuario');
    	   $this->_redirect('user');  	
    	} catch (ManagerException $e)
    	{
    		$this->setTitle('Editar Usuario (corregir problemas)');
            $this->view->error = $this->l10n->_('No se puede actualizar el usuario, ') . $e->getMessage();
            $this->view->setTpl('Form');
            $this->setLists();
    	}
    }
    
    /**
     * Acci�n para eliminar un usuario
     */
    public function deleteAction()
    {
        try 
        { 
           UserManager::getInstance()->delete($this->getRequest()->getParam('id'), $this->getUser()->getIdUser());
           $this->setFlash('ok','Se elimin� correctamente el usuario');
        } catch (ManagerException $e)
        {
        	$this->setFlash('error','No se pudo eliminar el usuario, '.$e->getMessage());
        }
        $this->_redirect('user');
    }
    
    /**
     * Rellena los combobox con la informaci�n de cat�logos
     */
    private function setLists()
    {
    	$this->view->accessRoles = AccessRoleCatalog::getInstance()->getActives(new Criteria())->toCombo();
        $this->view->maritalStatus = Person::$MaritalStatusLabel;
    }
    
}




