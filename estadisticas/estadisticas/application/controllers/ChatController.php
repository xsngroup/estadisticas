<?php
/**
 * ##$BRAND_NAME$##
 *
 * ##$DESCRIPTION$##
 *
 * @category   Project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */

/**
 * Dependences
 */
require_once "lib/controller/BaseController.php";
require_once "application/models/catalogs/ChatCatalog.php";
require_once 'application/models/catalogs/CompanyCatalog.php';
require_once 'application/models/catalogs/CustomeThemeCatalog.php';

class ChatController extends BaseController {

	public function listAction(){
		$idUser=$this->getUser()->getIdUser();
		$company = CompanyCatalog::getInstance()->getByUser($idUser)->getOne();  
		$criteria = new Criteria();
		$chats=null;
		if($company instanceof Company){
           $customTheme = CustomeThemeCatalog::getInstance()->getByIdCompany($company->getIdCompany())->getOne();
		  $criteria->add('id_custome_theme', $customTheme->getIdCustomeTheme(), Criteria::EQUAL);
		  $criteria->addDescendingOrderByColumn('timestamp');
		  $chats=ChatCatalog::getInstance()->getByCriteria($criteria);
		}		

		$this->view->chats=$chats;
	}
	
	public function excelAction(){
    	require_once 'lib/excel/excel.php';
    	require_once 'lib/excel/excel-ext.php';
        $this->view->setLayoutFile(false);
        $this->_helper->viewRenderer->setNoRender();

		$idUser=$this->getUser()->getIdUser();
		$company = CompanyCatalog::getInstance()->getByUser($idUser)->getOne();  
		$criteria = new Criteria();
		$chats=null;
		if($company instanceof Company){
           $customTheme = CustomeThemeCatalog::getInstance()->getByIdCompany($company->getIdCompany())->getOne();
		  $criteria->add('id_custome_theme', $customTheme->getIdCustomeTheme(), Criteria::EQUAL);
		  $criteria->addDescendingOrderByColumn('timestamp');
		  $chats=ChatCatalog::getInstance()->getByCriteria($criteria);
		}	       
        foreach($chats as $chat){
        $assoc[]=array("Fecha y hora"=>$chat->getTimestamp(),"Usuario"=>$chat->getUsername(),"Mensaje"=>utf8_decode($chat->getMsg()));
        }
		createExcel("descarga_chat.xls", $assoc);        
	}
}

?>