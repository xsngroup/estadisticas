<?php
require_once 'lib/controller/BaseController.php';
require_once "application/models/catalogs/FlashInstanceCatalog.php";
require_once 'application/models/catalogs/FlashInstanceStatCatalog.php';
require_once 'application/models/catalogs/StreamServerCatalog.php';
require_once 'application/models/catalogs/CompanyCatalog.php';
require_once "application/models/catalogs/FlashLiveCatalog.php";
require_once "application/models/catalogs/FlashLiveStatCatalog.php";
require_once 'application/models/catalogs/FlashStreamCatalog.php';
require_once "application/models/catalogs/FlashSuscriberCatalog.php";

class ReportController extends BaseController
{

	
    public function instanceReportAction()
    {
        $id             = $this->getUser()->getIdUser();
        $serverCatalog  = StreamServerCatalog::getInstance();
        $companyCatalog = CompanyCatalog::getInstance();
        
        $company = $companyCatalog->getByUser($id)->getOne();
        
        if (!$company instanceof Company) {
            $this->setFlash("error", "No existe ningun cliente asociado");
            $this->_redirect('index/index');
        }
        
        $server = $serverCatalog->getByCompany($company->getIdCompany())->getOne();
        if (!$server instanceof StreamServer) {
            $this->setFlash("error", "No existe ningun servidor asociado");
            $this->_redirect('index/index');
        }
        
        $instFlash = FlashInstanceCatalog::getInstance()->getByIdServer($server->getIdServer())->toCombo();
        
        $this->view->idCompany = $company->getIdCompany();
        $this->view->idServer  = $server->getIdServer();
        $this->view->instances = $instFlash;
        $this->setTitle('Histórico de las instancias');
        $this->view->setTpl('instanceReport');
    }
    
    public function instanceByDatesAction()
    {
        $serverCatalog  = StreamServerCatalog::getInstance();
        $companyCatalog = CompanyCatalog::getInstance();
        
        $dateStart  = $this->getRequest()->getParam('date_start');
        $dateEnd    = $this->getRequest()->getParam('date_end');
        $idCompany  = $this->getRequest()->getParam('idCompany');
        $idServer   = $this->getRequest()->getParam('idServer');
        $hour       = $this->getRequest()->getParam('hour');
        $idInstance = $this->getRequest()->getParam('instance');
        
        $company = $companyCatalog->getById($idCompany);
        
        if (!$company instanceof Company) {
            $this->setFlash("error", "No existe ningun cliente asociado");
            $this->_redirect('index/index');
        }
        
        $server = $serverCatalog->getById($idServer);
        if (!$server instanceof StreamServer) {
            $this->setFlash("error", "No existe ningun servidor asociado");
            $this->_redirect('index/index');
        }
        
        
        $dates = array();
        if (!is_null($dateStart) & !empty($dateStart)) {
            $dates[] = $dateStart;
        }
        if (!is_null($dateEnd) & !empty($dateEnd)) {
            $dates[] = $dateEnd;
        }
        $result = array();
        
        
        $totalConnects    = null;
        $totalDisconnects = null;
        
        if ($idInstance != 0) {
            $criteria = new Criteria();
            $criteria->add('id_instance', $idInstance, Criteria::EQUAL);
            
            $criteria->add('timestamp', $dates, Criteria::BETWEEN);
            
            $instStat = FlashInstanceStatCatalog::getInstance()->getByCriteria($criteria);
            if (!$instStat->isEmpty()) {
                $indice = 0;
                foreach ($instStat as $data) {
                    $totalConnects    = $data->getTotalConnects();
                    $totalDisconnects = $data->getTotalDisconnects();
                }
            }
        }
        
        $instCombo = FlashInstanceCatalog::getInstance()->getById($idInstance);
        
        
        
        
        $this->view->company  = $company;
        $this->view->server   = $server;
        $this->view->instance = $instCombo;
        
        $this->view->totalConnects    = $totalConnects;
        $this->view->totalDisconnects = $totalDisconnects;
        $this->view->dateStart        = $dateStart;
        $this->view->dateEnd          = $dateEnd;
        $this->view->setTpl("instanceByDates");
    }
      public function liveRegresar($idcompany){
	require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Log.php';
	require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Log\Writer\Stream.php';
	require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Exception.php';
	$logger = new Zend_Log();
	$writer = new Zend_Log_Writer_Stream('c:/file2.txt');
	$output=array();
	$ind=0;
$logger->addWriter($writer);
		$con=mysql_connect("localhost:3306","xsnuser","Xsnus3rpass1");
		if(!$con){

		}else{
			mysql_select_db("xsn_stats",$con);
			$order = "SELECT id_stream_helix FROM xsn_core_servers_companys where id_company=".$idcompany;
			$result = mysql_query($order,$con);	
			if($result){
				while($row=mysql_fetch_array($result)){
					
					$order2 = "SELECT name FROM xsn_core_helix_streams WHERE id_stream=".$row['id_stream_helix']." AND title NOT LIKE 'vod'";
					$result2 = mysql_query($order2,$con);
					if($result2){
						while($row2=mysql_fetch_array($result2)){
							$output[$ind]=$row;
							$ind++;
						}
					}else{
					}
					
				}  
			
			}else{
				
			}
			mysql_close($con);

			
		}
		return $output;
	}
    
    public function VodRegresar($idcompany){
	require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Log.php';
	require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Log\Writer\Stream.php';
	require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Exception.php';
	$logger = new Zend_Log();
	$writer = new Zend_Log_Writer_Stream('c:/file.txt');
	$output=array();
	$ind=0;
		$con=mysql_connect("localhost:3306","xsnuser","Xsnus3rpass1");
		if(!$con){

		}else{
			mysql_select_db("xsn_stats",$con);
			$order = "SELECT id_stream_helix FROM xsn_core_servers_companys where id_company=".$idcompany;
			$result = mysql_query($order,$con);	
			if($result){
				while($row=mysql_fetch_array($result)){
					
					$order2 = "SELECT name FROM xsn_core_helix_streams WHERE id_stream=".$row['id_stream_helix']." AND title LIKE 'vod'";
					$result2 = mysql_query($order2,$con);
					if($result2){
					
						while($row2=mysql_fetch_array($result2)){
						
							$output[$ind]=$row;
							$ind++;
						}
					}else{
					
					}
					
				}  
			
			}else{
				
			}
			mysql_close($con);

			
		}
		return $output;
	}
    public function streamReportAction()
    {
	require_once 'application/models/catalogs/HelixStreamCatalog.php';
        
        $id             = $this->getUser()->getIdUser();
        $serverCatalog  = StreamServerCatalog::getInstance();
        $companyCatalog = CompanyCatalog::getInstance();
        
        $company = $companyCatalog->getByUser($id)->getOne();
        
        if (!$company instanceof Company) {
            $this->setFlash("error", "No existe ningun cliente asociado");
            $this->_redirect('index/index');
        }
        
        $server = $serverCatalog->getByCompany($company->getIdCompany())->getOne();
        if (!$server instanceof StreamServer) {
            $this->setFlash("error", "No existe ningun servidor asociado");
            $this->_redirect('index/index');
        }
        
        
        $criteria = new Criteria();
        $criteria->add('id_company', $company->getIdCompany(), Criteria::EQUAL);
       // $criteria->add('description', 'VOD', Criteria::EQUAL);
	   $this->VodRegresar($company->getIdCompany());
        //$data = HelixStreamCatalog::getInstance()->getHelixStreamHelixLiveRelations($criteria);
        $data=$this->VodRegresar($company->getIdCompany());
        foreach ($data as $item) {
            $ids[] = $item['id_stream_helix'];
        }
        $streams = HelixStreamCatalog::getInstance()->getByIds($ids)->toCombo();
        
        $this->view->idCompany = $company->getIdCompany();
        $this->view->idServer  = $server->getIdServer();
        $this->view->streams   = $streams;
        $this->setTitle('Histórico de Helix Vod Stream');
        $this->view->setTpl('streamReport');
    }
    
    public function processStreamAction()
    {
        set_time_limit(0);
        $dateStart  = $this->getRequest()->getParam('date_start');
        $dateEnd    = $this->getRequest()->getParam('date_end');
        $idCompany  = $this->getRequest()->getParam('idCompany');
        $idServer   = $this->getRequest()->getParam('idServer');
        $initHour   = $this->getRequest()->getParam('initHour');
        $finishHour = $this->getRequest()->getParam('finishHour');
        $idStream   = $this->getRequest()->getParam('stream');
        $groupBy    = $this->getRequest()->getParam('groupBy');
        
        $dates = array();
        
        if ((!is_null($dateStart) & !empty($dateStart)) && (!is_null($dateEnd) & !empty($dateEnd))) {
            $dates[] = $dateStart . ' ' . $initHour . ':00';
            $dates[] = $dateEnd . ' ' . $finishHour . ':00';
        } else {
            if (!is_null($dateStart) & !empty($dateStart)) {
                $dates[] = $dateStart . ' ' . $initHour . ':00';
                $dates[] = $dateStart . ' ' . $finishHour . ':00';
            }
            if (!is_null($dateEnd) & !empty($dateEnd)) {
                $dates[] = $dateEnd . ' ' . $initHour . ':00';
                $dates[] = $dateEnd . ' ' . $finishHour . ':00';
            }
        }
        
        $totalViews = 0;
        $promedio   = 0;
        if ($idStream != 0) {
            $criteria = new Criteria();
            $criteria->add('id_stream', $idStream, Criteria::EQUAL);
            
            $criteria->add('`timestamp`', $dates, Criteria::BETWEEN);
            
            $liveStat = FlashLiveStatCatalog::getInstance(1)->getByCriteria($criteria);
            if (!$liveStat->isEmpty()) {
                $indice = 0;
                
                $i = 0;
                
                //$var= "Fecha,Conectados\n";
                $tempvar = 0;
                foreach ($liveStat as $data) {
                    if ($i == 0) {
                        //$var.= $data->getTimestamp().",".$data->getConnects().",".$data->getConnects().",".$data->getConnects().",".$data->getConnects()."\n";              		
                        //$totalCon+=$data->getConnects();
                        $tempvar    = $data->getConnects();
                        $totalViews = $data->getConnects();
                    } else {
                        if ($data->getConnects() > $tempvar) {
                            $newConnects = $data->getConnects() - $tempvar;
                            $totalViews += $newConnects;
                            //$var.= $data->getTimestamp().",".$data->getConnects().",".$tempvar.",".$newConnects.",".$totalViews."\n";              		
                            $tempvar  = $data->getConnects();
                            $datetime = $data->getTimestamp();
                        } else {
                            //$var.= $data->getTimestamp().",".$data->getConnects().",".$data->getConnects().",0".",".$totalViews."\n";              		
                            $datetime = $data->getTimestamp();
                            $tempvar  = $data->getConnects();
                        }
                    }
                    $i++;
                }
                
                
                $extrae = explode(' ', $datetime);
                
                $hrsArray = explode(':', $extrae[1]);
                $suma     = 0;
                for ($i = 0; $i < 3; $i++) {
                    switch ($i) {
                        case 0:
                            $temp = (int) $hrsArray[$i] * 3600;
                            if ($temp > 0) {
                                $suma += $temp;
                            }
                            break;
                        case 1:
                            $temp = (int) $hrsArray[$i] * 60;
                            if ($temp > 0) {
                                $suma += $temp;
                            }
                            break;
                        case 2:
                            if ((int) $hrsArray[$i] > 0) {
                                $suma += $temp;
                            }
                            break;
                    }
                }
                //echo $suma;
                //echo $totalViews;
                $promedio = $suma / $totalViews;
                $flagData = true;
            } else {
                $flagData = false;
            }
        }
        
        
        
        $stream = FlashStreamCatalog::getInstance()->getById($idStream);
        
        
        $this->view->flagData   = $flagData;
        $this->view->promedio   = $promedio;
        $this->view->stream     = $stream;
        $this->view->dateStart  = $dateStart;
        $this->view->dateEnd    = $dateEnd;
        $this->view->initHour   = $initHour;
        $this->view->finishHour = $finishHour;
        $this->view->dates      = $dates;
        $this->view->groupBy    = $groupBy;
        
        $this->view->setTpl("streamProcess");
    }
    
    public function processDataAction()
    {
        set_time_limit(0);
        $dateStart  = $this->getRequest()->getParam('dateStart');
        $dateEnd    = $this->getRequest()->getParam('dateEnd');
        $initHour   = $this->getRequest()->getParam('initHour');
        $finishHour = $this->getRequest()->getParam('finishHour');
        $idStream   = $this->getRequest()->getParam('idStream');
        $groupBy    = $this->getRequest()->getParam('groupBy');
        
        $dates = array();
        
        if ((!is_null($dateStart) & !empty($dateStart)) && (!is_null($dateEnd) & !empty($dateEnd))) {
            $dates[] = $dateStart . ' ' . $initHour . ':00';
            $dates[] = $dateEnd . ' ' . $finishHour . ':00';
        } else {
            if (!is_null($dateStart) & !empty($dateStart)) {
                $dates[] = $dateStart . ' ' . $initHour . ':00';
                $dates[] = $dateStart . ' ' . $finishHour . ':00';
            }
            if (!is_null($dateEnd) & !empty($dateEnd)) {
                $dates[] = $dateEnd . ' ' . $initHour . ':00';
                $dates[] = $dateEnd . ' ' . $finishHour . ':00';
            }
        }
        
        
        $totalViews = 0;
        
        if ($idStream != 0) {
            $criteria = new Criteria();
            $criteria->add('id_stream', $idStream, Criteria::EQUAL);
            
            $criteria->add('`timestamp`', $dates, Criteria::BETWEEN);
            
            $liveStat = FlashLiveStatCatalog::getInstance(1)->getByCriteria($criteria);
            if (!$liveStat->isEmpty()) {
                $indice = 0;
                
                $i                     = 0;
                $dataNumber            = array();
                $flashSuscriberCatalog = FlashSuscriberCatalog::getInstance();
                $totalCon              = 0;
                //$var= "Fecha,Conectados\n";
                $tempvar               = 0;
                
                $arrayGroupHr = array();
                foreach ($liveStat as $data) {
                    if ($groupBy == 1) {
                        if ($i == 0) {
                            $var .= $data->getTimestamp() . "," . $data->getConnects() . "," . $data->getConnects() . "," . $data->getConnects() . "," . $data->getConnects() . "\n";
                            $totalCon += $data->getConnects();
                            $tempvar    = $data->getConnects();
                            $totalViews = $data->getConnects();
                        } else {
                            if ($data->getConnects() > $tempvar) {
                                $newConnects = $data->getConnects() - $tempvar;
                                $totalViews += $newConnects;
                                $var .= $data->getTimestamp() . "," . $data->getConnects() . "," . $tempvar . "," . $newConnects . "," . $totalViews . "\n";
                                $tempvar = $data->getConnects();
                            } else {
                                $var .= $data->getTimestamp() . "," . $data->getConnects() . "," . $data->getConnects() . ",0" . "," . $totalViews . "\n";
                                $tempvar = $data->getConnects();
                            }
                        }
                        $i++;
                    } elseif ($groupBy == 2) {
                        $fechaHr = explode(" ", $data->getTimestamp());
                        //$horaTmp=explode(":",$fechaHr[1]);
                        
                        if ($i == 0) {
                            $totalViews = $data->getConnects();
                            $tempvar    = $data->getConnects();
                            $fechaAnt   = $fechaHr[0];
                        } else {
                            if ($fechaAnt == $fechaHr[0]) {
                                if ($data->getConnects() > $tempvar) {
                                    $newConnects = $data->getConnects() - $tempvar;
                                    $totalViews += $newConnects;
                                    $tempvar = $data->getConnects();
                                    //echo totalViews;
                                }
                            } else {
                                $arrayGroupHr[$fechaAnt] = $totalViews;
                                $totalViews              = $data->getConnects();
                                $tempvar                 = $data->getConnects();
                                $fechaAnt                = $fechaHr[0];
                            }
                        }
                        $i++;
                    }
                }
                
                
                //print_r($arrayGroupHr);	
                if ($groupBy == 2) {
                    foreach ($arrayGroupHr as $key => $value) {
                        $var .= $key . "," . $value . "\n";
                    }
                }
                
                print $var;
                die();
            }
        }
    }
    
    public function xmlAction()
    {
        set_time_limit(0);
        require_once 'lib/charts/XmlWriter.class.php';
        //if($this->getRequest()->isXmlHttpRequest())
        $this->view->setLayoutFile(false);
        $this->_helper->viewRenderer->setNoRender();
        
        $dateStart  = $this->getRequest()->getParam('dateStart');
        $dateEnd    = $this->getRequest()->getParam('dateEnd');
        $idInstance = $this->getRequest()->getParam('id');
        
        $dates = array();
        if (!is_null($dateStart) & !empty($dateStart)) {
            $dates[] = $dateStart;
        }
        if (!is_null($dateEnd) & !empty($dateEnd)) {
            $dates[] = $dateEnd;
        }
        $result = array();
        
        
        if ($idInstance != 0) {
            $criteria = new Criteria();
            $criteria->add('id_instance', $idInstance, Criteria::EQUAL);
            
            $criteria->add('timestamp', $dates, Criteria::BETWEEN);
            
            $instStat = FlashInstanceStatCatalog::getInstance()->getByCriteria($criteria);
            if (!$instStat->isEmpty()) {
                $indice = 0;
                
                $xml = new XmlWriter2();
                $xml->push('chart');
                $xml->push('series');
                $i          = 0;
                $dataNumber = array();
                foreach ($instStat as $data) {
                    $xml->element('value', $data->getTimestamp(), array(
                        'xid' => $i
                    ));
                    $dataNumber[] = $data->getTotalConnects();
                    $i++;
                }
                
                $xml->pop();
                
                $xml->push('graphs');
                $xml->push('graph', array(
                    'gid' => 1
                ));
                $x = 0;
                foreach ($dataNumber as $number) {
                    $xml->element('value', $number, array(
                        'xid' => $x
                    ));
                    $x++;
                }
                
                $xml->pop();
                $xml->pop();
                $xml->pop();
                header('Content-Type: text/xml');
                print $xml->getXml();
                die();
            }
        }
        
    }
    
    
    
    
    /*    public function testAction()
    {
    $this->view->setTpl("test");	
    }
    
    public function dataAction()
    {
    $var=	"Date,Temperature\n".
    "2008-05-07,75\n".
    "2008-05-08,70\n".
    "2008-05-09,80\n";
    print $var;
    die();
    }*/
    
    public function filterAction()
    {
        $this->view->setLayoutFile(false);
        
        $dateStart  = $this->getRequest()->getParam('dateStart');
        $dateEnd    = $this->getRequest()->getParam('dateEnd');
        $initHour   = $this->getRequest()->getParam('initHour');
        $finishHour = $this->getRequest()->getParam('finishHour');
        $idStream   = $this->getRequest()->getParam('idStream');
        $idLiveStat = $this->getRequest()->getParam('idLiveStat');
        
        $liveStat = FlashLiveStatCatalog::getInstance()->getById($idLiveStat);
        
        $suscribersCatalog = FlashSuscriberCatalog::getInstance();
        
        $suscribers = $suscribersCatalog->getByIdLiveStat($idLiveStat);
        
        if (!$suscribers->isEmpty()) {
            $tempData = explode(' ', $liveStat->getTimestamp());
            
            $tempTwo = explode(':', $tempData[1]);
            
            $tempThree = @explode('', $tempTwo[1]);
            
            if ($tempThree[0] > 0) {
                $hrMod = $tempData[0] . ' ' . $tempTwo[0] . ':05:00';
            } else {
                $hr = $tempTwo[0] - 1;
                
                $hrMod = $tempData[0] . ' ' . $hr . ':55:00';
            }
            
            $conteo   = array();
            $criteria = new Criteria();
            $criteria->add('suscribe_time', $hrMod, Criteria::LIKE);
            foreach ($suscribers as $item) {
                $criteria->add('client', $item->getClient(), Criteria::EQUAL);
                $suscribersMatch = $suscribersCatalog->countByCriteria($criteria);
                
                if ($suscribersMatch == 0) {
                    $conteo[] = $suscribersMatch;
                }
            }
            
        }
        $this->view->conteo = $conteo;
        $this->view->setTpl("Filter");
        
    }
    
    public function downloadAction()
    {
        require_once 'lib/excel/excel.php';
        require_once 'lib/excel/excel-ext.php';
        $this->view->setLayoutFile(false);
        $this->_helper->viewRenderer->setNoRender();
        
        
        set_time_limit(0);
        $dateStart  = $this->getRequest()->getParam('dateStart');
        $dateEnd    = $this->getRequest()->getParam('dateEnd');
        $initHour   = $this->getRequest()->getParam('initHour');
        $finishHour = $this->getRequest()->getParam('finishHour');
        $idStream   = $this->getRequest()->getParam('idStream');
        
        $dates = array();
        
        if ((!is_null($dateStart) & !empty($dateStart)) && (!is_null($dateEnd) & !empty($dateEnd))) {
            $dates[] = $dateStart . ' ' . $initHour . ':00';
            $dates[] = $dateEnd . ' ' . $finishHour . ':00';
        } else {
            if (!is_null($dateStart) & !empty($dateStart)) {
                $dates[] = $dateStart;
            }
            if (!is_null($dateEnd) & !empty($dateEnd)) {
                $dates[] = $dateEnd;
            }
        }
        
        
        $totalViews = 0;
        $assoc      = array();
        if ($idStream != 0) {
            $criteria = new Criteria();
            $criteria->add('id_stream', $idStream, Criteria::EQUAL);
            
            $criteria->add('`timestamp`', $dates, Criteria::BETWEEN);
            
            $liveStat = FlashLiveStatCatalog::getInstance(1)->getByCriteria($criteria);
            if (!$liveStat->isEmpty()) {
                $indice = 0;
                
                $i                     = 0;
                $dataNumber            = array();
                $flashSuscriberCatalog = FlashSuscriberCatalog::getInstance();
                $totalCon              = 0;
                //$var= "Fecha,Conectados\n";
                $tempvar               = 0;
                foreach ($liveStat as $data) {
                    if ($i == 0) {
                        $assoc[] = array(
                            "Fecha" => $data->getTimestamp(),
                            "Concurrentes" => $data->getConnects(),
                            "Usuarios únicos" => $data->getConnects(),
                            "Usuarios nuevos" => $data->getConnects(),
                            "Peticiones" => $data->getConnects()
                        );
                        //$var.= $data->getTimestamp().",".$data->getConnects().",".$data->getConnects().",".$data->getConnects().",".$data->getConnects()."\n";              		
                        $totalCon += $data->getConnects();
                        $tempvar    = $data->getConnects();
                        $totalViews = $data->getConnects();
                    } else {
                        if ($data->getConnects() > $tempvar) {
                            $newConnects = $data->getConnects() - $tempvar;
                            $totalViews += $newConnects;
                            $assoc[] = array(
                                "Fecha" => $data->getTimestamp(),
                                "Concurrentes" => $data->getConnects(),
                                "Usuarios únicos" => $tempvar,
                                "Usuarios nuevos" => $newConnects,
                                "Peticiones" => $totalViews
                            );
                            //$var.= $data->getTimestamp().",".$data->getConnects().",".$tempvar.",".$newConnects.",".$totalViews."\n";              		
                            $tempvar = $data->getConnects();
                        } else {
                            $assoc[] = array(
                                "Fecha" => $data->getTimestamp(),
                                "Concurrentes" => $data->getConnects(),
                                "Usuarios únicos" => $data->getConnects(),
                                "Usuarios nuevos" => 0,
                                "Peticiones" => $totalViews
                            );
                            //$var.= $data->getTimestamp().",".$data->getConnects().",".$data->getConnects().",0".",".$totalViews."\n";              		
                            $tempvar = $data->getConnects();
                        }
                    }
                    $i++;
                }
            }
        }
        
        
        
        createExcel("reporte-historico.xls", $assoc);
        
        exit;
    }
    
    public function geolocationAction()
    {
        require_once 'application/models/catalogs/FlashVodCatalog.php';
        
        $id             = $this->getUser()->getIdUser();
        $serverCatalog  = StreamServerCatalog::getInstance();
        $companyCatalog = CompanyCatalog::getInstance();
        
        $company = $companyCatalog->getByUser($id)->getOne();
        
        if (!$company instanceof Company) {
            $this->setFlash("error", "No existe ningun cliente asociado");
            $this->_redirect('index/index');
        }
        
        $server = $serverCatalog->getByCompany($company->getIdCompany())->getOne();
        if (!$server instanceof StreamServer) {
            $this->setFlash("error", "No existe ningun servidor asociado");
            $this->_redirect('index/index');
        }
        
        $criteria = new Criteria();
        $criteria->add('id_company', $company->getIdCompany(), Criteria::EQUAL);
        
        $data = FlashStreamCatalog::getInstance()->getFlashStreamFlashLiveRelations($criteria);
        
        $idsStream = array();
        $idsVod    = array();
        
        $flagStream = false;
        $flagVod    = false;
        foreach ($data as $item) {
            if (!is_null($item['id_stream_flash'])) {
                $flagStream  = true;
                $idsStream[] = $item['id_stream_flash'];
            }
            if (!is_null($item['id_vod_flash'])) {
                $flagVod  = true;
                $idsVod[] = $item['id_vod_flash'];
            }
        }
        
        $streams = FlashStreamCatalog::getInstance()->getByIds($idsStream)->toCombo();
        $vods    = FlashVodCatalog::getInstance()->getByIds($idsVod)->toCombo();
        
        $this->view->idCompany  = $company->getIdCompany();
        $this->view->idServer   = $server->getIdServer();
        $this->view->streams    = $streams;
        $this->view->vods       = $vods;
        $this->view->flagStream = $flagStream;
        $this->view->flagVod    = $flagVod;
        $this->setTitle('Histórico por ubicación');
        $this->view->setTpl('Geolocation');
    }
    
    public function processGeoAction()
    {
        set_time_limit(0);
        //require_once 'application/models/catalogs/ViewerliveCatalog.php';
        //require_once 'application/models/catalogs/ViewervodCatalog.php';
        require_once 'application/models/catalogs/FlashIpviewerStreamCatalog.php';
        require_once 'application/models/catalogs/FlashIpviewerVodCatalog.php';
        require_once 'application/models/catalogs/LocationCatalog.php';
        
        
        //if($this->getRequest()->isXmlHttpRequest())
        //$this->view->setLayoutFile(false);
        //require_once 'Zend/Soap/Client.php'; 
        require_once 'lib/charts/XmlWriter.class.php';
        
        $this->getUser()->setAttribute('rgeolocation', null);
        
        $dateStart  = $this->getRequest()->getParam('date_start');
        $dateEnd    = $this->getRequest()->getParam('date_end');
        $initHour   = $this->getRequest()->getParam('initHour');
        $finishHour = $this->getRequest()->getParam('finishHour');
        
        
        $flagstream = (int) $this->getRequest()->getParam('flagStream', 0);
        $flagvideo  = (int) $this->getRequest()->getParam('flagVideo', 0);
        
        
        $fp = fopen($this->getRegistry()->config->xmlFiles->path . "\\worldmap\\data.xml", 'w+');
        fwrite($fp, "");
        fclose($fp);
        
        $fp = fopen($this->getRegistry()->config->xmlFiles->path . "\\ammap\\ammap_data.xml", 'w+');
        fwrite($fp, "");
        fclose($fp);
        
        if (!empty($flagstream)) {
            $id = $this->getRequest()->getParam('stream');
        } elseif (!empty($flagvideo)) {
            $id = $this->getRequest()->getParam('vod');
        }
        
        
        $dates = array();
        if ((!is_null($dateStart) & !empty($dateStart)) && (!is_null($dateEnd) & !empty($dateEnd))) {
            $dates[] = $dateStart . ' ' . $initHour . ':00';
            $dates[] = $dateEnd . ' ' . $finishHour . ':00';
        } else {
            if (!is_null($dateStart) & !empty($dateStart)) {
                $dates[] = $dateStart;
            }
            if (!is_null($dateEnd) & !empty($dateEnd)) {
                $dates[] = $dateEnd;
            }
        }
        $result = array();
        
        $viewerLiveCatalog = FlashIpviewerStreamCatalog::getInstance(1);
        $viewerVodCatalog  = FlashIpviewerVodCatalog::getInstance(1);
        
        if ($id != 0) {
            $criteria = new Criteria();
            
            if (!empty($flagstream)) {
                $criteria->add('id_stream', $id, Criteria::EQUAL);
            } elseif (!empty($flagvideo)) {
                $criteria->add('id_vod', $id, Criteria::EQUAL);
            }
            
            $criteria->add('status', 1, Criteria::EQUAL);
            $criteria->add('timestamp', $dates, Criteria::BETWEEN);
            
            
            if (!empty($flagstream)) {
                $result = $viewerLiveCatalog->getByCriteria($criteria);
            } elseif (!empty($flagvideo)) {
                $result = $viewerVodCatalog->getByCriteria($criteria);
            }
            
            
            $xmlWorld = new XmlWriter2();
            $xmlWorld->push('countrydata');
            $xmlWorld->push('state', array(
                'id' => 'default_color'
            ));
            $xmlWorld->element('color', 'd8d8d8');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'background_color'
            ));
            $xmlWorld->element('color', 'FFFFFF');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'outline_color'
            ));
            $xmlWorld->element('color', '868686');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'default_point'
            ));
            $xmlWorld->element('color', 'e22000');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'scale_points'
            ));
            $xmlWorld->element('data', '50');
            $xmlWorld->pop();
            
            $xmlWorld->push('state', array(
                'id' => 'zoom_out_button'
            ));
            $xmlWorld->element('name', 'Zoom Out');
            $xmlWorld->element('data', 'SE');
            $xmlWorld->element('font_size', '12');
            $xmlWorld->element('font_color', 'FFFFFF');
            $xmlWorld->element('background_color', '585858');
            $xmlWorld->pop();
            
            if (!$result->isEmpty()) {
                $this->getUser()->setAttribute('totalviewersbyip', count($result));
                
                $dataArray   = array();
                $colorHover  = array();
                $colorNormal = array();
                
                $colorNormal['aguascalientes']                   = '#C8C8C8';
                $colorNormal['baja_california']                  = '#C8C8C8';
                $colorNormal['baja_california_sur']              = '#C8C8C8';
                $colorNormal['campeche']                         = '#C8C8C8';
                $colorNormal['chiapas']                          = '#C8C8C8';
                $colorNormal['chihuahua']                        = '#C8C8C8';
                $colorNormal['coahuila']                         = '#C8C8C8';
                $colorNormal['colima']                           = '#C8C8C8';
                $colorNormal['distrito_federal']                 = '#C8C8C8';
                $colorNormal['durango']                          = '#C8C8C8';
                $colorNormal['federally_administered_territory'] = '#C8C8C8';
                $colorNormal['guanajuato']                       = '#C8C8C8';
                $colorNormal['guerrero']                         = '#C8C8C8';
                $colorNormal['hidalgo']                          = '#C8C8C8';
                $colorNormal['jalisco']                          = '#C8C8C8';
                $colorNormal['mexico']                           = '#C8C8C8';
                $colorNormal['michoacan']                        = '#C8C8C8';
                $colorNormal['morelos']                          = '#C8C8C8';
                $colorNormal['nayarit']                          = '#C8C8C8';
                $colorNormal['nuevo_leon']                       = '#C8C8C8';
                $colorNormal['oaxaca']                           = '#C8C8C8';
                $colorNormal['puebla']                           = '#C8C8C8';
                $colorNormal['queretaro']                        = '#C8C8C8';
                $colorNormal['quintana_roo']                     = '#C8C8C8';
                $colorNormal['san_luis_potosi']                  = '#C8C8C8';
                $colorNormal['sinaloa']                          = '#C8C8C8';
                $colorNormal['sonora']                           = '#C8C8C8';
                $colorNormal['tabasco']                          = '#C8C8C8';
                $colorNormal['tamaulipas']                       = '#C8C8C8';
                $colorNormal['tlaxcala']                         = '#C8C8C8';
                $colorNormal['veracruz']                         = '#C8C8C8';
                $colorNormal['yucatan']                          = '#C8C8C8';
                $colorNormal['zacatecas']                        = '#C8C8C8';
                
                //$nameTpT=array();
                $locationCatalog = LocationCatalog::getInstance(0);
                $ipClients       = array();
                foreach ($result as $item) {
                    try {
                        $criteria    = new Criteria();
                        //$criteria->add('ip_start', $item->getIpviewer(), Criteria::LESS_OR_EQUAL, Criteria::INET_ATON);
                        $ipCli       = $this->Dot2LongIP($item->getIpviewer());
                        $ipClients[] = $item->getIpviewer();
                        $criteria->add('ip_start', $ipCli, Criteria::LESS_OR_EQUAL);
                        $criteria->addDescendingOrderByColumn('ip_start');
                        $criteria->setLimit(1);
                        //$resultGeo= $geolocationCatalog->getByCriteria($criteria)->getOne();
                        
                        $location = $locationCatalog->getByCriteria($criteria)->getOne();
                        
                        if ($location instanceof Location) {
                            //$location=$locationCatalog->getById($resultgc->getLocation());
                            
                            
                            if (!is_null($location->getCity()) && $location->getCity() != '-') {
                                $nameregion = $location->getCity();
                            } else {
                                $nameregion = $location->getRegion();
                            }
                            
                            $coordenadas = $location->getLatitude() . ',' . $location->getLongitude();
                            
                            $xmlWorld->push('state', array(
                                'id' => 'point'
                            ));
                            $xmlWorld->element('loc', $coordenadas);
                            $xmlWorld->element('name', $nameregion);
                            $xmlWorld->element('size', '3');
                            $xmlWorld->element('opacity', '50');
                            $xmlWorld->pop();
                            
                            $regionName = $nameregion;
                            $temp       = explode(" ", $regionName);
                            
                            $varName = "";
                            if (count($temp) > 1) {
                                $i = 0;
                                foreach ($temp as $it) {
                                    if ($i == 0) {
                                        $varName .= strtolower($it);
                                    } else {
                                        $varName .= '_' . strtolower($it);
                                    }
                                    $i++;
                                }
                            } else {
                                $varName = strtolower($temp[0]);
                            }
                            //echo "* ".$varName."\n";
                            //$nameTpT[]=$varName;
                            if (!empty($varName)) {
                                switch (true) {
                                    case ($this->findMatch($varName, "aguascalientes") == true):
                                        $dataArray['aguascalientes'] += 1;
                                        $colorHover['aguascalientes']  = '#CC0000';
                                        $colorNormal['aguascalientes'] = '#484F59';
                                        break;
                                    case ($varName == "baja_california"):
                                        //echo "california";
                                        $dataArray['baja_california'] += 1;
                                        $colorHover['baja_california']  = '#CC0000';
                                        $colorNormal['baja_california'] = '#484F59';
                                        break;
                                    case ($varName == "baja_california_sur"):
                                        //echo "baja california";
                                        $dataArray['baja_california_sur'] += 1;
                                        $colorHover['baja_california_sur']  = '#CC0000';
                                        $colorNormal['baja_california_sur'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "campeche") == true):
                                        $dataArray['campeche'] += 1;
                                        $colorHover['campeche']  = '#CC0000';
                                        $colorNormal['campeche'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "chiapas") == true):
                                        $dataArray['chiapas'] += 1;
                                        $colorHover['chiapas']  = '#CC0000';
                                        $colorNormal['chiapas'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "chihuahua") == true):
                                        $dataArray['chihuahua'] += 1;
                                        $colorHover['chihuahua']  = '#CC0000';
                                        $colorNormal['chihuahua'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "coahuila") == true):
                                        $dataArray['coahuila'] += 1;
                                        $colorHover['coahuila']  = '#CC0000';
                                        $colorNormal['coahuila'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "colima") == true):
                                        $dataArray['colima'] += 1;
                                        $colorHover['colima']  = '#CC0000';
                                        $colorNormal['colima'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "distrito federal") == true):
                                        $dataArray['distrito_federal'] += 1;
                                        $colorHover['distrito_federal']  = '#CC0000';
                                        $colorNormal['distrito_federal'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "durango") == true):
                                        $dataArray['durango'] += 1;
                                        $colorHover['durango']  = '#CC0000';
                                        $colorNormal['durango'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "federally administered territory") == true):
                                        $dataArray['federally_administered_territory'] += 1;
                                        $colorHover['federally_administered_territory']  = '#CC0000';
                                        $colorNormal['federally_administered_territory'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "guanajuato") == true):
                                        $dataArray['guanajuato'] += 1;
                                        $colorHover['guanajuato']  = '#CC0000';
                                        $colorNormal['guanajuato'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "guerrero") == true):
                                        $dataArray['guerrero'] += 1;
                                        $colorHover['guerrero']  = '#CC0000';
                                        $colorNormal['guerrero'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "hidalgo") == true):
                                        $dataArray['hidalgo'] += 1;
                                        $colorHover['hidalgo']  = '#CC0000';
                                        $colorNormal['hidalgo'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "jalisco") == true):
                                        $dataArray['jalisco'] += 1;
                                        $colorHover['jalisco']  = '#CC0000';
                                        $colorNormal['jalisco'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "mexico") == true):
                                        $dataArray['mexico'] += 1;
                                        $colorHover['mexico']  = '#CC0000';
                                        $colorNormal['mexico'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "michoacan") == true):
                                        $dataArray['michoacan'] += 1;
                                        $colorHover['michoacan']  = '#CC0000';
                                        $colorNormal['michoacan'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "morelos") == true):
                                        $dataArray['morelos'] += 1;
                                        $colorHover['morelos']  = '#CC0000';
                                        $colorNormal['morelos'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "nayarit") == true):
                                        $dataArray['nayarit'] += 1;
                                        $colorHover['nayarit']  = '#CC0000';
                                        $colorNormal['nayarit'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "nuevo leon") == true):
                                        $dataArray['nuevo_leon'] += 1;
                                        $colorHover['nuevo_leon']  = '#CC0000';
                                        $colorNormal['nuevo_leon'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "oaxaca") == true):
                                        $dataArray['oaxaca'] += 1;
                                        $colorHover['oaxaca']  = '#CC0000';
                                        $colorNormal['oaxaca'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "puebla") == true):
                                        $dataArray['puebla'] += 1;
                                        $colorHover['puebla']  = '#CC0000';
                                        $colorNormal['puebla'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "queretaro") == true):
                                        $dataArray['queretaro'] += 1;
                                        $colorHover['queretaro']  = '#CC0000';
                                        $colorNormal['queretaro'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "quintana roo") == true):
                                        $dataArray['quintana_roo'] += 1;
                                        $colorHover['quintana_roo']  = '#CC0000';
                                        $colorNormal['quintana_roo'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "san luis potosi") == true):
                                        $dataArray['san_luis_potosi'] += 1;
                                        $colorHover['san_luis_potosi']  = '#CC0000';
                                        $colorNormal['san_luis_potosi'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "sinaloa") == true):
                                        $dataArray['sinaloa'] += 1;
                                        $colorHover['sinaloa']  = '#CC0000';
                                        $colorNormal['sinaloa'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "sonora") == true):
                                        $dataArray['sonora'] += 1;
                                        $colorHover['sonora']  = '#CC0000';
                                        $colorNormal['sonora'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "tabasco") == true):
                                        $dataArray['tabasco'] += 1;
                                        $colorHover['tabasco']  = '#CC0000';
                                        $colorNormal['tabasco'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "tamaulipas") == true):
                                        $dataArray['tamaulipas'] += 1;
                                        $colorHover['tamaulipas']  = '#CC0000';
                                        $colorNormal['tamaulipas'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "tlaxcala") == true):
                                        $dataArray['tlaxcala'] += 1;
                                        $colorHover['tlaxcala']  = '#CC0000';
                                        $colorNormal['tlaxcala'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "veracruz") == true):
                                        $dataArray['veracruz'] += 1;
                                        $colorHover['veracruz']  = '#CC0000';
                                        $colorNormal['veracruz'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "yucatan") == true):
                                        $dataArray['yucatan'] += 1;
                                        $colorHover['yucatan']  = '#CC0000';
                                        $colorNormal['yucatan'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "zacatecas") == true):
                                        $dataArray['zacatecas'] += 1;
                                        $colorHover['zacatecas']  = '#CC0000';
                                        $colorNormal['zacatecas'] = '#484F59';
                                        break;
                                }
                            }
                        }
                        
                        //aqui
                    }
                    catch (Exception $e) {
                        echo "Error del tipo: " . $e;
                        return false;
                    }
                }
                $this->getUser()->setAttribute('ipClientsF', $ipClients);
                $this->getUser()->setAttribute('rgeolocation', $dataArray);
            }
            $xmlWorld->push('state', array(
                'id' => 'AF'
            ));
            $xmlWorld->element('name', 'Afghanistan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AL'
            ));
            $xmlWorld->element('name', 'Albania');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AG'
            ));
            $xmlWorld->element('name', 'Algeria');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AN'
            ));
            $xmlWorld->element('name', 'Andorra');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AO'
            ));
            $xmlWorld->element('name', 'Angola');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AC'
            ));
            $xmlWorld->element('name', 'Antigua and Barbuda');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AR'
            ));
            $xmlWorld->element('name', 'Argentina');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AM'
            ));
            $xmlWorld->element('name', 'Armenia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AA'
            ));
            $xmlWorld->element('name', 'Aruba');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AS'
            ));
            $xmlWorld->element('name', 'Australia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AU'
            ));
            $xmlWorld->element('name', 'Austria');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AJ'
            ));
            $xmlWorld->element('name', 'Azerbaijan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BF'
            ));
            $xmlWorld->element('name', 'The Bahamas');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BA'
            ));
            $xmlWorld->element('name', 'Bahrain');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FQ'
            ));
            $xmlWorld->element('name', 'Baker Island');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BG'
            ));
            $xmlWorld->element('name', 'Bangladesh');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BB'
            ));
            $xmlWorld->element('name', 'Barbados');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BO'
            ));
            $xmlWorld->element('name', 'Belarus');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BE'
            ));
            $xmlWorld->element('name', 'Belgium');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BH'
            ));
            $xmlWorld->element('name', 'Belize');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BN'
            ));
            $xmlWorld->element('name', 'Benin');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BD'
            ));
            $xmlWorld->element('name', 'Bermuda');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BT'
            ));
            $xmlWorld->element('name', 'Bhutan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BL'
            ));
            $xmlWorld->element('name', 'Bolivia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BK'
            ));
            $xmlWorld->element('name', 'Bosnia and Herzegovina');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BC'
            ));
            $xmlWorld->element('name', 'Botswana');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BV'
            ));
            $xmlWorld->element('name', 'Bouvet Island');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BR'
            ));
            $xmlWorld->element('name', 'Brazil');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IO'
            ));
            $xmlWorld->element('name', 'British Indian Ocean Territory');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'VI'
            ));
            $xmlWorld->element('name', 'British Virgin Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BX'
            ));
            $xmlWorld->element('name', 'Brunei');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BU'
            ));
            $xmlWorld->element('name', 'Bulgaria');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'UV'
            ));
            $xmlWorld->element('name', 'Burkina Faso');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BY'
            ));
            $xmlWorld->element('name', 'Burundi');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CB'
            ));
            $xmlWorld->element('name', 'Cambodia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CM'
            ));
            $xmlWorld->element('name', 'Cameroon');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CA'
            ));
            $xmlWorld->element('name', 'Canada');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CV'
            ));
            $xmlWorld->element('name', 'Cape Verde');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CJ'
            ));
            $xmlWorld->element('name', 'Cayman Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CT'
            ));
            $xmlWorld->element('name', 'Central African Republic');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CD'
            ));
            $xmlWorld->element('name', 'Chad');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CI'
            ));
            $xmlWorld->element('name', 'Chile');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CH'
            ));
            $xmlWorld->element('name', 'China');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CO'
            ));
            $xmlWorld->element('name', 'Colombia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CN'
            ));
            $xmlWorld->element('name', 'Comoros');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CF'
            ));
            $xmlWorld->element('name', 'Congo');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CW'
            ));
            $xmlWorld->element('name', 'Cook Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CS'
            ));
            $xmlWorld->element('name', 'Costa Rica');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IV'
            ));
            $xmlWorld->element("name", "Cote d'Ivoire");
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'HR'
            ));
            $xmlWorld->element('name', 'Croatia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CU'
            ));
            $xmlWorld->element('name', 'Cuba');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CY'
            ));
            $xmlWorld->element('name', 'Cyprus');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'EZ'
            ));
            $xmlWorld->element('name', 'Czech Republic');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CG'
            ));
            $xmlWorld->element('name', 'Democratic Republic of the Congo');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'DA'
            ));
            $xmlWorld->element('name', 'Denmark');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'DJ'
            ));
            $xmlWorld->element('name', 'Djibouti');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'DO'
            ));
            $xmlWorld->element('name', 'Dominica');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'DR'
            ));
            $xmlWorld->element('name', 'Dominican Republic');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'EC'
            ));
            $xmlWorld->element('name', 'Ecuador');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'EG'
            ));
            $xmlWorld->element('name', 'Egypt');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'ES'
            ));
            $xmlWorld->element('name', 'El Salvador');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'EK'
            ));
            $xmlWorld->element('name', 'Equatorial Guinea');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'ER'
            ));
            $xmlWorld->element('name', 'Eritrea');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'EN'
            ));
            $xmlWorld->element('name', 'Estonia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'ET'
            ));
            $xmlWorld->element('name', 'Ethiopia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FK'
            ));
            $xmlWorld->element('name', 'Falkland Islands (Islas Malvinas)');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FO'
            ));
            $xmlWorld->element('name', 'Faroe Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FM'
            ));
            $xmlWorld->element('name', 'Federated States of Micronesia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FJ'
            ));
            $xmlWorld->element('name', 'Fiji');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FI'
            ));
            $xmlWorld->element('name', 'Finland');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FR'
            ));
            $xmlWorld->element('name', 'France');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FG'
            ));
            $xmlWorld->element('name', 'French Guiana');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FP'
            ));
            $xmlWorld->element('name', 'French Polynesia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GB'
            ));
            $xmlWorld->element('name', 'Gabon');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GA'
            ));
            $xmlWorld->element('name', 'The Gambia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GG'
            ));
            $xmlWorld->element('name', 'Georgia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GM'
            ));
            $xmlWorld->element('name', 'Germany');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GH'
            ));
            $xmlWorld->element('name', 'Ghana');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GI'
            ));
            $xmlWorld->element('name', 'Gibraltar');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GO'
            ));
            $xmlWorld->element('name', 'Glorioso Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GR'
            ));
            $xmlWorld->element('name', 'Greece');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GL'
            ));
            $xmlWorld->element('name', 'Greenland');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GJ'
            ));
            $xmlWorld->element('name', 'Grenada');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GP'
            ));
            $xmlWorld->element('name', 'Guadeloupe');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GQ'
            ));
            $xmlWorld->element('name', 'Guam');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GT'
            ));
            $xmlWorld->element('name', 'Guatemala');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GK'
            ));
            $xmlWorld->element('name', 'Guernsey');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PU'
            ));
            $xmlWorld->element('name', 'Guinea-Bissau');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GV'
            ));
            $xmlWorld->element('name', 'Guinea');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GY'
            ));
            $xmlWorld->element('name', 'Guyana');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'HA'
            ));
            $xmlWorld->element('name', 'Haiti');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'HM'
            ));
            $xmlWorld->element('name', 'Heard Island &amp; McDonald Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'HO'
            ));
            $xmlWorld->element('name', 'Honduras');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'HU'
            ));
            $xmlWorld->element('name', 'Hungary');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IC'
            ));
            $xmlWorld->element('name', 'Iceland');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IN'
            ));
            $xmlWorld->element('name', 'India');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IO'
            ));
            $xmlWorld->element('name', 'British Indian Ocean Territory');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'ID'
            ));
            $xmlWorld->element('name', 'Indonesia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IR'
            ));
            $xmlWorld->element('name', 'Iran');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IZ'
            ));
            $xmlWorld->element('name', 'Iraq');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'EI'
            ));
            $xmlWorld->element('name', 'Ireland');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IM'
            ));
            $xmlWorld->element('name', 'Isle of Man');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IS'
            ));
            $xmlWorld->element('name', 'Israel');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GZ'
            ));
            $xmlWorld->element('name', 'Palestinian Authority');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IT'
            ));
            $xmlWorld->element('name', 'Italy');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'JM'
            ));
            $xmlWorld->element('name', 'Jamaica');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'JN'
            ));
            $xmlWorld->element('name', 'Jan Mayen');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'JA'
            ));
            $xmlWorld->element('name', 'Japan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'DQ'
            ));
            $xmlWorld->element('name', 'Jarvis Island');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'JE'
            ));
            $xmlWorld->element('name', 'Jersey');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'JQ'
            ));
            $xmlWorld->element('name', 'Johnston Atoll');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'JO'
            ));
            $xmlWorld->element('name', 'Jordan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'JU'
            ));
            $xmlWorld->element('name', 'Juan De Nova Island');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'KZ'
            ));
            $xmlWorld->element('name', 'Kazakhstan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'KE'
            ));
            $xmlWorld->element('name', 'Kenya');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'KR'
            ));
            $xmlWorld->element('name', 'Kiribati');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'KS'
            ));
            $xmlWorld->element('name', 'South Korea');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'KU'
            ));
            $xmlWorld->element('name', 'Kuwait');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'KG'
            ));
            $xmlWorld->element('name', 'Kyrgyzstan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LA'
            ));
            $xmlWorld->element('name', 'Laos');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LG'
            ));
            $xmlWorld->element('name', 'Latvia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LE'
            ));
            $xmlWorld->element('name', 'Lebanon');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LT'
            ));
            $xmlWorld->element('name', 'Lesotho');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LI'
            ));
            $xmlWorld->element('name', 'Liberia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LY'
            ));
            $xmlWorld->element('name', 'Libya');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LS'
            ));
            $xmlWorld->element('name', 'Liechtenstein');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LH'
            ));
            $xmlWorld->element('name', 'Lithuania');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LU'
            ));
            $xmlWorld->element('name', 'Luxembourg');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MC'
            ));
            $xmlWorld->element('name', 'Macau');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MK'
            ));
            $xmlWorld->element('name', 'Macedonia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MA'
            ));
            $xmlWorld->element('name', 'Madagascar');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MI'
            ));
            $xmlWorld->element('name', 'Malawi');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MY'
            ));
            $xmlWorld->element('name', 'Malaysia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MV'
            ));
            $xmlWorld->element('name', 'Maldives');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'ML'
            ));
            $xmlWorld->element('name', 'Mali');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MT'
            ));
            $xmlWorld->element('name', 'Malta');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'RM'
            ));
            $xmlWorld->element('name', 'Marshall Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MB'
            ));
            $xmlWorld->element('name', 'Martinique');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MR'
            ));
            $xmlWorld->element('name', 'Mauritania');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MP'
            ));
            $xmlWorld->element('name', 'Mauritius');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MF'
            ));
            $xmlWorld->element('name', 'Mayotte');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MX'
            ));
            $xmlWorld->element('name', 'Mexico');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MQ'
            ));
            $xmlWorld->element('name', 'Midway Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MD'
            ));
            $xmlWorld->element('name', 'Moldova');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MN'
            ));
            $xmlWorld->element('name', 'Monaco');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MG'
            ));
            $xmlWorld->element('name', 'Mongolia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MH'
            ));
            $xmlWorld->element('name', 'Montserrat');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MO'
            ));
            $xmlWorld->element('name', 'Morocco');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MZ'
            ));
            $xmlWorld->element('name', 'Mozambique');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BM'
            ));
            $xmlWorld->element('name', 'Myanmar (Burma)');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'WA'
            ));
            $xmlWorld->element('name', 'Namibia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NR'
            ));
            $xmlWorld->element('name', 'Nauru');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NP'
            ));
            $xmlWorld->element('name', 'Nepal');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NT'
            ));
            $xmlWorld->element('name', 'Netherlands Antilles');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NL'
            ));
            $xmlWorld->element('name', 'Netherlands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NC'
            ));
            $xmlWorld->element('name', 'New Caledonia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NZ'
            ));
            $xmlWorld->element('name', 'New Zealand');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NU'
            ));
            $xmlWorld->element('name', 'Nicaragua');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NG'
            ));
            $xmlWorld->element('name', 'Niger');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NI'
            ));
            $xmlWorld->element('name', 'Nigeria');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NE'
            ));
            $xmlWorld->element('name', 'Niue');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NF'
            ));
            $xmlWorld->element('name', 'Norfolk Island');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'KN'
            ));
            $xmlWorld->element('name', 'North Korea');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CQ'
            ));
            $xmlWorld->element('name', 'Northern Mariana Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NO'
            ));
            $xmlWorld->element('name', 'Norway');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MU'
            ));
            $xmlWorld->element('name', 'Oman');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PS'
            ));
            $xmlWorld->element('name', 'Pacific Islands (Palau)');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PK'
            ));
            $xmlWorld->element('name', 'Pakistan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PM'
            ));
            $xmlWorld->element('name', 'Panama');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PP'
            ));
            $xmlWorld->element('name', 'Papua New Guinea');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PA'
            ));
            $xmlWorld->element('name', 'Paraguay');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PE'
            ));
            $xmlWorld->element('name', 'Peru');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'RP'
            ));
            $xmlWorld->element('name', 'Philippines');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PL'
            ));
            $xmlWorld->element('name', 'Poland');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PO'
            ));
            $xmlWorld->element('name', 'Portugal');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'RQ'
            ));
            $xmlWorld->element('name', 'Puerto Rico');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'QA'
            ));
            $xmlWorld->element('name', 'Qatar');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'RE'
            ));
            $xmlWorld->element('name', 'Reunion');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'RO'
            ));
            $xmlWorld->element('name', 'Romania');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'RS'
            ));
            $xmlWorld->element('name', 'Russia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'RW'
            ));
            $xmlWorld->element('name', 'Rwanda');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SM'
            ));
            $xmlWorld->element('name', 'San Marino');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TP'
            ));
            $xmlWorld->element('name', 'Sao Tome and Principe');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SA'
            ));
            $xmlWorld->element('name', 'Saudi Arabia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SG'
            ));
            $xmlWorld->element('name', 'Senegal');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'KV'
            ));
            $xmlWorld->element('name', 'Kosovo');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MW'
            ));
            $xmlWorld->element('name', 'Montenegro');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SR'
            ));
            $xmlWorld->element('name', 'Serbia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SE'
            ));
            $xmlWorld->element('name', 'Seychelles');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SL'
            ));
            $xmlWorld->element('name', 'Sierra Leone');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SN'
            ));
            $xmlWorld->element('name', 'Singapore');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LO'
            ));
            $xmlWorld->element('name', 'Slovakia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SI'
            ));
            $xmlWorld->element('name', 'Slovenia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BP'
            ));
            $xmlWorld->element('name', 'Solomon Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SO'
            ));
            $xmlWorld->element('name', 'Somalia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SF'
            ));
            $xmlWorld->element('name', 'South Africa');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SX'
            ));
            $xmlWorld->element('name', 'South Georgia and the South Sandwich Is');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SP'
            ));
            $xmlWorld->element('name', 'Madrid');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SP'
            ));
            $xmlWorld->element('name', 'Spain');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PG'
            ));
            $xmlWorld->element('name', 'Spratly Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CE'
            ));
            $xmlWorld->element('name', 'Sri Lanka');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SC'
            ));
            $xmlWorld->element('name', 'St. Kitts and Nevis');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'ST'
            ));
            $xmlWorld->element('name', 'St. Lucia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SB'
            ));
            $xmlWorld->element('name', 'St. Pierre and Miquelon');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'VC'
            ));
            $xmlWorld->element('name', 'St. Vincent and the Grenadines');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SU'
            ));
            $xmlWorld->element('name', 'Sudan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NS'
            ));
            $xmlWorld->element('name', 'Suriname');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SV'
            ));
            $xmlWorld->element('name', 'Svalbard');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'WZ'
            ));
            $xmlWorld->element('name', 'Swaziland');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SW'
            ));
            $xmlWorld->element('name', 'Sweden');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SZ'
            ));
            $xmlWorld->element('name', 'Switzerland');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SY'
            ));
            $xmlWorld->element('name', 'Syria');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TE'
            ));
            $xmlWorld->element('name', 'East Timor');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TW'
            ));
            $xmlWorld->element('name', 'Taiwan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TI'
            ));
            $xmlWorld->element('name', 'Tajikistan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TZ'
            ));
            $xmlWorld->element('name', 'United Republic of Tanzania');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TH'
            ));
            $xmlWorld->element('name', 'Thailand');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TO'
            ));
            $xmlWorld->element('name', 'Togo');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TL'
            ));
            $xmlWorld->element('name', 'Tokelau');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TN'
            ));
            $xmlWorld->element('name', 'Tonga');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TD'
            ));
            $xmlWorld->element('name', 'Trinidad and Tobago');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TS'
            ));
            $xmlWorld->element('name', 'Tunisia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TU'
            ));
            $xmlWorld->element('name', 'Turkey');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TX'
            ));
            $xmlWorld->element('name', 'Turkmenistan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TK'
            ));
            $xmlWorld->element('name', 'Turks and Caicos Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TV'
            ));
            $xmlWorld->element('name', 'Tuvalu');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'UG'
            ));
            $xmlWorld->element('name', 'Uganda');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'UP'
            ));
            $xmlWorld->element('name', 'Ukraine');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TC'
            ));
            $xmlWorld->element('name', 'United Arab Emirates');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'UK'
            ));
            $xmlWorld->element('name', 'United Kingdom');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'US'
            ));
            $xmlWorld->element('name', 'United States');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'UY'
            ));
            $xmlWorld->element('name', 'Uruguay');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'UZ'
            ));
            $xmlWorld->element('name', 'Uzbekistan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NH'
            ));
            $xmlWorld->element('name', 'Vanuatu');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'VE'
            ));
            $xmlWorld->element('name', 'Venezuela');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'VM'
            ));
            $xmlWorld->element('name', 'Vietnam');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'WQ'
            ));
            $xmlWorld->element('name', 'Wake Island');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'WI'
            ));
            $xmlWorld->element('name', 'Western Sahara');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'WS'
            ));
            $xmlWorld->element('name', 'Western Samoa');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'YM'
            ));
            $xmlWorld->element('name', 'Yemen');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'ZA'
            ));
            $xmlWorld->element('name', 'Zambia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'ZI'
            ));
            $xmlWorld->element('name', 'Zimbabwe');
            $xmlWorld->pop();
            
            
            $xmlWorld->pop();
            $fp = fopen($this->getRegistry()->config->xmlFiles->path . "\\worldmap\\data.xml", 'w+');
            fwrite($fp, $xmlWorld->getXml());
            fclose($fp);
            /*
            var_dump($result);
            die();*/
            
            if (!$result->isEmpty()) {
                $indice = 0;
                
                $xml = new XmlWriter2();
                $xml->push('map', array(
                    'map_file' => 'ammap/maps/mexico.swf',
                    'tl_long' => '-118.390549',
                    'tl_lat' => '32.77794',
                    'br_long' => '-86.735161',
                    'br_lat' => '14.59382',
                    'zoom' => '86.6071%',
                    'zoom_x' => '5.69%',
                    'zoom_y' => '8.75%'
                ));
                $xml->push('areas');
                
                $xml->element('area', '', array(
                    'mc_name' => 'aguascalientes',
                    'title' => 'Aguascalientes: ' . (Int) $dataArray['aguascalientes'] . ' visitante(s)',
                    'color_hover' => $colorHover['aguascalientes'],
                    'color' => $colorNormal['aguascalientes']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'baja_california',
                    'title' => 'Baja California: ' . (Int) $dataArray['baja_california'] . ' visitante(s)',
                    'color_hover' => $colorHover['baja_california'],
                    'color' => $colorNormal['baja_california']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'baja_california_sur',
                    'title' => 'Baja California Sur: ' . (Int) $dataArray['baja_california_sur'] . ' visitante(s)',
                    'color_hover' => $colorHover['baja_california_sur'],
                    'color' => $colorNormal['baja_california_sur']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'campeche',
                    'title' => 'Campeche: ' . (Int) $dataArray['campeche'] . ' visitante(s)',
                    'color_hover' => $colorHover['campeche'],
                    'color' => $colorNormal['campeche']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'chiapas',
                    'title' => 'Chiapas: ' . (Int) $dataArray['chiapas'] . ' visitante(s)',
                    'color_hover' => $colorHover['chiapas'],
                    'color' => $colorNormal['chiapas']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'chihuahua',
                    'title' => 'Chihuahua: ' . (Int) $dataArray['chihuahua'] . ' visitante(s)',
                    'color_hover' => $colorHover['chihuahua'],
                    'color' => $colorNormal['chihuahua']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'coahuila',
                    'title' => 'Coahuila: ' . (Int) $dataArray['coahuila'] . ' visitante(s)',
                    'color_hover' => $colorHover['coahuila'],
                    'color' => $colorNormal['coahuila']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'colima',
                    'title' => 'Colima: ' . (Int) $dataArray['colima'] . ' visitante(s)',
                    'color_hover' => $colorHover['colima'],
                    'color' => $colorNormal['colima']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'distrito_federal',
                    'title' => 'Distrito Federal (Mexico): ' . (Int) $dataArray['distrito_federal'] . ' visitante(s)',
                    'color_hover' => $colorHover['distrito_federal'],
                    'color' => $colorNormal['distrito_federal']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'durango',
                    'title' => 'Durango: ' . (Int) $dataArray['durango'] . ' visitante(s)',
                    'color_hover' => $colorHover['durango'],
                    'color' => $colorNormal['durango']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'federally_administered_territory',
                    'title' => 'Federally Administered Territory: ' . (Int) $dataArray['federally_administered_territory'] . ' visitante(s)',
                    'color_hover' => $colorHover['federally_administered_territory'],
                    'color' => $colorNormal['federally_administered_territory']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'guanajuato',
                    'title' => 'Guanajuato: ' . (Int) $dataArray['guanajuato'] . ' visitante(s)',
                    'color_hover' => $colorHover['guanajuato'],
                    'color' => $colorNormal['guanajuato']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'guerrero',
                    'title' => 'Guerrero: ' . (Int) $dataArray['guerrero'] . ' visitante(s)',
                    'color_hover' => $colorHover['guerrero'],
                    'color' => $colorNormal['guerrero']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'hidalgo',
                    'title' => 'Hidalgo: ' . (Int) $dataArray['hidalgo'] . ' visitante(s)',
                    'color_hover' => $colorHover['hidalgo'],
                    'color' => $colorNormal['hidalgo']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'jalisco',
                    'title' => 'Jalisco: ' . (Int) $dataArray['jalisco'] . ' visitante(s)',
                    'color_hover' => $colorHover['jalisco'],
                    'color' => $colorNormal['jalisco']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'mexico',
                    'title' => 'Mexico: ' . (Int) $dataArray['mexico'] . ' visitante(s)',
                    'color_hover' => $colorHover['mexico'],
                    'color' => $colorNormal['mexico']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'michoacan',
                    'title' => 'Michoacan: ' . (Int) $dataArray['michoacan'] . ' visitante(s)',
                    'color_hover' => $colorHover['michoacan'],
                    'color' => $colorNormal['michoacan']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'morelos',
                    'title' => 'Morelos: ' . (Int) $dataArray['morelos'] . ' visitante(s)',
                    'color_hover' => $colorHover['morelos'],
                    'color' => $colorNormal['morelos']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'nayarit',
                    'title' => 'Nayarit: ' . (Int) $dataArray['nayarit'] . ' visitante(s)',
                    'color_hover' => $colorHover['nayarit'],
                    'color' => $colorNormal['nayarit']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'nuevo_leon',
                    'title' => 'Nuevo Leon: ' . (Int) $dataArray['nuevo_leon'] . ' visitante(s)',
                    'color_hover' => $colorHover['nuevo_leon'],
                    'color' => $colorNormal['nuevo_leon']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'oaxaca',
                    'title' => 'Oaxaca: ' . (Int) $dataArray['oaxaca'] . ' visitante(s)',
                    'color_hover' => $colorHover['oaxaca'],
                    'color' => $colorNormal['oaxaca']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'puebla',
                    'title' => 'Puebla: ' . (Int) $dataArray['puebla'] . ' visitante(s)',
                    'color_hover' => $colorHover['puebla'],
                    'color' => $colorNormal['puebla']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'queretaro',
                    'title' => 'Queretaro: ' . (Int) $dataArray['queretaro'] . ' visitante(s)',
                    'color_hover' => $colorHover['queretaro'],
                    'color' => $colorNormal['queretaro']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'quintana_roo',
                    'title' => 'Quintana Roo: ' . (Int) $dataArray['quintana_roo'] . ' visitante(s)',
                    'color_hover' => $colorHover['quintana_roo'],
                    'color' => $colorNormal['quintana_roo']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'san_luis_potosi',
                    'title' => 'San Luis Potosi: ' . (Int) $dataArray['san_luis_potosi'] . ' visitante(s)',
                    'color_hover' => $colorHover['san_luis_potosi'],
                    'color' => $colorNormal['san_luis_potosi']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'sinaloa',
                    'title' => 'Sinaloa: ' . (Int) $dataArray['sinaloa'] . ' visitante(s)',
                    'color_hover' => $colorHover['sinaloa'],
                    'color' => $colorNormal['sinaloa']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'sonora',
                    'title' => 'Sonora: ' . (Int) $dataArray['sonora'] . ' visitante(s)',
                    'color_hover' => $colorHover['sonora'],
                    'color' => $colorNormal['sonora']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'tabasco',
                    'title' => 'Tabasco: ' . (Int) $dataArray['tabasco'] . ' visitante(s)',
                    'color_hover' => $colorHover['tabasco'],
                    'color' => $colorNormal['tabasco']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'tamaulipas',
                    'title' => 'Tamaulipas: ' . (Int) $dataArray['tamaulipas'] . ' visitante(s)',
                    'color_hover' => $colorHover['tamaulipas'],
                    'color' => $colorNormal['tamaulipas']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'tlaxcala',
                    'title' => 'Tlaxcala: ' . (Int) $dataArray['tlaxcala'] . ' visitante(s)',
                    'color_hover' => $colorHover['tlaxcala'],
                    'color' => $colorNormal['tlaxcala']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'veracruz',
                    'title' => 'Veracruz: ' . (Int) $dataArray['veracruz'] . ' visitante(s)',
                    'color_hover' => $colorHover['veracruz'],
                    'color' => $colorNormal['veracruz']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'yucatan',
                    'title' => 'Yucatan: ' . (Int) $dataArray['yucatan'] . ' visitante(s)',
                    'color_hover' => $colorHover['yucatan'],
                    'color' => $colorNormal['yucatan']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'zacatecas',
                    'title' => 'Zacatecas: ' . (Int) $dataArray['zacatecas'] . ' visitante(s)',
                    'color_hover' => $colorHover['zacatecas'],
                    'color' => $colorNormal['zacatecas']
                ));
                
                $xml->pop();
                /*
                $xml->push('movies');             
                $xml->pop();*/
                $xml->pop();
                //$this->view->content=$xml->getXml();
                //$this->view->setTpl('xml');
                $fp = fopen($this->getRegistry()->config->xmlFiles->path . "\\ammap\\ammap_data.xml", 'w+');
                fwrite($fp, $xml->getXml());
                fclose($fp);
            }
        } else {
            $this->setFlash("error", "No se recibio ningun valor");
            $this->_redirect('index/index');
        }
        
        //print_r($nameTpT);
        $this->setTitle('Histórico por ubicación');
        $this->view->dates = $dates;
        $this->view->setTpl('geolocationProcess');
    }
    
    public function downloadGeolocationAction()
    {
        require_once 'lib/excel/excel.php';
        require_once 'lib/excel/excel-ext.php';
        $this->view->setLayoutFile(false);
        $this->_helper->viewRenderer->setNoRender();
        $geolocation      = $this->getUser()->getAttribute('rgeolocation', 0);
        $ips              = $this->getUser()->getAttribute('ipClientsF', $ipClients);
        $totalViewersByIp = $this->getUser()->getAttribute('totalviewersbyip', 0);
        
        set_time_limit(0);
        
        $assoc = array();
        
        $visitasMex = 0;
        if (count($geolocation) > 0) {
            foreach ($geolocation as $key => $value) {
                $assoc[] = array(
                    "Entidad" => $key,
                    "Visitantes" => $value,
                    "Ip de acceso" => ""
                );
                $visitasMex += $value;
            }
        }
        
        if (count($ips) > 0) {
            foreach ($ips as $ip) {
                $assoc[] = array(
                    "Ip de acceso" => $ip
                );
            }
        }
        
        $assoc[] = array(
            "Entidad" => "Total visitas Mexico: ",
            "Visitantes" => $visitasMex
        );
        $assoc[] = array(
            "Entidad" => "Total visitas General: ",
            "Visitantes" => $totalViewersByIp
        );
        
        createExcel("reporte-geoMexico.xls", $assoc);
        
        exit;
    }
    
    public function testIpAction()
    {
        require_once 'Zend/Soap/Client.php';
        
        $this->view->setLayoutFile(false);
        $this->_helper->viewRenderer->setNoRender();
        
        $client = new Zend_Soap_Client($this->_WSDL_URI);
        
        $param = array(
            "ip" => "186.42.229.177"
        );
        
        try {
            //$result = $client->getLocation($param);
            $result = $client->__call('getLocation', $param);
            if (count($result) > 0) {
                print_r($result);
                echo utf8_decode($result['ciudad']);
            } else {
                echo "No retorno datos";
            }
        }
        catch (Exception $e) {
            echo "Error del tipo: " . $e;
            return false;
        }
        
    }
    
    public function globalAction()
    {
        $this->view->setTpl("test");
    }
   
    
    public function streamHelixReportAction()
    {
        require_once 'application/models/catalogs/HelixStreamCatalog.php';
        
        $id             = $this->getUser()->getIdUser();
        $serverCatalog  = StreamServerCatalog::getInstance();
        $companyCatalog = CompanyCatalog::getInstance();
        
        $company = $companyCatalog->getByUser($id)->getOne();
        
        if (!$company instanceof Company) {
            $this->setFlash("error", "No existe ningun cliente asociado");
            $this->_redirect('index/index');
        }
        
        $server = $serverCatalog->getByCompany($company->getIdCompany())->getOne();
        if (!$server instanceof StreamServer) {
            $this->setFlash("error", "No existe ningun servidor asociado");
            $this->_redirect('index/index');
        }
        
        
        $criteria = new Criteria();
        $criteria->add('id_company', $company->getIdCompany(), Criteria::EQUAL);
        
        //$data = HelixStreamCatalog::getInstance()->getHelixStreamHelixLiveRelations($criteria);
         $data=$this->liveRegresar($company->getIdCompany());
		if(!is_null($data)){
	   foreach ($data as $item) {
            $ids[] = $item['id_stream_helix'];
        }
			if(!is_null($ids)){
			$streams = HelixStreamCatalog::getInstance()->getByIds($ids)->toCombo();
			}
        }
        $this->view->idCompany = $company->getIdCompany();
        $this->view->idServer  = $server->getIdServer();
        $this->view->streams   = $streams;
        $this->setTitle('Histórico de Helix Server LiveStream');
        $this->view->setTpl('StreamHelixReport');
    }
    
    public function processStreamHelixAction()
    {
        require_once 'application/models/catalogs/HelixStreamStatCatalog.php';
        require_once 'application/models/catalogs/HelixStreamCatalog.php';
        require_once 'application/models/catalogs/HelixIpviewerStreamCatalog.php';
        	require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Log.php';
	require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Log\Writer\Stream.php';
	require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Exception.php';
	$logger = new Zend_Log();
	$writer = new Zend_Log_Writer_Stream('c:/file.txt');
        $logger->addWriter($writer);
        set_time_limit(0);
        $dateStart  = $this->getRequest()->getParam('date_start');
        $dateEnd    = $this->getRequest()->getParam('date_end');
        $idCompany  = $this->getRequest()->getParam('idCompany');
        $idServer   = $this->getRequest()->getParam('idServer');
        $initHour   = $this->getRequest()->getParam('initHour');
        $finishHour = $this->getRequest()->getParam('finishHour');
        $idStream   = $this->getRequest()->getParam('stream');
        $groupBy    = $this->getRequest()->getParam('groupBy');
        
        
        $dates = array();
        
        if ((!is_null($dateStart) & !empty($dateStart)) && (!is_null($dateEnd) & !empty($dateEnd))) {
            $dates[] = $dateStart . ' ' . $initHour . ':00';
            $dates[] = $dateEnd . ' ' . $finishHour . ':00';
        } else {
            if (!is_null($dateStart) & !empty($dateStart)) {
                $dates[] = $dateStart . ' ' . $initHour . ':00';
                $dates[] = $dateStart . ' ' . $finishHour . ':00';
            }
            if (!is_null($dateEnd) & !empty($dateEnd)) {
                $dates[] = $dateEnd . ' ' . $initHour . ':00';
                $dates[] = $dateEnd . ' ' . $finishHour . ':00';
            }
        }
        
        $promedio = 0;
        if ($idStream != 0) {
            $criteria = new Criteria();
            $criteria->add('id_stream', $idStream, Criteria::EQUAL);
            
            $criteria->add('`timestamp`', $dates, Criteria::BETWEEN);
            $criteria->addGroupByColumn('`timestamp`');
            
            $criteria2 = new Criteria();
            $criteria2->add('id_stream', $idStream, Criteria::EQUAL);
            $criteria2->add('`timestamp`', $dates, Criteria::BETWEEN);
            
            
            $liveStat = HelixStreamStatCatalog::getInstance(1)->getByCriteria($criteria, false);
            
            $liveIp = HelixIpviewerStreamCatalog::getInstance(1)->getByCriteria($criteria2);
            
            
            $win     = 0;
            $bb      = 0;
            $android = 0;
            $ipad    = 0;
            $iphone  = 0;
            $ipod    = 0;
            $otro    = 0;
            
            $rtmp = 0;
            $rtsp = 0;
            $http = 0;
            $con  = 0;
            if (!$liveIp->isEmpty()) {
                foreach ($liveIp as $item) {
                    switch ($item->getProtocol()) {
                        case 1:
                            $rtmp += 1;
                            break;
                        case 2:
                            $rtsp += 1;
                            break;
                        case 3:
                            $http += 1;
                            break;
                    }
                    
                    switch ($item->getUserAgent()) {
                        case 1:
                            $win += 1;
                            break;
                        case 2:
                            $bb += 1;
                            break;
                        case 3:
                            $android += 1;
                            break;
                        case 4:
                            $iphone += 1;
                            break;
                        case 5:
                            $ipad += 1;
                            break;
                        case 6:
                            $ipod += 1;
                            break;
                        case 7:
                            $otro += 1;
                            break;
                    }
                    $con++;
                }
            }
            

            $totalViews  = 0;
            $server1     = 0;
            $server2     = 0;
            $server3     = 0;
            $server4     = 0;
            $server5     = 0;
            $server6     = 0;
            $serverDate1 = "";
            $serverDate2 = "";
            $serverDate3 = "";
            $serverDate4 = "";
            $serverDate5 = "";
            $serverDate6 = "";
            $connects    = 0;
            $newConnects = 0;
            $unicos      = 0;
            $dateAcum    = $dateStart . ' ' . $initHour . ':00';
            
            $initDate      = explode(" ", $dateStart);
            $var           = "";
            $fechaMultiple = $initDate[0];
            $server        = 0;
            
            for ($index = 0; $index <= $this->getDiasDiferencia($dateStart, $dateEnd); $index++) {
                $i       = 0;
                $tempvar = 0;
                foreach ($liveStat as $data) {
                    $aux = $data->getTimestamp();
                    
                    
                    if (!(strpos($data->getTimestamp(), $fechaMultiple) === false)) {
                        if (!empty($serverDate1) && !empty($aux)) {
                            if ($this->retornaDiferencia($serverDate1, $aux) == 0) {
                                $server1 = 0;
                            }
                            
                        }
                        if (!empty($serverDate2) & !empty($aux)) {
                            if ($this->retornaDiferencia($serverDate2, $aux) == 0) {
                                $server2 = 0;
                            }
                            
                        }
                        if (!empty($serverDate3) & !empty($aux)) {
                            if ($this->retornaDiferencia($serverDate3, $aux) == 0) {
                                $server3 = 0;
                            }
                            
                        }
                        if (!empty($serverDate4) & !empty($aux)) {
                            if ($this->retornaDiferencia($serverDate4, $aux) == 0) {
                                $server4 = 0;
                            }
                            
                        }
                        if (!empty($serverDate5) & !empty($aux)) {
                            if ($this->retornaDiferencia($serverDate5, $aux) == 0) {
                                $server5 = 0;
                            }
                            
                        }
                        if (!empty($serverDate6) & !empty($aux)) {
                            if ($this->retornaDiferencia($serverDate6, $aux) == 0) {
                                $server6 = 0;
                            }
                            
                        }
                        switch ($data->getServer()) {
                            case 1:
                                
                                if ($i != 0 && $this->retornaDiferencia($dateAcum, $data->getTimestamp()) == 0) {
                                    $server1 = 0;
                                }
                                
                                if (strcmp($data->getTimestamp(), $serverDate1) != 0) {
                                    if ($data->getConnects() > $server1) {
                                        $newConnects = $data->getConnects() - $server1;
                                        $unicos      = $data->getConnects() - $server1;
                                        $server1     = $data->getConnects();
                                        
                                    } else if ($data->getConnects() == $server1) {
                                        $server1 = $server1;
                                    } else {
                                        $server1 = $data->getConnects();
                                    }
                                    
                                }
                                $serverDate1 = $data->getTimestamp();
                                break;
                            case 2:
                                if ($i != 0 && $this->retornaDiferencia($dateAcum, $data->getTimestamp()) == 0) {
                                    $server2 = 0;
                                    
                                }
                                
                                if (strcmp($data->getTimestamp(), $serverDate2) != 0) {
                                    if ($data->getConnects() > $server2) {
                                        $newConnects = $data->getConnects() - $server2;
                                        $unicos      = $data->getConnects() - $server2;
                                        $server2     = $data->getConnects();
                                        
                                    } else if ($data->getConnects() == $server2) {
                                        $server2 = $server2;
                                    } else {
                                        $server2 = $data->getConnects();
                                    }
                                    
                                }
                                $serverDate2 = $data->getTimestamp();
                                break;
                            
                            case 3:
                                if ($i != 0 && $this->retornaDiferencia($dateAcum, $data->getTimestamp()) == 0) {
                                    $server3 = 0;
                                }
                                
                                
                                if (strcmp($data->getTimestamp(), $serverDate3) != 0) {
                                    if ($data->getConnects() > $server3) {
                                        $newConnects = $data->getConnects() - $server3;
                                        $unicos      = $data->getConnects() - $server3;
                                        $server3     = $data->getConnects();
                                        
                                    } else if ($data->getConnects() == $server3) {
                                        $server3 = $server3;
                                    } else {
                                        $server3 = $data->getConnects();
                                    }
                                    
                                }
                                $serverDate3 = $data->getTimestamp();
                                break;
                            
                            case 4:
                                if ($i != 0 && $this->retornaDiferencia($dateAcum, $data->getTimestamp()) == 0) {
                                    $server4 = 0;
                                }
                                
                                
                                if (strcmp($data->getTimestamp(), $serverDate4) != 0) {
                                    if ($data->getConnects() > $server4) {
                                        $newConnects = $data->getConnects() - $server4;
                                        $unicos      = $data->getConnects() - $server4;
                                        $server4     = $data->getConnects();
                                        
                                    } else if ($data->getConnects() == $server4) {
                                        $server4 = $server4;
                                    } else {
                                        $server4 = $data->getConnects();
                                    }
                                    
                                }
                                $serverDate4 = $data->getTimestamp();
                                break;
                            
                            case 5:
                                if ($i != 0 && $this->retornaDiferencia($dateAcum, $data->getTimestamp()) == 0) {
                                    $server5 = 0;
                                }
                                
                                if (strcmp($data->getTimestamp(), $serverDate5) != 0) {
                                    if ($data->getConnects() > $server5) {
                                        $newConnects = $data->getConnects() - $server5;
                                        $unicos      = $data->getConnects() - $server5;
                                        $server5     = $data->getConnects();
                                    } else if ($data->getConnects() == $server5) {
                                        $server5 = $server5;
                                    } else {
                                        $server5 = $data->getConnects();
                                    }
                                    
                                }
                                $serverDate5 = $data->getTimestamp();
                                break;
                            
                            case 6:
                                if ($i != 0 && $this->retornaDiferencia($dateAcum, $data->getTimestamp()) == 0) {
                                    $server6 = 0;
                                }
                                
                                
                                if (strcmp($data->getTimestamp(), $serverDate6) != 0) {
                                    if ($data->getConnects() > $server6) {
                                        $newConnects = $data->getConnects() - $server6;
                                        $unicos      = $data->getConnects() - $server6;
                                        $server6     = $data->getConnects();
                                    } else if ($data->getConnects() == $server6) {
                                        $server6 = $server6;
                                    } else {
                                        $server6 = $data->getConnects();
                                    }
                                    
                                }
                                $serverDate6 = $data->getTimestamp();
                                break;
                                
                        }
                        
                        $totalViews += $newConnects;
                        $dateAcum = $data->getTimestamp();
                        
                        $newConnects = 0;
                        $unicos      = 0;
                        
                    }
                    $i++;
                }
                
                $fechaMultiple = $this->getNextDate($fechaMultiple);
                
            }
            
            
            $flagData = true;
        } else {
            $flagData = false;
        }
        if($totalViews>0){
		   $flagData=true;	
		}else{
		$flagData=false;
		}
        
        
    
        $stream = HelixStreamCatalog::getInstance()->getById($idStream);
        
        
        $this->view->flagData   = $flagData;
        $this->view->promedio   = $promedio;
        $this->view->stream     = $stream;
        $this->view->dateStart  = $dateStart;
        $this->view->dateEnd    = $dateEnd;
        $this->view->initHour   = $initHour;
        $this->view->finishHour = $finishHour;
        $this->view->dates      = $dates;
        $this->view->groupBy    = $groupBy;
        
        $sumaDevices = $win + $android + $bb + $iphone + $ipod + $ipad + $otro;
        
        
        $promWin = round(($win * 100) / $sumaDevices);
        $promAnd = round(($android * 100) / $sumaDevices);
        $promBB  = round(($bb * 100) / $sumaDevices);
        $promIph = round(($iphone * 100) / $sumaDevices);
        $promIpo = round(($ipod * 100) / $sumaDevices);
        $promIpa = round(($ipad * 100) / $sumaDevices);
        $promOtr = round(($otro * 100) / $sumaDevices);
        
        $this->view->windows = round(($totalViews * $promWin) / 100);
        $this->view->bb      = round(($totalViews * $promBB) / 100);
        $this->view->android = round(($totalViews * $promAnd) / 100);
        $this->view->iphone  = round(($totalViews * $promIph) / 100);
        $this->view->ipad    = round(($totalViews * $promIpa) / 100);
        $this->view->ipod    = round(($totalViews * $promIpo) / 100);
        $this->view->other   = round(($totalViews * $promOtr) / 100);
        
        
        $this->view->rtmp         = $rtmp;
        $this->view->rtsp         = $rtsp;
        $this->view->http         = $http;
        $this->view->sumaProtocol = $rtmp + $rtsp + $http;
        //echo $rtsp;
        $this->view->setTpl("StreamHelixProcess");
    }
    
	public function processTableHelixAction(){
	    require_once 'application/models/catalogs/HelixStreamStatCatalog.php';
		require_once 'application/models/catalogs/HelixStreamCatalog.php';
		require_once 'application/models/catalogs/HelixIpviewerStreamCatalog.php';
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Log.php';
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Log\Writer\Stream.php';
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Exception.php';
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Soap\Client.php';	
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Soap\AutoDiscover.php';  
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Soap\Server.php';  
		
		$tiempo_inicio = microtime(true);
		$logger = new Zend_Log();
		$writer = new Zend_Log_Writer_Stream('c:/fileELNUEVO.txt');
		$logger->addWriter($writer);
		$fecha1="";
		$fecha2="";
        $dateStart  = $this->getRequest()->getParam('dateStart');
        $dateEnd    = $this->getRequest()->getParam('dateEnd');
        $initHour   = $this->getRequest()->getParam('initHour');
        $finishHour = $this->getRequest()->getParam('finishHour');
        $idStream   = $this->getRequest()->getParam('idStream');
        $groupBy    = $this->getRequest()->getParam('groupBy');
		
		if ((!is_null($dateStart) & !empty($dateStart)) && (!is_null($dateEnd) & !empty($dateEnd))) {
            $fecha1 = $dateStart . ' ' . $initHour . ':00';
            $fecha2 = $dateEnd . ' ' . $finishHour . ':00';
        } else {
            if (!is_null($dateStart) & !empty($dateStart)) {
                $fecha1 = $dateStart . ' ' . $initHour . ':00';
                $fecha2 = $dateStart . ' ' . $finishHour . ':00';
            }
            if (!is_null($dateEnd) & !empty($dateEnd)) {
                $fecha1 = $dateEnd . ' ' . $initHour . ':00';
                $fecha2 = $dateEnd . ' ' . $finishHour . ':00';
            }
        }

		try{
		$servicio="http://xsn-stat.cloudapp.net:8080/XSNEstadisticas/Estadistica?WSDL";		
		$parametros=array();
		$parametros['_fecha1']=$fecha1;
		$parametros['_fecha2']=$fecha2;
		$parametros['_idStream']=$idStream;
		$parametros['_groupBy']=$groupBy;
		$_fecha1=$fecha1;
		$_fecha2=$fecha2;
		$_idStream=$idStream;
		$_groupBy=$groupBy;
		$client = new Zend_Soap_Client("http://xsn-stat.cloudapp.net:8080/XSNEstadisticas/Estadistica?WSDL");
		$client->setSoapVersion(SOAP_1_1);
		$result = (array) $client->sendEstadistia(array('_fecha1' => $fecha1, '_fecha2' => $fecha2,'_idStream'=>$idStream,'_groupBy'=>$groupBy));
		}catch(Exception $e){
										$logger->info($e->getMessage());

		}

		print $result['return'];
														$logger->info('TERMINE');

		$tiempo_fin = microtime(true);
		$tiempo=$tiempo_fin - $tiempo_inicio;

		

       die();
	
	}
	
    public function processHelixAction(){
		require_once 'application/models/catalogs/HelixStreamStatCatalog.php';
		require_once 'application/models/catalogs/HelixStreamCatalog.php';
		require_once 'application/models/catalogs/HelixIpviewerStreamCatalog.php';
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Log.php';
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Log\Writer\Stream.php';
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Exception.php';
		$logger = new Zend_Log();
		$writer = new Zend_Log_Writer_Stream('c:/file.txt');
		$logger->addWriter($writer);
					
		$dateStart  = $this->getRequest()->getParam('date_start');
        $dateEnd    = $this->getRequest()->getParam('date_end');
        $idCompany  = $this->getRequest()->getParam('idCompany');
        $idServer   = $this->getRequest()->getParam('idServer');
        $initHour   = $this->getRequest()->getParam('initHour');
        $finishHour = $this->getRequest()->getParam('finishHour');
        $idStream   = $this->getRequest()->getParam('stream');
        $groupBy    = $this->getRequest()->getParam('groupBy');
        $fecha1="";
		$fecha2="";
        if ((!is_null($dateStart) & !empty($dateStart)) && (!is_null($dateEnd) & !empty($dateEnd))) {
            $fecha1 = $dateStart . ' ' . $initHour . ':00';
            $fecha2 = $dateEnd . ' ' . $finishHour . ':00';
        } else {
            if (!is_null($dateStart) & !empty($dateStart)) {
                $fecha1 = $dateStart . ' ' . $initHour . ':00';
                $fecha2 = $dateStart . ' ' . $finishHour . ':00';
            }
            if (!is_null($dateEnd) & !empty($dateEnd)) {
                $fecha1 = $dateEnd . ' ' . $initHour . ':00';
                $fecha2 = $dateEnd . ' ' . $finishHour . ':00';
            }
        }
		
        $dates = array();
			$win     = 0;
            $bb      = 0;
            $android = 0;
            $ipad    = 0;
            $iphone  = 0;
            $ipod    = 0;
            $otro    = 0;
            
            $rtmp = 0;
            $rtsp = 0;
            $http = 0;
            $con  = 0;
			
			$link = mysql_connect("localhost", "xsnuser","Xsnus3rpass1"); 
			mysql_select_db("xsn_stats", $link); 
			$sql="SELECT * FROM xsn_core_helix_stream where fInicio >='".$fecha1."' and fFin <='".$fecha2."' and idsenal='".$idStream."'";
			$result = mysql_query($sql, $link); 
			if (!$result){die('ERROR CONEXION CON BD: '.mysql_error());}

	        
			while ($row = mysql_fetch_array($result)){ 

                    switch ($row[4]) {
                        case 1:
                            $rtmp += 1;
                            break;
                        case 2:
                            $rtsp += 1;
                            break;
                        case 3:
                            $http += 1;
                            break;
                    }
                    
                    switch ($row[6]) {
                        case 1:
                            $win += 1;
                            break;
                        case 2:
                            $bb += 1;       

                            break;
                        case 3:
                            $android += 1;        

                            break;
                        case 4:
                            $iphone += 1;       

                            break;
                        case 5:
                            $ipad += 1;        

                            break;
                        case 6:
                            $ipod += 1;      

                            break;
                        case 7:
                            $otro += 1;      

                            break;
                    }
                }
		
        $stream = HelixStreamCatalog::getInstance()->getById($idStream);
        $sumaDevices = $win + $android + $bb + $iphone + $ipod + $ipad + $otro;
		$sumaProtocol=$rtmp + $rtsp +http;
		
        if($sumaDevices>0){
		   $flagData=true;	
		}else{
		$flagData=false;
		}
					$promedio =(($sumaDevices*$sumaDevices)*.075)/60;
	    $this->view->promedioo   = $promedio;
        $this->view->flagData   = $flagData;
        $this->view->stream     = $stream;
        $this->view->dateStart  = $dateStart;
        $this->view->dateEnd    = $dateEnd;
        $this->view->initHour   = $initHour;
        $this->view->finishHour = $finishHour;
        $this->view->dates      = $dates;
        $this->view->groupBy    = $groupBy;	                    

        $this->view->windows = $win;
        $this->view->bb      = $bb;
        $this->view->android = $android;
        $this->view->iphone  = $iphone;
        $this->view->ipad    = $ipad;
        $this->view->ipod    = $ipod;
        $this->view->other   = $otro;
                $this->view->date_initial     = $fecha1;
				$this->view->date_final    = $fecha2;
        
        
        $this->view->rtmp         = $rtmp;
        $this->view->rtsp         = $rtsp;
        $this->view->http         = $http;
        $this->view->sumaProtocol = $rtmp + $rtsp + $http;
        //echo $rtsp;
        $this->view->setTpl("StreamHelixProcess");
		
	}
		
    public function retornaDiferencia($oldDate, $newDate)
    {

        $result      = 0;
        $oldArray    = explode(" ", $oldDate);
        $oldSubArray = explode(":", $oldArray[1]);
        $oldHour     = $oldSubArray[0];
        $oldMinute   = $oldSubArray[1];
        $oldSeconds  = ($oldHour * 60 * 60) + ($oldMinute * 60) + $oldSubArray[2];
        $newArray    = explode(" ", $newDate);
        $newSubArray = explode(":", $newArray[1]);
        $newHour     = $newSubArray[0];
        $newMinute   = $newSubArray[1];
        $newSeconds  = ($newHour * 60 * 60) + ($newMinute * 60) + $newSubArray[2];
        if (($newSeconds - $oldSeconds) <= 300) {
            $result = 1;
        }
        return $result;
    }
    public function getDiasDiferencia($firstDate, $secondDate)
    {
        if (empty($secondDate)) {
            $secondDate = $firstDate;
        }
        $oldArray     = explode(" ", $firstDate);
        $oldSubArray  = explode("-", $oldArray[0]);
        $timestamp1   = mktime(0, 0, 0, $oldSubArray[1], $oldSubArray[2], $oldSubArray[0]);
        $newArray     = explode(" ", $secondDate);
        $newSubArray  = explode("-", $newArray[0]);
        $timestamp2   = mktime(0, 0, 0, $newSubArray[1], $newSubArray[2], $newSubArray[0]);
        $segundos_dif = $timestamp1 - $timestamp2;
        $dias_dif     = $segundos_dif / (60 * 60 * 24);
        $dias_dif     = abs($dias_dif);
        return $dias_dif;
    }
    public function getNextDate($date)
    {
        $oldArray = explode(" ", $date);
        $result   = date('Y-m-d', strtotime($oldArray[0] . '+ 1 days'));
        return $result;
    }
	
	
    public function processDataHelixAction()
    {
	
        require_once 'application/models/catalogs/HelixStreamStatCatalog.php';
		
		        require_once 'application/models/catalogs/HelixStreamStatCatalog.php';
        require_once 'application/models/catalogs/HelixStreamCatalog.php';
        require_once 'application/models/catalogs/HelixIpviewerStreamCatalog.php';
        	require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Log.php';
	require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Log\Writer\Stream.php';
	require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Exception.php';
            
        set_time_limit(0);
        $dateStart  = $this->getRequest()->getParam('dateStart');
        $dateEnd    = $this->getRequest()->getParam('dateEnd');
        $initHour   = $this->getRequest()->getParam('initHour');
        $finishHour = $this->getRequest()->getParam('finishHour');
        $idStream   = $this->getRequest()->getParam('idStream');
        $groupBy    = $this->getRequest()->getParam('groupBy');
        	$logger = new Zend_Log();
	$writer = new Zend_Log_Writer_Stream('c:/file1111.txt');
        $logger->addWriter($writer);
        $dates = array();
        
        if ((!is_null($dateStart) & !empty($dateStart)) && (!is_null($dateEnd) & !empty($dateEnd))) {
            $dates[] = $dateStart . ' ' . $initHour . ':00';
            $dates[] = $dateEnd . ' ' . $finishHour . ':00';
        } else {
            if (!is_null($dateStart) & !empty($dateStart)) {
                $dates[] = $dateStart . ' ' . $initHour . ':00';
                $dates[] = $dateStart . ' ' . $finishHour . ':00';
            }
            if (!is_null($dateEnd) & !empty($dateEnd)) {
                $dates[] = $dateEnd . ' ' . $initHour . ':00';
                $dates[] = $dateEnd . ' ' . $finishHour . ':00';
            }
        }
        
        
        $totalViews  = 0;
        $server1     = 0;
        $server2     = 0;
        $server3     = 0;
        $server4     = 0;
        $server5     = 0;
        $server6     = 0;
        $serverDate1 = "";
        $serverDate2 = "";
        $serverDate3 = "";
        $serverDate4 = "";
        $serverDate5 = "";
        $serverDate6 = "";
        $connects    = 0;
        $newConnects = 0;
        $dateAcum    = $dateStart . ' ' . $initHour . ':00';
        $unicos      = 0;
        if ($idStream != 0) {
            $criteria = new Criteria();
            $criteria->add('id_stream', $idStream, Criteria::EQUAL);
            $criteria->add('`timestamp`', $dates, Criteria::BETWEEN);
            $criteria->addGroupByColumn('`timestamp`');
            $liveStat = HelixStreamStatCatalog::getInstance(1)->getByCriteria($criteria, false);
            if (!$liveStat->isEmpty()) {
                $initDate      = explode(" ", $dateStart);
                $var           = "";
                $fechaMultiple = $initDate[0];
                for ($index = 0; $index <= $this->getDiasDiferencia($dateStart, $dateEnd); $index++) {
                    $i       = 0;
                    $tempvar = 0;
                    foreach ($liveStat as $data) {
                        $aux = $data->getTimestamp();
                        
                        
                        if (!(strpos($data->getTimestamp(), $fechaMultiple) === false)) {
                            if (!empty($serverDate1) && !empty($aux)) {
                                if ($this->retornaDiferencia($serverDate1, $aux) == 0) {
                                    $server1 = 0;
                                }
                                
                            }
                            if (!empty($serverDate2) & !empty($aux)) {
                                if ($this->retornaDiferencia($serverDate2, $aux) == 0) {
                                    $server2 = 0;
                                }
                                
                            }
                            if (!empty($serverDate3) & !empty($aux)) {
                                if ($this->retornaDiferencia($serverDate3, $aux) == 0) {
                                    $server3 = 0;
                                }
                                
                            }
                            if (!empty($serverDate4) & !empty($aux)) {
                                if ($this->retornaDiferencia($serverDate4, $aux) == 0) {
                                    $server4 = 0;
                                }
                                
                            }
                            if (!empty($serverDate5) & !empty($aux)) {
                                if ($this->retornaDiferencia($serverDate5, $aux) == 0) {
                                    $server5 = 0;
                                }
                                
                            }
                            if (!empty($serverDate6) & !empty($aux)) {
                                if ($this->retornaDiferencia($serverDate6, $aux) == 0) {
                                    $server6 = 0;
                                }
                                
                            }
                            switch ($data->getServer()) {
                                case 1:
                                    
                                    if ($i != 0 && $this->retornaDiferencia($dateAcum, $data->getTimestamp()) == 0) {
                                        $server1 = 0;
                                    }
                                    
                                    
                                    if (strcmp($data->getTimestamp(), $serverDate1) != 0) {
                                        if ($data->getConnects() > $server1) {
                                            $newConnects = $data->getConnects() - $server1;
                                            $unicos      = $data->getConnects() - $server1;
                                            $server1     = $data->getConnects();
                                            
                                        } else if ($data->getConnects() == $server1) {
                                            $server1 = $server1;
                                        } else {
                                            $server1 = $data->getConnects();
                                        }
                                        
                                    }
                                    $serverDate1 = $data->getTimestamp();
                                    break;
                                case 2:
                                    if ($i != 0 && $this->retornaDiferencia($dateAcum, $data->getTimestamp()) == 0) {
                                        $server2 = 0;
                                        
                                    }
                                    
                                    if (strcmp($data->getTimestamp(), $serverDate2) != 0) {
                                        if ($data->getConnects() > $server2) {
                                            $newConnects = $data->getConnects() - $server2;
                                            $unicos      = $data->getConnects() - $server2;
                                            $server2     = $data->getConnects();
                                            
                                        } else if ($data->getConnects() == $server2) {
                                            $server2 = $server2;
                                        } else {
                                            $server2 = $data->getConnects();
                                        }
                                        
                                    }
                                    $serverDate2 = $data->getTimestamp();
                                    break;
                                
                                case 3:
                                    if ($i != 0 && $this->retornaDiferencia($dateAcum, $data->getTimestamp()) == 0) {
                                        $server3 = 0;
                                    }
                                    
                                    
                                    if (strcmp($data->getTimestamp(), $serverDate3) != 0) {
                                        if ($data->getConnects() > $server3) {
                                            $newConnects = $data->getConnects() - $server3;
                                            $unicos      = $data->getConnects() - $server3;
                                            $server3     = $data->getConnects();
                                            
                                        } else if ($data->getConnects() == $server3) {
                                            $server3 = $server3;
                                        } else {
                                            $server3 = $data->getConnects();
                                        }
                                        
                                    }
                                    $serverDate3 = $data->getTimestamp();
                                    break;
                                
                                case 4:
                                    if ($i != 0 && $this->retornaDiferencia($dateAcum, $data->getTimestamp()) == 0) {
                                        $server4 = 0;
                                    }
                                    
                                    
                                    if (strcmp($data->getTimestamp(), $serverDate4) != 0) {
                                        if ($data->getConnects() > $server4) {
                                            $newConnects = $data->getConnects() - $server4;
                                            $unicos      = $data->getConnects() - $server4;
                                            $server4     = $data->getConnects();
                                            
                                        } else if ($data->getConnects() == $server4) {
                                            $server4 = $server4;
                                        } else {
                                            $server4 = $data->getConnects();
                                        }
                                        
                                    }
                                    $serverDate4 = $data->getTimestamp();
                                    break;
                                
                                case 5:
                                    if ($i != 0 && $this->retornaDiferencia($dateAcum, $data->getTimestamp()) == 0) {
                                        $server5 = 0;
                                    }
                                    
                                    if (strcmp($data->getTimestamp(), $serverDate5) != 0) {
                                        if ($data->getConnects() > $server5) {
                                            $newConnects = $data->getConnects() - $server5;
                                            $unicos      = $data->getConnects() - $server5;
                                            $server5     = $data->getConnects();
                                        } else if ($data->getConnects() == $server5) {
                                            $server5 = $server5;
                                        } else {
                                            $server5 = $data->getConnects();
                                        }
                                        
                                    }
                                    $serverDate5 = $data->getTimestamp();
                                    break;
                                
                                case 6:
                                    if ($i != 0 && $this->retornaDiferencia($dateAcum, $data->getTimestamp()) == 0) {
                                        $server6 = 0;
                                    }
                                    
                                    
                                    if (strcmp($data->getTimestamp(), $serverDate6) != 0) {
                                        if ($data->getConnects() > $server6) {
                                            $newConnects = $data->getConnects() - $server6;
                                            $unicos      = $data->getConnects() - $server6;
                                            $server6     = $data->getConnects();
                                        } else if ($data->getConnects() == $server6) {
                                            $server6 = $server6;
                                        } else {
                                            $server6 = $data->getConnects();
                                        }
                                        
                                    }
                                    $serverDate6 = $data->getTimestamp();
                                    break;
                                    
                            }
                            
                            
                            $connects = $server1 + $server2 + $server3 + $server4 + $server5 + $server6;
                            $dateAcum = $data->getTimestamp();
                            $totalViews += $newConnects;

                            if ($groupBy == 2) {

                                $var .= $data->getTimestamp() . "," . $connects . "\n";
                            } else {

                                $var .= $data->getTimestamp() . "," . $connects . "," . $newConnects . "," . $totalViews . "\n";
                            }
                            
                            $newConnects = 0;
                            $unicos      = 0;
                            
                        }
                        
                        $i++;
                    }
                    
                    $fechaMultiple = $this->getNextDate($fechaMultiple);
                    
                }
            }
		if($totalViews>0){
		   $flagData=true;	
		}else{
		$flagData=false;
		}
		$this->view->flagData= $flagData;

            print $var;
            die();
            
            
        }
    }
    public function downloadHelixStatAction()
    {
        require_once 'application/models/catalogs/HelixStreamStatCatalog.php';
        require_once 'application/models/catalogs/HelixIpviewerStreamCatalog.php';
        require_once 'lib/excel/excel.php';
        require_once 'lib/excel/excel-ext.php';
		require_once 'application/models/catalogs/HelixStreamStatCatalog.php';
		require_once 'application/models/catalogs/HelixStreamCatalog.php';
		require_once 'application/models/catalogs/HelixIpviewerStreamCatalog.php';
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Log.php';
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Log\Writer\Stream.php';
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Exception.php';
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Soap\Client.php';	
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Soap\AutoDiscover.php';  
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Soap\Server.php'; 
		
		$logger = new Zend_Log();
		$writer = new Zend_Log_Writer_Stream('c:/fileDESCARGA.txt');
		$logger->addWriter($writer);
        $this->view->setLayoutFile(false);
        $this->_helper->viewRenderer->setNoRender();
$tiempo_inicio = microtime(true);

        set_time_limit(0);
		
        $dateEnd    = $this->getRequest()->getParam('dateEnd');
        $idCompany  = $this->getRequest()->getParam('idCompany');
        $idServer   = $this->getRequest()->getParam('idServer');
        $initHour   = $this->getRequest()->getParam('initHour');
        $finishHour = $this->getRequest()->getParam('finishHour');
        $idStream   = $this->getRequest()->getParam('idStream');
        $groupBy    = $this->getRequest()->getParam('groupBy');
        $dateStart  = $this->getRequest()->getParam('dateStart');
        $fecha1='';
		$fecha2='';
        $assoc[] = array(
            "" => "",
            "" => "",
            "" => "",
            "Helix histórico live streaming" => ""
        );
        $assoc[] = array(
            "GraficaTexto" => "Registros"
        );
		if($groupBy == 1 ){
        $assoc[] = array(
            "Fecha" => "Fecha",
            "Concurrentes" => "Concurrentes",
            "Usuarios nuevos" => "Usuarios nuevos",
            "Peticiones" => "Peticiones"
        );
		}
		else{
        $assoc[] = array(
            "Fecha" => "Fecha",
            "Concurrentes" => "Concurrentes",
        );
		}
        		if ((!is_null($dateStart) & !empty($dateStart)) && (!is_null($dateEnd) & !empty($dateEnd))) {
            $fecha1 = $dateStart . ' ' . $initHour . ':00';
            $fecha2 = $dateEnd . ' ' . $finishHour . ':00';
        } else {
            if (!is_null($dateStart) & !empty($dateStart)) {
                $fecha1 = $dateStart . ' ' . $initHour . ':00';
                $fecha2 = $dateStart . ' ' . $finishHour . ':00';
            }
            if (!is_null($dateEnd) & !empty($dateEnd)) {
                $fecha1 = $dateEnd . ' ' . $initHour . ':00';
                $fecha2 = $dateEnd . ' ' . $finishHour . ':00';
            }
        }
		
		$link = mysql_connect("localhost", "xsnuser","Xsnus3rpass1"); 
		mysql_select_db("xsn_stats", $link); 
		$sql1="SELECT * FROM xsn_core_helix_stream where fInicio >='".$fecha1."' and fFin <='".$fecha2."' and idsenal='".$idStream."'";
		$result1 = mysql_query($sql1, $link); 
		if (!$result1){die('ERROR CONEXION CON BD: '.mysql_error());}
		

		
				try{
		$servicio="http://xsn-stat.cloudapp.net:8080/XSNEstadisticas/Estadistica?WSDL";		
		$parametros=array();

		$client = new Zend_Soap_Client("http://xsn-stat.cloudapp.net:8080/XSNEstadisticas/Estadistica?WSDL");
		$client->setSoapVersion(SOAP_1_1);
		$re = (array) $client->sendEstadistia(array('_fecha1' => $fecha1, '_fecha2' => $fecha2,'_idStream'=>$idStream,'_groupBy'=>$groupBy));
		}catch(Exception $e){
										$logger->info($e->getMessage());

		}
		
		

		$grafica = explode("\n", $re['return']);


		for($i=0;$i<count($grafica);$i++){
			$grafica1=explode(",",$grafica[$i]);
			$fecha=$grafica1[0];
			$concurrente=$grafica1[1];
			$newuser=$grafica1[2];
			$total=$grafica1[3];
			if($groupBy == 1 ){
				$assoc[] = array(
					"Fecha" =>$fecha,
					"Concurrentes" => $concurrente,
					"Usuarios nuevos" => $newuser,
					"Peticiones" => $total
				);
			}
			else {
				$assoc[] = array(
					"Fecha" =>$fecha,
					"Concurrentes" => $concurrente
				);			
			}
        }

			$win     = 0;
            $bb      = 0;
            $android = 0;
            $ipad    = 0;
            $iphone  = 0;
            $ipod    = 0;
            $otro    = 0;
            
            $rtmp = 0;
            $rtsp = 0;
            $http = 0;
			
			


	          
			while ($row1 = mysql_fetch_array($result1)){ 
                    switch ($row1[4]) {
                        case 1:
                            $rtmp += 1;
                            break;
                        case 2:
                            $rtsp += 1;
                            break;
                        case 3:
                            $http += 1;
                            break;
                    }
                    
                    switch ($row1[6]) {
                        case 1:
                            $win += 1;
                            break;
                        case 2:
                            $bb += 1;       

                            break;
                        case 3:
                            $android += 1;        

                            break;
                        case 4:
                            $iphone += 1;        
                            break;
                        case 5:
                            $ipad += 1;       
                            break;
                        case 6:
                            $ipod += 1;        

                            break;
                        case 7:
                            $otro += 1;      

                            break;
                    }
                }
		
		$sumaProtocol=$rtmp+$rtsp+$http;
	   $assoc[]      = array(
            "Protocolos" => "Detalle Protocolos"
        );
        $assoc[]      = array(
            "RTMP" => "RTMP=" . round(($rtmp * 100) / $sumaProtocol) . "%"
        );
        $assoc[]      = array(
            "RTSP" => "RTSP=" . round(($rtsp * 100) / $sumaProtocol) . "%"
        );
        $assoc[]      = array(
            "HTTP" => "HTTP=" . round(($http * 100) / $sumaProtocol) . "%"
        );
        $assoc[]      = array(
            "Dispositivos" => "Detalle Dispositivos"
        );
        $assoc[]      = array(
            "Windows" => "Windows=" . $win
        );
        $assoc[]      = array(
            "Android" => "Android=" . $android
        );
        $assoc[]      = array(
            "Blackberry" => "Blackberry=" . $bb
        );
        $assoc[]      = array(
            "Iphone" => "Iphone=" . $iphone
        );
        $assoc[]      = array(
            "Ipod" => "Ipod=" . $ipod
        );
        $assoc[]      = array(
            "Ipad" => "Ipad=" . $ipad
        );
        $assoc[]      = array(
            "Otros" => "Otros=" . $otro
        );
        createExcel("reporte-historico-helix.xls", $assoc);
        $tiempo_fin = microtime(true);	
        $tiempo=$tiempo_fin - $tiempo_inicio;
	
        exit;
    }
    
    public function geolocationHelixAction()
    {
        require_once 'application/models/catalogs/HelixStreamCatalog.php';
        
        $id             = $this->getUser()->getIdUser();
        $serverCatalog  = StreamServerCatalog::getInstance();
        $companyCatalog = CompanyCatalog::getInstance();
        
        $company = $companyCatalog->getByUser($id)->getOne();
        
        if (!$company instanceof Company) {
            $this->setFlash("error", "No existe ningun cliente asociado");
            $this->_redirect('index/index');
        }
        
        $server = $serverCatalog->getByCompany($company->getIdCompany())->getOne();
        if (!$server instanceof StreamServer) {
            $this->setFlash("error", "No existe ningun servidor asociado");
            $this->_redirect('index/index');
        }
        
        $criteria = new Criteria();
        $criteria->add('id_company', $company->getIdCompany(), Criteria::EQUAL);
        
        //$data = HelixStreamCatalog::getInstance()->getHelixStreamHelixLiveRelations($criteria);
         $data=$this->liveRegresar($company->getIdCompany());
        $idsStream = array();
        //$idsVod=array();
        
        $flagStream = false;
        //$flagVod=false;
		if(!is_null($data)){
        foreach ($data as $item) {
            if (!is_null($item['id_stream_helix'])) {
                $flagStream  = true;
                $idsStream[] = $item['id_stream_helix'];
            }
            /*
            if(!is_null($item['id_vod_helix'])){
            $flagVod=true;	
            $idsVod[]= $item['id_vod_helix'];
            }*/
        }
        if(!is_null($idsStream)){
        $streams = HelixStreamCatalog::getInstance()->getByIds($idsStream)->toCombo();
		}
        }
		//$vods = FlashVodCatalog::getInstance()->getByIds($idsVod)->toCombo();
        
        $this->view->idCompany  = $company->getIdCompany();
        $this->view->idServer   = $server->getIdServer();
        $this->view->streams    = $streams;
        //$this->view->vods= $vods;
        $this->view->flagStream = $flagStream;
        //$this->view->flagVod=$flagVod;
        $this->setTitle('Histórico por ubicación');
        $this->view->setTpl('GeolocationHelix');
    }
    
    public function processGeoHelixAction()
    {
        set_time_limit(0);
        
        require_once 'application/models/catalogs/HelixIpviewerStreamCatalog.php';
        require_once 'application/models/catalogs/LocationCatalog.php';
        require_once 'application/models/catalogs/HelixStreamCatalog.php';
        
        require_once 'lib/charts/XmlWriter.class.php';
        
        $this->getUser()->setAttribute('rgeolocation', null);
        
        
        $dateStart  = $this->getRequest()->getParam('date_start');
        $dateEnd    = $this->getRequest()->getParam('date_end');
        $initHour   = $this->getRequest()->getParam('initHour');
        $finishHour = $this->getRequest()->getParam('finishHour');
        
        
        $flagstream = (int) $this->getRequest()->getParam('flagStream', 0);
        $flagvideo  = (int) $this->getRequest()->getParam('flagVideo', 0);
        
        if (!empty($flagstream)) {
            $id        = $this->getRequest()->getParam('stream');
            $streamObj = HelixStreamCatalog::getInstance(0)->getById($id);
        } elseif (!empty($flagvideo)) {
            $id = $this->getRequest()->getParam('vod');
        }
        
        
        $dates = array();
        if ((!is_null($dateStart) & !empty($dateStart)) && (!is_null($dateEnd) & !empty($dateEnd))) {
            $dates[] = $dateStart . ' ' . $initHour . ':00';
            $dates[] = $dateEnd . ' ' . $finishHour . ':00';
        } else {
            if (!is_null($dateStart) & !empty($dateStart)) {
                $dates[] = $dateStart;
            }
            if (!is_null($dateEnd) & !empty($dateEnd)) {
                $dates[] = $dateEnd;
            }
        }
        $result = array();
        
        $locationCatalog      = LocationCatalog::getInstance();
        $helixIpViewerCatalog = HelixIpviewerStreamCatalog::getInstance();
        if ($id != 0) {
            $criteria = new Criteria();
            
            if (!empty($flagstream)) {
                $criteria->add('id_stream', $id, Criteria::EQUAL);
            } elseif (!empty($flagvideo)) {
                $criteria->add('id_vod', $id, Criteria::EQUAL);
            }
            
            $criteria->add('status', 1, Criteria::EQUAL);
            $criteria->add('timestamp', $dates, Criteria::BETWEEN);
            //$criteria->addGroupByColumn('`timestamp`');
            $criteria->addGroupByColumn('`ipviewer`');
            $criteria->addGroupByColumn('`user_agent`');
            
            
            if (!empty($flagstream)) {
                $result = $helixIpViewerCatalog->getByCriteria($criteria);
            } elseif (!empty($flagvideo)) {
                //$result = $viewerVodCatalog->getByCriteria($criteria);
            }
            
            
            $xmlWorld = new XmlWriter2();
            $xmlWorld->push('countrydata');
            $xmlWorld->push('state', array(
                'id' => 'default_color'
            ));
            $xmlWorld->element('color', 'd8d8d8');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'background_color'
            ));
            $xmlWorld->element('color', 'FFFFFF');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'outline_color'
            ));
            $xmlWorld->element('color', '868686');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'default_point'
            ));
            $xmlWorld->element('color', 'e22000');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'scale_points'
            ));
            $xmlWorld->element('data', '50');
            $xmlWorld->pop();
            
            $xmlWorld->push('state', array(
                'id' => 'zoom_out_button'
            ));
            $xmlWorld->element('name', 'Zoom Out');
            $xmlWorld->element('data', 'SE');
            $xmlWorld->element('font_size', '12');
            $xmlWorld->element('font_color', 'FFFFFF');
            $xmlWorld->element('background_color', '585858');
            $xmlWorld->pop();
            
            if (!$result->isEmpty()) {
                $this->getUser()->setAttribute('totalviewersbyip', count($result));
                 $this->getUser()->setAttribute('countx', count($result));
                $dataArray   = array();
                $colorHover  = array();
                $colorNormal = array();
                
                $colorNormal['aguascalientes']                   = '#C8C8C8';
                $colorNormal['baja_california']                  = '#C8C8C8';
                $colorNormal['baja_california_sur']              = '#C8C8C8';
                $colorNormal['campeche']                         = '#C8C8C8';
                $colorNormal['chiapas']                          = '#C8C8C8';
                $colorNormal['chihuahua']                        = '#C8C8C8';
                $colorNormal['coahuila']                         = '#C8C8C8';
                $colorNormal['colima']                           = '#C8C8C8';
                $colorNormal['distrito_federal']                 = '#C8C8C8';
                $colorNormal['durango']                          = '#C8C8C8';
                $colorNormal['federally_administered_territory'] = '#C8C8C8';
                $colorNormal['guanajuato']                       = '#C8C8C8';
                $colorNormal['guerrero']                         = '#C8C8C8';
                $colorNormal['hidalgo']                          = '#C8C8C8';
                $colorNormal['jalisco']                          = '#C8C8C8';
                $colorNormal['mexico']                           = '#C8C8C8';
                $colorNormal['michoacan']                        = '#C8C8C8';
                $colorNormal['morelos']                          = '#C8C8C8';
                $colorNormal['nayarit']                          = '#C8C8C8';
                $colorNormal['nuevo_leon']                       = '#C8C8C8';
                $colorNormal['oaxaca']                           = '#C8C8C8';
                $colorNormal['puebla']                           = '#C8C8C8';
                $colorNormal['queretaro']                        = '#C8C8C8';
                $colorNormal['quintana_roo']                     = '#C8C8C8';
                $colorNormal['san_luis_potosi']                  = '#C8C8C8';
                $colorNormal['sinaloa']                          = '#C8C8C8';
                $colorNormal['sonora']                           = '#C8C8C8';
                $colorNormal['tabasco']                          = '#C8C8C8';
                $colorNormal['tamaulipas']                       = '#C8C8C8';
                $colorNormal['tlaxcala']                         = '#C8C8C8';
                $colorNormal['veracruz']                         = '#C8C8C8';
                $colorNormal['yucatan']                          = '#C8C8C8';
                $colorNormal['zacatecas']                        = '#C8C8C8';
                
                //$nameTpT=array();
                $ipClients = array();
			    
                foreach ($result as $item) {
				
                    try {
                        $criteria    = new Criteria();
                        //$criteria->add('ip_start', $item->getIpviewer(), Criteria::LESS_OR_EQUAL, Criteria::INET_ATON);
                        $ipCli       = $this->Dot2LongIP($item->getIpviewer());
                        $ipClients[] = $item->getIpviewer();
                        $criteria->add('ip_start', $ipCli, Criteria::LESS_OR_EQUAL);
                        $criteria->addDescendingOrderByColumn('ip_start');
                        $criteria->setLimit(1);
                        //$resultGeo= $geolocationCatalog->getByCriteria($criteria)->getOne();
                        
                        $location = $locationCatalog->getByCriteria($criteria)->getOne();
                        //var_dump($location);
                        if ($location instanceof Location) {
                            //$location=$locationCatalog->getById($resultgc->getLocation());
                            
                            
                            
                            if (!is_null($location->getCity()) && $location->getCity() != '-') {
                                $nameregion = $location->getCity();
                            } else {
                                $nameregion = $location->getRegion();
                            }
                            
                            $coordenadas = $location->getLatitude() . ',' . $location->getLongitude();
                            
                            $xmlWorld->push('state', array(
                                'id' => 'point'
                            ));
                            $xmlWorld->element('loc', $coordenadas);
                            $xmlWorld->element('name', $nameregion);
                            $xmlWorld->element('size', '3');
                            $xmlWorld->element('opacity', '50');
                            $xmlWorld->pop();
                            
                            $regionName = $nameregion;
                            $temp       = explode(" ", $regionName);
                            
                            $varName = "";
                            if (count($temp) > 1) {
                                $i = 0;
                                foreach ($temp as $it) {
                                    if ($i == 0) {
                                        $varName .= strtolower($it);
                                    } else {
                                        $varName .= '_' . strtolower($it);
                                    }
                                    $i++;
                                }
                            } else {
                                $varName = strtolower($temp[0]);
                            }
                            //echo "* ".$varName."\n";
                            //$nameTpT[]=$varName;
                            if (!empty($varName)) {
                                switch (true) {
                                    case ($this->findMatch($varName, "aguascalientes") == true):
                                        $dataArray['aguascalientes'] += 1;
                                        $colorHover['aguascalientes']  = '#CC0000';
                                        $colorNormal['aguascalientes'] = '#484F59';
                                        break;
                                    case ($varName == "baja_california"):
                                        //echo "california";
                                        $dataArray['baja_california'] += 1;
                                        $colorHover['baja_california']  = '#CC0000';
                                        $colorNormal['baja_california'] = '#484F59';
                                        break;
                                    case ($varName == "baja_california_sur"):
                                        //echo "baja california";
                                        $dataArray['baja_california_sur'] += 1;
                                        $colorHover['baja_california_sur']  = '#CC0000';
                                        $colorNormal['baja_california_sur'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "campeche") == true):
                                        $dataArray['campeche'] += 1;
                                        $colorHover['campeche']  = '#CC0000';
                                        $colorNormal['campeche'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "chiapas") == true):
                                        $dataArray['chiapas'] += 1;
                                        $colorHover['chiapas']  = '#CC0000';
                                        $colorNormal['chiapas'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "chihuahua") == true):
                                        $dataArray['chihuahua'] += 1;
                                        $colorHover['chihuahua']  = '#CC0000';
                                        $colorNormal['chihuahua'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "coahuila") == true):
                                        $dataArray['coahuila'] += 1;
                                        $colorHover['coahuila']  = '#CC0000';
                                        $colorNormal['coahuila'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "colima") == true):
                                        $dataArray['colima'] += 1;
                                        $colorHover['colima']  = '#CC0000';
                                        $colorNormal['colima'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "distrito federal") == true):
                                        $dataArray['distrito_federal'] += 1;
                                        $colorHover['distrito_federal']  = '#CC0000';
                                        $colorNormal['distrito_federal'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "durango") == true):
                                        $dataArray['durango'] += 1;
                                        $colorHover['durango']  = '#CC0000';
                                        $colorNormal['durango'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "federally administered territory") == true):
                                        $dataArray['federally_administered_territory'] += 1;
                                        $colorHover['federally_administered_territory']  = '#CC0000';
                                        $colorNormal['federally_administered_territory'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "guanajuato") == true):
                                        $dataArray['guanajuato'] += 1;
                                        $colorHover['guanajuato']  = '#CC0000';
                                        $colorNormal['guanajuato'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "guerrero") == true):
                                        $dataArray['guerrero'] += 1;
                                        $colorHover['guerrero']  = '#CC0000';
                                        $colorNormal['guerrero'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "hidalgo") == true):
                                        $dataArray['hidalgo'] += 1;
                                        $colorHover['hidalgo']  = '#CC0000';
                                        $colorNormal['hidalgo'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "jalisco") == true):
                                        $dataArray['jalisco'] += 1;
                                        $colorHover['jalisco']  = '#CC0000';
                                        $colorNormal['jalisco'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "mexico") == true):
                                        $dataArray['mexico'] += 1;
                                        $colorHover['mexico']  = '#CC0000';
                                        $colorNormal['mexico'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "michoacan") == true):
                                        $dataArray['michoacan'] += 1;
                                        $colorHover['michoacan']  = '#CC0000';
                                        $colorNormal['michoacan'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "morelos") == true):
                                        $dataArray['morelos'] += 1;
                                        $colorHover['morelos']  = '#CC0000';
                                        $colorNormal['morelos'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "nayarit") == true):
                                        $dataArray['nayarit'] += 1;
                                        $colorHover['nayarit']  = '#CC0000';
                                        $colorNormal['nayarit'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "nuevo leon") == true):
                                        $dataArray['nuevo_leon'] += 1;
                                        $colorHover['nuevo_leon']  = '#CC0000';
                                        $colorNormal['nuevo_leon'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "oaxaca") == true):
                                        $dataArray['oaxaca'] += 1;
                                        $colorHover['oaxaca']  = '#CC0000';
                                        $colorNormal['oaxaca'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "puebla") == true):
                                        $dataArray['puebla'] += 1;
                                        $colorHover['puebla']  = '#CC0000';
                                        $colorNormal['puebla'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "queretaro") == true):
                                        $dataArray['queretaro'] += 1;
                                        $colorHover['queretaro']  = '#CC0000';
                                        $colorNormal['queretaro'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "quintana roo") == true):
                                        $dataArray['quintana_roo'] += 1;
                                        $colorHover['quintana_roo']  = '#CC0000';
                                        $colorNormal['quintana_roo'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "san luis potosi") == true):
                                        $dataArray['san_luis_potosi'] += 1;
                                        $colorHover['san_luis_potosi']  = '#CC0000';
                                        $colorNormal['san_luis_potosi'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "sinaloa") == true):
                                        $dataArray['sinaloa'] += 1;
                                        $colorHover['sinaloa']  = '#CC0000';
                                        $colorNormal['sinaloa'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "sonora") == true):
                                        $dataArray['sonora'] += 1;
                                        $colorHover['sonora']  = '#CC0000';
                                        $colorNormal['sonora'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "tabasco") == true):
                                        $dataArray['tabasco'] += 1;
                                        $colorHover['tabasco']  = '#CC0000';
                                        $colorNormal['tabasco'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "tamaulipas") == true):
                                        $dataArray['tamaulipas'] += 1;
                                        $colorHover['tamaulipas']  = '#CC0000';
                                        $colorNormal['tamaulipas'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "tlaxcala") == true):
                                        $dataArray['tlaxcala'] += 1;
                                        $colorHover['tlaxcala']  = '#CC0000';
                                        $colorNormal['tlaxcala'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "veracruz") == true):
                                        $dataArray['veracruz'] += 1;
                                        $colorHover['veracruz']  = '#CC0000';
                                        $colorNormal['veracruz'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "yucatan") == true):
                                        $dataArray['yucatan'] += 1;
                                        $colorHover['yucatan']  = '#CC0000';
                                        $colorNormal['yucatan'] = '#484F59';
                                        break;
                                    case ($this->findMatch($varName, "zacatecas") == true):
                                        $dataArray['zacatecas'] += 1;
                                        $colorHover['zacatecas']  = '#CC0000';
                                        $colorNormal['zacatecas'] = '#484F59';
                                        break;
                                }
                            }
                        }
                        
                        //aqui
                    }
                    catch (Exception $e) {
                        echo "Error del tipo: " . $e;
                        //$this->view->setTpl('GeolocationHelixProcess'); 
                        $this->setFlash("error", "Error del tipo: " . $e);
                        $this->_redirect('index/index');
                        return false;
                    }
                }
                $this->getUser()->setAttribute('ipClients', $ipClients);
                $this->getUser()->setAttribute('rgeolocation', $dataArray);
            }
            $xmlWorld->push('state', array(
                'id' => 'AF'
            ));
            $xmlWorld->element('name', 'Afghanistan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AL'
            ));
            $xmlWorld->element('name', 'Albania');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AG'
            ));
            $xmlWorld->element('name', 'Algeria');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AN'
            ));
            $xmlWorld->element('name', 'Andorra');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AO'
            ));
            $xmlWorld->element('name', 'Angola');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AC'
            ));
            $xmlWorld->element('name', 'Antigua and Barbuda');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AR'
            ));
            $xmlWorld->element('name', 'Argentina');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AM'
            ));
            $xmlWorld->element('name', 'Armenia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AA'
            ));
            $xmlWorld->element('name', 'Aruba');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AS'
            ));
            $xmlWorld->element('name', 'Australia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AU'
            ));
            $xmlWorld->element('name', 'Austria');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'AJ'
            ));
            $xmlWorld->element('name', 'Azerbaijan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BF'
            ));
            $xmlWorld->element('name', 'The Bahamas');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BA'
            ));
            $xmlWorld->element('name', 'Bahrain');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FQ'
            ));
            $xmlWorld->element('name', 'Baker Island');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BG'
            ));
            $xmlWorld->element('name', 'Bangladesh');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BB'
            ));
            $xmlWorld->element('name', 'Barbados');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BO'
            ));
            $xmlWorld->element('name', 'Belarus');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BE'
            ));
            $xmlWorld->element('name', 'Belgium');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BH'
            ));
            $xmlWorld->element('name', 'Belize');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BN'
            ));
            $xmlWorld->element('name', 'Benin');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BD'
            ));
            $xmlWorld->element('name', 'Bermuda');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BT'
            ));
            $xmlWorld->element('name', 'Bhutan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BL'
            ));
            $xmlWorld->element('name', 'Bolivia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BK'
            ));
            $xmlWorld->element('name', 'Bosnia and Herzegovina');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BC'
            ));
            $xmlWorld->element('name', 'Botswana');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BV'
            ));
            $xmlWorld->element('name', 'Bouvet Island');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BR'
            ));
            $xmlWorld->element('name', 'Brazil');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IO'
            ));
            $xmlWorld->element('name', 'British Indian Ocean Territory');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'VI'
            ));
            $xmlWorld->element('name', 'British Virgin Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BX'
            ));
            $xmlWorld->element('name', 'Brunei');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BU'
            ));
            $xmlWorld->element('name', 'Bulgaria');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'UV'
            ));
            $xmlWorld->element('name', 'Burkina Faso');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BY'
            ));
            $xmlWorld->element('name', 'Burundi');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CB'
            ));
            $xmlWorld->element('name', 'Cambodia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CM'
            ));
            $xmlWorld->element('name', 'Cameroon');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CA'
            ));
            $xmlWorld->element('name', 'Canada');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CV'
            ));
            $xmlWorld->element('name', 'Cape Verde');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CJ'
            ));
            $xmlWorld->element('name', 'Cayman Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CT'
            ));
            $xmlWorld->element('name', 'Central African Republic');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CD'
            ));
            $xmlWorld->element('name', 'Chad');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CI'
            ));
            $xmlWorld->element('name', 'Chile');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CH'
            ));
            $xmlWorld->element('name', 'China');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CO'
            ));
            $xmlWorld->element('name', 'Colombia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CN'
            ));
            $xmlWorld->element('name', 'Comoros');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CF'
            ));
            $xmlWorld->element('name', 'Congo');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CW'
            ));
            $xmlWorld->element('name', 'Cook Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CS'
            ));
            $xmlWorld->element('name', 'Costa Rica');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IV'
            ));
            $xmlWorld->element("name", "Cote d'Ivoire");
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'HR'
            ));
            $xmlWorld->element('name', 'Croatia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CU'
            ));
            $xmlWorld->element('name', 'Cuba');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CY'
            ));
            $xmlWorld->element('name', 'Cyprus');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'EZ'
            ));
            $xmlWorld->element('name', 'Czech Republic');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CG'
            ));
            $xmlWorld->element('name', 'Democratic Republic of the Congo');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'DA'
            ));
            $xmlWorld->element('name', 'Denmark');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'DJ'
            ));
            $xmlWorld->element('name', 'Djibouti');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'DO'
            ));
            $xmlWorld->element('name', 'Dominica');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'DR'
            ));
            $xmlWorld->element('name', 'Dominican Republic');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'EC'
            ));
            $xmlWorld->element('name', 'Ecuador');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'EG'
            ));
            $xmlWorld->element('name', 'Egypt');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'ES'
            ));
            $xmlWorld->element('name', 'El Salvador');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'EK'
            ));
            $xmlWorld->element('name', 'Equatorial Guinea');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'ER'
            ));
            $xmlWorld->element('name', 'Eritrea');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'EN'
            ));
            $xmlWorld->element('name', 'Estonia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'ET'
            ));
            $xmlWorld->element('name', 'Ethiopia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FK'
            ));
            $xmlWorld->element('name', 'Falkland Islands (Islas Malvinas)');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FO'
            ));
            $xmlWorld->element('name', 'Faroe Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FM'
            ));
            $xmlWorld->element('name', 'Federated States of Micronesia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FJ'
            ));
            $xmlWorld->element('name', 'Fiji');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FI'
            ));
            $xmlWorld->element('name', 'Finland');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FR'
            ));
            $xmlWorld->element('name', 'France');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FG'
            ));
            $xmlWorld->element('name', 'French Guiana');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'FP'
            ));
            $xmlWorld->element('name', 'French Polynesia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GB'
            ));
            $xmlWorld->element('name', 'Gabon');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GA'
            ));
            $xmlWorld->element('name', 'The Gambia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GG'
            ));
            $xmlWorld->element('name', 'Georgia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GM'
            ));
            $xmlWorld->element('name', 'Germany');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GH'
            ));
            $xmlWorld->element('name', 'Ghana');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GI'
            ));
            $xmlWorld->element('name', 'Gibraltar');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GO'
            ));
            $xmlWorld->element('name', 'Glorioso Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GR'
            ));
            $xmlWorld->element('name', 'Greece');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GL'
            ));
            $xmlWorld->element('name', 'Greenland');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GJ'
            ));
            $xmlWorld->element('name', 'Grenada');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GP'
            ));
            $xmlWorld->element('name', 'Guadeloupe');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GQ'
            ));
            $xmlWorld->element('name', 'Guam');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GT'
            ));
            $xmlWorld->element('name', 'Guatemala');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GK'
            ));
            $xmlWorld->element('name', 'Guernsey');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PU'
            ));
            $xmlWorld->element('name', 'Guinea-Bissau');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GV'
            ));
            $xmlWorld->element('name', 'Guinea');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GY'
            ));
            $xmlWorld->element('name', 'Guyana');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'HA'
            ));
            $xmlWorld->element('name', 'Haiti');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'HM'
            ));
            $xmlWorld->element('name', 'Heard Island &amp; McDonald Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'HO'
            ));
            $xmlWorld->element('name', 'Honduras');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'HU'
            ));
            $xmlWorld->element('name', 'Hungary');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IC'
            ));
            $xmlWorld->element('name', 'Iceland');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IN'
            ));
            $xmlWorld->element('name', 'India');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IO'
            ));
            $xmlWorld->element('name', 'British Indian Ocean Territory');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'ID'
            ));
            $xmlWorld->element('name', 'Indonesia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IR'
            ));
            $xmlWorld->element('name', 'Iran');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IZ'
            ));
            $xmlWorld->element('name', 'Iraq');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'EI'
            ));
            $xmlWorld->element('name', 'Ireland');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IM'
            ));
            $xmlWorld->element('name', 'Isle of Man');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IS'
            ));
            $xmlWorld->element('name', 'Israel');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'GZ'
            ));
            $xmlWorld->element('name', 'Palestinian Authority');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'IT'
            ));
            $xmlWorld->element('name', 'Italy');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'JM'
            ));
            $xmlWorld->element('name', 'Jamaica');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'JN'
            ));
            $xmlWorld->element('name', 'Jan Mayen');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'JA'
            ));
            $xmlWorld->element('name', 'Japan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'DQ'
            ));
            $xmlWorld->element('name', 'Jarvis Island');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'JE'
            ));
            $xmlWorld->element('name', 'Jersey');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'JQ'
            ));
            $xmlWorld->element('name', 'Johnston Atoll');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'JO'
            ));
            $xmlWorld->element('name', 'Jordan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'JU'
            ));
            $xmlWorld->element('name', 'Juan De Nova Island');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'KZ'
            ));
            $xmlWorld->element('name', 'Kazakhstan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'KE'
            ));
            $xmlWorld->element('name', 'Kenya');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'KR'
            ));
            $xmlWorld->element('name', 'Kiribati');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'KS'
            ));
            $xmlWorld->element('name', 'South Korea');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'KU'
            ));
            $xmlWorld->element('name', 'Kuwait');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'KG'
            ));
            $xmlWorld->element('name', 'Kyrgyzstan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LA'
            ));
            $xmlWorld->element('name', 'Laos');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LG'
            ));
            $xmlWorld->element('name', 'Latvia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LE'
            ));
            $xmlWorld->element('name', 'Lebanon');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LT'
            ));
            $xmlWorld->element('name', 'Lesotho');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LI'
            ));
            $xmlWorld->element('name', 'Liberia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LY'
            ));
            $xmlWorld->element('name', 'Libya');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LS'
            ));
            $xmlWorld->element('name', 'Liechtenstein');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LH'
            ));
            $xmlWorld->element('name', 'Lithuania');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LU'
            ));
            $xmlWorld->element('name', 'Luxembourg');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MC'
            ));
            $xmlWorld->element('name', 'Macau');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MK'
            ));
            $xmlWorld->element('name', 'Macedonia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MA'
            ));
            $xmlWorld->element('name', 'Madagascar');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MI'
            ));
            $xmlWorld->element('name', 'Malawi');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MY'
            ));
            $xmlWorld->element('name', 'Malaysia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MV'
            ));
            $xmlWorld->element('name', 'Maldives');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'ML'
            ));
            $xmlWorld->element('name', 'Mali');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MT'
            ));
            $xmlWorld->element('name', 'Malta');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'RM'
            ));
            $xmlWorld->element('name', 'Marshall Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MB'
            ));
            $xmlWorld->element('name', 'Martinique');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MR'
            ));
            $xmlWorld->element('name', 'Mauritania');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MP'
            ));
            $xmlWorld->element('name', 'Mauritius');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MF'
            ));
            $xmlWorld->element('name', 'Mayotte');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MX'
            ));
            $xmlWorld->element('name', 'Mexico');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MQ'
            ));
            $xmlWorld->element('name', 'Midway Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MD'
            ));
            $xmlWorld->element('name', 'Moldova');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MN'
            ));
            $xmlWorld->element('name', 'Monaco');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MG'
            ));
            $xmlWorld->element('name', 'Mongolia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MH'
            ));
            $xmlWorld->element('name', 'Montserrat');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MO'
            ));
            $xmlWorld->element('name', 'Morocco');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MZ'
            ));
            $xmlWorld->element('name', 'Mozambique');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BM'
            ));
            $xmlWorld->element('name', 'Myanmar (Burma)');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'WA'
            ));
            $xmlWorld->element('name', 'Namibia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NR'
            ));
            $xmlWorld->element('name', 'Nauru');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NP'
            ));
            $xmlWorld->element('name', 'Nepal');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NT'
            ));
            $xmlWorld->element('name', 'Netherlands Antilles');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NL'
            ));
            $xmlWorld->element('name', 'Netherlands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NC'
            ));
            $xmlWorld->element('name', 'New Caledonia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NZ'
            ));
            $xmlWorld->element('name', 'New Zealand');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NU'
            ));
            $xmlWorld->element('name', 'Nicaragua');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NG'
            ));
            $xmlWorld->element('name', 'Niger');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NI'
            ));
            $xmlWorld->element('name', 'Nigeria');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NE'
            ));
            $xmlWorld->element('name', 'Niue');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NF'
            ));
            $xmlWorld->element('name', 'Norfolk Island');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'KN'
            ));
            $xmlWorld->element('name', 'North Korea');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CQ'
            ));
            $xmlWorld->element('name', 'Northern Mariana Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NO'
            ));
            $xmlWorld->element('name', 'Norway');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MU'
            ));
            $xmlWorld->element('name', 'Oman');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PS'
            ));
            $xmlWorld->element('name', 'Pacific Islands (Palau)');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PK'
            ));
            $xmlWorld->element('name', 'Pakistan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PM'
            ));
            $xmlWorld->element('name', 'Panama');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PP'
            ));
            $xmlWorld->element('name', 'Papua New Guinea');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PA'
            ));
            $xmlWorld->element('name', 'Paraguay');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PE'
            ));
            $xmlWorld->element('name', 'Peru');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'RP'
            ));
            $xmlWorld->element('name', 'Philippines');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PL'
            ));
            $xmlWorld->element('name', 'Poland');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PO'
            ));
            $xmlWorld->element('name', 'Portugal');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'RQ'
            ));
            $xmlWorld->element('name', 'Puerto Rico');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'QA'
            ));
            $xmlWorld->element('name', 'Qatar');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'RE'
            ));
            $xmlWorld->element('name', 'Reunion');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'RO'
            ));
            $xmlWorld->element('name', 'Romania');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'RS'
            ));
            $xmlWorld->element('name', 'Russia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'RW'
            ));
            $xmlWorld->element('name', 'Rwanda');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SM'
            ));
            $xmlWorld->element('name', 'San Marino');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TP'
            ));
            $xmlWorld->element('name', 'Sao Tome and Principe');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SA'
            ));
            $xmlWorld->element('name', 'Saudi Arabia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SG'
            ));
            $xmlWorld->element('name', 'Senegal');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'KV'
            ));
            $xmlWorld->element('name', 'Kosovo');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'MW'
            ));
            $xmlWorld->element('name', 'Montenegro');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SR'
            ));
            $xmlWorld->element('name', 'Serbia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SE'
            ));
            $xmlWorld->element('name', 'Seychelles');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SL'
            ));
            $xmlWorld->element('name', 'Sierra Leone');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SN'
            ));
            $xmlWorld->element('name', 'Singapore');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'LO'
            ));
            $xmlWorld->element('name', 'Slovakia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SI'
            ));
            $xmlWorld->element('name', 'Slovenia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'BP'
            ));
            $xmlWorld->element('name', 'Solomon Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SO'
            ));
            $xmlWorld->element('name', 'Somalia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SF'
            ));
            $xmlWorld->element('name', 'South Africa');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SX'
            ));
            $xmlWorld->element('name', 'South Georgia and the South Sandwich Is');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SP'
            ));
            $xmlWorld->element('name', 'Madrid');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SP'
            ));
            $xmlWorld->element('name', 'Spain');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'PG'
            ));
            $xmlWorld->element('name', 'Spratly Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'CE'
            ));
            $xmlWorld->element('name', 'Sri Lanka');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SC'
            ));
            $xmlWorld->element('name', 'St. Kitts and Nevis');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'ST'
            ));
            $xmlWorld->element('name', 'St. Lucia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SB'
            ));
            $xmlWorld->element('name', 'St. Pierre and Miquelon');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'VC'
            ));
            $xmlWorld->element('name', 'St. Vincent and the Grenadines');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SU'
            ));
            $xmlWorld->element('name', 'Sudan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NS'
            ));
            $xmlWorld->element('name', 'Suriname');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SV'
            ));
            $xmlWorld->element('name', 'Svalbard');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'WZ'
            ));
            $xmlWorld->element('name', 'Swaziland');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SW'
            ));
            $xmlWorld->element('name', 'Sweden');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SZ'
            ));
            $xmlWorld->element('name', 'Switzerland');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'SY'
            ));
            $xmlWorld->element('name', 'Syria');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TE'
            ));
            $xmlWorld->element('name', 'East Timor');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TW'
            ));
            $xmlWorld->element('name', 'Taiwan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TI'
            ));
            $xmlWorld->element('name', 'Tajikistan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TZ'
            ));
            $xmlWorld->element('name', 'United Republic of Tanzania');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TH'
            ));
            $xmlWorld->element('name', 'Thailand');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TO'
            ));
            $xmlWorld->element('name', 'Togo');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TL'
            ));
            $xmlWorld->element('name', 'Tokelau');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TN'
            ));
            $xmlWorld->element('name', 'Tonga');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TD'
            ));
            $xmlWorld->element('name', 'Trinidad and Tobago');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TS'
            ));
            $xmlWorld->element('name', 'Tunisia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TU'
            ));
            $xmlWorld->element('name', 'Turkey');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TX'
            ));
            $xmlWorld->element('name', 'Turkmenistan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TK'
            ));
            $xmlWorld->element('name', 'Turks and Caicos Islands');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TV'
            ));
            $xmlWorld->element('name', 'Tuvalu');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'UG'
            ));
            $xmlWorld->element('name', 'Uganda');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'UP'
            ));
            $xmlWorld->element('name', 'Ukraine');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'TC'
            ));
            $xmlWorld->element('name', 'United Arab Emirates');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'UK'
            ));
            $xmlWorld->element('name', 'United Kingdom');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'US'
            ));
            $xmlWorld->element('name', 'United States');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'UY'
            ));
            $xmlWorld->element('name', 'Uruguay');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'UZ'
            ));
            $xmlWorld->element('name', 'Uzbekistan');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'NH'
            ));
            $xmlWorld->element('name', 'Vanuatu');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'VE'
            ));
            $xmlWorld->element('name', 'Venezuela');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'VM'
            ));
            $xmlWorld->element('name', 'Vietnam');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'WQ'
            ));
            $xmlWorld->element('name', 'Wake Island');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'WI'
            ));
            $xmlWorld->element('name', 'Western Sahara');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'WS'
            ));
            $xmlWorld->element('name', 'Western Samoa');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'YM'
            ));
            $xmlWorld->element('name', 'Yemen');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'ZA'
            ));
            $xmlWorld->element('name', 'Zambia');
            $xmlWorld->pop();
            $xmlWorld->push('state', array(
                'id' => 'ZI'
            ));
            $xmlWorld->element('name', 'Zimbabwe');
            $xmlWorld->pop();
            
            
            $xmlWorld->pop();
            $fp = fopen($this->getRegistry()->config->xmlFiles->path . "\\worldmap\\data.xml", 'w+');
            fwrite($fp, $xmlWorld->getXml());
            fclose($fp);
            /*
            var_dump($result);
            die();*/
            
            if (!$result->isEmpty()) {
                $indice = 0;
                
                $xml = new XmlWriter2();
                $xml->push('map', array(
                    'map_file' => 'ammap/maps/mexico.swf',
                    'tl_long' => '-118.390549',
                    'tl_lat' => '32.77794',
                    'br_long' => '-86.735161',
                    'br_lat' => '14.59382',
                    'zoom' => '86.6071%',
                    'zoom_x' => '5.69%',
                    'zoom_y' => '8.75%'
                ));
                $xml->push('areas');
                
                $xml->element('area', '', array(
                    'mc_name' => 'aguascalientes',
                    'title' => 'Aguascalientes: ' . (Int) $dataArray['aguascalientes'] . ' visitante(s)',
                    'color_hover' => $colorHover['aguascalientes'],
                    'color' => $colorNormal['aguascalientes']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'baja_california',
                    'title' => 'Baja California: ' . (Int) $dataArray['baja_california'] . ' visitante(s)',
                    'color_hover' => $colorHover['baja_california'],
                    'color' => $colorNormal['baja_california']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'baja_california_sur',
                    'title' => 'Baja California Sur: ' . (Int) $dataArray['baja_california_sur'] . ' visitante(s)',
                    'color_hover' => $colorHover['baja_california_sur'],
                    'color' => $colorNormal['baja_california_sur']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'campeche',
                    'title' => 'Campeche: ' . (Int) $dataArray['campeche'] . ' visitante(s)',
                    'color_hover' => $colorHover['campeche'],
                    'color' => $colorNormal['campeche']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'chiapas',
                    'title' => 'Chiapas: ' . (Int) $dataArray['chiapas'] . ' visitante(s)',
                    'color_hover' => $colorHover['chiapas'],
                    'color' => $colorNormal['chiapas']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'chihuahua',
                    'title' => 'Chihuahua: ' . (Int) $dataArray['chihuahua'] . ' visitante(s)',
                    'color_hover' => $colorHover['chihuahua'],
                    'color' => $colorNormal['chihuahua']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'coahuila',
                    'title' => 'Coahuila: ' . (Int) $dataArray['coahuila'] . ' visitante(s)',
                    'color_hover' => $colorHover['coahuila'],
                    'color' => $colorNormal['coahuila']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'colima',
                    'title' => 'Colima: ' . (Int) $dataArray['colima'] . ' visitante(s)',
                    'color_hover' => $colorHover['colima'],
                    'color' => $colorNormal['colima']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'distrito_federal',
                    'title' => 'Distrito Federal (Mexico): ' . (Int) $dataArray['distrito_federal'] . ' visitante(s)',
                    'color_hover' => $colorHover['distrito_federal'],
                    'color' => $colorNormal['distrito_federal']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'durango',
                    'title' => 'Durango: ' . (Int) $dataArray['durango'] . ' visitante(s)',
                    'color_hover' => $colorHover['durango'],
                    'color' => $colorNormal['durango']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'federally_administered_territory',
                    'title' => 'Federally Administered Territory: ' . (Int) $dataArray['federally_administered_territory'] . ' visitante(s)',
                    'color_hover' => $colorHover['federally_administered_territory'],
                    'color' => $colorNormal['federally_administered_territory']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'guanajuato',
                    'title' => 'Guanajuato: ' . (Int) $dataArray['guanajuato'] . ' visitante(s)',
                    'color_hover' => $colorHover['guanajuato'],
                    'color' => $colorNormal['guanajuato']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'guerrero',
                    'title' => 'Guerrero: ' . (Int) $dataArray['guerrero'] . ' visitante(s)',
                    'color_hover' => $colorHover['guerrero'],
                    'color' => $colorNormal['guerrero']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'hidalgo',
                    'title' => 'Hidalgo: ' . (Int) $dataArray['hidalgo'] . ' visitante(s)',
                    'color_hover' => $colorHover['hidalgo'],
                    'color' => $colorNormal['hidalgo']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'jalisco',
                    'title' => 'Jalisco: ' . (Int) $dataArray['jalisco'] . ' visitante(s)',
                    'color_hover' => $colorHover['jalisco'],
                    'color' => $colorNormal['jalisco']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'mexico',
                    'title' => 'Mexico: ' . (Int) $dataArray['mexico'] . ' visitante(s)',
                    'color_hover' => $colorHover['mexico'],
                    'color' => $colorNormal['mexico']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'michoacan',
                    'title' => 'Michoacan: ' . (Int) $dataArray['michoacan'] . ' visitante(s)',
                    'color_hover' => $colorHover['michoacan'],
                    'color' => $colorNormal['michoacan']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'morelos',
                    'title' => 'Morelos: ' . (Int) $dataArray['morelos'] . ' visitante(s)',
                    'color_hover' => $colorHover['morelos'],
                    'color' => $colorNormal['morelos']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'nayarit',
                    'title' => 'Nayarit: ' . (Int) $dataArray['nayarit'] . ' visitante(s)',
                    'color_hover' => $colorHover['nayarit'],
                    'color' => $colorNormal['nayarit']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'nuevo_leon',
                    'title' => 'Nuevo Leon: ' . (Int) $dataArray['nuevo_leon'] . ' visitante(s)',
                    'color_hover' => $colorHover['nuevo_leon'],
                    'color' => $colorNormal['nuevo_leon']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'oaxaca',
                    'title' => 'Oaxaca: ' . (Int) $dataArray['oaxaca'] . ' visitante(s)',
                    'color_hover' => $colorHover['oaxaca'],
                    'color' => $colorNormal['oaxaca']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'puebla',
                    'title' => 'Puebla: ' . (Int) $dataArray['puebla'] . ' visitante(s)',
                    'color_hover' => $colorHover['puebla'],
                    'color' => $colorNormal['puebla']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'queretaro',
                    'title' => 'Queretaro: ' . (Int) $dataArray['queretaro'] . ' visitante(s)',
                    'color_hover' => $colorHover['queretaro'],
                    'color' => $colorNormal['queretaro']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'quintana_roo',
                    'title' => 'Quintana Roo: ' . (Int) $dataArray['quintana_roo'] . ' visitante(s)',
                    'color_hover' => $colorHover['quintana_roo'],
                    'color' => $colorNormal['quintana_roo']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'san_luis_potosi',
                    'title' => 'San Luis Potosi: ' . (Int) $dataArray['san_luis_potosi'] . ' visitante(s)',
                    'color_hover' => $colorHover['san_luis_potosi'],
                    'color' => $colorNormal['san_luis_potosi']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'sinaloa',
                    'title' => 'Sinaloa: ' . (Int) $dataArray['sinaloa'] . ' visitante(s)',
                    'color_hover' => $colorHover['sinaloa'],
                    'color' => $colorNormal['sinaloa']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'sonora',
                    'title' => 'Sonora: ' . (Int) $dataArray['sonora'] . ' visitante(s)',
                    'color_hover' => $colorHover['sonora'],
                    'color' => $colorNormal['sonora']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'tabasco',
                    'title' => 'Tabasco: ' . (Int) $dataArray['tabasco'] . ' visitante(s)',
                    'color_hover' => $colorHover['tabasco'],
                    'color' => $colorNormal['tabasco']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'tamaulipas',
                    'title' => 'Tamaulipas: ' . (Int) $dataArray['tamaulipas'] . ' visitante(s)',
                    'color_hover' => $colorHover['tamaulipas'],
                    'color' => $colorNormal['tamaulipas']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'tlaxcala',
                    'title' => 'Tlaxcala: ' . (Int) $dataArray['tlaxcala'] . ' visitante(s)',
                    'color_hover' => $colorHover['tlaxcala'],
                    'color' => $colorNormal['tlaxcala']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'veracruz',
                    'title' => 'Veracruz: ' . (Int) $dataArray['veracruz'] . ' visitante(s)',
                    'color_hover' => $colorHover['veracruz'],
                    'color' => $colorNormal['veracruz']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'yucatan',
                    'title' => 'Yucatan: ' . (Int) $dataArray['yucatan'] . ' visitante(s)',
                    'color_hover' => $colorHover['yucatan'],
                    'color' => $colorNormal['yucatan']
                ));
                $xml->element('area', '', array(
                    'mc_name' => 'zacatecas',
                    'title' => 'Zacatecas: ' . (Int) $dataArray['zacatecas'] . ' visitante(s)',
                    'color_hover' => $colorHover['zacatecas'],
                    'color' => $colorNormal['zacatecas']
                ));
                
                $xml->pop();
                /*
                $xml->push('movies');             
                $xml->pop();*/
                $xml->pop();
                //$this->view->content=$xml->getXml();
                //$this->view->setTpl('xml');
                $fp = fopen($this->getRegistry()->config->xmlFiles->path . "\\ammap\\ammap_data.xml", 'w+');
                fwrite($fp, $xml->getXml());
                fclose($fp);
            }
        } else {
            $this->setFlash("error", "No se recibio ningun valor");
            $this->_redirect('index/index');
        }
        
        //print_r($nameTpT);
        $this->setTitle('Histórico por ubicación');
        $this->view->dates      = $dates;
        $this->view->streamName = $streamObj->getName();
        $this->view->setTpl('GeolocationHelixProcess');
    }
    
    public function downloadGeolocationHelixAction()
    {
        require_once 'lib/excel/excel.php';
        require_once 'lib/excel/excel-ext.php';
        $this->view->setLayoutFile(false);
        $this->_helper->viewRenderer->setNoRender();
        $geolocation      = $this->getUser()->getAttribute('rgeolocation', 0);
        $ips              = $this->getUser()->getAttribute('ipClients', 0);
        $totalViewersByIp = $this->getUser()->getAttribute('totalviewersbyip', 0);
        
        set_time_limit(0);
        
        $assoc = array();
        
        $visitasMex = 0;
        if (count($geolocation) > 0) {
            foreach ($geolocation as $key => $value) {
                $assoc[] = array(
                    "Entidad" => $key,
                    "Visitantes" => $value
                );
                $visitasMex += $value;
            }
        }
        
        if (count($ips) > 0) {
            foreach ($ips as $ip) {
                $assoc[] = array(
                    "Ip" => $ip
                );
            }
        }
        
        $assoc[] = array(
            "Entidad" => "Total visitas: ",
            "Visitantes" => $visitasMex
        );
        //$assoc[]= array("Entidad" => "Otros paises: ", "Visitantes" => $totalViewersByIp-$visitasMex);
       // $assoc[]= array("Entidad" => "Total visitas General: ", "Visitantes" => $totalViewersByIp);
        
        createExcel("reporte-geoMexico-Helix.xls", $assoc);
        
        exit;
    }
    
    private function findMatch($string, $patron)
    {
        //echo $string." => ".$patron."\n";
        $tempOne = explode('_', $string);
        $tempTwo = explode(' ', $patron);
        
        $flag = false;
        if (count($tempOne) > 1) {
            foreach ($tempOne as $it) {
                if (count($tempTwo) > 1) {
                    foreach ($tempTwo as $child) {
                        if ($child == $it) {
                            $flag = true;
                        }
                    }
                } else {
                    if ($tempTwo[0] == $it) {
                        $flag = true;
                    }
                }
            }
        } else {
            if (count($tempTwo) > 1) {
                foreach ($tempTwo as $child) {
                    if ($child == $tempOne[0]) {
                        $flag = true;
                    }
                }
            } else {
                if ($tempTwo[0] == $tempOne[0]) {
                    $flag = true;
                }
            }
        }
        
        return $flag;
    }
    
    
    function Dot2LongIP($IPaddr)
    {
        if ($IPaddr == "") {
            return 0;
        } else {
            $ips = split("\.", "$IPaddr");
            return ($ips[3] + $ips[2] * 256 + $ips[1] * 256 * 256 + $ips[0] * 256 * 256 * 256);
        }
    }
    
    
    public function mapHelixAction()
    {
        set_time_limit(0);
        
        require_once 'application/models/catalogs/HelixIpviewerStreamCatalog.php';
        require_once 'application/models/catalogs/LocationCatalog.php';
        require_once 'application/models/catalogs/HelixStreamCatalog.php';
	    require_once 'application/models/catalogs/HelixStreamStatCatalog.php';
		require_once 'application/models/catalogs/HelixStreamCatalog.php';
		require_once 'application/models/catalogs/HelixIpviewerStreamCatalog.php';
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Log.php';
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Log\Writer\Stream.php';
		require_once 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Zend\Exception.php';
		
		$logger = new Zend_Log();
		$writer = new Zend_Log_Writer_Stream('c:/file.txt');
		$logger->addWriter($writer);
         
        $this->getUser()->setAttribute('rgeolocation', null);
        
        
        $dateStart  = $this->getRequest()->getParam('date_start');
        $dateEnd    = $this->getRequest()->getParam('date_end');
        $initHour   = $this->getRequest()->getParam('initHour');
        $finishHour = $this->getRequest()->getParam('finishHour');
        
        $flagstream = (int) $this->getRequest()->getParam('flagStream', 0);
        $flagvideo  = (int) $this->getRequest()->getParam('flagVideo', 0);
        
        if (!empty($flagstream)) {
            $id        = $this->getRequest()->getParam('stream');
            $streamObj = HelixStreamCatalog::getInstance(0)->getById($id);
        } elseif (!empty($flagvideo)) {
            $id = $this->getRequest()->getParam('vod');
        }
        $idSteam	= $id;
        
        $dates = array();
        if ((!is_null($dateStart) & !empty($dateStart)) && (!is_null($dateEnd) & !empty($dateEnd))) {
            $dates[] = $dateStart . ' ' . $initHour . ':00';
			$fecha1=$dateStart . ' ' . $initHour . ':00';
            $dates[] = $dateEnd . ' ' . $finishHour . ':00';
			$fecha2=$dateEnd . ' ' . $finishHour . ':00';
        } else {
            if (!is_null($dateStart) & !empty($dateStart)) {
                $dates[] = $dateStart . ' ' . $initHour . ':00';
				$fecha1=$dateStart . ' ' . $initHour . ':00';
                $dates[] = $dateStart . ' ' . $finishHour . ':00';
				$fecha2=$dateStart . ' ' . $finishHour . ':00';
            }
            if (!is_null($dateEnd) & !empty($dateEnd)) {
                $dates[] = $dateEnd . ' ' . $initHour . ':00';
				$fecha1=$dateEnd . ' ' . $initHour . ':00';
                $dates[] = $dateEnd . ' ' . $finishHour . ':00';
				$fecha2=$dateEnd . ' ' . $finishHour . ':00';
            }
        }
        $result = array();
        
        $locationCatalog      = LocationCatalog::getInstance();
        $helixIpViewerCatalog = HelixIpviewerStreamCatalog::getInstance();
        if ($id != 0) {
            $criteria = new Criteria();
            
            if (!empty($flagstream)) {
                $criteria->add('id_stream', $id, Criteria::EQUAL);
            } elseif (!empty($flagvideo)) {
                $criteria->add('id_vod', $id, Criteria::EQUAL);
            }
            
            $criteria->add('status', 1, Criteria::EQUAL);
            $criteria->add('timestamp', $dates, Criteria::BETWEEN);
            //$criteria->addGroupByColumn('`timestamp`');
            $criteria->addGroupByColumn('`ipviewer`');
            $criteria->addGroupByColumn('`user_agent`');
            
            
            if (!empty($flagstream)) {
                $result = $helixIpViewerCatalog->getByCriteria($criteria);
            } elseif (!empty($flagvideo)) {
                //$result = $viewerVodCatalog->getByCriteria($criteria);
            }
            
            $link = mysql_connect("localhost", "xsnuser","Xsnus3rpass1"); 
			mysql_select_db("xsn_stats", $link); 
			$sql="SELECT * FROM xsn_core_helix_stream where fInicio >='".$fecha1."' and fFin <='".$fecha2."' and idsenal='".$idSteam."'";
			$result1 = mysql_query($sql, $link); 
			if (!$result1){die('ERROR CONEXION CON BD: '.mysql_error());}

	          
            
            
            if (true) {

               // $this->getUser()->setAttribute('totalviewersbyip', count($result));
                

                $ipAccess  = array();
                $latlon    = array();
                $ipClients = array();

                //foreach ($result as $item) {
				while ($row = mysql_fetch_array($result1)){ 

                    try {
                        $criteria    = new Criteria();
                        //$criteria->add('ip_start', $item->getIpviewer(), Criteria::LESS_OR_EQUAL, Criteria::INET_ATON);
                        $ipCli       = $this->Dot2LongIP($row[1]);
                        $ipClients[] = $row[1];
                        $criteria->add('ip_start', $ipCli, Criteria::LESS_OR_EQUAL);							
						$criteria->addDescendingOrderByColumn('ip_start');
                        $criteria->setLimit(1);
                        //$resultGeo= $geolocationCatalog->getByCriteria($criteria)->getOne();
                        
                        $location = $locationCatalog->getByCriteria($criteria)->getOne();
                        //var_dump($location);
                        if ($location instanceof Location) {
                            //$location=$locationCatalog->getById($resultgc->getLocation());
                            
                            if (!is_null($location->getCountry()) && $location->getCountry() != '-') {
                                ///$nameregion=$location->getCity();	

                                if (!is_null($ipAccess[$location->getCountry().','.$location->getCity()]) && isset($ipAccess[$location->getCountry().','.$location->getCity()])) {
                                    $ipAccess[$location->getCountry().','.$location->getCity()]++;
                                } else {
                                    $ipAccess[$location->getCountry().','.$location->getCity()] = 1;
                                    $latlon[$location->getCountry().','.$location->getCity()]   = $location->getLatitude() . ',' . $location->getLongitude();
                                }
                            }
                            
                            //$coordenadas=$location->getLatitude().','.$location->getLongitude();
                            
                            //$coordenadas
                            //$nameregion									
                            
                        }
                        
                        
                        //aqui
                    }
                    catch (Exception $e) {
                        echo "Error del tipo: " . $e;
                        //$this->view->setTpl('GeolocationHelixProcess'); 
                        $this->setFlash("error", "Error del tipo: " . $e);
                        $this->_redirect('index/index');
                        return false;
                    }
                }
		   	
                $this->getUser()->setAttribute('ipClients', $ipClients);
                $this->getUser()->setAttribute('rgeolocation', $ipAccess);
            }
        } else {
            $this->setFlash("error", "No se recibio ningun valor");
            $this->_redirect('index/index');
        }
        
        //print_r($ipAccess);
        echo ".";
        $this->setTitle('Histórico por ubicación');
        $this->view->dates      = $dates;
        $this->view->streamName = $streamObj->getName();
        $this->view->ipAccess   = $ipAccess;
		$this->view->tamanio 	= count($ipAccess);
        $this->view->latlon     = $latlon;
        $this->view->setTpl('mapHelix');
    }
    
    public function downloadMapsHelixAction()
    {
        require_once 'lib/excel/excel.php';
        require_once 'lib/excel/excel-ext.php';
        $this->view->setLayoutFile(false);
        $this->_helper->viewRenderer->setNoRender();
        $geolocation      = $this->getUser()->getAttribute('rgeolocation', 0);
        $ips              = $this->getUser()->getAttribute('ipClients', 0);
        $totalViewersByIp = $this->getUser()->getAttribute('totalviewersbyip', 0);
        
        set_time_limit(0);
        
        $assoc = array();
        
        $visitasMex = 0;
        if (count($geolocation) > 0) {
            foreach ($geolocation as $key => $value) {
                $assoc[] = array(
                    "Pais" => $key,
                    "Visitantes" => $value
                );
                $visitasMex += $value;
            }
        }
        $assoc[] = array(
            "Entidad" => "Total visitas: ",
            "Visitantes" => $visitasMex
        );
        $assoc[] = array(
            "Entidad" => "",
            "Visitantes" => ""
        );
        $assoc[] = array(
            "Entidad" => "Ip de Acceso",
            "Visitantes" => ""
        );
        if (count($ips) > 0) {
            foreach ($ips as $ip) {
                $assoc[] = array(
                    "Ip" => $ip
                );
            }
        }
        
        createExcel("reporte-geoMexico-Helix.xls", $assoc);
        
        exit;
    }
    
    
    public function mapFlashAction()
    {
        set_time_limit(0);
        require_once 'application/models/catalogs/FlashIpviewerStreamCatalog.php';
        require_once 'application/models/catalogs/FlashIpviewerVodCatalog.php';
        require_once 'application/models/catalogs/LocationCatalog.php';
        require_once 'application/models/catalogs/FlashStreamCatalog.php';
        
        
        $this->getUser()->setAttribute('rgeolocation', null);
        
        $dateStart  = $this->getRequest()->getParam('date_start');
        $dateEnd    = $this->getRequest()->getParam('date_end');
        $initHour   = $this->getRequest()->getParam('initHour');
        $finishHour = $this->getRequest()->getParam('finishHour');
        
        
        $flagstream = (int) $this->getRequest()->getParam('flagStream', 0);
        $flagvideo  = (int) $this->getRequest()->getParam('flagVideo', 0);
        
        if (!empty($flagstream)) {
            $id          = $this->getRequest()->getParam('stream');
            $flashObject = FlashStreamCatalog::getInstance()->getById($id);
        } elseif (!empty($flagvideo)) {
            $id = $this->getRequest()->getParam('vod');
        }
        
        
        $dates = array();
        if ((!is_null($dateStart) & !empty($dateStart)) && (!is_null($dateEnd) & !empty($dateEnd))) {
            $dates[] = $dateStart . ' ' . $initHour . ':00';
			$fecha1=$dateStart . ' ' . $initHour . ':00';
            $dates[] = $dateEnd . ' ' . $finishHour . ':00';
			$fecha2=$dateEnd . ' ' . $finishHour . ':00';
        } else {
            if (!is_null($dateStart) & !empty($dateStart)) {
                $dates[] = $dateStart . ' ' . $initHour . ':00';
				$fecha1=$dateStart . ' ' . $initHour . ':00';
                $dates[] = $dateStart . ' ' . $finishHour . ':00';
				$fecha2=$dateStart . ' ' . $finishHour . ':00';
            }
            if (!is_null($dateEnd) & !empty($dateEnd)) {
                $dates[] = $dateEnd . ' ' . $initHour . ':00';
				$fecha1=$dateEnd . ' ' . $initHour . ':00';
                $dates[] = $dateEnd . ' ' . $finishHour . ':00';
				$fecha2=$dateEnd . ' ' . $finishHour . ':00';
            }
        }
        $result = array();
        
        $viewerLiveCatalog = FlashIpviewerStreamCatalog::getInstance(1);
        $viewerVodCatalog  = FlashIpviewerVodCatalog::getInstance(1);
        
        if ($id != 0) {
            $criteria = new Criteria();
            
            if (!empty($flagstream)) {
                $criteria->add('id_stream', $id, Criteria::EQUAL);
            } elseif (!empty($flagvideo)) {
                $criteria->add('id_vod', $id, Criteria::EQUAL);
            }
            
            $criteria->add('status', 1, Criteria::EQUAL);
            $criteria->add('timestamp', $dates, Criteria::BETWEEN);
            
            
            if (!empty($flagstream)) {
                $result = $viewerLiveCatalog->getByCriteria($criteria);
            } elseif (!empty($flagvideo)) {
                $result = $viewerVodCatalog->getByCriteria($criteria);
            }
            
            if (!$result->isEmpty()) {
                $this->getUser()->setAttribute('totalviewersbyip', count($result));
                
                //$dataArray=array();
                
                
                //$nameTpT=array();
                $locationCatalog = LocationCatalog::getInstance(0);
                $ipClients       = array();
                
                
                $ipAccess = array();
                $latlon   = array();
                
                foreach ($result as $item) {
                    try {
                        $criteria    = new Criteria();
                        //$criteria->add('ip_start', $item->getIpviewer(), Criteria::LESS_OR_EQUAL, Criteria::INET_ATON);
                        $ipCli       = $this->Dot2LongIP($item->getIpviewer());
                        $ipClients[] = $item->getIpviewer();
                        $criteria->add('ip_start', $ipCli, Criteria::LESS_OR_EQUAL);
                        $criteria->addDescendingOrderByColumn('ip_start');
                        $criteria->setLimit(1);
                        //$resultGeo= $geolocationCatalog->getByCriteria($criteria)->getOne();
                        
                        $location = $locationCatalog->getByCriteria($criteria)->getOne();
                        
                        if ($location instanceof Location) {
                            if (!is_null($location->getCountry()) && $location->getCountry() != '-') {
 
                                ///$nameregion=$location->getCity();	
                                if (!is_null($ipAccess[$location->getCountry()]) && isset($ipAccess[$location->getCountry()])) {
                                    $ipAccess[$location->getCountry()]++;
                                } else {
                                    $ipAccess[$location->getCountry()] = 1;
                                    $latlon[$location->getCountry()]   = $location->getLatitude() . ',' . $location->getLongitude();
                                }
                            }
                        }
                        
                        //aqui
                    }
                    catch (Exception $e) {
                        echo "Error del tipo: " . $e;
                        return false;
                    }
                }
                $this->getUser()->setAttribute('ipClientsF', $ipClients);
                $this->getUser()->setAttribute('rgeolocation', $ipAccess);
            }
        } else {
            $this->setFlash("error", "No se recibio ningun valor");
            $this->_redirect('index/index');
        }
        
        //print_r($ipAccess);
        echo "1";
        $this->setTitle('Histórico por ubicación');
        $this->view->dates      = $dates;
        $this->view->streamName = $flashObject->getName();
        $this->view->ipAccess   = $ipAccess;
        $this->view->latlon     = $latlon;
        $this->view->setTpl('mapFlash');
    }
    
    public function downloadMapsFlashAction()
    {
        require_once 'lib/excel/excel.php';
        require_once 'lib/excel/excel-ext.php';
        $this->view->setLayoutFile(false);
        $this->_helper->viewRenderer->setNoRender();
        $geolocation      = $this->getUser()->getAttribute('rgeolocation', 0);
        $ips              = $this->getUser()->getAttribute('ipClientsF', 0);
        $totalViewersByIp = $this->getUser()->getAttribute('totalviewersbyip', 0);
        
        set_time_limit(0);
        
        $assoc = array();
        
        $visitasMex = 0;
        if (count($geolocation) > 0) {
            foreach ($geolocation as $key => $value) {
                $assoc[] = array(
                    "Pais" => $key,
                    "Visitantes" => $value
                );
                $visitasMex += $value;
            }
        }
        
        $assoc[] = array(
            "Entidad" => "Total visitas: ",
            "Visitantes" => $visitasMex
        );
        $assoc[] = array(
            "Entidad" => "",
            "Visitantes" => ""
        );
        $assoc[] = array(
            "Entidad" => "Ip de Acceso",
            "Visitantes" => ""
        );
        if (count($ips) > 0) {
            foreach ($ips as $ip) {
                $assoc[] = array(
                    "Ip" => $ip
                );
            }
        }
        
        createExcel("reporte-geoMexico.xls", $assoc);
        
        exit;
    }
}
