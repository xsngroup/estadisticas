<?php
require_once "lib/controller/BaseController.php";
require_once 'application/models/catalogs/CompanyCatalog.php';
require_once 'application/models/catalogs/FlashAppCatalog.php';
require_once 'application/models/catalogs/FlashAppStatCatalog.php';
require_once 'application/models/catalogs/StreamServerCatalog.php';
require_once 'application/models/catalogs/FlashSuscriberCatalog.php';
require_once 'application/models/catalogs/FlashLiveStatCatalog.php';

class FlashAppController extends BaseController
{
	public function statsAction()
	{		
		$id=$this->getUser()->getIdUser();
		$serverCatalog= StreamServerCatalog::getInstance();
		$companyCatalog= CompanyCatalog::getInstance();
		
		
        $company= $companyCatalog->getByUser($id)->getOne();
        
        if(!$company instanceof Company){
        	$this->setFlash("error","No existe ningun cliente asociado");
        	$this->_redirect('index/index');
        }

          $server = $serverCatalog->getByCompany($company->getIdCompany())->getOne();
        if(!$server instanceof StreamServer){
        	$this->setFlash("error","No existe ningun servidor asociado");
        	$this->_redirect('index/index');
        }
        
        //aqui el patch para obtener los datos de live solo del cliente igual aplicaria para el Vod
        
	    $criteria = new Criteria();
        $criteria->add('id_company', $company->getIdCompany(), Criteria::EQUAL);
        $relArray=$companyCatalog->getCompanyStreamServerRelations($criteria);
        
        $flashLiveStatCatalog=FlashLiveStatCatalog::getInstance();
        
        $liveStats = array();
        //print_r($relArray);
        foreach($relArray as $item)
        {
        	if(!is_null($item['id_stream_flash']))
        	{
	        	$idsStream= $item['id_stream_flash'];  
	        	$criteriaLive = new Criteria();
	        	$criteriaLive->add('id_stream',$idsStream,Criteria::EQUAL);
	        	$criteriaLive->addDescendingOrderByColumn('timestamp');
	        	$criteriaLive->setLimit(1);
	        	
	        	$liveStats[] = $flashLiveStatCatalog->getByCriteria($criteriaLive)->getOne();
        	}      	
        }        
        
        $flagLive=false;
        if(count($liveStats)>0){
        	$flagLive=true;
	        $concurrent=0;
	        
	        foreach($liveStats as $dataItem){
	        	if($dataItem instanceof FlashLiveStat){
	        	  //print_r($dataItem);
	        	  $concurrent+=$dataItem->getConnects();
	        	}
	        }
        }
        
        $appsFlash = FlashAppCatalog::getInstance()->getByIdServer($server->getIdServer());
        $result= array();
        if(!$appsFlash->isEmpty()){        	
        	foreach($appsFlash as $item){
        		$criteria = new Criteria();
        		$criteria->add('id_app', $item->getIdApp(), Criteria::EQUAL);
        		$criteria->addDescendingOrderByColumn('timestamp');
        		$criteria->setLimit(1);
        		
        		$appStat= FlashAppStatCatalog::getInstance()->getByCriteria($criteria)->getOne();
        		
        		if($appStat instanceof FlashAppStat)
        		{
        			$result[]=$appStat;
        		}
        	}
        }
        else{
        	$this->setFlash("error","No existe ninguna aplicacion asociada");
        	$this->_redirect('index/index');        	
        }
        
        $appsCombo = FlashAppCatalog::getInstance()->getByIdServer($server->getIdServer())->toCombo();
        
        

        
        
        $this->view->company=$company;
        $this->view->server=$server;
        $this->view->apps=$appsCombo;
        $this->view->result=$result;
        $this->view->concurrentLive=$concurrent;
        $this->view->flagLive=$flagLive;
        $this->view->setTpl('Stats');
	}	
	
	public function updateAction()
	{
       if($this->getRequest()->isXmlHttpRequest())  
        $this->view->setLayoutFile(false);		
		$id=$this->getUser()->getIdUser();
		$serverCatalog= StreamServerCatalog::getInstance();
		$companyCatalog= CompanyCatalog::getInstance();
		
		
        $company= $companyCatalog->getByUser($id)->getOne();
        
        if(!$company instanceof Company){
        	$this->setFlash("error","No existe ningun cliente asociado");
        	$this->_redirect('index/index');
        }

          $server = $serverCatalog->getByCompany($company->getIdCompany())->getOne();
        if(!$server instanceof StreamServer){
        	$this->setFlash("error","No existe ningun servidor asociado");
        	$this->_redirect('index/index');
        }
        
        //aqui el patch para obtener los datos de live solo del cliente igual aplicaria para el Vod
        
	    $criteria = new Criteria();
        $criteria->add('id_company', $company->getIdCompany(), Criteria::EQUAL);
        $relArray=$companyCatalog->getCompanyStreamServerRelations($criteria);
        
        $flashLiveStatCatalog=FlashLiveStatCatalog::getInstance();
        
        $liveStats = array();
        //print_r($relArray);
        foreach($relArray as $item)
        {
        	if(!is_null($item['id_stream_flash']))
        	{
	        	$idsStream= $item['id_stream_flash'];  
	        	$criteriaLive = new Criteria();
	        	$criteriaLive->add('id_stream',$idsStream,Criteria::EQUAL);
	        	$criteriaLive->addDescendingOrderByColumn('timestamp');
	        	$criteriaLive->setLimit(1);
	        	
	        	$liveStats[] = $flashLiveStatCatalog->getByCriteria($criteriaLive)->getOne();
        	}      	
        }        
        
        $flagLive=false;
        if(count($liveStats)>0){
        	$flagLive=true;
	        $concurrent=0;
	        foreach($liveStats as $dataItem){
	        	if($dataItem instanceof FlashLiveStat){
	        	   $concurrent+=$dataItem->getConnects();
	        	}   
	        }
        }
        
        $appsFlash = FlashAppCatalog::getInstance()->getByIdServer($server->getIdServer());
        $result= array();
        if(!$appsFlash->isEmpty()){        	
        	foreach($appsFlash as $item){
        		$criteria = new Criteria();
        		$criteria->add('id_app', $item->getIdApp(), Criteria::EQUAL);
        		$criteria->addDescendingOrderByColumn('timestamp');
        		$criteria->setLimit(1);
        		
        		$appStat= FlashAppStatCatalog::getInstance()->getByCriteria($criteria)->getOne();
        		
        		if($appStat instanceof FlashAppStat)
        		{
        			$result[]=$appStat;
        		}
        	}
        }
        else{
        	$this->setFlash("error","No existe ninguna aplicacion asociada");
        	$this->_redirect('index/index');        	
        }
        
        $appsCombo = FlashAppCatalog::getInstance()->getByIdServer($server->getIdServer())->toCombo();       
        
        $timeUpdate = date("H:i:s"); 
        $this->view->timeUpdate=$timeUpdate;       
        $this->view->company=$company;
        $this->view->server=$server;
        $this->view->apps=$appsCombo;
        $this->view->result=$result;
        $this->view->concurrentLive=$concurrent;
        $this->view->flagLive=$flagLive;      
        $this->view->setTpl('Ajax');		
	}
}
?>