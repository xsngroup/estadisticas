<?php
/**
 * WebServiceController
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';
require_once 'application/models/webservice/xsnstats.php';
class WebServiceController extends Zend_Controller_Action
{
	private $_WSDL_URI = "http://reporte.xsn.com.mx/web-service/soap?wsdl";
    /**
     * The default action - show the home page
     */
    public function indexAction ()
    {
        // TODO Auto-generated WebServiceController::indexAction() default action
    }
    public function soapAction()
    {
    	$this->_helper->viewRenderer->setNoRender();
        if(isset($_GET['wsdl'])) {
    		//return the WSDL
    		//echo "ok";
    		$this->handleWSDL();
		} else {
			//handle SOAP request
    		$this->handleSOAP();
		}    	
    }
    
	private function handleWSDL() {
		 set_time_limit ( 0 );
		require_once 'Zend/Soap/AutoDiscover.php';
		$autodiscover = new Zend_Soap_AutoDiscover();
    	$autodiscover->setClass('xsnstats');
    	$autodiscover->handle();
	}
    
	private function handleSOAP() {
		 set_time_limit ( 0 );
		require_once 'Zend/Soap/Server.php';
		$soap = new Zend_Soap_Server($this->_WSDL_URI); 		
    	$soap->setClass('xsnstats');
    	$soap->handle();
	}     
}
