<?php
/**
 * Xsn
 *
 * Xsn Monitor Stream Servers
 *
 * @category   Application
 * @package    Application_Controllers
 * @copyright  Copyright (c) 2010-2011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */

/**
 * Dependences
 */
require_once "lib/controller/CrudController.php";
require_once "application/models/catalogs/CompanyCatalog.php";
require_once 'lib/managers/UserManager.php';
require_once "application/models/catalogs/StreamServerCatalog.php";
require_once 'application/models/catalogs/FlashLiveCatalog.php';
require_once 'application/models/catalogs/FlashStreamCatalog.php';
require_once 'application/models/catalogs/EmailCatalog.php';
/**
 * CompanyController (CRUD for the Company Objects)
 *
 * @category   Project
 * @package    Project_Controllers
 * @copyright  Copyright (c) 2010-2011 Xsn Group (http://www.xsn.com.mx) 
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     <zetta> & <chentepixtol>
 * @version    1.0.2 SVN: $Revision$
 */
class CompanyController extends CrudController
{
    
    /**
     * alias for the list action
     */
    public function indexAction()
    {
        $this->_forward('list');
    }
    
    /**
     * List the objects Company actives
     */
    public function listAction()
    {
    	$relations=EmailCatalog::getInstance()->getEmailCompanyRelations();
    	$emails= array();
    	
    	foreach($relations as $item){
    		$temp=EmailCatalog::getInstance()->getById($item['id_email']);
    		$emails[$item['id_company']]=$temp->getEmail();
    	}
    	
    	$this->view->emails=$emails;
        $this->view->companys = CompanyCatalog::getInstance()->getActives();
        $this->view->status = Company::$StatusLabel;
        $this->view->accessRoles = AccessRoleCatalog::getInstance()->getByCriteria()->toCombo();
        $this->setTitle('List the Company');
    }
    
    /**
     * delete an Company
     */
    public function deleteAction()
    {
        $companyCatalog = CompanyCatalog::getInstance();
        $idCompany = $this->getRequest()->getParam('idCompany');
        $company = $companyCatalog->getById($idCompany);
        $companyCatalog->deactivate($company);
        $this->setFlash('ok','Successfully removed the Company');
        $this->_redirect('company/list');
    }
    
    /**
     * Form for edit an Company
     */
    public function editAction()
    {
        $companyCatalog = CompanyCatalog::getInstance();
        $idCompany = $this->getRequest()->getParam('idCompany');
        $company = $companyCatalog->getById($idCompany);
        $email = EmailCatalog::getInstance()->getByCompany($idCompany)->getOne();
        $idEmail=0;
        $emailTxt="";
        if($email instanceof Email){
          $idEmail=$email->getIdEmail();
          $emailTxt=$email->getEmail();
        }
        $post = array(
            'id_company' => $company->getIdCompany(),
            'name_company' => $company->getNameCompany(),
            'name_contac' => $company->getNameContac(),
            'telephone' => $company->getTelephone(),
            'status' => $company->getStatus(),
            'idEmail' => $idEmail,
            'email' => $emailTxt
        );
        $this->view->status = Company::$StatusLabel;
        $this->view->post = $post;
        $this->setTitle('Edit Company');
    }
    
    /**
     * Create an Company
     */
    public function createAction()
    {   
        $companyCatalog = CompanyCatalog::getInstance();
  
        $idUser=UserManager::getInstance()->create($this->getRequest(), $this->getUser()->getIdUser());
        
        $nameCompany = utf8_decode($this->getRequest()->getParam('name_company'));
        $nameContac = utf8_decode($this->getRequest()->getParam('name_contac'));
        $telephone = utf8_decode($this->getRequest()->getParam('telephone'));
        $email = utf8_decode($this->getRequest()->getParam('email'));        
        $status = utf8_decode($this->getRequest()->getParam('status'));
        
        
        $company = CompanyFactory::create($nameCompany, $nameContac, $telephone, $status);
        $companyCatalog->create($company);
        
        $email = EmailFactory::create($email);
        EmailCatalog::getInstance()->create($email);
        EmailCatalog::getInstance()->linkToCompany($email->getIdEmail(), $company->getIdCompany());

        $relation = CompanyCatalog::getInstance()->linkToUser($company->getIdCompany(), $idUser);
        $this->view->setTpl('_row');
        $this->view->setLayoutFile(false);
        $this->view->status = Company::$StatusLabel;
        $this->view->company = $company;
    }
    
    /**
     * Update an Company
     */
    public function updateAction()
    {
        $companyCatalog = CompanyCatalog::getInstance();
        $idCompany = $this->getRequest()->getParam('idCompany');
        $company = $companyCatalog->getById($idCompany);
        $company->setNameCompany($this->getRequest()->getParam('name_company'));
        $company->setNameContac($this->getRequest()->getParam('name_contac'));
        $company->setTelephone($this->getRequest()->getParam('telephone'));
        $company->setStatus($this->getRequest()->getParam('status'));
        $email = utf8_decode($this->getRequest()->getParam('email')); 
        $idEmail = $this->getRequest()->getParam('idEmail',0); 
        
        if(empty($idEmail)){
	        $email = EmailFactory::create($email);
	        EmailCatalog::getInstance()->create($email);
	        EmailCatalog::getInstance()->linkToCompany($email->getIdEmail(), $company->getIdCompany());        	
        }
        else{
        	$emailObj= EmailCatalog::getInstance()->getById($idEmail);
        	$emailObj->setEmail($email);
        	EmailCatalog::getInstance()->update($emailObj);
        }
        
        $companyCatalog->update($company);
        $this->setFlash('ok','Successfully edited the Company');
        $this->_redirect('company/list');
    }
    
    public function relAction()
    {
    	require_once 'application/models/catalogs/FlashVodCatalog.php';
    	require_once 'application/models/catalogs/HelixStreamCatalog.php';
    	require_once 'application/models/catalogs/HelixVodCatalog.php';
    	$idCompany = utf8_decode($this->getRequest()->getParam('idCompany'));
    	
    	$criteria = new Criteria();
    	$criteria->add('id_company', $idCompany, Criteria::EQUAL);
    	
    	$relation = CompanyCatalog::getInstance()->getCompanyStreamServerRelations($criteria);
    	
    	$idsStreamFlash= array();
    	$idsVodFlash=array();
    	$idsStreamHelix= array();
    	$idsVodHelix= array();
    	if(count($relation)>0){
    		foreach($relation as $item){    			
    			switch (true){
    				case (!is_null($item['id_stream_flash'])&&!empty($item['id_stream_flash'])) :
    					     $idsStreamFlash[]=$item['id_stream_flash'];
    					break;
    				case (!is_null($item['id_vod_flash'])&&!empty($item['id_vod_flash'])) :
    					     $idsVodFlash[]=$item['id_vod_flash'];
    					break;
    				case (!is_null($item['id_stream_helix'])&&!empty($item['id_stream_helix'])) :
    					     $idsStreamHelix[]=$item['id_stream_helix'];
    					break;
    				case (!is_null($item['id_vod_helix'])&&!empty($item['id_vod_helix'])) :
    					     $idsVodHelix[]=$item['id_vod_helix'];
    					break;			
    			}
    		}
    	}

    	
    	$this->view->idCompany=$idCompany;
    	$this->view->Servers = StreamServerCatalog::getInstance()->getByCriteria();
    	$this->view->Lives = array("0"=>"Seleccione un servidor...");
    	$this->view->Streams = array("0"=>"Seleccione un montaje...");
    	$this->view->Vod = array("0"=>"Seleccione un montaje...");
    	$this->view->serversCombo=StreamServerCatalog::getInstance()->getByCriteria()->toCombo();
    	$this->view->listStreamFlash=FlashStreamCatalog::getInstance()->getByIds($idsStreamFlash);
    	$this->view->listVodFlash=FlashVodCatalog::getInstance()->getByIds($idsVodFlash);
    	$this->view->listStreamHelix=HelixStreamCatalog::getInstance()->getByIds($idsStreamHelix);
    	$this->view->listVodHelix=HelixVodCatalog::getInstance()->getByIds($idsVodHelix);
    }
    
    public function reladdAction()
    {
    	$idCompany = $this->getRequest()->getParam('idCompany');

    	$typeStream = $this->getRequest()->getParam('typeStreamServer');
    	$typeRel = $this->getRequest()->getParam('typeRelation');

    	if($typeStream==1){
	    	if($typeRel==1){
		    	$idServer = $this->getRequest()->getParam('id_server');
		    	$idLive = $this->getRequest()->getParam('id_live');
		    	$idStream = $this->getRequest()->getParam('id_stream');    	
	
	    	    CompanyCatalog::getInstance()->linkToStreamServer($idCompany, $idServer, $idLive, $idStream, null);	    	
	    	}
	    	elseif($typeRel==2){
		    	$idServer = $this->getRequest()->getParam('id_server2');
		    	$idVod = $this->getRequest()->getParam('id_vod');
	
	    	    CompanyCatalog::getInstance()->linkToStreamServer($idCompany, $idServer, null, null, $idVod);	    	
	    	}    		
    	}
    	elseif($typeStream>1){
	    	if($typeRel==1){
		    	$idServer = $this->getRequest()->getParam('id_server_helix1');
		    	$idLiveHelix = $this->getRequest()->getParam('id_live_helix');
		    	$idStreamHelix = $this->getRequest()->getParam('id_stream_helix');    	
	
	    	    CompanyCatalog::getInstance()->linkToStreamServer($idCompany, $idServer, null, null, null, $idLiveHelix, $idStreamHelix);	    	
	    	}
	    	elseif($typeRel==2){
		    	$idServer = $this->getRequest()->getParam('id_server_helix2');
		    	$idVodHelix = $this->getRequest()->getParam('id_vod_helix');
	
	    	    CompanyCatalog::getInstance()->linkToStreamServer($idCompany, $idServer, null, null, null, null, null, $idVodHelix);	    	
	    	}     		
    	}


        $this->setFlash('ok','Successfully linked the Company');
        $this->_redirect('company/index');    	
    }
    
  public function combosAction()
  {
  	require_once 'application/models/catalogs/FlashVodCatalog.php';
  	require_once 'application/models/catalogs/HelixLiveCatalog.php';
  	require_once 'application/models/catalogs/HelixStreamCatalog.php';
  	require_once 'application/models/catalogs/HelixVodCatalog.php';
  	
         if($this->getRequest()->isXmlHttpRequest())  
            $this->view->setLayoutFile(false);
            $this->_helper->viewRenderer->setNoRender();    
            $id = $this->getRequest()->getParam('id');   
            $combo = $this->getRequest()->getParam('combo');  

            switch($combo){            
            	case "id_server":
            		   $lives=NULL;
				       $lives=FlashLiveCatalog::getInstance()->getByIdServer($id);                      
            		  
            		  if(!$lives->isEmpty()){
            		  	//var_dump($officesRelations);   
            		  	echo "<option value='0'>Selecciona un valor...</option>";         		  	
           		  	    foreach ($lives as $live){
                            //var_dump($office);
            		  		echo "<option value='".$live->getIdLive()."'>".$live->getName()."</option>";
            		  		$idLive=$live->getIdLive();
            		  	}
            		  }
            		  else{
            		  	echo "<option value='0'>No existen valores</option>";
            		  }
            		break;
            	case "id_live":
                       $streams=NULL;				    	
				       $streams=FlashStreamCatalog::getInstance()->getByIdLive($id);                      
            		  
            		  if(!$streams->isEmpty()){
            		  	//var_dump($officesRelations);
            		  	
           		  	    foreach ($streams as $stream){
                            //var_dump($office);
            		  		echo "<option value='".$stream->getIdStream()."'>".$stream->getName()."</option>";
            		  	}
            		  }
            		  else{
            		  	echo "<option value='0'>No existen valores</option>";
            		  }
            		break;
            	case "id_server2":
                       $vods=NULL;				    	
				       $vods=FlashVodCatalog::getInstance()->getByIdServer($id);                      
            		  
            		  if(!$vods->isEmpty()){
            		  	//var_dump($officesRelations);
            		  	
           		  	    foreach ($vods as $vod){
                            //var_dump($office);
            		  		echo "<option value='".$vod->getIdVod()."'>".$vod->getNameVideo()."</option>";
            		  	}
            		  }
            		  else{
            		  	echo "<option value='0'>No existen valores</option>";
            		  }
            		break;  
            	case "id_server_helix1":
                       $lives=NULL;
				       $lives=HelixLiveCatalog::getInstance()->getByIdServer($id);                      
            		  
            		  if(!$lives->isEmpty()){
            		  	//var_dump($officesRelations);   
            		  	echo "<option value='0'>Selecciona un valor...</option>";         		  	
           		  	    foreach ($lives as $live){
                            //var_dump($office);
            		  		echo "<option value='".$live->getIdLive()."'>".$live->getName()."</option>";
            		  		//$idLive=$live->getIdLive();
            		  	}
            		  }
            		  else{
            		  	echo "<option value='0'>No existen valores</option>";
            		  }            		
            		break;
            	case "id_live_helix":
                       $streams=NULL;	
                       $criteria = new Criteria();
                       $criteria->add('id_live', $id, Criteria::EQUAL);	
                       $criteria->addGroupByColumn('id_stream');		    	
				       $rels=HelixStreamCatalog::getInstance()->getHelixStreamHelixProtocolRelations($criteria);                      
            		  
				       $ids=array();
				       foreach($rels as $rel){
				       	  $ids[]=$rel['id_stream'];
				       }
				       
				       $streams = HelixStreamCatalog::getInstance()->getByIds($ids);
            		  if(!$streams->isEmpty()){
            		  	//var_dump($officesRelations);
            		  	
           		  	    foreach ($streams as $stream){
                            //var_dump($office);
            		  		echo "<option value='".$stream->getIdStream()."'>".$stream->getName()."</option>";
            		  	}
            		  }
            		  else{
            		  	echo "<option value='0'>No existen valores</option>";
            		  }            		
            		break;	
            	case "id_server_helix2":
                       $vods=NULL;				    	
				       $vods=HelixVodCatalog::getInstance()->getByIdServer($id);                    
            		  
            		  if(!$vods->isEmpty()){
            		  	//var_dump($officesRelations);
            		  	
           		  	    foreach ($vods as $vod){
                            //var_dump($office);
            		  		echo "<option value='".$vod->getIdVod()."'>".$vod->getNameVideo()."</option>";
            		  	}
            		  }
            		  else{
            		  	echo "<option value='0'>No existen valores</option>";
            		  }            		
            		break;		          				
            }
    }    
}
