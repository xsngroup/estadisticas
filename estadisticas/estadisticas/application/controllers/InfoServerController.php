<?php
require_once "lib/controller/BaseController.php";
require_once 'application/models/catalogs/CompanyCatalog.php';
require_once 'application/models/catalogs/StreamServerCatalog.php';
require_once 'application/models/catalogs/FlashServerStatCatalog.php';


class InfoServerController extends BaseController 
{
	public function statsAction()
	{
		$id=$this->getUser()->getIdUser();
		$serverCatalog= StreamServerCatalog::getInstance();
		$companyCatalog= CompanyCatalog::getInstance();
		
		
        $company= $companyCatalog->getByUser($id)->getOne();
        
        if(!$company instanceof Company){
        	$this->setFlash("error","No existe ningun cliente asociado");
        	$this->_redirect('index/index');
        }

        $server = $serverCatalog->getByCompany($company->getIdCompany())->getOne();
        if(!$server instanceof StreamServer){
        	$this->setFlash("error","No existe ningun servidor asociado");
        	$this->_redirect('index/index');
        }	

        $criteria = new Criteria();
        $criteria->add('id_server',$server->getIdServer(),Criteria::EQUAL);
        $criteria->addDescendingOrderByColumn('timestamp');
        $criteria->setLimit(1);
        $serverStats = FlashServerStatCatalog::getInstance()->getByCriteria($criteria)->getOne();
        
        $this->view->company=$company;
        $this->view->server=$server;
        $this->view->serverStat = $serverStats;
        $this->view->setTpl('Stats');                
	}
	public function updateAction()
	{
       if($this->getRequest()->isXmlHttpRequest())  
        $this->view->setLayoutFile(false);		
		$id=$this->getUser()->getIdUser();
		$serverCatalog= StreamServerCatalog::getInstance();
		$companyCatalog= CompanyCatalog::getInstance();
		
		
        $company= $companyCatalog->getByUser($id)->getOne();
        
        if(!$company instanceof Company){
        	$this->setFlash("error","No existe ningun cliente asociado");
        	$this->_redirect('index/index');
        }

        $server = $serverCatalog->getByCompany($company->getIdCompany())->getOne();
        if(!$server instanceof StreamServer){
        	$this->setFlash("error","No existe ningun servidor asociado");
        	$this->_redirect('index/index');
        }	

        $criteria = new Criteria();
        $criteria->add('id_server',$server->getIdServer(),Criteria::EQUAL);
        $criteria->addDescendingOrderByColumn('timestamp');
        $criteria->setLimit(1);
        $serverStats = FlashServerStatCatalog::getInstance()->getByCriteria($criteria)->getOne();
        
        //date("Y-m-d H:i:s",time()-3600);
        $timeUpdate = date("H:i:s");
        $this->view->company=$company;
        $this->view->server=$server;
        $this->view->serverStat = $serverStats;
        $this->view->timeUpdate=$timeUpdate;
        $this->view->setTpl('Ajax');		
	}
}