<tr>
    <td>{$helixVod->getIdVod()}</td>
    <td>{$helixVod->getPrefix()}</td>
    <td>{$helixVod->getNameVideo()}</td>
    <td>{$helixVod->getPort()}</td>
    <td>{$helixVod->getTitleVideo()}</td>
    <td>{$helixVod->getDescriptionVideo()}</td>
    <td>{$types[$helixVod->getIdType()]} > {$prefix[$helixVod->getPosition()]}</td>
    <td>{$servers[$helixVod->getIdServer()]}</td>
    <td>{$helixVod->getPptSync()}</td>
    <td>{$protocols[$protocol]}</td>
    <td><a href="{url action=edit idVod=$helixVod->getIdVod()}">{icon src=pencil class=tip title=$l10n->_('Edit')}</a></td>
    <td><a href="{url action=delete idVod=$helixVod->getIdVod()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Delete')}</a></td>
</tr>
