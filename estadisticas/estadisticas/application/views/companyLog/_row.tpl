_row.tpl

<tr>
    <td>{$companyLog->getIdCompanyLog()}</td>
    <td>{$companyLog->getEventType()}</td>
    <td>{$companyLog->getTimestamp()}</td>
    <td>{$companyLog->getIdCompany()}</td>
    <td>{$companyLog->getIdUser()}</td>
    <td><a href="{url action=edit idCompanyLog=$companyLog->getIdCompanyLog()}">{icon src=pencil class=tip title=$i18n->_('Edit')}</a></td>
    <td><a href="{url action=delete idCompanyLog=$companyLog->getIdCompanyLog()}" class="confirm">{icon src=delete class=tip title=$i18n->_('Delete')}</a></td>
</tr>
