<form action="{url action=create}" method="post" class="validate ajaxForm">
<table class="center">
    <caption>{$i18n->_('Viewer')}</caption>
    <tfoot>
        <tr>
            <td colspan="2"><input type="submit" value="{$i18n->_('Save')}" /></td>
        </tr>
    </tfoot>
    <tbody>
        {include file='viewer/Form.tpl'}
    </tbody>
</table>
</form>
<hr/>


<table class="center">
    <caption>{$i18n->_('List')}</caption>
    <thead>
        <tr>
            <td>{$i18n->_('IdViewer')}</td>
            <td>{$i18n->_('Name')}</td>
            <td>{$i18n->_('Timestamp')}</td>
            <td colspan="2">{$i18n->_('Actions')}</td>
        </tr>
    </thead>
    <tbody id="ajaxList">
        {foreach $viewers as $viewer}
            <tr class="{$viewer@iteration|odd}">
                <td>{$viewer->getIdViewer()}</td>
                <td>{$viewer->getName()}</td>
                <td>{$viewer->getTimestamp()}</td>
                <td><a href="{url action=edit idViewer=$viewer->getIdViewer()}">{icon src=pencil class=tip title=$i18n->_('Edit')}</a></td>
                <td><a href="{url action=delete idViewer=$viewer->getIdViewer()}" class="confirm">{icon src=delete class=tip title=$i18n->_('Delete')}</a></td>
            </tr>
        {/foreach}
    </tbody>
</table>

