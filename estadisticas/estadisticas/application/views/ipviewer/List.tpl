<form action="{url action=create}" method="post" class="validate ajaxForm">
<table class="center">
    <caption>{$i18n->_('Ipviewer')}</caption>
    <tfoot>
        <tr>
            <td colspan="2"><input type="submit" value="{$i18n->_('Save')}" /></td>
        </tr>
    </tfoot>
    <tbody>
        {include file='ipviewer/Form.tpl'}
    </tbody>
</table>
</form>
<hr/>


<table class="center">
    <caption>{$i18n->_('List')}</caption>
    <thead>
        <tr>
            <td>{$i18n->_('IdIpviewer')}</td>
            <td>{$i18n->_('Ipviewer')}</td>
            <td colspan="2">{$i18n->_('Actions')}</td>
        </tr>
    </thead>
    <tbody id="ajaxList">
        {foreach $ipviewers as $ipviewer}
            <tr class="{$ipviewer@iteration|odd}">
                <td>{$ipviewer->getIdIpviewer()}</td>
                <td>{$ipviewer->getIpviewer()}</td>
                <td><a href="{url action=edit idIpviewer=$ipviewer->getIdIpviewer()}">{icon src=pencil class=tip title=$i18n->_('Edit')}</a></td>
                <td><a href="{url action=delete idIpviewer=$ipviewer->getIdIpviewer()}" class="confirm">{icon src=delete class=tip title=$i18n->_('Delete')}</a></td>
            </tr>
        {/foreach}
    </tbody>
</table>

