<tr>
    <td>{$helixTypesId->getIdType()}</td>
    <td>{$helixTypesId->getName()}</td>
    <td><a href="{url action=edit idType=$helixTypesId->getIdType()}">{icon src=pencil class=tip title=$l10n->_('Edit')}</a></td>
    <td><a href="{url action=delete idType=$helixTypesId->getIdType()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Delete')}</a></td>
</tr>
