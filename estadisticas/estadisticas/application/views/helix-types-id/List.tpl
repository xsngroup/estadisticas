<div class="column_left">
    <div class="header"><span>{$l10n->_('Helix Server Prefijos Identificadores')}</span></div>
    <br class="clear" />
    <div class="content">
<form action="{url action=create}" method="post" class="validate ajaxForm">
<h4>{$l10n->_('Guardar nuevo Prefijo Identificador')}</h4>
{include file='forms/HelixTypesId.tpl'}
<br/>
<p> 
<input type="submit" value="{$l10n->_('Guardar')}" />
</p>      
</form>
	</div>
</div>

<div class="column_right">
    <div class="header"><span>{$l10n->_('Listado de Prefijos Identificadores')}</span></div>
    <br class="clear" />
    <div class="content">
<table width="100%" cellspacing="0" cellpadding="0" class="data">
    <thead>
        <tr>
            <th>{$l10n->_('IdType')}</th>
            <th>{$l10n->_('Name')}</th>
            <th colspan="2">{$l10n->_('Actions')}</th>
        </tr>
    </thead>
    <tbody id="ajaxList">
        {foreach $helixTypesIds as $helixTypesId}
            <tr class="{$helixTypesId@iteration|odd}">
                <td>{$helixTypesId->getIdType()}</td>
                <td>{$helixTypesId->getName()}</td>
                <td><a href="{url action=edit idType=$helixTypesId->getIdType()}">{icon src=pencil class=tip title=$l10n->_('Edit')}</a></td>
                <td><a href="{url action=delete idType=$helixTypesId->getIdType()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Delete')}</a></td>
            </tr>
        {foreachelse}
           <tr><td colspan="5" align="center">No existen elementos para mostrar...</td></tr>               
        {/foreach}
    </tbody>
</table>
	</div>
</div>

