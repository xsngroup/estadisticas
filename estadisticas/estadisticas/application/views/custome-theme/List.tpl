<div class="onecolumn">
    <div class="header"><span>{$l10n->_('Listado de plantillas')}</span></div>
    <br class="clear" />
    <div class="content">
<table width="100%" cellspacing="0" cellpadding="0" class="data">
    <thead>
        <tr>
            <td>{$l10n->_('Id')}</td>
            <td>{$l10n->_('Color')}</td>
            <td>{$l10n->_('Sidebar')}</td>
            <td>{$l10n->_('Tipo de inicio')}</td>
            <td>{$l10n->_('Botones')}</td>
            <td>{$l10n->_('Estado')}</td>
            <td>{$l10n->_('Empresa')}</td>
            <td colspan="4" align="center">{$l10n->_('Opciones')}</td>
        </tr>
    </thead>
    <tbody id="ajaxList">
        {foreach $customeThemes as $customeTheme}
           {assign var="buttons" value='|'|explode:$customeTheme->getButtons()} 
            <tr class="{$customeTheme@iteration|odd}">
                <td>{$customeTheme->getIdCustomeTheme()}</td>
                <td>{$colors[$customeTheme->getColorTheme()]}</td>
                <td>{$gallery[$customeTheme->getTypeGallery()]}</td>
                <td>{$typeinit[$customeTheme->getTypeLogin()]}</td>
                <td>         
                {foreach $buttons as $button}
        			{if $button == "find"}        				
        				      <img src='{$baseUrl}/images/template/theme/search.png' alt='search' title="Buscar">
        			{/if}
        			{if $button == "chat"}        				
        				      <img src='{$baseUrl}/images/template/theme/chat2.png' alt='Chat' title="Chat">
        		    {/if}
        			{if $button == "facebook"}        				
        				      <img src='{$baseUrl}/images/template/theme/social_facebook.png' alt='Facebook' title="Facebook">
        		    {/if}
        			{if $button == "twitter"}        				
        				      <img src='{$baseUrl}/images/template/theme/social_twitter.png' alt='Twitter' title="Twitter">
        		    {/if}  
                {/foreach}
               </td>
                <td>{$status[$customeTheme->getStatus()]}</td>
                <td>{$company}</td>
                <td><a href="{url action=edit idCustomeTheme=$customeTheme->getIdCustomeTheme()}">{icon src=pencil class=tip title=$l10n->_('Editar plantilla')}</a></td>
                <td><a href="{url action=delete idCustomeTheme=$customeTheme->getIdCustomeTheme()}" class="confirm">{icon src=video class=tip title=$l10n->_('Configurar videos')}</a></td>
                {if $customeTheme->getTypeGallery()==2}
                <td><a href="{url action=delete idCustomeTheme=$customeTheme->getIdCustomeTheme()}" class="confirm">{icon src=ppt class=tip title=$l10n->_('Presentaciones')}</a></td>
                {/if}
                <td><a href="{url action=delete idCustomeTheme=$customeTheme->getIdCustomeTheme()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Borrar')}</a></td>
            </tr>
        {/foreach}
    </tbody>
</table>
	</div>
</div>
