<link href="{$baseUrl}/css/uploadify.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="{$baseUrl}/css/tab.css" type="text/css" />
<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/plugins/upload/swfobject.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/plugins/upload/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript">
<!--

$(document).ready(function(){
var logo;
$('#url_logo').uploadify({
    'uploader'  : '{$baseUrl}/files/uploadify.swf',
    'script'    : '{$baseUrl}/uploadify.php',
    'cancelImg' : '{$baseUrl}/images/template/theme/cancel.png',
    'folder'    : '{$baseUrl}/imagesTemp',
    'multi' : false,
    'buttonText': 'Elegir archivo',
    'auto' : true,
    'fileExt' : '*.jpg;*.png',
    'fileDesc' : 'Image Files (.JPG,.PNG)',
    'queueID' : 'custom-queue1',
    'removeCompleted': false,
    'onComplete'  : function(event, ID, fileObj, response, data) {
    	
    	var newLogo=fileObj.name;
        var fragmentos = newLogo.split(' ');
        var x=0;
        
        for(var i=0; i<fragmentos.length; i++){
            if(x==0){
            	logo=fragmentos[i];
            }
            else{
            	logo+='_'+fragmentos[i];
            }
            x++;
        }
      $.post(baseUrl + "/custome-theme/upload-image", {
		datafile: logo,
		idCompany: $("#id_company").val(),
		option:1
	  }, function(data){
		if (data.length > 0) {
			$('#loadresponse').fadeIn('slow', function() {
				$('#loadresponse').show();
				$('#loadresponse').html("<p>"+data+"</p>");
				setTimeout(hideDiv ,3000);
			});	
		}
	  });		    
     $('#status-message1').text(data.filesUploaded + ' files uploaded, ' + data.errors + ' errors.');
    },
	'onCancel': function(event, data){
		
	      $.post(baseUrl + "/custome-theme/delete-image", {
	    		datafile: logo,
	    		idCompany: $("#id_company").val(),
	    		option:1
	    	  }, function(data){
	    		if (data.length > 0) {
	    			$('#loadresponse').fadeIn('slow', function() {
	    				$('#loadresponse').show();
	    				$('#loadresponse').html("<p>"+data+"</p>");
	    				setTimeout(hideDiv ,3000);
	    			});	
	    			$('#status-message1').text(data);
	    		}
	    	  });					 
	}
});

var banner;
$('#url_banner').uploadify({
    'uploader'  : '{$baseUrl}/files/uploadify.swf',
    'script'    : '{$baseUrl}/uploadify.php',
    'cancelImg' : '{$baseUrl}/images/template/theme/cancel.png',
    'folder'    : '{$baseUrl}/imagesTemp',
	'multi' : false,
	'buttonText': 'Elegir archivo',
	'auto' : true,
	'fileExt' : '*.jpg;*.png',
	'fileDesc' : 'Image Files (.JPG,.PNG)',
	'queueID' : 'custom-queue2',
	'removeCompleted': false,
    'onComplete'  : function(event, ID, fileObj, response, data) {

    	var newBanner=fileObj.name;
        var fragmentos = newBanner.split(' ');
        var x=0;
        
        for(var i=0; i<fragmentos.length; i++){
            if(x==0){
            	banner=fragmentos[i];
            }
            else{
            	banner+='_'+fragmentos[i];
            }
            x++;
        }
      $.post(baseUrl + "/custome-theme/upload-image", {
    		datafile: banner,
    		idCompany: $("#id_company").val(),
    		option:2
    	  }, function(data){
    		if (data.length > 0) {
    			$('#loadresponse').fadeIn('slow', function() {
    				$('#loadresponse').show();
    				$('#loadresponse').html("<p>"+data+"</p>");
    				setTimeout(hideDiv ,3000);
    			});	
    		}
    	  });
		
	 $('#status-message2').text(data.filesUploaded + ' files uploaded, ' + data.errors + ' errors.');
	},
	'onCancel': function(event, data){
				
	      $.post(baseUrl + "/custome-theme/delete-image", {
	    		datafile: banner,
	    		idCompany: $("#id_company").val(),
	    		option:2
	    	  }, function(data){
	    		if (data.length > 0) {
	    			$('#loadresponse').fadeIn('slow', function() {
	    				$('#loadresponse').show();
	    				$('#loadresponse').html("<p>"+data+"</p>");
	    				setTimeout(hideDiv ,3000);
	    			});	
	    			$('#status-message2').text(data);
	    		}
	    	  });					 
	}
});

var bannerMini;
$('#url_bannermini').uploadify({
    'uploader'  : '{$baseUrl}/files/uploadify.swf',
    'script'    : '{$baseUrl}/uploadify.php',
    'cancelImg' : '{$baseUrl}/images/template/theme/cancel.png',
    'folder'    : '{$baseUrl}/imagesTemp',
	'multi' : false,
	'buttonText': 'Elegir archivo',
	'auto' : true,
	'fileExt' : '*.jpg;*.png',
	'fileDesc' : 'Image Files (.JPG,.PNG)',
	'queueID' : 'custom-queue3',
	'removeCompleted': false,
    'onComplete'  : function(event, ID, fileObj, response, data) {

    	var newBannerMini=fileObj.name;
        var fragmentos = newBannerMini.split(' ');
        var x=0;
        
        for(var i=0; i<fragmentos.length; i++){
            if(x==0){
            	bannerMini=fragmentos[i];
            }
            else{
            	bannerMini+='_'+fragmentos[i];
            }
            x++;
        }
      $.post(baseUrl + "/custome-theme/upload-image", {
    		datafile: bannerMini,
    		idCompany: $("#id_company").val(),
    		option:3
    	  }, function(data){
    		if (data.length > 0) {
    			$('#loadresponse').fadeIn('slow', function() {
    				$('#loadresponse').show();
    				$('#loadresponse').html("<p>"+data+"</p>");
    				setTimeout(hideDiv ,3000);
    			});	
    		}
    	  });
		
	 $('#status-message3').text(data.filesUploaded + ' files uploaded, ' + data.errors + ' errors.');
	},
	'onCancel': function(event, data){
		
	      $.post(baseUrl + "/custome-theme/delete-image", {
	    		datafile: bannerMini,
	    		idCompany: $("#id_company").val(),
	    		option:3
	    	  }, function(data){
	    		if (data.length > 0) {
	    			$('#loadresponse').fadeIn('slow', function() {
	    				$('#loadresponse').show();
	    				$('#loadresponse').html("<p>"+data+"</p>");
	    				setTimeout(hideDiv ,3000);
	    			});	
	    			$('#status-message3').text(data);
	    		}
	    	  });					 
	}
});

var namePpt;
$('#url_ppt').uploadify({
    'uploader'  : '{$baseUrl}/files/uploadify.swf',
    'script'    : '{$baseUrl}/uploadify.php',
    'cancelImg' : '{$baseUrl}/images/template/theme/cancel.png',
    'folder'    : '{$baseUrl}/pptsTemp',
    'multi' : true,
    'buttonText': 'Elegir archivo',
    'auto' : true,
    'fileExt' : '*.ppt;*.pptx',
    'queueID' : 'custom-queue4',
    'removeCompleted': false,
    'onComplete'  : function(event, ID, fileObj, response, data) {

    	var newName=fileObj.name;
        var fragmentos = newName.split(' ');
        var x=0;
        
        for(var i=0; i<fragmentos.length; i++){
            if(x==0){
            	namePpt=fragmentos[i];
            }
            else{
                namePpt+='_'+fragmentos[i];
            }
            x++;
        }
        
      $.post(baseUrl + "/custome-theme/upload-ppt", {
    	title:escape($("#nameppt").val()),  
		datafile: namePpt,
		idCompany: $("#id_company").val(),
	  }, function(data){
		if (data.length > 0) {
			$('#loadresponse').fadeIn('slow', function() {
				$("#nameppt").attr('value','');
				$('#loadresponse').show();
				$('#loadresponse').html("<p>"+data+"</p>");
				setTimeout(hideDiv ,3000);
			});	
		}
	  });		    
     $('#status-message4').text(data.filesUploaded + ' files uploaded, ' + data.errors + ' errors.');
    },
	'onCancel': function(event, data){
	      $.post(baseUrl + "/custome-theme/delete-ppt", {
	    		datafile: namePpt,
	    		idCompany: $("#id_company").val(),
	    	  }, function(data){
	    		if (data.length > 0) {
	    			$('#loadresponse').fadeIn('slow', function() {
	    				$('#loadresponse').show();
	    				$('#loadresponse').html("<p>"+data+"</p>");
	    				setTimeout(hideDiv ,3000);
	    			});	
	    			$('#status-message4').text(data);
	    		}
	    	  });					 
	}
});

$(".tab_content").hide();
$("ul.tabs li:first").addClass("active").show();
$(".tab_content:first").show();

$("ul.tabs li").click(function()
   {
	$("ul.tabs li").removeClass("active");
	$(this).addClass("active");
	$(".tab_content").hide();

	var activeTab = $(this).find("a").attr("href");
	$(activeTab).fadeIn();
	return false;
});	

var background;
$('#url_background').uploadify({
    'uploader'  : '{$baseUrl}/files/uploadify.swf',
    'script'    : '{$baseUrl}/uploadify.php',
    'cancelImg' : '{$baseUrl}/images/template/theme/cancel.png',
    'folder'    : '{$baseUrl}/imagesTemp',
	'multi' : false,
	'buttonText': 'Elegir archivo',
	'auto' : true,
	'fileExt' : '*.jpg;*.png',
	'fileDesc' : 'Image Files (.JPG,.PNG)',
	'queueID' : 'custom-queue4',
	'removeCompleted': false,
    'onComplete'  : function(event, ID, fileObj, response, data) {

    	var newBackground=fileObj.name;
        var fragmentos = newBackground.split(' ');
        var x=0;
        
        for(var i=0; i<fragmentos.length; i++){
            if(x==0){
            	background=fragmentos[i];
            }
            else{
            	background+='_'+fragmentos[i];
            }
            x++;
        }
      $.post(baseUrl + "/custome-theme/upload-image", {
    		datafile: background,
    		idCompany: $("#id_company").val(),
    		option:4
    	  }, function(data){
    		if (data.length > 0) {
    			$('#loadresponse').fadeIn('slow', function() {
    				$('#loadresponse').show();
    				$('#loadresponse').html("<p>"+data+"</p>");
    				setTimeout(hideDiv ,3000);
    			});	
    		}
    	  });
		
	 $('#status-message4').text(data.filesUploaded + ' files uploaded, ' + data.errors + ' errors.');
	},
	'onCancel': function(event, data){
		
	      $.post(baseUrl + "/custome-theme/delete-image", {
	    		datafile: background,
	    		idCompany: $("#id_company").val(),
	    		option:4
	    	  }, function(data){
	    		if (data.length > 0) {
	    			$('#loadresponse').fadeIn('slow', function() {
	    				$('#loadresponse').show();
	    				$('#loadresponse').html("<p>"+data+"</p>");
	    				setTimeout(hideDiv ,3000);
	    			});	
	    			$('#status-message4').text(data);
	    		}
	    	  });					 
	}
});

});

//-->
</script>
<script type="text/javascript" src="{$baseUrl}/js/plugins/Wizard.js"></script>
<link rel="stylesheet" href="{$baseUrl}/css/wizard.css" type="text/css" />

<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery.simplemodal-1.3.min.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/expanded.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery-validate/jquery.validate.js"></script>

<script type="text/javascript" src="{$baseUrl}/js/plugins/coin-slider/coin-slider.js"></script>
<link rel="stylesheet" href="{$baseUrl}/css/coin-slider-styles.css" type="text/css" />
<script type="text/javascript" src="{$baseUrl}/js/modules/custome-theme/process.js"></script>

<div class="onecolumn">
 <div class="header"><span>{$l10n->_('Configurar nueva plantilla')}</span></div>
 <br class="clear" />
 <div class="content">
 <div class="notice alert_info" id="loadresponse" style="display:none;"></div>
 <div class="box">

<form class="validateCustom" id="customeTheme" action="{$baseUrl}/custome-theme/save-theme" method="post"> 
<input size="40" type="hidden" name="id_company" id="id_company" value="{$idCompany}" />
<fieldset>
  <legend>Apariencia</legend>
 <div id="tabDiv">
		<ul class="tabs">
		    <li><a href="#tab1">{$l10n->_('Tema para pc')}</a></li>
		    <li><a href="#tab2">{$l10n->_('Tema para m&oacute;viles')}</a></li>
		</ul>
		<div class="tab_container">
         <div id="tab1" class="tab_content">
	           {assign var="ind" value=1}
		        {foreach $themePc as $pc}
		           <input type="radio" name="themePc" class="themePc skinPcEvent" value="{$pc->getIdThemePc()}"  {if $ind==1}checked="checked"{/if} /><span>{$pc->getName()}</span>        
		          {$ind=$ind+1}
		        {/foreach}  
		        <br/> 
				<p id="choiceColor">
				    <input type="radio" name="colorTheme" class="colorTheme" value="1" /><span>Blanco</span>
				    <input type="radio" name="colorTheme" class="colorTheme" value="2" /><span>Negro</span>
				</p>		         
		        <br/>    
		        <center>
			        <div id="contentPcTheme">
						<div id='coin-slider'>
						{assign var="indice" value=1}
					        {foreach $themePc as $pc}
									<a href="image{$indice}_url" target="_blank">
										<img src='{$baseUrl}/images/themeThumbnails/screen-pc/{$pc->getScreenShot()}' >
										<span>
											Tema: {$pc->getName()}
										</span>
									</a>  
									{$indice=$indice+1}      		            
					        {/foreach}
						</div>
					</div>	
				</center>         
         </div>
         <div id="tab2" class="tab_content">
		    {assign var="ind" value=1}
		    {foreach $themeMobile as $mobile}
		        <input type="radio" name="themeMobile" class="themeMobile skinMobileEvent" value="{$mobile->getIdThemeMobile()}" {if $ind==1}checked="checked"{/if} /><span>{$mobile->getName()}</span>	        
		       {$ind=$ind+1}
		    {/foreach} 
	        <br/>  
	        <br/>    
	        <center>
		        <div id="contentMobileTheme">
					<div id='coin-slider-2'>
					{assign var="indice" value=1}
				        {foreach $themeMobile as $mobile}
								<a href="mobile{$indice}_url" target="_blank">
									<img src='{$baseUrl}/images/themeThumbnails/screen-mobile/{$mobile->getScreenShot()}' >
									<span>
										Tema: {$mobile->getName()}
									</span>
								</a>  
								{$indice=$indice+1}      		            
				        {/foreach}
					</div>
				</div>	
			</center>         
         </div>         
        </div>              
   </div>      

    <br/>
    <br/>  
  
	<p><span style="color:red; font-style: italic;">{$l10n->_('*Formatos permit�dos: "png" para im�genes con transparencias y "jpg" para im�genes s�lidas (Im�genes con transparencias dan mejor aspecto a la plantilla).')}</span></p> 
     <br/> 
  
	<label>{$l10n->_('Logo:')}</label><br/>
	<div id="custom-demo1">
	   <div id="status-message1"></div>
	   <span id="medidaLogo" style="float:left; margin-left: 38%;">Medida: 252 x 90 pixeles</span>
	   <div id="custom-queue1" class="custom-queue uploadifyQueue"></div>   
	   <input id="url_logo" name="url_logo" type="file" />
	 </div> 
	 <br/>
	<label>{$l10n->_('Banner header:')}</label><br/>
	<div id="custom-demo2">
	   <div id="status-message2"></div>
	   <span id="medidaBanner1" style="float:left; margin-left: 38%;">Medida: 728 x 90 pixeles</span>	   
	   <div id="custom-queue2" class="custom-queue uploadifyQueue"></div>   
	   <input id="url_banner" name="url_banner" type="file" />
	 </div> 
	<br/>
  <div id="imagesTheme1">
	<label>{$l10n->_('Banner sidebar:')}</label><br/>
	<div id="custom-demo3">
	   <div id="status-message3"></div>
	   <span id="medidaBanner2" style="float:left; margin-left: 38%;">Medida: 347 x 108 pixeles</span>	   
	   <div id="custom-queue3" class="custom-queue uploadifyQueue"></div>   
	   <input id="url_bannermini" name="url_bannermini" type="file" />
	 </div> 
	<br/>
  </div>
  
  <div id="imagesTheme2" style="display:none;">
	<label>{$l10n->_('Fondo de pantalla:')}</label><br/>
	<div id="custom-demo4">
	   <div id="status-message4"></div>
	   <span id="medidaFondo" style="float:left; margin-left: 38%;">Medida: 1850 x 900 pixeles</span>		   
	   <div id="custom-queue4" class="custom-queue uploadifyQueue"></div>   
	   <input id="url_background" name="url_background" type="file" />
	 </div> 
	<br/>
  </div>	  
  
	<br/>
	<br/>  
</fieldset>
<fieldset>

   <legend>Configuraci�n</legend>
  
   
	<p>
	    <label>{$l10n->_('Color del tema:')}</label><br/>
	    <input type="radio" name="colorTheme" class="colorTheme" value="1" /><span>Blanco</span><br/>
	    <input type="radio" name="colorTheme" class="colorTheme" value="2" /><span>Negro</span>
	</p>
	<br/>
	
	<p>
	    <label>{$l10n->_('Tipo de sidebar:')}</label><br/>
	    <input type="radio" name="typesidebar" class="typesidebar" value="1" /><span>Galer�a de streaming</span><br/>
	    <input type="radio" name="typesidebar" class="typesidebar" value="2" /><span>Slide share</span>
	</p>
	<div id="loadppt" style="display: none;">
	<br/>
	<p>
	    <label>{$l10n->_('Nombre de la sesi�n:')}</label><br/>
        <input type="text" name="nameppt" id="nameppt" />
	</p>
	<br/>
	<p>
	   <input type="button" name="saveppt" id="saveppt" value="Guardar" />
	</p>
	<br/>

	</div>
	<br/>
	<p>
	    <label>{$l10n->_('Botones del tema:')}</label><br/>
	    <input type="checkbox" name="buttons" class="buttons" value="find" /><span>Buscar</span><br/>
	    <input type="checkbox" name="buttons" class="buttons" value="chat" /><span>Chat</span><br/>
	    <input type="checkbox" name="buttons" class="buttons" value="comment" /><span>Comentarios</span><br/>
	    <input type="checkbox" name="buttons" class="buttons" value="facebook" /><span>Facebook</span><br/>
	    <input type="checkbox" name="buttons" class="buttons" value="twitter" /><span>Twitter</span>
	</p>
	<br/>
	<!-- 		
	<p>
	<input type="button" name="preview" class="preview" value="Vista previa" />
	</p>
	-->
	<br/>
	<br/>
</fieldset>
<fieldset>
  <legend>Seguridad</legend>
  <p>
    <label>{$l10n->_('Tipo de inicio:')}</label><br/>
    <input type="radio" name="typeinit" class="typeinit" value="2" /><span>De libre acceso</span><br/> 
	<input type="radio" name="typeinit" class="typeinit" value="1" /><span>Con autenticaci�n</span>	   
  </p>
  <div id="loadsecure" style="display: none;"> 
    <br/>
    <p> 
      <label>{$l10n->_('Generar datos de acceso:')}</label><br/>
      <input type="checkbox" name="usernameCbx" id="usernameCbx" value="1" /><span>Usuarios</span><br/>
      <input type="checkbox" name="passwordCbx" id="passwordCbx" value="1" /><span>Passwords</span>
    </p> 
    <br/>
    <p> 
      <label>{$l10n->_('No. accesos aleatorios:')}</label><br/>
      <input type="text" name="noAccess" id="noAccess" value="10" size="3" />
    </p>     
  </div>
  <br/> 
  <p> 
	<!-- <input type="button" name="preview" class="preview" value="Vista previa" /> -->  
	<input type="submit" value="Guardar plantilla" id="generate">
  </p>
  <br/>
  <br/> 
</fieldset>

</form>
</div>
</div>
</div>