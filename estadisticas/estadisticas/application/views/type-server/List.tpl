<div class="column_left">
    <div class="header"><span>{$l10n->_('Tipo de servidores')}</span></div>
    <br class="clear" />
    <div class="content">
<form action="{url action=create}" method="post" class="validate ajaxForm">
<h4>{$l10n->_('Guardar nuevo tipo de servidor')}</h4>
{include file='forms/type-server.tpl'}
<br/>
<p> 
<input type="submit" value="{$l10n->_('Guardar')}" />
</p>
</form>
	</div>
</div>

<div class="column_right">
    <div class="header"><span>{$l10n->_('Listado de tipos')}</span></div>
    <br class="clear" />
    <div class="content">
<table width="100%" cellspacing="0" cellpadding="0" class="data">
    <thead>
        <tr>
            <th>{$l10n->_('Id')}</th>
            <th>{$l10n->_('Nombre')}</th>
            <th>{$l10n->_('Status')}</th>
            <th colspan="2">{$l10n->_('Actions')}</th>
        </tr>
    </thead>
    <tbody id="ajaxList">
        {foreach $typeServers as $typeServer}
            <tr class="{$typeServer@iteration|odd}">
                <td>{$typeServer->getIdType()}</td>
                <td>{$typeServer->getName()}</td>
                <td>{$status[$typeServer->getStatus()]}</td>
                <td><a href="{url action=edit idServer=$typeServer->getIdType()}">{icon src=pencil class=tip title=$l10n->_('Edit')}</a></td>
                <td><a href="{url action=delete idServer=$typeServer->getIdType()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Delete')}</a></td>
            </tr>
        {/foreach}
    </tbody>
</table>
	</div>
</div>

