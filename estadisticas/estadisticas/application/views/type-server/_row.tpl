<tr>
    <td>{$typeServer->getIdType()}</td>
    <td>{$typeServer->getName()}</td>
    <td>{$typeServer->getStatus()}</td>
    <td><a href="{url action=edit idServer=$typeServer->getIdServer()}">{icon src=pencil class=tip title=$l10n->_('Edit')}</a></td>
    <td><a href="{url action=delete idServer=$typeServer->getIdServer()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Delete')}</a></td>
</tr>
