<div class="onecolumn">
    <div class="header"><span>{$l10n->_('Listado de Usuarios')}</span></div>
    <br class="clear" />
    <div class="content">
	<table width="100%" cellspacing="0" cellpadding="0" class="data">
		<caption>{$l10n->_('Lista de Usuarios')}</caption>
		<thead>
			<tr>
				<th>#</th>
				<th>{$l10n->_('Username')}</th>
				<th>{$l10n->_('Nombre')}</th>
				<th>{$l10n->_('Grupo')}</th>
				<th colspan="2">{$l10n->_('Acciones')}</th>
			</tr>
		</thead>
		<tbody id="ajaxList">
			{foreach $users as $user}
				<tr class="{$user@iteration|even}">
					<td>{$user->getIdUser()}</td>
					<td>{$user->getUsername()}</td>
					<td>{$user->getFullName()}</td>
					<td>{$accessRoles[$user->getIdAccessRole()]}</td>
					<td><a href="{url action=edit id=$user->getIdUser()}">{icon src=pencil class=tip title=Editar}</a></td>
					<td><a href="{url action=delete id=$user->getIdUser()}" class="confirm">{icon src=delete class=tip title=Borrar}</a></td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	</div>
</div>
