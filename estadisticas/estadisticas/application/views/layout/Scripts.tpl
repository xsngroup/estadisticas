<script type="text/javascript"> 
var baseUrl = "{$baseUrl}";
var domain = "{$domain}";
var codeRef = "{$username}";
</script>
<!-- Production
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
-->
<!-- Development -->
<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery-ui-1.7.2.custom.min.js"></script>

<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery.delegate.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery.simplemodal-1.3.min.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery-validate/localization/messages_es.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery-autocomplete/jquery.autocomplete.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery.jeditable.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/tooltip.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/expanded.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/utils.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/actions.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery_005.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/hint.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery_006.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery_002.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/jquery/jquery_003.js"></script>
<script type="text/javascript" src="{$baseUrl}/js/custom_blue.js"></script>