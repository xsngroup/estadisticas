<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/> 
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	<title>{$systemTitle} | {$contentTitle}</title>
	<link rel="stylesheet" href="{$baseUrl}/css/style.css" type="text/css" />	
	<link rel="stylesheet" href="{$baseUrl}/css/screen.css" type="text/css" />
	{include file="layout/Scripts.tpl"}
</head>
<body {if !$systemUser}style="background: #fff;"{/if}>
<div class="content_wrapper">
<!-- Begin header -->
  <div id="header">
    <div id="logo">
	{if $systemUser != null}
	{if $systemUser->getIdUser()!= 103}
	<img src="{$baseUrl}/images/template/theme/logo.png" alt="logo">
	{/if}
	{/if}
	</div>
    <div id="baseUrl">{if $systemUser}<div class="subHeader"><div>{$systemTitle} &raquo; {$contentTitle}</div></div>{/if}</div>
    <div id="account_info">       
		{if $systemUser}<img class="mid_align" alt="Online" src="{$baseUrl}/images/template/theme/icon_online.png">{$l10n->_('Bienvenido')} <a href="#">{$systemUser->getUsername()}</a> (<a href="#">{$systemAccessRole->getName()}</a>) | <a href="{$baseUrl}/auth/logout">{$l10n->_('Cerrar Sesi�n')}</a>{/if}      
    </div>
  </div>
<!-- End header -->

	<!-- Begin left panel -->
	<a href="javascript:;" id="show_menu">�</a>
	<div id="left_menu" {if !$systemUser}style="display: none;"{/if}>
		<a href="javascript:;" id="hide_menu">�</a>
		{include file='layout/Menu.tpl'}
		<br class="clear">
		
		
	</div>
	<!-- End left panel -->
	
	<!-- Begin content panel -->
	<div id="content" {if !$systemUser}style="display: none;"{/if}>
	  <div class="inner"> 	    
	    <div id="contentPanel">
          	 {include file="layout/Messages.tpl"}
          	 {$contentPlaceHolder}	    
	    </div>
	  </div>
	    <br class="clear" />
	    <br class="clear" />
	    {include file="layout/Modals.tpl"}
		<!-- Begin footer -->
		<div id="footer">
			� Copyright 2010 by Xsn Group
		</div>
		<!-- End footer -->	    	  
	</div>
	<!-- End content panel -->
</div>
</body>
</html>
