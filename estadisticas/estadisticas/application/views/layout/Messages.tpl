{if $ok}<div class="ok alert_success"><p>{$ok}</p></div>{/if}
{if $error}<div class="error alert_error"><p>{$error}</p></div>{/if}
{if $notice}<div class="notice alert_info"><p>{$notice}</p></div>{/if}
{if $alert}<div class="alert"><p>{$alert}</p></div>{/if}
{if $warning}<div class="warning alert_warning"><p>{$warning}</p></div>{/if}
{if $note}<div class="note alert_warning"><p>{$note}</p></div>{/if}