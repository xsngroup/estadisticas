_row.tpl

<tr>
    <td>{$ppt->getIdPpt()}</td>
    <td>{$ppt->getName()}</td>
    <td>{$ppt->getCode()}</td>
    <td><a href="{url action=edit idPpt=$ppt->getIdPpt()}">{icon src=pencil class=tip title=$i18n->_('Edit')}</a></td>
    <td><a href="{url action=delete idPpt=$ppt->getIdPpt()}" class="confirm">{icon src=delete class=tip title=$i18n->_('Delete')}</a></td>
</tr>
