<script type="text/javascript" src="{$baseUrl}/js/modules/flash-app/script.js"></script> 
<span><b>�ltima actualizaci�n: <em>{$timeUpdate}</em></b></span><br/><br/>
{foreach $result as $item}
{if $flagLive==true} 
<table width="100%" cellspacing="0" cellpadding="0" class="data"> 
  <tbody>
     <tr>
                <th align="right">{$l10n->_('Aplicaci�n:')}</th>
                <td class="last">{$apps[$item->getIdApp()]}</td>       
     </tr>  
     <tr>
                <th align="right">{$l10n->_('Fecha y hora:')}</th>
                <td class="last">{$item->getTimestamp()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Bytes de entrada:')}{icon src=information class=tip style="padding-left:5px;" title="Total de bytes recibidos por la aplicaci�n"}</th>
                <td class="last">{$item->getBytesIn()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Bytes de salida:')}{icon src=information class=tip style="padding-left:5px;" title="Total de bytes enviados por la aplicaci�n"}</th>
                <td class="last">{$item->getBytesOut()}</td>       
     </tr>
     
     <tr>
                <th align="right">{$l10n->_('Conectados actuales:')}{icon src=information class=tip style="padding-left:5px;" title="Usuarios conectados actualmente"}</th>
                <td class="last">
                {if $apps[$item->getIdApp()]=='live'}
                {$concurrentLive}
                {else}
                {$item->getConnected()}
                {/if}
                </td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Conexiones aceptadas:')}{icon src=information class=tip style="padding-left:5px;" title="Total de conexiones (viewers) aceptadas por la aplicaci�n"}</th>
                <td class="last">{$item->getAccepted()}</td>       
     </tr>     
     <tr>
                <th align="right">{$l10n->_('Conexiones Rechazadas:')}{icon src=information class=tip style="padding-left:5px;" title="Total de conexiones (viewers) rechazadas por la aplicaci�n"}</th>
                <td class="last">{$item->getRejected()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Inicio de ejecuci�n:')}{icon src=information class=tip style="padding-left:5px;" title="Fecha y hora en que se inici� la aplicaci�n"}</th>
                <td class="last">{$item->getLaunchTime()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Ancho de banda de entrada:')}{icon src=information class=tip style="padding-left:5px;" title="Ancho de banda consumido por la aplicaci�n"}</th>
                <td class="last">{$item->getBwIn()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Ancho de banda de salida:')}{icon src=information class=tip style="padding-left:5px;" title="Ancho de banda consumido por la aplicaci�n"}</th>
                <td class="last">{$item->getBwOut()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Total conectados:')}{icon src=information class=tip style="padding-left:5px;" title="Total de conectados (viewers) a la aplicaci�n desde su inici� de ejecuci�n"}</th>
                <td class="last">{$item->getTotalConnects()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Total desconectados:')}{icon src=information class=tip style="padding-left:5px;" title="Total de desconectados (viewers) a la aplicaci�n desde su inici� de ejecuci�n"}</th>
                <td class="last">{$item->getTotalDisconnects()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Total de instancias cargadas:')}</th>
                <td class="last">{$item->getTotalInstancesLoaded()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Total de instancias cerradas:')}</th>
                <td class="last">{$item->getTotalInstancesUnloaded()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Tiempo transcurrido:')}{icon src=information class=tip style="padding-left:5px;" title="Tiempo transcurrido en segundos desde que se inici� la ejecuci�n de la aplicaci�n"}</th>
                <td class="last">{$item->getUpTime()}</td>       
     </tr>
                                                                        
 
  </tbody>
</table>
{else}
 {if $apps[$item->getIdApp()]!='live'}
	<table width="100%" cellspacing="0" cellpadding="0" class="data"> 
	  <tbody>
	     <tr>
	                <th align="right">{$l10n->_('Aplicaci�n:')}</th>
	                <td class="last">{$apps[$item->getIdApp()]}</td>       
	     </tr>  
	     <tr>
	                <th align="right">{$l10n->_('Fecha y hora:')}</th>
	                <td class="last">{$item->getTimestamp()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Bytes de entrada:')}{icon src=information class=tip style="padding-left:5px;" title="Total de bytes recibidos por la aplicaci�n"}</th>
	                <td class="last">{$item->getBytesIn()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Bytes de salida:')}{icon src=information class=tip style="padding-left:5px;" title="Total de bytes enviados por la aplicaci�n"}</th>
	                <td class="last">{$item->getBytesOut()}</td>       
	     </tr>
	     
	     <tr>
	                <th align="right">{$l10n->_('Conectados actuales:')}{icon src=information class=tip style="padding-left:5px;" title="Usuarios conectados actualmente"}</th>
	                <td class="last">
	                {$item->getConnected()}
	                </td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Conexiones aceptadas:')}{icon src=information class=tip style="padding-left:5px;" title="Total de conexiones (viewers) aceptadas por la aplicaci�n"}</th>
	                <td class="last">{$item->getAccepted()}</td>       
	     </tr>     
	     <tr>
	                <th align="right">{$l10n->_('Conexiones Rechazadas:')}{icon src=information class=tip style="padding-left:5px;" title="Total de conexiones (viewers) rechazadas por la aplicaci�n"}</th>
	                <td class="last">{$item->getRejected()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Inicio de ejecuci�n:')}{icon src=information class=tip style="padding-left:5px;" title="Fecha y hora en que se inici� la aplicaci�n"}</th>
	                <td class="last">{$item->getLaunchTime()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Ancho de banda de entrada:')}{icon src=information class=tip style="padding-left:5px;" title="Ancho de banda consumido por la aplicaci�n"}</th>
	                <td class="last">{$item->getBwIn()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Ancho de banda de salida:')}{icon src=information class=tip style="padding-left:5px;" title="Ancho de banda consumido por la aplicaci�n"}</th>
	                <td class="last">{$item->getBwOut()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Total conectados:')}{icon src=information class=tip style="padding-left:5px;" title="Total de conectados (viewers) a la aplicaci�n desde su inici� de ejecuci�n"}</th>
	                <td class="last">{$item->getTotalConnects()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Total desconectados:')}{icon src=information class=tip style="padding-left:5px;" title="Total de desconectados (viewers) a la aplicaci�n desde su inici� de ejecuci�n"}</th>
	                <td class="last">{$item->getTotalDisconnects()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Total de instancias cargadas:')}</th>
	                <td class="last">{$item->getTotalInstancesLoaded()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Total de instancias cerradas:')}</th>
	                <td class="last">{$item->getTotalInstancesUnloaded()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Tiempo transcurrido:')}{icon src=information class=tip style="padding-left:5px;" title="Tiempo transcurrido en segundos desde que se inici� la ejecuci�n de la aplicaci�n"}</th>
	                <td class="last">{$item->getUpTime()}</td>       
	     </tr>
	                                                                        
	 
	  </tbody>
	</table>
 {/if}
{/if}
<br/>
{foreachelse}
                <span>No existe informaci�n por el momento...</span>         
{/foreach} 