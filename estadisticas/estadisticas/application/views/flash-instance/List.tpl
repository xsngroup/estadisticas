<div class="column_left">
    <div class="header"><span>{$l10n->_('Instancias en vivo')}</span></div>
    <br class="clear" />
    <div class="content">
<form action="{url action=create}" method="post" class="validate ajaxForm">
<h4>{$l10n->_('Guardar nueva Instancia')}</h4>
        {include file='forms/FlashInstance.tpl'}
<br/>
<p> 
<input type="submit" value="{$l10n->_('Guardar')}" />
</p>
</form>
	</div>
</div>

<div class="column_right">
    <div class="header"><span>{$l10n->_('Listado de Instancias')}</span></div>
    <br class="clear" />
    <div class="content">
<table  width="100%" cellspacing="0" cellpadding="0" class="data">
    <thead>
        <tr>
            <th>{$l10n->_('Id')}</th>
            <th>{$l10n->_('Nombre')}</th>
            <th>{$l10n->_('Servidor')}</th>
            <th colspan="2">{$l10n->_('Actions')}</th>
        </tr>
    </thead>
    <tbody id="ajaxList">
        {foreach $flashInstances as $flashInstance}
            <tr class="{$flashInstance@iteration|odd}">
                <td>{$flashInstance->getIdInstance()}</td>
                <td>{$flashInstance->getName()}</td>
                <td>{$Servers[$flashInstance->getIdServer()]}</td>
                <td><a href="{url action=edit idInstance=$flashInstance->getIdInstance()}">{icon src=pencil class=tip title=$l10n->_('Edit')}</a></td>
                <td><a href="{url action=delete idInstance=$flashInstance->getIdInstance()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Delete')}</a></td>
            </tr>
        {/foreach}
    </tbody>
</table>
	</div>
</div>
