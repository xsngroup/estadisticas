<form action="{url action=create}" method="post" class="validate ajaxForm">
<table class="center">
    <caption>{$i18n->_('Usertemp')}</caption>
    <tfoot>
        <tr>
            <td colspan="2"><input type="submit" value="{$i18n->_('Save')}" /></td>
        </tr>
    </tfoot>
    <tbody>
        {include file='usertemp/Form.tpl'}
    </tbody>
</table>
</form>
<hr/>


<table class="center">
    <caption>{$i18n->_('List')}</caption>
    <thead>
        <tr>
            <td>{$i18n->_('IdUsertemp')}</td>
            <td>{$i18n->_('Username')}</td>
            <td>{$i18n->_('Password')}</td>
            <td>{$i18n->_('Status')}</td>
            <td>{$i18n->_('IdAccessRole')}</td>
            <td colspan="2">{$i18n->_('Actions')}</td>
        </tr>
    </thead>
    <tbody id="ajaxList">
        {foreach $usertemps as $usertemp}
            <tr class="{$usertemp@iteration|odd}">
                <td>{$usertemp->getIdUsertemp()}</td>
                <td>{$usertemp->getUsername()}</td>
                <td>{$usertemp->getPassword()}</td>
                <td>{$usertemp->getStatus()}</td>
                <td>{$usertemp->getIdAccessRole()}</td>
                <td><a href="{url action=edit idUsertemp=$usertemp->getIdUsertemp()}">{icon src=pencil class=tip title=$i18n->_('Edit')}</a></td>
                <td><a href="{url action=delete idUsertemp=$usertemp->getIdUsertemp()}" class="confirm">{icon src=delete class=tip title=$i18n->_('Delete')}</a></td>
            </tr>
        {/foreach}
    </tbody>
</table>

