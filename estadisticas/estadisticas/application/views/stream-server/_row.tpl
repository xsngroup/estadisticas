<tr>
    <td>{$streamServer->getIdServer()}</td>
    <td>{$streamServer->getUrl()}</td>
    <td>{{$status[$streamServer->getStatus()]}}</td>
    <td>{$ServerTypes[$idType]}</td>
    <td><a href="{url action=edit idServer=$streamServer->getIdServer()}">{icon src=pencil class=tip title=$l10n->_('Edit')}</a></td>
    <td><a href="{url action=delete idServer=$streamServer->getIdServer()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Delete')}</a></td>
</tr>
