<div class="column_left">
    <div class="header"><span>{$l10n->_('Servidores streaming')}</span></div>
    <br class="clear" />
    <div class="content">
<form action="{url action=create}" method="post" class="validate ajaxForm">
<h4>{$l10n->_('Guardar nuevo')}</h4>
{include file='forms/StreamServer.tpl'}
<br/>
<p> 
<input type="submit" value="{$l10n->_('Guardar')}" />
</p>
</form>
	</div>
</div>

<div class="column_right">
    <div class="header"><span>{$l10n->_('Listado de Servidores')}</span></div>
    <br class="clear" />
    <div class="content">
<table width="100%" cellspacing="0" cellpadding="0" class="data">
    <thead>
        <tr>
            <th>{$l10n->_('Id')}</th>
            <th>{$l10n->_('Ip')}</th>
            <th>{$l10n->_('Estado')}</th>
            <th>{$l10n->_('Tipo de servidor')}</th>
            <th colspan="2">{$l10n->_('Acciones')}</th>
        </tr>
    </thead>
    <tbody id="ajaxList">
        {foreach $streamServers as $streamServer}
            <tr class="{$streamServer@iteration|odd}">
                <td>{$streamServer->getIdServer()}</td>
                <td>{$streamServer->getUrl()}</td>
                <td>{$status[$streamServer->getStatus()]}</td>
                <td>
                {foreach $serverTypeRel as $item}
                  {if $item['id_server']==$streamServer->getIdServer()}
                   {$ServerTypes[$item['id_type']]},
                  {/if} 
                {/foreach}                
                </td>
                <td><a href="{url action=edit idServer=$streamServer->getIdServer()}">{icon src=pencil class=tip title=$l10n->_('Edit')}</a></td>
                <td><a href="{url action=delete idServer=$streamServer->getIdServer()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Delete')}</a></td>
            </tr>
        {/foreach}
    </tbody>
</table>
	</div>
</div>
