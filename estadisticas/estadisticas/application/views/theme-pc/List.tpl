<div class="column_left">
    <div class="header"><span>{$l10n->_('Temas pc')}</span></div>
    <br class="clear" />
    <div class="content">
<form action="{url action=create}" method="post" class="validate ajaxForm">        
<h4>{$l10n->_('Guardar nuevo tema pc')}</h4>
{include file='forms/ThemePc.tpl'}
<br/>
<p> 
<input type="submit" value="{$l10n->_('Guardar')}" />
</p>
</form>
	</div>
</div>

<div class="column_right">
    <div class="header"><span>{$l10n->_('Listado de temas pc')}</span></div>
    <br class="clear" />
    <div class="content">
<table width="100%" cellspacing="0" cellpadding="0" class="data">
    <thead>
        <tr>
            <td>{$l10n->_('Id')}</td>
            <td>{$l10n->_('Nombre')}</td>
            <td>{$l10n->_('ScreenShot')}</td>
            <td colspan="2">{$l10n->_('Acciones')}</td>
        </tr>
    </thead>
    <tbody id="ajaxList">
        {foreach $themePcs as $themePc}
            <tr class="{$themePc@iteration|odd}">
                <td>{$themePc->getIdThemePc()}</td>
                <td>{$themePc->getName()}</td>
                <td>{$themePc->getScreenShot()}</td>
                <td><a href="{url action=edit idThemePc=$themePc->getIdThemePc()}">{icon src=pencil class=tip title=$l10n->_('Edit')}</a></td>
                <td><a href="{url action=delete idThemePc=$themePc->getIdThemePc()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Delete')}</a></td>
            </tr>
        {foreachelse}
            <tr>
               <td colspan="5" align="center">No existen elementos para mostrar...</td>
            </tr>    
        {/foreach}
    </tbody>
</table>
	</div>
</div>
