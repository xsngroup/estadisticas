_row.tpl

<tr>
    <td>{$flashStream->getIdStream()}</td>
    <td>{$flashStream->getName()}</td>
    <td>{$flashStream->getIdLive()}</td>
    <td><a href="{url action=edit idStream=$flashStream->getIdStream()}">{icon src=pencil class=tip title=$i18n->_('Edit')}</a></td>
    <td><a href="{url action=delete idStream=$flashStream->getIdStream()}" class="confirm">{icon src=delete class=tip title=$i18n->_('Delete')}</a></td>
</tr>
