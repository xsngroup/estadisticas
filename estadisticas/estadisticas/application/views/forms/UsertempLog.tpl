<tr>
    <th>{$i18n->_('id_usertemp_log')}</th>
    <td># {$post['id_usertemp_log']}<input type="hidden" name="id_usertemp_log" id="id_usertemp_log" value="{$post['id_usertemp_log']}" /></td>
</tr>
<tr>
    <th>{$i18n->_('id_usertemp')}</th>
    <td>{html_options name=id_usertemp id=id_usertemp options=$Usertemps selected=$post['id_usertemp'] }</td>
</tr>
<tr>
    <th>{$i18n->_('event_type')}</th>
    <td><input type="text" name="event_type" id="event_type" value="{$post['event_type']}" class="number required" /></td>
</tr>
<tr>
    <th>{$i18n->_('ip')}</th>
    <td><input type="text" name="ip" id="ip" value="{$post['ip']}" class=" required" /></td>
</tr>
<tr>
    <th>{$i18n->_('timestamp')}</th>
    <td><input type="text" name="timestamp" id="timestamp" value="{$post['timestamp']}" class=" required" /></td>
</tr>
<tr>
    <th>{$i18n->_('note')}</th>
    <td><textarea name="note" id="note" class="">{$post['note']}</textarea></td>
</tr>

<!--
$idUsertempLog = $this->getRequest()->getParam('id_usertemp_log');
$eventType = $this->getRequest()->getParam('event_type');
$ip = $this->getRequest()->getParam('ip');
$timestamp = $this->getRequest()->getParam('timestamp');
$note = $this->getRequest()->getParam('note');
$idUsertemp = $this->getRequest()->getParam('id_usertemp');
-->

<!--
$post = array(
    'id_usertemp_log' => $usertempLog->getIdUsertempLog(),
    'event_type' => $usertempLog->getEventType(),
    'ip' => $usertempLog->getIp(),
    'timestamp' => $usertempLog->getTimestamp(),
    'note' => $usertempLog->getNote(),
    'id_usertemp' => $usertempLog->getIdUsertemp(),
);
-->
