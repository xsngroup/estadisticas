<tr>
    <th>{$i18n->_('id_stream_stat')}</th>
    <td># {$post['id_stream_stat']}<input type="hidden" name="id_stream_stat" id="id_stream_stat" value="{$post['id_stream_stat']}" /></td>
</tr>
<tr>
    <th>{$i18n->_('id_stream')}</th>
    <td>{html_options name=id_stream id=id_stream options=$Streams selected=$post['id_stream'] }</td>
</tr>
<tr>
    <th>{$i18n->_('timestamp')}</th>
    <td><input type="text" name="timestamp" id="timestamp" value="{$post['timestamp']}" class="" /></td>
</tr>
<tr>
    <th>{$i18n->_('type')}</th>
    <td><input type="text" name="type" id="type" value="{$post['type']}" class="" /></td>
</tr>
<tr>
    <th>{$i18n->_('client')}</th>
    <td><input type="text" name="client" id="client" value="{$post['client']}" class="" /></td>
</tr>
<tr>
    <th>{$i18n->_('publish_time')}</th>
    <td><input type="text" name="publish_time" id="publish_time" value="{$post['publish_time']}" class="" /></td>
</tr>
<tr>
    <th>{$i18n->_('connects')}</th>
    <td><input type="text" name="connects" id="connects" value="{$post['connects']}" class="number" /></td>
</tr>

<!--
$idStreamStat = $this->getRequest()->getParam('id_stream_stat');
$timestamp = $this->getRequest()->getParam('timestamp');
$type = $this->getRequest()->getParam('type');
$client = $this->getRequest()->getParam('client');
$publishTime = $this->getRequest()->getParam('publish_time');
$connects = $this->getRequest()->getParam('connects');
$idStream = $this->getRequest()->getParam('id_stream');
-->

<!--
$post = array(
    'id_stream_stat' => $helixStreamStat->getIdStreamStat(),
    'timestamp' => $helixStreamStat->getTimestamp(),
    'type' => $helixStreamStat->getType(),
    'client' => $helixStreamStat->getClient(),
    'publish_time' => $helixStreamStat->getPublishTime(),
    'connects' => $helixStreamStat->getConnects(),
    'id_stream' => $helixStreamStat->getIdStream(),
);
-->
