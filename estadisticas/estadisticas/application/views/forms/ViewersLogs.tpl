<tr>
    <th>{$i18n->_('id_viewers_log')}</th>
    <td># {$post['id_viewers_log']}<input type="hidden" name="id_viewers_log" id="id_viewers_log" value="{$post['id_viewers_log']}" /></td>
</tr>
<tr>
    <th>{$i18n->_('id_viewer')}</th>
    <td>{html_options name=id_viewer id=id_viewer options=$Viewers selected=$post['id_viewer'] }</td>
</tr>
<tr>
    <th>{$i18n->_('event_type')}</th>
    <td><input type="text" name="event_type" id="event_type" value="{$post['event_type']}" class="number required" /></td>
</tr>
<tr>
    <th>{$i18n->_('ip')}</th>
    <td><input type="text" name="ip" id="ip" value="{$post['ip']}" class=" required" /></td>
</tr>
<tr>
    <th>{$i18n->_('timestamp')}</th>
    <td><input type="text" name="timestamp" id="timestamp" value="{$post['timestamp']}" class=" required" /></td>
</tr>
<tr>
    <th>{$i18n->_('note')}</th>
    <td><textarea name="note" id="note" class="">{$post['note']}</textarea></td>
</tr>

<!--
$idViewersLog = $this->getRequest()->getParam('id_viewers_log');
$eventType = $this->getRequest()->getParam('event_type');
$ip = $this->getRequest()->getParam('ip');
$timestamp = $this->getRequest()->getParam('timestamp');
$note = $this->getRequest()->getParam('note');
$idViewer = $this->getRequest()->getParam('id_viewer');
-->

<!--
$post = array(
    'id_viewers_log' => $viewersLogs->getIdViewersLog(),
    'event_type' => $viewersLogs->getEventType(),
    'ip' => $viewersLogs->getIp(),
    'timestamp' => $viewersLogs->getTimestamp(),
    'note' => $viewersLogs->getNote(),
    'id_viewer' => $viewersLogs->getIdViewer(),
);
-->
