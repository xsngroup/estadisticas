<tr>
    <th>{$i18n->_('id_app_stat')}</th>
    <td># {$post['id_app_stat']}<input type="hidden" name="id_app_stat" id="id_app_stat" value="{$post['id_app_stat']}" /></td>
</tr>
<tr>
    <th>{$i18n->_('id_app')}</th>
    <td>{html_options name=id_app id=id_app options=$Apps selected=$post['id_app'] }</td>
</tr>
<tr>
    <th>{$i18n->_('timestamp')}</th>
    <td><input type="text" name="timestamp" id="timestamp" value="{$post['timestamp']}" class="datePicker dateISO required" /></td>
</tr>
<tr>
    <th>{$i18n->_('accepted')}</th>
    <td><input type="text" name="accepted" id="accepted" value="{$post['accepted']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('bytes_in')}</th>
    <td><input type="text" name="bytes_in" id="bytes_in" value="{$post['bytes_in']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('bytes_out')}</th>
    <td><input type="text" name="bytes_out" id="bytes_out" value="{$post['bytes_out']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('connected')}</th>
    <td><input type="text" name="connected" id="connected" value="{$post['connected']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('rejected')}</th>
    <td><input type="text" name="rejected" id="rejected" value="{$post['rejected']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('launch_time')}</th>
    <td><input type="text" name="launch_time" id="launch_time" value="{$post['launch_time']}" class="datePicker dateISO required" /></td>
</tr>
<tr>
    <th>{$i18n->_('bw_in')}</th>
    <td><input type="text" name="bw_in" id="bw_in" value="{$post['bw_in']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('bw_out')}</th>
    <td><input type="text" name="bw_out" id="bw_out" value="{$post['bw_out']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('total_connects')}</th>
    <td><input type="text" name="total_connects" id="total_connects" value="{$post['total_connects']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('total_disconnects')}</th>
    <td><input type="text" name="total_disconnects" id="total_disconnects" value="{$post['total_disconnects']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('total_instances_loaded')}</th>
    <td><input type="text" name="total_instances_loaded" id="total_instances_loaded" value="{$post['total_instances_loaded']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('total_instances_unloaded')}</th>
    <td><input type="text" name="total_instances_unloaded" id="total_instances_unloaded" value="{$post['total_instances_unloaded']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('up_time')}</th>
    <td><input type="text" name="up_time" id="up_time" value="{$post['up_time']}" class="number" /></td>
</tr>

<!--
$idAppStat = $this->getRequest()->getParam('id_app_stat');
$timestamp = $this->getRequest()->getParam('timestamp');
$accepted = $this->getRequest()->getParam('accepted');
$bytesIn = $this->getRequest()->getParam('bytes_in');
$bytesOut = $this->getRequest()->getParam('bytes_out');
$connected = $this->getRequest()->getParam('connected');
$rejected = $this->getRequest()->getParam('rejected');
$launchTime = $this->getRequest()->getParam('launch_time');
$bwIn = $this->getRequest()->getParam('bw_in');
$bwOut = $this->getRequest()->getParam('bw_out');
$totalConnects = $this->getRequest()->getParam('total_connects');
$totalDisconnects = $this->getRequest()->getParam('total_disconnects');
$totalInstancesLoaded = $this->getRequest()->getParam('total_instances_loaded');
$totalInstancesUnloaded = $this->getRequest()->getParam('total_instances_unloaded');
$upTime = $this->getRequest()->getParam('up_time');
$idApp = $this->getRequest()->getParam('id_app');
-->

<!--
$post = array(
    'id_app_stat' => $flashAppStat->getIdAppStat(),
    'timestamp' => $flashAppStat->getTimestamp(),
    'accepted' => $flashAppStat->getAccepted(),
    'bytes_in' => $flashAppStat->getBytesIn(),
    'bytes_out' => $flashAppStat->getBytesOut(),
    'connected' => $flashAppStat->getConnected(),
    'rejected' => $flashAppStat->getRejected(),
    'launch_time' => $flashAppStat->getLaunchTime(),
    'bw_in' => $flashAppStat->getBwIn(),
    'bw_out' => $flashAppStat->getBwOut(),
    'total_connects' => $flashAppStat->getTotalConnects(),
    'total_disconnects' => $flashAppStat->getTotalDisconnects(),
    'total_instances_loaded' => $flashAppStat->getTotalInstancesLoaded(),
    'total_instances_unloaded' => $flashAppStat->getTotalInstancesUnloaded(),
    'up_time' => $flashAppStat->getUpTime(),
    'id_app' => $flashAppStat->getIdApp(),
);
-->
