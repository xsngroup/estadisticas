<script type="text/javascript" src="{$baseUrl}/js/plugins/dygraph-combined.js"></script>
<script type="text/javascript">
$(document).ready( function()
{
	var dateStart="{$dateStart}";
	var dateEnd="{$dateEnd}";
	var initHour="{$initHour}";
	var finishHour="{$finishHour}";
	var idStream={$stream->getIdStream()};
		
	$('#download').click(function(){	
		//var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, width=508, height=365, top=85, left=140";
		//window.open(baseUrl + "/report/download","",opciones);	 

		location.href=baseUrl + "/report/download/dateStart/"+dateStart+"/dateEnd/"+dateEnd+"/initHour/"+initHour+"/finishHour/"+finishHour+"/idStream/"+idStream;
		
	});
});
</script>
<div class="onecolumn">
 <div class="header"><span>{$l10n->_('Hist�rico de Streaming en vivo')}</span></div>
 <br class="clear" />
 <div class="content">
<form action="#" class="validate" method="post">
<p>
<label>Del: </label>{$dates[0]}
<label> al: </label>{$dates[1]}
</p>
<br/><br/>
<p>
  <label>Streaming: </label>{$stream->getName()}
</p>
<br/>
<label>Tiempo promedio de visualizaci�n:</label><span> {$promedio} Seg.</span>
<br/>
<br/>
    <p><b>Series visibles:</b></p>

    <p>
      <input type=checkbox id="0" checked onClick="change(this)">
      <label for="0">Concurrentes</label><br/>
      <input type=checkbox id="1" checked onClick="change(this)">
      <label for="1">Usuarios �nicos</label><br/>
      <input type=checkbox id="2" checked onClick="change(this)">
      <label for="2">Usuarios nuevos</label><br/>
      <input type=checkbox id="3" checked  onClick="change(this)">
      <label for="3">Peticiones</label>            
    </p>
<br/>
<br/>
{if $flagData==true}
<div id="graphdiv" style="600px; height: 300px;"><img src="{$baseUrl}/images/template/basic/bar-loader.gif" /></div>
<div id="labelGraph"></div>
<div id="loadInfo"></div>
<script type="text/javascript">


  g = new Dygraph(

    // containing div
    document.getElementById("graphdiv"),

    // CSV or path to a CSV file.
   "{$baseUrl}/report/process-data/dateStart/{$dateStart}/dateEnd/{$dateEnd}/initHour/{$initHour}/finishHour/{$finishHour}/idStream/{$stream->getIdStream()}/groupBy/{$groupBy}",
		
   {
        fillGraph: true,
    	visibility: [true, true, true, true] ,
    	{if $groupBy==1}
        labels: [ "Date",  "Concurrentes", "Usuarios �nicos", "Usuarios nuevos", "Hits" ],
        {else}
    	labels: [ "Date",  "Concurrentes" ],
    	{/if}
        
        labelsDiv: document.getElementById("labelGraph"),  
        labelsSeparateLines: true     
        //labelsDivWidth: 300
   
    }


  );

  function change(el) {
      g.setVisibility(parseInt(el.id), el.checked);
    }
  
</script>
{else}
<center><h4>No se encontraron resultados para mostrar...</h4></center> 
{/if}	
	<br/>
<!-- 
<p> 
 <label>Total Conectados: </label>{$totalCon}
</p>-->

  <br/>
    <br/>
  	<p>        
       <input type="button" value="Descargar excel" id="download">
       <input type="button" value="{$l10n->_('Regresar')}" class="back" />       
    </p>

</form>
</div>
</div>
<div>
</div>