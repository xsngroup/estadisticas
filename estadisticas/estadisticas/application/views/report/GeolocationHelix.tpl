<script type="text/javascript">
<!--
$(document).ready(function(){
	   $('.typeVisit').click(function(){
		      if($(this).val()==1){
		    	  $("#loadVideo").hide();
		    	  $("#loadSignal").show();
		      }
		      else{
		    	  $("#loadVideo").show();
		    	  $("#loadSignal").hide();
		      }			      
		});	
});	
//-->
function validateHour(){
        var iHour=(document.getElementById('initHour').value).split(":");
        var eHour=(document.getElementById('finishHour').value).split(":");

        var iH=parseInt(iHour[0],10);
        var iM=parseInt(iHour[1],10);

        var eH=parseInt(eHour[0],10);
        var eM=parseInt(eHour[1],10);

        if((iH>eH)||(iH==eH && iM>eM)){
                return 'false';
        }else{
	return 'true';
        }
}

function validate(){
if(document.getElementById('date_start').value!=""){
var start=new Date(Date.parse(document.getElementById('date_start').value.replace(/-/g," ")));
var end=new Date(Date.parse(document.getElementById('date_end').value.replace(/-/g," ")));
var val=validateHour();
        if((start.getTime()-end.getTime())>0){
        alert("La fecha final no puede ser menor a la inicial");
        }else if((start.getTime()-end.getTime())==0){
         
        document.forms["estForm"].submit();
        }else{

                if(val=='true'){
                document.forms["estForm"].submit();
                }else{
                alert("La hora inicial no puede ser mayor a la final.");
                }
        }
	}else{
              	alert("Es necesario especificar una fecha inicial.");
        }
}

</script>
<div class="column_left">
 <div class="header"><span>{$l10n->_('Helix hist&oacute;rico geolocalizador')}</span></div>
 <br class="clear" />
 <div class="content">
<form action="{$baseUrl}/report/map-helix" class="validate" method="post" id="estForm">
<label>Filtro de Fechas:</label><br/>
<p>   

     <label>*Desde:</label>  
     <input type="text" name="date_start" id="date_start" value="{$post['date_start']}{$dateStart}" class="field text medium datePicker dateISO required picker" />                    
</p> 
<br/>
<p>
     <label style="padding-right:3px;">*Hasta: </label> 
     <input type="text" name="date_end" id="date_end" value="{$post['date_end']}{$dateEnd}" class="field text medium datePicker dateISO picker" />                   
</p>
<br/>
<br/>
<p>
<label>Filtro de horarios:</label><br/>
<label>De: </label><input type="text" id="initHour" name="initHour" size="10" class="field text small" value="09:00" /><label style="padding-left:15px;">a: </label><input type="text" size="10" class="field text small" id="finishHour" name="finishHour" value="14:00" />
</p>
{if $flagStream==true && $flagVod==true}
<br/>
<br/>
<p>
<label>Filtro de visitas:</label><br/>
<input type="radio" name="typeVisit" class="typeVisit" value="1" checked="checked" /><label>Se�al</label>
<input type="radio" name="typeVisit" class="typeVisit" value="2" /><label>Videos</label>
</p>
{/if}
{if $flagStream==true}
<div id="loadSignal">
<br/>
<p>
<label>Se�ales:</label><br/>
{html_options name=stream id=stream options=$streams}
<input type="hidden" name="flagStream" value="1"/>
</p>
</div>
{/if}
{if $flagVod==true}
<div id="loadVideo" {if $flagStream==true}style="display: none;"{/if}>
<br/>
<p>
<label>Videos:</label><br/>
{html_options name=vod id=vod options=$vods}
<input type="hidden" name="flagVideo" value="1"/>
</p>
</div>
{/if}
<br/> 
<br/> 
<p> 
<input type="hidden" name="idCompany" value="{$idCompany}" />
<input type="hidden" name="idServer" value="{$idServer}" />
<input type="button" value="{$l10n->_('Generar')}" onclick="validate()"/>
</p>
</form>
</div>
</div>
