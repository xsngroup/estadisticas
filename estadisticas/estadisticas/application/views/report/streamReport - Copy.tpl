<script type="text/javascript">
$(document).ready(function(){
	$('#groupBy').change(function(){	
        //alert($(this).val());	
        if($(this).val()==1)
        {
        	$("#firstDate").show();
            $("#secondDate").hide();
            $("#meses").hide();
        }

        if($(this).val()==2)
        {
        	$("#firstDate").show();
       	    $("#secondDate").show();
            $("#meses").hide();
        }

        if($(this).val()==3)
        {
        	$("#firstDate").hide();
       	    $("#secondDate").hide();
        	$("#meses").show();
        }                
	});	
});	

</script>
<div class="column_left">
 <div class="header"><span>{$l10n->_('Hist�rico de Streaming en vivo')}</span></div>
 <br class="clear" />
 <div class="content">
<form action="{$baseUrl}/report/process-stream" class="validate" method="post">
<label>Filtro de Fechas:</label><br/><br/>
<p>
     <label>Agrupar por:</label>  
     <select id="groupBy" name="groupBy">
        <option value="1">Horas</option>
        <option value="2">D�as</option>
       <!-- <option value="3">Mes</option> --> 
     </select>
</p>    
<br/>
<p id="firstDate">   

     <label>*Desde:</label>  
     <input type="text" name="date_start" id="date_start" value="{$post['date_start']}{$dateStart}" class="field text medium datePicker dateISO required picker" />                    
<br/>
</p> 

<p id="secondDate" style="display: none;">
<br/>
     <label style="padding-right:3px;">*Hasta: </label> 
     <input type="text" name="date_end" id="date_end" value="{$post['date_end']}{$dateEnd}" class="field text medium datePicker dateISO picker" />                   
<br/>
</p>
<br/>
<br/>
<p>
<label>Filtro de horarios:</label><br/>
<label>De: </label><input type="text" name="initHour" size="10" class="field text small" value="09:00" /><label style="padding-left:15px;">a: </label><input type="text" size="10" class="field text small"  name="finishHour" value="14:00" />
</p>
<br/>
<p>
<label>Se�ales:</label><br/>
{html_options name=stream id=stream options=$streams}
</p>
<br/> 
<br/> 
<p> 
<input type="hidden" name="idCompany" value="{$idCompany}" />
<input type="hidden" name="idServer" value="{$idServer}" />
<input type="submit" value="{$l10n->_('Generar')}" />
</p>
</form>
</div>
</div>