<script type="text/javascript" src="{$baseUrl}/js/plugins/swfobject.js"></script>	
<div class="onecolumn">
 <div class="header"><span>{$l10n->_('Histórico de las instancias')}</span></div>
 <br class="clear" />
 <div class="content">
<form action="#" class="validate" method="post">
<p>
  <label>Instancia: </label>{$instance->getName()}
</p>
    <center>
	<div id="flashcontent">
		<strong>You need to upgrade your Flash Player</strong>
	</div>
	
	<script type="text/javascript">
		// <![CDATA[		
		var so = new SWFObject("{$baseUrl}/settings/amline.swf", "amline", "520", "400", "8", "#FFFFFF");
		so.addVariable("path", "{$baseUrl}/settings/");
		so.addVariable("settings_file", encodeURIComponent("{$baseUrl}/settings/amline_settings.xml"));                // you can set two or more different settings files here (separated by commas)
		so.addVariable("data_file", encodeURIComponent("{$baseUrl}/report/xml/id/{$instance->getIdInstance()}/dateStart/{$dateStart}/dateEnd/{$dateEnd}"));
		
//	so.addVariable("chart_data", encodeURIComponent("data in CSV or XML format"));                    // you can pass chart data as a string directly from this file
//	so.addVariable("chart_settings", encodeURIComponent("<settings>...</settings>"));                 // you can pass chart settings as a string directly from this file
//	so.addVariable("additional_chart_settings", encodeURIComponent("<settings>...</settings>"));      // you can append some chart settings to the loaded ones
//  so.addVariable("loading_settings", "LOADING SETTINGS");                                           // you can set custom "loading settings" text here
//  so.addVariable("loading_data", "LOADING DATA");                                                   // you can set custom "loading data" text here
//	so.addVariable("preloader_color", "#999999");
//  so.addVariable("error_loading_file", "ERROR LOADING FILE");                                   // you can set custom "error loading file" text here
		so.write("flashcontent");
		// ]]>
	</script>
	</center>
	<br/>
  <p>
    <label>Total conectados: </label>{$totalConnects}
  </p>
  <p>
    <label>Total desconectados: </label>{$totalDisconnects}
  </p>
  <br/>
  	<p> 
<input type="button" value="{$l10n->_('Regresar')}" class="back" />
</p>
</form>
</div>
</div>