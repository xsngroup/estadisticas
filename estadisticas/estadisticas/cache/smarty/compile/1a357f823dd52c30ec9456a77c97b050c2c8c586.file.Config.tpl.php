<?php /* Smarty version 3.0rc1, created on 2013-01-21 11:32:54
         compiled from "application/views/auth/Config.tpl" */ ?>
<?php /*%%SmartyHeaderCode:137468128050fd7bc6350b12-57325272%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1a357f823dd52c30ec9456a77c97b050c2c8c586' => 
    array (
      0 => 'application/views/auth/Config.tpl',
      1 => 1307657330,
    ),
  ),
  'nocache_hash' => '137468128050fd7bc6350b12-57325272',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/js/modules/auth/config.js"></script>
<div class="column_left">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Facultades de Usuarios');?>
</span></div>
    <br class="clear" />
    <div id="progressbar" ></div>  
    <table width="100%" cellspacing="0" cellpadding="0" class="data">        
        <thead>
            <tr>
                <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Acci�n/Grupo de Usuario');?>
</th>
                <?php $_smarty_tpl->assign('totalRoles',count($_smarty_tpl->getVariable('accessRoles')->value),null,null);?>
            <?php  $_smarty_tpl->tpl_vars['accessRole'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('accessRoles')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['accessRole']->key => $_smarty_tpl->tpl_vars['accessRole']->value){
?>
                <th><?php echo $_smarty_tpl->getVariable('accessRole')->value->getName();?>
 <br/>
                  <img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/icons/tick.png" title="checkAll" class="masterChecker" id="parent_<?php echo $_smarty_tpl->getVariable('accessRole')->value->getIdAccessRole();?>
"/>
                  <img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/icons/cross.png" title="uncheckAll" class="masterUnchecker" id="parent_<?php echo $_smarty_tpl->getVariable('accessRole')->value->getIdAccessRole();?>
"/> 
                </th>
            <?php }} ?>
            </tr>
        </thead>
        <tbody>
            <?php  $_smarty_tpl->tpl_vars['controller'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('controllers')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['controller']->key => $_smarty_tpl->tpl_vars['controller']->value){
?>
                <tr>
                    <th colspan="<?php echo $_smarty_tpl->getVariable('totalRoles')->value+1;?>
"><?php echo $_smarty_tpl->getVariable('controller')->value->getName();?>
</th>
                </tr>
                  <?php  $_smarty_tpl->tpl_vars['action'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('actions')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['action']->key => $_smarty_tpl->tpl_vars['action']->value){
?>
                    <?php if ($_smarty_tpl->getVariable('action')->value->getIdController()==$_smarty_tpl->getVariable('controller')->value->getIdController()){?>
                  <tr>
                    <td><?php echo $_smarty_tpl->getVariable('action')->value->getName();?>
</td>
                    <?php  $_smarty_tpl->tpl_vars['accessRole'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('accessRoles')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['accessRole']->key => $_smarty_tpl->tpl_vars['accessRole']->value){
?>
                        <td class="spaced">
                            <input type="checkbox" name="allow[<?php echo $_smarty_tpl->getVariable('action')->value->getIdAction();?>
][<?php echo $_smarty_tpl->getVariable('accessRole')->value->getIdAccessRole();?>
]" id="allow_<?php echo $_smarty_tpl->getVariable('action')->value->getIdAction();?>
_<?php echo $_smarty_tpl->getVariable('accessRole')->value->getIdAccessRole();?>
"
                            class="ajaxed childOf<?php echo $_smarty_tpl->getVariable('accessRole')->value->getIdAccessRole();?>
ar "
                            <?php $_smarty_tpl->assign('idAction',$_smarty_tpl->getVariable('action')->value->getIdAction(),null,null);?>
                            <?php $_smarty_tpl->assign('idAccessRole',$_smarty_tpl->getVariable('accessRole')->value->getIdAccessRole(),null,null);?>
                            <?php if ($_smarty_tpl->getVariable('permissions')->value[$_smarty_tpl->getVariable('idAction')->value][$_smarty_tpl->getVariable('idAccessRole')->value]==1){?> checked="checked" <?php }?>
                            value="1"/>
                        </td>
                     <?php }} ?>
                   </tr>
                    <?php }?>
                  <?php }} ?>
                </tr>
            <?php }} ?>
        </tbody>
    </table>
	</div>
</div>