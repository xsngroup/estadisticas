<?php /* Smarty version 3.0rc1, created on 2013-06-06 12:42:48
         compiled from "application/views\report/mapHelix.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1219251b0ca185e8611-88649357%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e4599cd4f6048fbb553e16c157244d6a2218b8d5' => 
    array (
      0 => 'application/views\\report/mapHelix.tpl',
      1 => 1370540151,
    ),
  ),
  'nocache_hash' => '1219251b0ca185e8611-88649357',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<style type="text/css">
  html { height: 100% }
  body { height: 100%; margin: 0px; padding: 0px }
  #map_canvas { height: 100% }
</style>

<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&language=es"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	$('#download').click(function(){	
		//var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, width=508, height=365, top=85, left=140";
		//window.open(baseUrl + "/report/download","",opciones);	 

		location.href=baseUrl + "/report/download-maps-helix";
		
	});	
	var a = <?php echo $_smarty_tpl->getVariable('tamanio')->value;?>
;
	<?php if ($_smarty_tpl->getVariable('tamanio')->value<57){?>
    var latlng = new google.maps.LatLng(41.244772343082076, -13.359375);
    var myOptions = {
      zoom: 2,
      center: latlng,
      mapTypeControl: false,
      navigationControl: false,
		streetViewControl: false,
		zoomControl: true,		  
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"),
        myOptions);
    <?php $_smarty_tpl->assign("indicador",1,null,null);?>
    <?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('ipAccess')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
        var latlng2<?php echo $_smarty_tpl->getVariable('indicador')->value;?>
 = new google.maps.LatLng(<?php echo $_smarty_tpl->getVariable('latlon')->value[$_smarty_tpl->tpl_vars['key']->value];?>
);
	    var contentString<?php echo $_smarty_tpl->getVariable('indicador')->value;?>
 = '<div id="contentTip">'+
	    '<div id="siteNotice">'+
	    '</div>'+
	    '<h3 id="firstHeading" class="firstHeading">Accesos a se\xF1al en vivo</h3>'+
	    '<div id="bodyContent">'+
	    '<p><b><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
</b>, <?php echo $_smarty_tpl->tpl_vars['value']->value;?>
 visitantes   ' +
	    //'<a href="javascript:void(0);">'+
	    //'ver detalle...</a></p>'+
	    '</div>'+
	    '</div>';
	
		var infowindow<?php echo $_smarty_tpl->getVariable('indicador')->value;?>
 = new google.maps.InfoWindow({
		    content: contentString<?php echo $_smarty_tpl->getVariable('indicador')->value;?>

		});
		
		var marker<?php echo $_smarty_tpl->getVariable('indicador')->value;?>
 = new google.maps.Marker({
		    position: latlng2<?php echo $_smarty_tpl->getVariable('indicador')->value;?>
,
		    map: map,
		    title:"<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"
		});
		
		google.maps.event.addListener(marker<?php echo $_smarty_tpl->getVariable('indicador')->value;?>
, 'click', function() {
		  infowindow<?php echo $_smarty_tpl->getVariable('indicador')->value;?>
.open(map,marker<?php echo $_smarty_tpl->getVariable('indicador')->value;?>
);
		});	
		<?php $_smarty_tpl->assign('indicador',$_smarty_tpl->getVariable('indicador')->value+1,null,null);?>
	<?php }} ?>
	<?php }?>
	});	

</script>
<div class="onecolumn">
 <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Helix hist&oacute;rico geolocalizador');?>
</span></div>
 <br class="clear" />
 <div class="content">
<form action="#" class="validate" method="post">
<p>
<label>Del: </label><?php echo $_smarty_tpl->getVariable('dates')->value[0];?>

<label> al: </label><?php echo $_smarty_tpl->getVariable('dates')->value[1];?>

</p>
<br/>
<p>
<label>Se�al: </label><?php echo $_smarty_tpl->getVariable('streamName')->value;?>

</p>
<br/>
<?php if ($_smarty_tpl->getVariable('tamanio')->value<57){?>
<div id="mapcontainer" style="position:relative;width:100%;height:100%;">
  <div id="map_canvas" style="position:absolute;width:100%; height:100%,z-index:1"></div>
</div>
<?php }?>
<?php if ($_smarty_tpl->getVariable('tamanio')->value>=57){?>
<div id="textoError" style="position:relative; padding-left:125px; font-weight:bold; font-size:14px"><p>Las IP's exceden el numero permito para mostrar visualmente, para m�s detalle porfavor descarga el reporte en excel.</p></div>
<?php }?>
<br/>
<br/> 
<input type="button" value="Descargar excel" id="download"> 
<input type="button" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Regresar');?>
" class="back" />
</p>
</form>
</div>
</div>