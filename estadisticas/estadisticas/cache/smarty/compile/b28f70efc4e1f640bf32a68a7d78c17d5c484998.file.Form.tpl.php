<?php /* Smarty version 3.0rc1, created on 2013-02-15 19:07:49
         compiled from "application/views\user/Form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1160511e8785d767d8-30180945%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b28f70efc4e1f640bf32a68a7d78c17d5c484998' => 
    array (
      0 => 'application/views\\user/Form.tpl',
      1 => 1307635730,
    ),
  ),
  'nocache_hash' => '1160511e8785d767d8-30180945',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_html_options')) include 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Smarty3\plugins\function.html_options.php';
?><div class="onecolumn">
 <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Listado de Usuarios');?>
</span></div>
 <br class="clear" />
 <div class="content">
<form action="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/user/<?php if ($_smarty_tpl->getVariable('post')->value['id']){?>update<?php }else{ ?>create<?php }?>/" method="post" class="validate">
<h4><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Guardar nuevo usuario');?>
</h4>
<p>
<label for="username"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Usuario');?>
:</label><br/>
<input type="text" name="username" id="username" value="<?php echo $_smarty_tpl->getVariable('post')->value['username'];?>
" size="40" class="required" <?php if ($_smarty_tpl->getVariable('isNew')->value==false){?>readonly="readonly"<?php }?> />
</p>
<p>
<label for="name"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Nombre');?>
:</label><br/>
<input type="text" name="name" id="name" value="<?php echo $_smarty_tpl->getVariable('post')->value['name'];?>
" size="40" class="required"/>
</p>
<p>
<label for="middlename"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Apellido Paterno');?>
:</label><br/>
<input type="text" name="middlename" id="middlename" value="<?php echo $_smarty_tpl->getVariable('post')->value['middlename'];?>
" size="40" class="required"/>
</p>
<p>
<label for="lastname"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Apellido Materno');?>
:</label><br/>
<input type="text" name="lastname" id="lastname" value="<?php echo $_smarty_tpl->getVariable('post')->value['lastname'];?>
" size="40" />
</p>
<p> 
<label for="password"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Contraseņa');?>
:</label><br/>
<input type="password" name="password" id="password" value="" size="40" class="<?php if ($_smarty_tpl->getVariable('isNew')->value){?>required<?php }?>"/>
</p>
<p> 
<label for="passwordConfirm"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Confirmar Contraseņa');?>
:</label><br/>
<input type="password" name="passwordConfirm" id="passwordConfirm" value="" size="40"  class="<?php if ($_smarty_tpl->getVariable('isNew')->value){?>required<?php }?>"/>
</p>
<p> 
<label for="group"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Grupo');?>
:</label><br/>
<?php echo smarty_function_html_options(array('name'=>'accessRole','id'=>'accessRole','options'=>$_smarty_tpl->getVariable('accessRoles')->value,'selected'=>$_smarty_tpl->getVariable('post')->value['accessRole'],'class'=>'required'),$_smarty_tpl->smarty,$_smarty_tpl);?>

</p>
<p> 
<label for="group"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Estado civil');?>
:</label><br/>
<?php echo smarty_function_html_options(array('name'=>'maritalStatus','id'=>'maritalStatus','options'=>$_smarty_tpl->getVariable('maritalStatus')->value,'selected'=>$_smarty_tpl->getVariable('post')->value['maritalStatus']),$_smarty_tpl->smarty,$_smarty_tpl);?>

</p>
<br/>
<p> 
  <input type="submit" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Guardar');?>
"/>
  <input type="button" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Cancelar');?>
"/>
  <input type="hidden" name="id" value="<?php echo $_smarty_tpl->getVariable('post')->value['id'];?>
" id="id" />
</p>
</form>
 </div>
</div>