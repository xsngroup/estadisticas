<?php /* Smarty version 3.0rc1, created on 2013-03-12 12:10:10
         compiled from "application/views\menu/Manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:28160513f6172aca7b9-64955001%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '46c25f84181e489287be168fcf7e91cae632796b' => 
    array (
      0 => 'application/views\\menu/Manage.tpl',
      1 => 1307635730,
    ),
  ),
  'nocache_hash' => '28160513f6172aca7b9-64955001',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/js/modules/menu/manage.js"></script>
<div class="column_left">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Opciones disponibles');?>
</span></div>
    <br class="clear" />
    <div class="content"> 
    <table class="wide" width="100%" cellspacing="0" cellpadding="0" class="data">
        <tbody>
            <?php  $_smarty_tpl->tpl_vars['controller'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('controllers')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['controller']->key => $_smarty_tpl->tpl_vars['controller']->value){
?>
                <tr>
                    <td><img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/icons/magnifier.png" alt="toggle" id="parent_<?php echo $_smarty_tpl->getVariable('controller')->value->getIdController();?>
" class="parentMenu" /></td>
                    <th><?php echo $_smarty_tpl->getVariable('controller')->value->getName();?>
</th>
                    <th>(0)</th>
                </tr>
                  <?php  $_smarty_tpl->tpl_vars['action'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('actions')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['action']->key => $_smarty_tpl->tpl_vars['action']->value){
?>
                    <?php if ($_smarty_tpl->getVariable('action')->value->getIdController()==$_smarty_tpl->getVariable('controller')->value->getIdController()){?>
                  <tr class="child childOf<?php echo $_smarty_tpl->getVariable('controller')->value->getIdController();?>
" id="action_item_<?php echo $_smarty_tpl->getVariable('action')->value->getIdAction();?>
">
                    <td></td>
                    <td><?php echo $_smarty_tpl->getVariable('action')->value->getName();?>
</td>
                    <td>
                        <img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/icons/add.png" alt="add" id="add_child_<?php echo $_smarty_tpl->getVariable('action')->value->getIdAction();?>
" class="moveItem" />
                    </td>
            
                   </tr>
                    <?php }?>
                  <?php }} ?>
            <?php }} ?>
        </tbody>
    </table>               
</div>
</div>
<div class="column_right">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Resultado de Men�');?>
</span></div>
    <br class="clear" />
    <div class="content">
      <p>
                <label for="name"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Nombre');?>
</label>
                <input id="name" name="name" value="">
       </p> 
       <br/>   
    <?php echo $_smarty_tpl->getVariable('menuItems')->value;?>

	</div>
</div>    