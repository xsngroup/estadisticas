<?php /* Smarty version 3.0rc1, created on 2013-02-15 19:07:44
         compiled from "application/views\user/List.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11756511e8780e50a48-68846700%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1fa652159d95e5c495a89dc1240a5e33631063ce' => 
    array (
      0 => 'application/views\\user/List.tpl',
      1 => 1307635730,
    ),
  ),
  'nocache_hash' => '11756511e8780e50a48-68846700',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_even')) include 'lib/smarty/plugins\modifier.even.php';
if (!is_callable('smarty_function_url')) include 'lib/smarty/plugins\function.url.php';
if (!is_callable('smarty_function_icon')) include 'lib/smarty/plugins\function.icon.php';
?><div class="onecolumn">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Listado de Usuarios');?>
</span></div>
    <br class="clear" />
    <div class="content">
	<table width="100%" cellspacing="0" cellpadding="0" class="data">
		<caption><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Lista de Usuarios');?>
</caption>
		<thead>
			<tr>
				<th>#</th>
				<th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Username');?>
</th>
				<th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Nombre');?>
</th>
				<th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Grupo');?>
</th>
				<th colspan="2"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Acciones');?>
</th>
			</tr>
		</thead>
		<tbody id="ajaxList">
			<?php  $_smarty_tpl->tpl_vars['user'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('users')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['user']->iteration=0;
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['user']->key => $_smarty_tpl->tpl_vars['user']->value){
 $_smarty_tpl->tpl_vars['user']->iteration++;
?>
				<tr class="<?php echo smarty_modifier_even($_smarty_tpl->tpl_vars['user']->iteration);?>
">
					<td><?php echo $_smarty_tpl->getVariable('user')->value->getIdUser();?>
</td>
					<td><?php echo $_smarty_tpl->getVariable('user')->value->getUsername();?>
</td>
					<td><?php echo $_smarty_tpl->getVariable('user')->value->getFullName();?>
</td>
					<td><?php echo $_smarty_tpl->getVariable('accessRoles')->value[$_smarty_tpl->getVariable('user')->value->getIdAccessRole()];?>
</td>
					<td><a href="<?php echo smarty_function_url(array('action'=>'edit','id'=>$_smarty_tpl->getVariable('user')->value->getIdUser()),$_smarty_tpl->smarty,$_smarty_tpl);?>
"><?php echo smarty_function_icon(array('src'=>'pencil','class'=>'tip','title'=>'Editar'),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
					<td><a href="<?php echo smarty_function_url(array('action'=>'delete','id'=>$_smarty_tpl->getVariable('user')->value->getIdUser()),$_smarty_tpl->smarty,$_smarty_tpl);?>
" class="confirm"><?php echo smarty_function_icon(array('src'=>'delete','class'=>'tip','title'=>'Borrar'),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
				</tr>
			<?php }} ?>
		</tbody>
	</table>
	</div>
</div>
