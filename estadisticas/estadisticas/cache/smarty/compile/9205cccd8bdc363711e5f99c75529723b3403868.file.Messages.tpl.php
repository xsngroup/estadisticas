<?php /* Smarty version 3.0rc1, created on 2012-08-05 23:16:18
         compiled from "application/views/layout/Messages.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1628285829501f45124a41e7-88596930%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9205cccd8bdc363711e5f99c75529723b3403868' => 
    array (
      0 => 'application/views/layout/Messages.tpl',
      1 => 1307657330,
    ),
  ),
  'nocache_hash' => '1628285829501f45124a41e7-88596930',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_smarty_tpl->getVariable('ok')->value){?><div class="ok alert_success"><p><?php echo $_smarty_tpl->getVariable('ok')->value;?>
</p></div><?php }?>
<?php if ($_smarty_tpl->getVariable('error')->value){?><div class="error alert_error"><p><?php echo $_smarty_tpl->getVariable('error')->value;?>
</p></div><?php }?>
<?php if ($_smarty_tpl->getVariable('notice')->value){?><div class="notice alert_info"><p><?php echo $_smarty_tpl->getVariable('notice')->value;?>
</p></div><?php }?>
<?php if ($_smarty_tpl->getVariable('alert')->value){?><div class="alert"><p><?php echo $_smarty_tpl->getVariable('alert')->value;?>
</p></div><?php }?>
<?php if ($_smarty_tpl->getVariable('warning')->value){?><div class="warning alert_warning"><p><?php echo $_smarty_tpl->getVariable('warning')->value;?>
</p></div><?php }?>
<?php if ($_smarty_tpl->getVariable('note')->value){?><div class="note alert_warning"><p><?php echo $_smarty_tpl->getVariable('note')->value;?>
</p></div><?php }?>