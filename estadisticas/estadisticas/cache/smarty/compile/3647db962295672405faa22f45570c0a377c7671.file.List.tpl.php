<?php /* Smarty version 3.0rc1, created on 2013-01-21 17:09:51
         compiled from "application/views/helix-live/List.tpl" */ ?>
<?php /*%%SmartyHeaderCode:182133585750fdcabf1a7ce5-93082988%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3647db962295672405faa22f45570c0a377c7671' => 
    array (
      0 => 'application/views/helix-live/List.tpl',
      1 => 1309474442,
    ),
  ),
  'nocache_hash' => '182133585750fdcabf1a7ce5-93082988',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_url')) include 'lib/smarty/plugins/function.url.php';
if (!is_callable('smarty_modifier_odd')) include 'lib/smarty/plugins/modifier.odd.php';
if (!is_callable('smarty_function_icon')) include 'lib/smarty/plugins/function.icon.php';
?><div class="column_left">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Helix Server Puntos de Montaje');?>
</span></div>
    <br class="clear" />
    <div class="content">
<form action="<?php echo smarty_function_url(array('action'=>'create'),$_smarty_tpl->smarty,$_smarty_tpl);?>
" method="post" class="validate ajaxForm">
<h4><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Guardar nuevo punto de montaje');?>
</h4>
<?php $_template = new Smarty_Internal_Template('forms/HelixLive.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

<br/>
<p> 
<input type="submit" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Guardar');?>
" />
</p>
</form>
	</div>
</div>

<div class="column_right">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Listado de Puntos de Montaje');?>
</span></div>
    <br class="clear" />
    <div class="content">
<table width="100%" cellspacing="0" cellpadding="0" class="data">
    <thead>
        <tr>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('IdLive');?>
</th>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Name');?>
</th>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('IdServer');?>
</th>
            <th colspan="2"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Actions');?>
</th>
        </tr>
    </thead>
    <tbody id="ajaxList">
        <?php  $_smarty_tpl->tpl_vars['helixLive'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('helixLives')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['helixLive']->iteration=0;
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['helixLive']->key => $_smarty_tpl->tpl_vars['helixLive']->value){
 $_smarty_tpl->tpl_vars['helixLive']->iteration++;
?>
            <tr class="<?php echo smarty_modifier_odd($_smarty_tpl->tpl_vars['helixLive']->iteration);?>
">
                <td><?php echo $_smarty_tpl->getVariable('helixLive')->value->getIdLive();?>
</td>
                <td><?php echo $_smarty_tpl->getVariable('helixLive')->value->getName();?>
</td>
                <td><?php echo $_smarty_tpl->getVariable('servers')->value[$_smarty_tpl->getVariable('helixLive')->value->getIdServer()];?>
</td>
                <td><a href="<?php echo smarty_function_url(array('action'=>'edit','idLive'=>$_smarty_tpl->getVariable('helixLive')->value->getIdLive()),$_smarty_tpl->smarty,$_smarty_tpl);?>
"><?php echo smarty_function_icon(array('src'=>'pencil','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Edit')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
                <td><a href="<?php echo smarty_function_url(array('action'=>'delete','idLive'=>$_smarty_tpl->getVariable('helixLive')->value->getIdLive()),$_smarty_tpl->smarty,$_smarty_tpl);?>
" class="confirm"><?php echo smarty_function_icon(array('src'=>'delete','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Delete')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
            </tr>
        <?php }} else { ?>
           <tr><td colspan="5" align="center">No existen elementos para mostrar...</td></tr>               
        <?php } ?>
    </tbody>
</table>
	</div>
</div>

