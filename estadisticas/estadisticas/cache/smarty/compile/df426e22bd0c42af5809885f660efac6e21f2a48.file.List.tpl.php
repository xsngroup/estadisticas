<?php /* Smarty version 3.0rc1, created on 2013-03-25 17:28:57
         compiled from "application/views\access-role/List.tpl" */ ?>
<?php /*%%SmartyHeaderCode:53365150cfa9e71be8-29713338%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'df426e22bd0c42af5809885f660efac6e21f2a48' => 
    array (
      0 => 'application/views\\access-role/List.tpl',
      1 => 1307635728,
    ),
  ),
  'nocache_hash' => '53365150cfa9e71be8-29713338',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_url')) include 'lib/smarty/plugins\function.url.php';
if (!is_callable('smarty_modifier_odd')) include 'lib/smarty/plugins\modifier.odd.php';
if (!is_callable('smarty_function_icon')) include 'lib/smarty/plugins\function.icon.php';
?><div class="column_left">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Perfiles de Usuario');?>
</span></div>
    <br class="clear" />
    <div class="content">
<form action="<?php echo smarty_function_url(array('action'=>'create'),$_smarty_tpl->smarty,$_smarty_tpl);?>
" method="post" class="validate ajaxForm">
<h4><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Guardar nuevo Grupo de Usuario');?>
</h4>
<p>
<label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Nombre');?>
</label><br/>
<input type="text" id="name" name="name" class="required" size="40"/>
</p>
<br/>
<p> 
<input type="submit" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Guardar');?>
" />
</p>
</form>
	</div>
</div>

<div class="column_right">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Listado de Perfiles');?>
</span></div>
    <br class="clear" />
    <div class="content">
<table width="100%" cellspacing="0" cellpadding="0" class="data">
	<thead>
		<tr>
			<th>#</th>
			<th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Nombre');?>
</th>
			<th colspan="2"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Acciones');?>
</th>
		</tr>
	</thead>
	<tbody id="ajaxList">
		<?php  $_smarty_tpl->tpl_vars['accessRole'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('accessRoles')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['accessRole']->iteration=0;
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['accessRole']->key => $_smarty_tpl->tpl_vars['accessRole']->value){
 $_smarty_tpl->tpl_vars['accessRole']->iteration++;
?>
			<tr class="<?php echo smarty_modifier_odd($_smarty_tpl->tpl_vars['accessRole']->iteration);?>
">
				<td><?php echo $_smarty_tpl->getVariable('accessRole')->value->getIdAccessRole();?>
</td>
				<td><?php echo $_smarty_tpl->getVariable('accessRole')->value->getName();?>
</td>
				<td><a href="<?php echo smarty_function_url(array('action'=>'edit','id'=>$_smarty_tpl->getVariable('accessRole')->value->getIdAccessRole()),$_smarty_tpl->smarty,$_smarty_tpl);?>
"><?php echo smarty_function_icon(array('src'=>'pencil','class'=>'tip','title'=>'Editar'),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
				<td><a href="<?php echo smarty_function_url(array('action'=>'delete','id'=>$_smarty_tpl->getVariable('accessRole')->value->getIdAccessRole()),$_smarty_tpl->smarty,$_smarty_tpl);?>
" class="confirm"><?php echo smarty_function_icon(array('src'=>'delete','class'=>'tip','title'=>'Borrar'),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
			</tr>
		<?php }} ?>
	</tbody>
</table>
	</div>
</div>