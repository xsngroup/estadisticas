<?php /* Smarty version 3.0rc1, created on 2013-02-15 19:15:18
         compiled from "application/views\helix-live/_row.tpl" */ ?>
<?php /*%%SmartyHeaderCode:30432511e8946c96ac4-40159902%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9b3a6f049525ec0b6d89c145ec534ae8c8e14db0' => 
    array (
      0 => 'application/views\\helix-live/_row.tpl',
      1 => 1309452842,
    ),
  ),
  'nocache_hash' => '30432511e8946c96ac4-40159902',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_url')) include 'lib/smarty/plugins\function.url.php';
if (!is_callable('smarty_function_icon')) include 'lib/smarty/plugins\function.icon.php';
?><tr>
    <td><?php echo $_smarty_tpl->getVariable('helixLive')->value->getIdLive();?>
</td>
    <td><?php echo $_smarty_tpl->getVariable('helixLive')->value->getName();?>
</td>
    <td><?php echo $_smarty_tpl->getVariable('servers')->value[$_smarty_tpl->getVariable('helixLive')->value->getIdServer()];?>
</td>
    <td><a href="<?php echo smarty_function_url(array('action'=>'edit','idLive'=>$_smarty_tpl->getVariable('helixLive')->value->getIdLive()),$_smarty_tpl->smarty,$_smarty_tpl);?>
"><?php echo smarty_function_icon(array('src'=>'pencil','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Edit')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
    <td><a href="<?php echo smarty_function_url(array('action'=>'delete','idLive'=>$_smarty_tpl->getVariable('helixLive')->value->getIdLive()),$_smarty_tpl->smarty,$_smarty_tpl);?>
" class="confirm"><?php echo smarty_function_icon(array('src'=>'delete','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Delete')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
</tr>
