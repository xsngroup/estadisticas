<?php /* Smarty version 3.0rc1, created on 2013-02-15 19:08:27
         compiled from "application/views\company/List.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3617511e87abe6aa18-43212568%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cf3511049e9af453078b161902b8a0c3e42e42c9' => 
    array (
      0 => 'application/views\\company/List.tpl',
      1 => 1330626328,
    ),
  ),
  'nocache_hash' => '3617511e87abe6aa18-43212568',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_url')) include 'lib/smarty/plugins\function.url.php';
if (!is_callable('smarty_function_html_options')) include 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Smarty3\plugins\function.html_options.php';
if (!is_callable('smarty_modifier_odd')) include 'lib/smarty/plugins\modifier.odd.php';
if (!is_callable('smarty_function_icon')) include 'lib/smarty/plugins\function.icon.php';
?><div class="onecolumn">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Cat�logo de clientes');?>
</span></div>
    <br class="clear" />
    <div class="content">
<form action="<?php echo smarty_function_url(array('action'=>'create'),$_smarty_tpl->smarty,$_smarty_tpl);?>
" method="post" class="validate ajaxForm">
<h4><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Datos del cliente');?>
</h4>
        <?php $_template = new Smarty_Internal_Template('forms/Company.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

<br/>
<h4><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Datos de acceso');?>
</h4>
<p>
<label for="name"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Nombre');?>
:</label><br/>
<input type="text" name="name" id="name" value="<?php echo $_smarty_tpl->getVariable('post')->value['name'];?>
" size="40" class="required"/>
</p>
<p>
<label for="middlename"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Apellido Paterno');?>
:</label><br/>
<input type="text" name="middlename" id="middlename" value="<?php echo $_smarty_tpl->getVariable('post')->value['middlename'];?>
" size="40" class="required"/>
</p>
<p>
<label for="username"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Usuario');?>
:</label><br/>
<input type="text" name="username" id="username" value="<?php echo $_smarty_tpl->getVariable('post')->value['username'];?>
" size="40" class="required" />
</p>
<p> 
<label for="password"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Contrase�a');?>
:</label><br/>
<input type="password" name="password" id="password" value="" size="40" class="<?php if ($_smarty_tpl->getVariable('isNew')->value){?>required<?php }?>"/>
</p>
<p> 
<label for="passwordConfirm"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Confirmar Contrase�a');?>
:</label><br/>
<input type="password" name="passwordConfirm" id="passwordConfirm" value="" size="40"  class="<?php if ($_smarty_tpl->getVariable('isNew')->value){?>required<?php }?>"/>
</p>
<p> 
<label for="group"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Grupo');?>
:</label><br/>
<?php echo smarty_function_html_options(array('name'=>'accessRole','id'=>'accessRole','options'=>$_smarty_tpl->getVariable('accessRoles')->value,'selected'=>$_smarty_tpl->getVariable('post')->value['accessRole'],'class'=>'required'),$_smarty_tpl->smarty,$_smarty_tpl);?>

</p>
<br/>
<p> 
<input type="submit" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Guardar');?>
" />
</p>
</form>
	</div>
</div>

<div class="onecolumn">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Listado de Clientes');?>
</span></div>
    <br class="clear" />
    <div class="content">
<table width="100%" cellspacing="0" cellpadding="0" class="data">
    <thead>
        <tr>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Id');?>
</th>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Cliente');?>
</th>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Contacto');?>
</th>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Email');?>
</th>
            <!-- <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Status');?>
</th> -->
            <th colspan="2"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Actions');?>
</th>
        </tr>
    </thead>
    <tbody id="ajaxList">
        <?php  $_smarty_tpl->tpl_vars['company'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('companys')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['company']->iteration=0;
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['company']->key => $_smarty_tpl->tpl_vars['company']->value){
 $_smarty_tpl->tpl_vars['company']->iteration++;
?>
            <tr class="<?php echo smarty_modifier_odd($_smarty_tpl->tpl_vars['company']->iteration);?>
">
                <td><?php echo $_smarty_tpl->getVariable('company')->value->getIdCompany();?>
</td>
                <td><?php echo $_smarty_tpl->getVariable('company')->value->getNameCompany();?>
</td>
                <td><?php echo $_smarty_tpl->getVariable('company')->value->getNameContac();?>
</td>
                <td><?php echo $_smarty_tpl->getVariable('emails')->value[$_smarty_tpl->getVariable('company')->value->getIdCompany()];?>
</td>
               <!--  <td><?php echo $_smarty_tpl->getVariable('status')->value[$_smarty_tpl->getVariable('company')->value->getStatus()];?>
</td> -->
                <td><a href="<?php echo smarty_function_url(array('action'=>'edit','idCompany'=>$_smarty_tpl->getVariable('company')->value->getIdCompany()),$_smarty_tpl->smarty,$_smarty_tpl);?>
"><?php echo smarty_function_icon(array('src'=>'pencil','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Edit')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
                <!-- <td><a href="<?php echo smarty_function_url(array('action'=>'delete','idCompany'=>$_smarty_tpl->getVariable('company')->value->getIdCompany()),$_smarty_tpl->smarty,$_smarty_tpl);?>
" class="confirm"><?php echo smarty_function_icon(array('src'=>'delete','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Delete')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td> -->
                <td><a href="<?php echo smarty_function_url(array('action'=>'rel','idCompany'=>$_smarty_tpl->getVariable('company')->value->getIdCompany()),$_smarty_tpl->smarty,$_smarty_tpl);?>
"><?php echo smarty_function_icon(array('src'=>'add','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Add')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
            </tr>
        <?php }} ?>
    </tbody>
</table>
	</div>
</div>
