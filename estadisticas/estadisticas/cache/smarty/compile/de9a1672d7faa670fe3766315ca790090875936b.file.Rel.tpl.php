<?php /* Smarty version 3.0rc1, created on 2013-01-21 17:18:44
         compiled from "application/views/company/Rel.tpl" */ ?>
<?php /*%%SmartyHeaderCode:79834254150fdccd4177975-80586567%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'de9a1672d7faa670fe3766315ca790090875936b' => 
    array (
      0 => 'application/views/company/Rel.tpl',
      1 => 1311262366,
    ),
  ),
  'nocache_hash' => '79834254150fdccd4177975-80586567',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_url')) include 'lib/smarty/plugins/function.url.php';
if (!is_callable('smarty_function_html_options')) include '/var/www/html/library/Smarty3/plugins/function.html_options.php';
?><script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/js/modules/company/process.js"></script>
<div class="onecolumn">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Configurar informaci�n del cliente');?>
</span></div>
    <br class="clear" />
    <div class="content">
  
<form action="<?php echo smarty_function_url(array('action'=>'reladd'),$_smarty_tpl->smarty,$_smarty_tpl);?>
" method="post" class="validate">
<p>
<label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Plataforma de streaming:');?>
</label><br/>
<input type="radio" name="typeStreamServer" class="typeStreamServer" value="1" checked="checked"/><label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('FMS');?>
</label>
<input type="radio" name="typeStreamServer" class="typeStreamServer" value="2" /><label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Helix');?>
</label>
</p> 
<br/> 
<p>
<label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Tipo de se�al:');?>
</label><br/>
<input type="radio" name="typeRelation" class="typeRelation" value="1" checked="checked"/><label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Live stream');?>
</label>
<input type="radio" name="typeRelation" class="typeRelation" value="2" /><label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('VOD stream');?>
</label>
</p>
<br/>
<div id="displayFlashOption">
	<div class="loadLive">
	<p>
	    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Servidor:');?>
</label><br/>
	    <select name="id_server" id="id_server" class="selectLive">
	        <option value="0">Seleccione una opci�n...</option>
	    <?php  $_smarty_tpl->tpl_vars['server'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('Servers')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['server']->key => $_smarty_tpl->tpl_vars['server']->value){
?>
	        <option value="<?php echo $_smarty_tpl->getVariable('server')->value->getIdServer();?>
"><?php echo $_smarty_tpl->getVariable('server')->value->getUrl();?>
</option>
	    <?php }} ?>
	    </select>
	</p>
	<br/>
	<p>
	    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Punto de montaje:');?>
</label><br/>
	    <?php echo smarty_function_html_options(array('name'=>'id_live','id'=>'id_live','class'=>'selectLive','options'=>$_smarty_tpl->getVariable('Lives')->value,'selected'=>$_smarty_tpl->getVariable('post')->value['id_server']),$_smarty_tpl->smarty,$_smarty_tpl);?>

	</p>
	<br/>
	<p>
	    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Stream:');?>
</label><br/>
	    <?php echo smarty_function_html_options(array('name'=>'id_stream','id'=>'id_stream','options'=>$_smarty_tpl->getVariable('Streams')->value,'selected'=>$_smarty_tpl->getVariable('post')->value['id_server']),$_smarty_tpl->smarty,$_smarty_tpl);?>

	</p>
	</div>
	<div class="loadVod" style="display:none;">
	<p>
	    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Servidor:');?>
</label><br/>
	    <select name="id_server2" id="id_server2" class="selectVod">
	        <option value="0">Seleccione una opci�n...</option>
	    <?php  $_smarty_tpl->tpl_vars['server'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('Servers')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['server']->key => $_smarty_tpl->tpl_vars['server']->value){
?>
	        <option value="<?php echo $_smarty_tpl->getVariable('server')->value->getIdServer();?>
"><?php echo $_smarty_tpl->getVariable('server')->value->getUrl();?>
</option>
	    <?php }} ?>
	    </select>
	</p>
	<br/>
	<p>
	    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Video on Demand:');?>
</label><br/>
	    <?php echo smarty_function_html_options(array('name'=>'id_vod','id'=>'id_vod','options'=>$_smarty_tpl->getVariable('Vod')->value),$_smarty_tpl->smarty,$_smarty_tpl);?>

	</p>
	</div>
</div>
<div id="displayHelixOption" style="display: none;">
<div class="loadLive">
	<p>
	    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Servidor:');?>
</label><br/>
	    <select name="id_server_helix1" id="id_server_helix1" class="selectLiveHelix">
	        <option value="0">Seleccione una opci�n...</option>
	    <?php  $_smarty_tpl->tpl_vars['server'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('Servers')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['server']->key => $_smarty_tpl->tpl_vars['server']->value){
?>
	        <option value="<?php echo $_smarty_tpl->getVariable('server')->value->getIdServer();?>
"><?php echo $_smarty_tpl->getVariable('server')->value->getUrl();?>
</option>
	    <?php }} ?>
	    </select>
	</p>
	<br/>
	<p>
	    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Punto de montaje:');?>
</label><br/>
	    <?php echo smarty_function_html_options(array('name'=>'id_live_helix','id'=>'id_live_helix','class'=>'selectLiveHelix','options'=>$_smarty_tpl->getVariable('Lives')->value),$_smarty_tpl->smarty,$_smarty_tpl);?>

	</p>
	<br/>
	<p>
	    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Stream:');?>
</label><br/>
	    <?php echo smarty_function_html_options(array('name'=>'id_stream_helix','id'=>'id_stream_helix','options'=>$_smarty_tpl->getVariable('Streams')->value),$_smarty_tpl->smarty,$_smarty_tpl);?>

	</p>
	</div>
	<div class="loadVod" style="display:none;">
	<p>
	    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Servidor:');?>
</label><br/>
	    <select name="id_server_helix2" id="id_server_helix2" class="selectVodHelix">
	        <option value="0">Seleccione una opci�n...</option>
	    <?php  $_smarty_tpl->tpl_vars['server'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('Servers')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['server']->key => $_smarty_tpl->tpl_vars['server']->value){
?>
	        <option value="<?php echo $_smarty_tpl->getVariable('server')->value->getIdServer();?>
"><?php echo $_smarty_tpl->getVariable('server')->value->getUrl();?>
</option>
	    <?php }} ?>
	    </select>
	</p>
	<br/>
	<p>
	    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Video on Demand:');?>
</label><br/>
	    <?php echo smarty_function_html_options(array('name'=>'id_vod_helix','id'=>'id_vod_helix','options'=>$_smarty_tpl->getVariable('Vod')->value),$_smarty_tpl->smarty,$_smarty_tpl);?>

	</p>
	</div>
</div>
<br/>
<p> 
<input type="hidden" name="idCompany" value="<?php echo $_smarty_tpl->getVariable('idCompany')->value;?>
" />
<input type="submit" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Guardar');?>
" />
<input type="button" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Regresar');?>
" class="back" />
</p> 
</form>

<br/>
<br/>
<h3 align="center">Lista de se�ales en vivo actualmente asignadas</h3>
<table width="100%" cellspacing="0" cellpadding="0" class="data"> 
  <thead>
      <tr>
                <th align="justify"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Nombre');?>
</th>
                <th align="justify"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Servidor');?>
</th>                  
                <th align="justify"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Titulo');?>
</th> 
                <th align="justify"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Descripci&oacute;n');?>
</th>                                                                      
     </tr> 
  </thead>
  <tbody>
   <?php if (count($_smarty_tpl->getVariable('listStreamFlash')->value)>0){?>
	   <?php  $_smarty_tpl->tpl_vars['streamItem'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('listStreamFlash')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['streamItem']->key => $_smarty_tpl->tpl_vars['streamItem']->value){
?>
	     <tr>
	                <td class="last"><?php echo $_smarty_tpl->getVariable('streamItem')->value->getName();?>
</td> 
	                <td class="last"><?php echo $_smarty_tpl->getVariable('serversCombo')->value[$_smarty_tpl->getVariable('streamItem')->value->getIdLive()];?>
</td> 
	                <td class="last"><?php echo $_smarty_tpl->getVariable('streamItem')->value->getTitle();?>
</td> 
	                <td class="last"><?php echo $_smarty_tpl->getVariable('streamItem')->value->getDescription();?>
</td>                                                       
	     </tr>
	   <?php }} ?>
   <?php }?>
   <?php if (count($_smarty_tpl->getVariable('listStreamHelix')->value)>0){?>
	   <?php  $_smarty_tpl->tpl_vars['streamItem'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('listStreamHelix')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['streamItem']->key => $_smarty_tpl->tpl_vars['streamItem']->value){
?>
	     <tr>
	                <td class="last"><?php echo $_smarty_tpl->getVariable('streamItem')->value->getName();?>
</td> 
	                <td class="last"><?php echo $_smarty_tpl->getVariable('serversCombo')->value[$_smarty_tpl->getVariable('streamItem')->value->getIdServer()];?>
</td> 
	                <td class="last"><?php echo $_smarty_tpl->getVariable('streamItem')->value->getTitle();?>
</td> 
	                <td class="last"><?php echo $_smarty_tpl->getVariable('streamItem')->value->getDescription();?>
</td>                                                       
	     </tr>
	   <?php }} ?>   
   <?php }?>
  <?php if (count($_smarty_tpl->getVariable('listStreamFlash')->value)<=0&&count($_smarty_tpl->getVariable('listStreamHelix')->value)<=0){?>
     <tr>
                <td class="last" colspan="4" align="center">No tiene se�ales en vivo asociadas...</td>       
     </tr>   
  <?php }?>  
  </tbody>
</table>
<br/>
<br/>
<h3 align="center">Lista de se�ales VOD actualmente asignadas</h3>
<table width="100%" cellspacing="0" cellpadding="0" class="data"> 
  <thead>
      <tr>
                <th align="justify"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Prefijo');?>
</th>
                <th align="justify"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Nombre');?>
</th>
                <th align="justify"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Servidor');?>
</th>                  
                <th align="justify"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Titulo');?>
</th> 
                <th align="justify"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Descripci&oacute;n');?>
</th>                                                                      
     </tr> 
  </thead>
  <tbody>
  <?php if (count($_smarty_tpl->getVariable('listVodFlash')->value)>0){?>
   <?php  $_smarty_tpl->tpl_vars['vodItem'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('listVodFlash')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['vodItem']->key => $_smarty_tpl->tpl_vars['vodItem']->value){
?>
     <tr>
                <td class="last"><?php echo $_smarty_tpl->getVariable('vodItem')->value->getPrefix();?>
</td> 
                <td class="last"><?php echo $_smarty_tpl->getVariable('vodItem')->value->getNameVideo();?>
</td> 
                <td class="last"><?php echo $_smarty_tpl->getVariable('serversCombo')->value[$_smarty_tpl->getVariable('vodItem')->value->getIdServer()];?>
</td> 
                <td class="last"><?php echo $_smarty_tpl->getVariable('vodItem')->value->getTitleVideo();?>
</td> 
                <td class="last"><?php echo $_smarty_tpl->getVariable('vodItem')->value->getDescriptionVideo();?>
</td>                                                       
     </tr>  
   <?php }} ?>
  <?php }?>
  <?php if (count($_smarty_tpl->getVariable('listVodHelix')->value)>0){?> 
   <?php  $_smarty_tpl->tpl_vars['vodItem'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('listVodHelix')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['vodItem']->key => $_smarty_tpl->tpl_vars['vodItem']->value){
?>
     <tr>
                <td class="last"><?php echo $_smarty_tpl->getVariable('vodItem')->value->getPrefix();?>
</td> 
                <td class="last"><?php echo $_smarty_tpl->getVariable('vodItem')->value->getNameVideo();?>
</td> 
                <td class="last"><?php echo $_smarty_tpl->getVariable('serversCombo')->value[$_smarty_tpl->getVariable('vodItem')->value->getIdServer()];?>
</td> 
                <td class="last"><?php echo $_smarty_tpl->getVariable('vodItem')->value->getTitleVideo();?>
</td> 
                <td class="last"><?php echo $_smarty_tpl->getVariable('vodItem')->value->getDescriptionVideo();?>
</td>                                                       
     </tr>  
   <?php }} ?>   
  <?php }?>
  <?php if (count($_smarty_tpl->getVariable('listVodFlash')->value)<=0&&count($_smarty_tpl->getVariable('listVodHelix')->value)<=0){?>
     <tr>
                <td class="last" colspan="5" align="center">No tiene se�ales en bajo demanda asociadas...</td>       
     </tr>        
  <?php }?> 
  </tbody>
</table>
	</div>
</div>    