<?php /* Smarty version 3.0rc1, created on 2012-08-05 23:16:18
         compiled from "application/views/layout/Layout.tpl" */ ?>
<?php /*%%SmartyHeaderCode:81599193501f4512361510-44097356%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '34d2b9b1f5ac7952390dca06a210675566752a75' => 
    array (
      0 => 'application/views/layout/Layout.tpl',
      1 => 1307657330,
    ),
  ),
  'nocache_hash' => '81599193501f4512361510-44097356',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/> 
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	<title><?php echo $_smarty_tpl->getVariable('systemTitle')->value;?>
 | <?php echo $_smarty_tpl->getVariable('contentTitle')->value;?>
</title>
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/css/style.css" type="text/css" />	
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/css/screen.css" type="text/css" />
	<?php $_template = new Smarty_Internal_Template("layout/Scripts.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

</head>
<body <?php if (!$_smarty_tpl->getVariable('systemUser')->value){?>style="background: #fff;"<?php }?>>
<div class="content_wrapper">
<!-- Begin header -->
  <div id="header">
    <div id="logo"><img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/logo.png" alt="logo"></div>
    <div id="baseUrl"><?php if ($_smarty_tpl->getVariable('systemUser')->value){?><div class="subHeader"><div><?php echo $_smarty_tpl->getVariable('systemTitle')->value;?>
 &raquo; <?php echo $_smarty_tpl->getVariable('contentTitle')->value;?>
</div></div><?php }?></div>
    <div id="account_info">       
		<?php if ($_smarty_tpl->getVariable('systemUser')->value){?><img class="mid_align" alt="Online" src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/icon_online.png"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Bienvenido');?>
 <a href="#"><?php echo $_smarty_tpl->getVariable('systemUser')->value->getUsername();?>
</a> (<a href="#"><?php echo $_smarty_tpl->getVariable('systemAccessRole')->value->getName();?>
</a>) | <a href="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/auth/logout"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Cerrar Sesi�n');?>
</a><?php }?>      
    </div>
  </div>
<!-- End header -->

	<!-- Begin left panel -->
	<a href="javascript:;" id="show_menu">�</a>
	<div id="left_menu" <?php if (!$_smarty_tpl->getVariable('systemUser')->value){?>style="display: none;"<?php }?>>
		<a href="javascript:;" id="hide_menu">�</a>
		<?php $_template = new Smarty_Internal_Template('layout/Menu.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

		<br class="clear">
		
		
	</div>
	<!-- End left panel -->
	
	<!-- Begin content panel -->
	<div id="content" <?php if (!$_smarty_tpl->getVariable('systemUser')->value){?>style="display: none;"<?php }?>>
	  <div class="inner"> 	    
	    <div id="contentPanel">
          	 <?php $_template = new Smarty_Internal_Template("layout/Messages.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

          	 <?php echo $_smarty_tpl->getVariable('contentPlaceHolder')->value;?>
	    
	    </div>
	  </div>
	    <br class="clear" />
	    <br class="clear" />
	    <?php $_template = new Smarty_Internal_Template("layout/Modals.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

		<!-- Begin footer -->
		<div id="footer">
			� Copyright 2010 by Xsn Group
		</div>
		<!-- End footer -->	    	  
	</div>
	<!-- End content panel -->
</div>
</body>
</html>
