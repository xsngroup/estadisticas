<?php /* Smarty version 3.0rc1, created on 2013-01-21 17:10:11
         compiled from "application/views/helix-live/_row.tpl" */ ?>
<?php /*%%SmartyHeaderCode:81762459050fdcad3031548-86936797%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd771835873f365adecbd5fc0c36d41cff7e3d4b5' => 
    array (
      0 => 'application/views/helix-live/_row.tpl',
      1 => 1309474442,
    ),
  ),
  'nocache_hash' => '81762459050fdcad3031548-86936797',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_url')) include 'lib/smarty/plugins/function.url.php';
if (!is_callable('smarty_function_icon')) include 'lib/smarty/plugins/function.icon.php';
?><tr>
    <td><?php echo $_smarty_tpl->getVariable('helixLive')->value->getIdLive();?>
</td>
    <td><?php echo $_smarty_tpl->getVariable('helixLive')->value->getName();?>
</td>
    <td><?php echo $_smarty_tpl->getVariable('servers')->value[$_smarty_tpl->getVariable('helixLive')->value->getIdServer()];?>
</td>
    <td><a href="<?php echo smarty_function_url(array('action'=>'edit','idLive'=>$_smarty_tpl->getVariable('helixLive')->value->getIdLive()),$_smarty_tpl->smarty,$_smarty_tpl);?>
"><?php echo smarty_function_icon(array('src'=>'pencil','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Edit')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
    <td><a href="<?php echo smarty_function_url(array('action'=>'delete','idLive'=>$_smarty_tpl->getVariable('helixLive')->value->getIdLive()),$_smarty_tpl->smarty,$_smarty_tpl);?>
" class="confirm"><?php echo smarty_function_icon(array('src'=>'delete','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Delete')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
</tr>
