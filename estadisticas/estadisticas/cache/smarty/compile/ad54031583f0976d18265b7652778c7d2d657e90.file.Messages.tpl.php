<?php /* Smarty version 3.0rc1, created on 2013-02-13 22:35:51
         compiled from "application/views\layout/Messages.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19055511c1547e83b07-57767677%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ad54031583f0976d18265b7652778c7d2d657e90' => 
    array (
      0 => 'application/views\\layout/Messages.tpl',
      1 => 1307635730,
    ),
  ),
  'nocache_hash' => '19055511c1547e83b07-57767677',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_smarty_tpl->getVariable('ok')->value){?><div class="ok alert_success"><p><?php echo $_smarty_tpl->getVariable('ok')->value;?>
</p></div><?php }?>
<?php if ($_smarty_tpl->getVariable('error')->value){?><div class="error alert_error"><p><?php echo $_smarty_tpl->getVariable('error')->value;?>
</p></div><?php }?>
<?php if ($_smarty_tpl->getVariable('notice')->value){?><div class="notice alert_info"><p><?php echo $_smarty_tpl->getVariable('notice')->value;?>
</p></div><?php }?>
<?php if ($_smarty_tpl->getVariable('alert')->value){?><div class="alert"><p><?php echo $_smarty_tpl->getVariable('alert')->value;?>
</p></div><?php }?>
<?php if ($_smarty_tpl->getVariable('warning')->value){?><div class="warning alert_warning"><p><?php echo $_smarty_tpl->getVariable('warning')->value;?>
</p></div><?php }?>
<?php if ($_smarty_tpl->getVariable('note')->value){?><div class="note alert_warning"><p><?php echo $_smarty_tpl->getVariable('note')->value;?>
</p></div><?php }?>