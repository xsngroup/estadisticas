<?php /* Smarty version 3.0rc1, created on 2013-02-15 19:11:52
         compiled from "application/views\forms/HelixStream.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20666511e8878096e14-79900418%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd89154192475c91fda06e2ae1cf7c324a87da08d' => 
    array (
      0 => 'application/views\\forms/HelixStream.tpl',
      1 => 1338304354,
    ),
  ),
  'nocache_hash' => '20666511e8878096e14-79900418',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_html_options')) include 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Smarty3\plugins\function.html_options.php';
?><p>
    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Id');?>
</label>
    # <?php echo $_smarty_tpl->getVariable('post')->value['id_stream'];?>
<input type="hidden" name="id_stream" id="id_stream" value="<?php echo $_smarty_tpl->getVariable('post')->value['id_stream'];?>
" />
</p>
<br/>
<p>
    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Activar protocolo:');?>
</label><br/>
    <?php  $_smarty_tpl->tpl_vars['protocol'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('protocols')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['protocol']->key => $_smarty_tpl->tpl_vars['protocol']->value){
?>
     <?php $_smarty_tpl->assign('protX',"0",null,null);?>
     <?php $_smarty_tpl->assign('pointMount',"0",null,null);?>
     <?php $_smarty_tpl->assign('prefix',"0",null,null);?>
     <?php $_smarty_tpl->assign('position',"0",null,null);?>
     <?php $_smarty_tpl->assign('port','',null,null);?> 
        
     <?php if (count($_smarty_tpl->getVariable('relHP')->value)>0){?> 
      <?php  $_smarty_tpl->tpl_vars['relation'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('relHP')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['relation']->key => $_smarty_tpl->tpl_vars['relation']->value){
?>
        <?php if ($_smarty_tpl->tpl_vars['relation']->value['id_protocol']==$_smarty_tpl->getVariable('protocol')->value->getIdProtocol()){?>
            <?php $_smarty_tpl->assign('protX',$_smarty_tpl->tpl_vars['relation']->value['id_protocol'],null,null);?>
            <?php $_smarty_tpl->assign('pointMount',$_smarty_tpl->tpl_vars['relation']->value['id_live'],null,null);?>
            <?php $_smarty_tpl->assign('prefix',$_smarty_tpl->tpl_vars['relation']->value['id_type'],null,null);?>
            <?php $_smarty_tpl->assign('position',$_smarty_tpl->tpl_vars['relation']->value['position'],null,null);?>
            <?php $_smarty_tpl->assign('port',$_smarty_tpl->tpl_vars['relation']->value['port'],null,null);?>
        <?php }?>
      <?php }} ?>     
     <?php }?> 
      <input type="checkbox" name="protocol[]" class="protocol" value="<?php echo $_smarty_tpl->getVariable('protocol')->value->getIdProtocol();?>
" <?php if ($_smarty_tpl->getVariable('protX')->value!=0){?> checked="checked" <?php }?> /><label><?php echo $_smarty_tpl->getVariable('protocol')->value->getName();?>
</label><br/>           
		<div id="loadConf-<?php echo $_smarty_tpl->getVariable('protocol')->value->getIdProtocol();?>
" class="confPro" <?php if ($_smarty_tpl->getVariable('protX')->value!=0){?>style="display: block;"<?php }?>>
		<p>
		    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Servidor:');?>
</label><br/>
		    <?php if ($_smarty_tpl->getVariable('post')->value['id_server']){?>
		       <?php echo smarty_function_html_options(array('name'=>"server-".($_smarty_tpl->getVariable('protocol')->value->getIdProtocol()),'id'=>"server-".($_smarty_tpl->getVariable('protocol')->value->getIdProtocol()),'class'=>'selectLive','options'=>$_smarty_tpl->getVariable('servers')->value,'selected'=>$_smarty_tpl->getVariable('post')->value['id_server']),$_smarty_tpl->smarty,$_smarty_tpl);?>

		    <?php }else{ ?>
			    <select name="server-<?php echo $_smarty_tpl->getVariable('protocol')->value->getIdProtocol();?>
" id="server-<?php echo $_smarty_tpl->getVariable('protocol')->value->getIdProtocol();?>
" class="selectLive">
			      <option value="0">Selecciona una opci&oacute;n...</option>
			      <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('streamServers')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
?>
			        <option value="<?php echo $_smarty_tpl->getVariable('item')->value->getIdServer();?>
"><?php echo $_smarty_tpl->getVariable('item')->value->getUrl();?>
</option>
			      <?php }} ?>
			    </select>    
		    <?php }?>
		</p>
		<br/>
		<p>
		   <input type="checkbox" style="margin:0; margin-right:5px;" id="ckbx-<?php echo $_smarty_tpl->getVariable('protocol')->value->getIdProtocol();?>
" class="ckbxPort" /><label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Puerto:');?>
</label><br/>
		    <input type="text" name="port-<?php echo $_smarty_tpl->getVariable('protocol')->value->getIdProtocol();?>
" id="port-<?php echo $_smarty_tpl->getVariable('protocol')->value->getIdProtocol();?>
" value="<?php echo $_smarty_tpl->getVariable('port')->value;?>
" style="width: 50px;" disabled="disabled" />
		</p>
		<br/>
		<p>
		    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Punto de montaje:');?>
</label><br/>
		    <?php if ($_smarty_tpl->getVariable('pointMount')->value!=0){?>
		         <?php echo smarty_function_html_options(array('name'=>"id_live-".($_smarty_tpl->getVariable('protocol')->value->getIdProtocol()),'id'=>"id_live-".($_smarty_tpl->getVariable('protocol')->value->getIdProtocol()),'class'=>'selectPoint','options'=>$_smarty_tpl->getVariable('lives')->value,'selected'=>$_smarty_tpl->getVariable('pointMount')->value),$_smarty_tpl->smarty,$_smarty_tpl);?>
 
		    <?php }else{ ?>
		         <?php echo smarty_function_html_options(array('name'=>"id_live-".($_smarty_tpl->getVariable('protocol')->value->getIdProtocol()),'id'=>"id_live-".($_smarty_tpl->getVariable('protocol')->value->getIdProtocol()),'class'=>'selectPoint','options'=>$_smarty_tpl->getVariable('helixLive')->value),$_smarty_tpl->smarty,$_smarty_tpl);?>

		    <?php }?>
		    
		</p>
		<!--  
		<br/>
			
			<div id="showPrefix-<?php echo $_smarty_tpl->getVariable('protocol')->value->getIdProtocol();?>
" <?php if ($_smarty_tpl->getVariable('position')->value==0){?>style="display: none;"<?php }?>>	
			<p>
			    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Posici�n ante el punto de montaje:');?>
</label><br/>
			    <input type="radio" name="position-<?php echo $_smarty_tpl->getVariable('protocol')->value->getIdProtocol();?>
" value="1" <?php if ($_smarty_tpl->getVariable('position')->value==1){?>checked="checked"<?php }?>/><label>Antes</label>
			    <input type="radio" name="position-<?php echo $_smarty_tpl->getVariable('protocol')->value->getIdProtocol();?>
" value="2" <?php if ($_smarty_tpl->getVariable('position')->value==2){?>checked="checked"<?php }?>/><label>Desp&uacute;es</label>    
			</p>
			
			<br/>
			</div>
			-->
		<br/>
		</div>      
    <?php }} ?>
</p>
<br/>


<!-- aqui ya se a�aden los datos genericos de la se�al -->
<br/>
<p>
    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Se�al:');?>
</label><br/>
    <input type="text" name="name" id="name" value="<?php echo $_smarty_tpl->getVariable('post')->value['name'];?>
" class=" required" style="width: 270px;" />
</p>
<br/>
<p>
    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Titulo:');?>
</label><br/>
    <input type="text" name="title" id="title" value="<?php echo $_smarty_tpl->getVariable('post')->value['title'];?>
" class="" style="width: 270px;" />
</p>
<br/>
<p>
    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Descripci�n:');?>
</label><br/>
    <textarea name="description" id="description" class="" style="width: 270px;"><?php echo $_smarty_tpl->getVariable('post')->value['description'];?>
</textarea>
</p>

<!--
$idStream = $this->getRequest()->getParam('id_stream');
$name = $this->getRequest()->getParam('name');
$title = $this->getRequest()->getParam('title');
$description = $this->getRequest()->getParam('description');
$idLive = $this->getRequest()->getParam('id_live');
-->

<!--
$post = array(
    'id_stream' => $helixStream->getIdStream(),
    'name' => $helixStream->getName(),
    'title' => $helixStream->getTitle(),
    'description' => $helixStream->getDescription(),
    'id_live' => $helixStream->getIdLive(),
);
-->
