<?php /* Smarty version 3.0rc1, created on 2013-02-04 15:54:19
         compiled from "application/views/report/GeolocationHelix.tpl" */ ?>
<?php /*%%SmartyHeaderCode:93792383951102e0bb67e52-32756586%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3bcc265efed98e8343326fb4902af46ac53544ab' => 
    array (
      0 => 'application/views/report/GeolocationHelix.tpl',
      1 => 1360014855,
    ),
  ),
  'nocache_hash' => '93792383951102e0bb67e52-32756586',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_html_options')) include '/var/www/html/library/Smarty3/plugins/function.html_options.php';
?><script type="text/javascript">
<!--
$(document).ready(function(){
	   $('.typeVisit').click(function(){
		      if($(this).val()==1){
		    	  $("#loadVideo").hide();
		    	  $("#loadSignal").show();
		      }
		      else{
		    	  $("#loadVideo").show();
		    	  $("#loadSignal").hide();
		      }			      
		});	
});	
//-->
function validateHour(){
        var iHour=(document.getElementById('initHour').value).split(":");
        var eHour=(document.getElementById('finishHour').value).split(":");

        var iH=parseInt(iHour[0],10);
        var iM=parseInt(iHour[1],10);

        var eH=parseInt(eHour[0],10);
        var eM=parseInt(eHour[1],10);

        if((iH>eH)||(iH==eH && iM>eM)){
                return 'false';
        }else{
	return 'true';
        }
}

function validate(){
if(document.getElementById('date_start').value!=""){
var start=new Date(Date.parse(document.getElementById('date_start').value.replace(/-/g," ")));
var end=new Date(Date.parse(document.getElementById('date_end').value.replace(/-/g," ")));
var val=validateHour();
        if((start.getTime()-end.getTime())>0){
        alert("La fecha final no puede ser menor a la inicial");
        }else if((start.getTime()-end.getTime())==0){
         
        document.forms["estForm"].submit();
        }else{

                if(val=='true'){
                document.forms["estForm"].submit();
                }else{
                alert("La hora inicial no puede ser mayor a la final.");
                }
        }
	}else{
              	alert("Es necesario especificar una fecha inicial.");
        }
}

</script>
<div class="column_left">
 <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Helix hist&oacute;rico geolocalizador');?>
</span></div>
 <br class="clear" />
 <div class="content">
<form action="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/report/map-helix" class="validate" method="post" id="estForm">
<label>Filtro de Fechas:</label><br/>
<p>   

     <label>*Desde:</label>  
     <input type="text" name="date_start" id="date_start" value="<?php echo $_smarty_tpl->getVariable('post')->value['date_start'];?>
<?php echo $_smarty_tpl->getVariable('dateStart')->value;?>
" class="field text medium datePicker dateISO required picker" />                    
</p> 
<br/>
<p>
     <label style="padding-right:3px;">*Hasta: </label> 
     <input type="text" name="date_end" id="date_end" value="<?php echo $_smarty_tpl->getVariable('post')->value['date_end'];?>
<?php echo $_smarty_tpl->getVariable('dateEnd')->value;?>
" class="field text medium datePicker dateISO picker" />                   
</p>
<br/>
<br/>
<p>
<label>Filtro de horarios:</label><br/>
<label>De: </label><input type="text" id="initHour" name="initHour" size="10" class="field text small" value="09:00" /><label style="padding-left:15px;">a: </label><input type="text" size="10" class="field text small" id="finishHour" name="finishHour" value="14:00" />
</p>
<?php if ($_smarty_tpl->getVariable('flagStream')->value==true&&$_smarty_tpl->getVariable('flagVod')->value==true){?>
<br/>
<br/>
<p>
<label>Filtro de visitas:</label><br/>
<input type="radio" name="typeVisit" class="typeVisit" value="1" checked="checked" /><label>Se�al</label>
<input type="radio" name="typeVisit" class="typeVisit" value="2" /><label>Videos</label>
</p>
<?php }?>
<?php if ($_smarty_tpl->getVariable('flagStream')->value==true){?>
<div id="loadSignal">
<br/>
<p>
<label>Se�ales:</label><br/>
<?php echo smarty_function_html_options(array('name'=>'stream','id'=>'stream','options'=>$_smarty_tpl->getVariable('streams')->value),$_smarty_tpl->smarty,$_smarty_tpl);?>

<input type="hidden" name="flagStream" value="1"/>
</p>
</div>
<?php }?>
<?php if ($_smarty_tpl->getVariable('flagVod')->value==true){?>
<div id="loadVideo" <?php if ($_smarty_tpl->getVariable('flagStream')->value==true){?>style="display: none;"<?php }?>>
<br/>
<p>
<label>Videos:</label><br/>
<?php echo smarty_function_html_options(array('name'=>'vod','id'=>'vod','options'=>$_smarty_tpl->getVariable('vods')->value),$_smarty_tpl->smarty,$_smarty_tpl);?>

<input type="hidden" name="flagVideo" value="1"/>
</p>
</div>
<?php }?>
<br/> 
<br/> 
<p> 
<input type="hidden" name="idCompany" value="<?php echo $_smarty_tpl->getVariable('idCompany')->value;?>
" />
<input type="hidden" name="idServer" value="<?php echo $_smarty_tpl->getVariable('idServer')->value;?>
" />
<input type="button" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Generar');?>
" onclick="validate()"/>
</p>
</form>
</div>
</div>
