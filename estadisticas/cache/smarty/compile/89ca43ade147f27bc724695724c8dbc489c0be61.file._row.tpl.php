<?php /* Smarty version 3.0rc1, created on 2013-02-15 19:17:25
         compiled from "application/views\helix-stream/_row.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3481511e89c59004f8-25859694%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '89ca43ade147f27bc724695724c8dbc489c0be61' => 
    array (
      0 => 'application/views\\helix-stream/_row.tpl',
      1 => 1311180194,
    ),
  ),
  'nocache_hash' => '3481511e89c59004f8-25859694',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_url')) include 'lib/smarty/plugins\function.url.php';
if (!is_callable('smarty_function_icon')) include 'lib/smarty/plugins\function.icon.php';
?><tr>
    <td><?php echo $_smarty_tpl->getVariable('helixStream')->value->getIdStream();?>
</td>
    <td><?php echo $_smarty_tpl->getVariable('helixStream')->value->getName();?>
</td>
    <td><?php echo $_smarty_tpl->getVariable('helixStream')->value->getTitle();?>
</td>
    <td><?php echo $_smarty_tpl->getVariable('helixStream')->value->getDescription();?>
</td>
    <td><?php echo $_smarty_tpl->getVariable('servers')->value[$_smarty_tpl->getVariable('helixStream')->value->getIdServer()];?>
</td>
    <td><a href="<?php echo smarty_function_url(array('action'=>'edit','idStream'=>$_smarty_tpl->getVariable('helixStream')->value->getIdStream()),$_smarty_tpl->smarty,$_smarty_tpl);?>
"><?php echo smarty_function_icon(array('src'=>'pencil','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Edit')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
    <td><a href="<?php echo smarty_function_url(array('action'=>'delete','idStream'=>$_smarty_tpl->getVariable('helixStream')->value->getIdStream()),$_smarty_tpl->smarty,$_smarty_tpl);?>
" class="confirm"><?php echo smarty_function_icon(array('src'=>'delete','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Delete')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
</tr>
