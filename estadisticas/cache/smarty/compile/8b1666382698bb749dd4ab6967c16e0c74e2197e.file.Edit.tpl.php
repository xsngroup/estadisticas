<?php /* Smarty version 3.0rc1, created on 2013-01-21 14:11:15
         compiled from "application/views/access-role/Edit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:82408939750fda0e32f3568-18027691%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8b1666382698bb749dd4ab6967c16e0c74e2197e' => 
    array (
      0 => 'application/views/access-role/Edit.tpl',
      1 => 1307657328,
    ),
  ),
  'nocache_hash' => '82408939750fda0e32f3568-18027691',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="column_left">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Perfiles de usuario');?>
</span></div>
    <br class="clear" />
    <div class="content">
<form action="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/access-role/update" method="post" class="validate">
<h4><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Editar Grupo de Usuario');?>
</h4>
<p> 
<label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Nombre');?>
</label><br/>
<input type="text" id="name" name="name" class="required" value="<?php echo $_smarty_tpl->getVariable('accessRole')->value->getName();?>
" size="40"/>
<input type="hidden" name="id" value="<?php echo $_smarty_tpl->getVariable('accessRole')->value->getIdAccessRole();?>
">
</p>
<br/>
<p> 
<input type="submit" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Guardar');?>
" />
<input type="button" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Regresar');?>
" class="back" />
</p>
</form>
	</div>
</div>