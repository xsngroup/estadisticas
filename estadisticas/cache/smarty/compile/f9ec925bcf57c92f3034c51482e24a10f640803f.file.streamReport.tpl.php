<?php /* Smarty version 3.0rc1, created on 2013-03-25 17:35:42
         compiled from "application/views\report/streamReport.tpl" */ ?>
<?php /*%%SmartyHeaderCode:76035150d13e6a3632-68817852%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f9ec925bcf57c92f3034c51482e24a10f640803f' => 
    array (
      0 => 'application/views\\report/streamReport.tpl',
      1 => 1364250939,
    ),
  ),
  'nocache_hash' => '76035150d13e6a3632-68817852',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_html_options')) include 'F:\Programas\Apache\Apache Server\Apache2\htdocs\library\Smarty3\plugins\function.html_options.php';
?><script type="text/javascript">
$(document).ready(function(){
	$('#groupBy').change(function(){	
        //alert($(this).val());	
        if($(this).val()==1)
        {
        	$("#firstDate").show();
            $("#secondDate").disable();
            $("#meses").hide();
        }

        if($(this).val()==2)
        {
        	$("#firstDate").show();
       	    $("#secondDate").show();
            $("#meses").hide();
        }

    //    if($(this).val()==3)
     //   {
      //  	$("#firstDate").hide();
       //	    $("#secondDate").hide();
        //	$("#meses").show();
       // }                
	});	
});	

</script>
<script>
function validateHour(){
	var iHour=(document.getElementById('initHour').value).split(":");
	var eHour=(document.getElementById('finishHour').value).split(":");
	
	var iH=parseInt(iHour[0],10);
	var iM=parseInt(iHour[1],10);
	
	var eH=parseInt(eHour[0],10);
	var eM=parseInt(eHour[1],10);
	
	if((iH>eH)||(iH==eH && iM>eM)){
		return 'false';
	}else{
	return 'true';
	}
}
function validate(){
if(document.getElementById('date_start').value!=""){
var start=new Date(Date.parse(document.getElementById('date_start').value.replace(/-/g," ")));
var end=new Date(Date.parse(document.getElementById('date_end').value.replace(/-/g," ")));
var val=validateHour();
	if((start.getTime()-end.getTime())>0){
	alert("La fecha final no puede ser menor a la inicial");
	}else if((start.getTime()-end.getTime())==0){
	 document.forms["estForm"]["groupBy"].value="1";
	 document.forms["estForm"].submit(); 
	}else{

		if(val=='true'){
		document.forms["estForm"].submit(); 
		}else{
		alert("La hora inicial no puede ser mayor a la final.");
		}
	}
	}else{
		alert("Es necesario especificar una fecha inicial.");
	}
}
function load()
{
document.all("groupBy").selectedIndex=0;
}

</script>


<div class="column_left">
 <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Helix Hist&oacute;rico Vod Streaming');?>
</span></div>
 <br class="clear" />
 <div class="content">
<form action="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/report/process-stream-helix" class="validate" method="post"  id="estForm">
<label>Filtro de Fechas:</label><br/><br/>
<p>
     <label>Agrupar por:</label>  
     <select id="groupBy" name="groupBy" onload="load()">
        <option  value="1" selected>Horas</option>
        <option value="2" >D�as</option>
       <!-- <option value="3">Mes</option> --> 
     </select>
</p>    
<br/>   
<p id="firstDate">   

     <label>*Desde:</label>  
     <input type="text" name="date_start" id="date_start" value="<?php echo $_smarty_tpl->getVariable('post')->value['date_start'];?>
<?php echo $_smarty_tpl->getVariable('dateStart')->value;?>
" class="field text medium datePicker dateISO required picker" />                    
<br/>
</p> 

<p id="secondDate" style="display: none;">
<br/>
     <label style="padding-right:3px;">*Hasta: </label> 
     <input type="text" name="date_end" id="date_end" value="<?php echo $_smarty_tpl->getVariable('post')->value['date_end'];?>
<?php echo $_smarty_tpl->getVariable('dateEnd')->value;?>
" class="field text medium datePicker dateISO picker" />                   
<br/>
</p>
<!-- 
<p id="meses" style="display: none;">
     <label>Mes:</label>  
     <select id="mesesFil">
        <option value="1">Enero</option>
        <option value="2">Febrero</option>
        <option value="3">Marzo</option>
        <option value="4">Abril</option>
        <option value="5">Mayo</option>
        <option value="6">Junio</option>
        <option value="7">Julio</option>
        <option value="8">Agosto</option>
        <option value="9">Septiembre</option>
        <option value="10">Octubre</option>
        <option value="11">Noviembre</option>
        <option value="12">Diciembre</option>
     </select>
     <br/>
</p> 
  -->
<br/>
<br/>
<p>
<label>Filtro de horarios:</label><br/>
<label>De: </label><input type="text" id="initHour" name="initHour" size="10" class="field text small" value="09:00" /><label style="padding-left:15px;">a: </label><input type="text" size="10" class="field text small"  id="finishHour" name="finishHour" value="14:00" />

</p>
<br/>
<p>
<label>Se�ales:</label><br/>
<?php echo smarty_function_html_options(array('name'=>'stream','id'=>'stream','options'=>$_smarty_tpl->getVariable('streams')->value),$_smarty_tpl->smarty,$_smarty_tpl);?>

</p>
<br/> 
<br/> 
<p> 
<input type="hidden" name="idCompany" value="<?php echo $_smarty_tpl->getVariable('idCompany')->value;?>
" />
<input type="hidden" name="idServer" value="<?php echo $_smarty_tpl->getVariable('idServer')->value;?>
" />
<input type="button" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Generar');?>
" onclick="validate()" />
</p>
</form>
</div>
</div>