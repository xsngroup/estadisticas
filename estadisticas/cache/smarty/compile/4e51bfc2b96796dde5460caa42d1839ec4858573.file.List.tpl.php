<?php /* Smarty version 3.0rc1, created on 2013-01-21 12:05:28
         compiled from "application/views/helix-stream/List.tpl" */ ?>
<?php /*%%SmartyHeaderCode:174578276650fd8368719595-86306915%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4e51bfc2b96796dde5460caa42d1839ec4858573' => 
    array (
      0 => 'application/views/helix-stream/List.tpl',
      1 => 1311201794,
    ),
  ),
  'nocache_hash' => '174578276650fd8368719595-86306915',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_url')) include 'lib/smarty/plugins/function.url.php';
if (!is_callable('smarty_modifier_odd')) include 'lib/smarty/plugins/modifier.odd.php';
if (!is_callable('smarty_function_icon')) include 'lib/smarty/plugins/function.icon.php';
?><script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/js/modules/helix-stream/process.js"></script>
<div class="onecolumn">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Helix Server Live Stream');?>
</span></div>
    <br class="clear" />
    <div class="content">
<form action="<?php echo smarty_function_url(array('action'=>'create'),$_smarty_tpl->smarty,$_smarty_tpl);?>
" method="post" class="validate ajaxForm">
<h4><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Guardar nueva se�al en vivo');?>
</h4>
<?php $_template = new Smarty_Internal_Template('forms/HelixStream.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

<br/>
<p> 
<input type="submit" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Guardar');?>
" />
</p>
</form>
	</div>
</div>

<div class="onecolumn">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Listado de Live Stream');?>
</span></div>
    <br class="clear" />
    <div class="content">
<table width="100%" cellspacing="0" cellpadding="0" class="data">
    <thead>
        <tr>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Id');?>
</th>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Nombre');?>
</th>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('T&iacute;tulo');?>
</th>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Descripci&oacute;n');?>
</th>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Servidor');?>
</th>
            <th colspan="2"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Actions');?>
</th>
        </tr>
    </thead>
    <tbody id="ajaxList">
        <?php  $_smarty_tpl->tpl_vars['helixStream'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('helixStreams')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['helixStream']->iteration=0;
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['helixStream']->key => $_smarty_tpl->tpl_vars['helixStream']->value){
 $_smarty_tpl->tpl_vars['helixStream']->iteration++;
?>
            <tr class="<?php echo smarty_modifier_odd($_smarty_tpl->tpl_vars['helixStream']->iteration);?>
">
                <td><?php echo $_smarty_tpl->getVariable('helixStream')->value->getIdStream();?>
</td>
                <td><?php echo $_smarty_tpl->getVariable('helixStream')->value->getName();?>
</td>
                <td><?php echo $_smarty_tpl->getVariable('helixStream')->value->getTitle();?>
</td>
                <td><?php echo $_smarty_tpl->getVariable('helixStream')->value->getDescription();?>
</td>
                <td><?php echo $_smarty_tpl->getVariable('servers')->value[$_smarty_tpl->getVariable('helixStream')->value->getIdServer()];?>
</td>
                <td><a href="<?php echo smarty_function_url(array('action'=>'edit','idStream'=>$_smarty_tpl->getVariable('helixStream')->value->getIdStream()),$_smarty_tpl->smarty,$_smarty_tpl);?>
"><?php echo smarty_function_icon(array('src'=>'pencil','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Edit')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
                <td><a href="<?php echo smarty_function_url(array('action'=>'delete','idStream'=>$_smarty_tpl->getVariable('helixStream')->value->getIdStream()),$_smarty_tpl->smarty,$_smarty_tpl);?>
" class="confirm"><?php echo smarty_function_icon(array('src'=>'delete','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Delete')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
            </tr>
        <?php }} else { ?>
           <tr><td colspan="11" align="center">No existen elementos para mostrar...</td></tr>    
        <?php } ?>
    </tbody>
</table>
	</div>
</div>

                 