<?php /* Smarty version 3.0rc1, created on 2013-01-21 17:06:05
         compiled from "application/views/type-server/_row.tpl" */ ?>
<?php /*%%SmartyHeaderCode:110259169350fdc9dd329828-17106131%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1e33afc069036e6d46be8a2b370f9833e50d575f' => 
    array (
      0 => 'application/views/type-server/_row.tpl',
      1 => 1307657328,
    ),
  ),
  'nocache_hash' => '110259169350fdc9dd329828-17106131',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_url')) include 'lib/smarty/plugins/function.url.php';
if (!is_callable('smarty_function_icon')) include 'lib/smarty/plugins/function.icon.php';
?><tr>
    <td><?php echo $_smarty_tpl->getVariable('typeServer')->value->getIdType();?>
</td>
    <td><?php echo $_smarty_tpl->getVariable('typeServer')->value->getName();?>
</td>
    <td><?php echo $_smarty_tpl->getVariable('typeServer')->value->getStatus();?>
</td>
    <td><a href="<?php echo smarty_function_url(array('action'=>'edit','idServer'=>$_smarty_tpl->getVariable('typeServer')->value->getIdServer()),$_smarty_tpl->smarty,$_smarty_tpl);?>
"><?php echo smarty_function_icon(array('src'=>'pencil','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Edit')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
    <td><a href="<?php echo smarty_function_url(array('action'=>'delete','idServer'=>$_smarty_tpl->getVariable('typeServer')->value->getIdServer()),$_smarty_tpl->smarty,$_smarty_tpl);?>
" class="confirm"><?php echo smarty_function_icon(array('src'=>'delete','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Delete')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
</tr>
