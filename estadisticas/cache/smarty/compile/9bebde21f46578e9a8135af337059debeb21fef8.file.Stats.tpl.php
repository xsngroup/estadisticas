<?php /* Smarty version 3.0rc1, created on 2013-01-21 17:27:37
         compiled from "application/views/flash-instance/Stats.tpl" */ ?>
<?php /*%%SmartyHeaderCode:36609001550fdcee9a92cd9-07659152%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9bebde21f46578e9a8135af337059debeb21fef8' => 
    array (
      0 => 'application/views/flash-instance/Stats.tpl',
      1 => 1307657328,
    ),
  ),
  'nocache_hash' => '36609001550fdcee9a92cd9-07659152',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_icon')) include 'lib/smarty/plugins/function.icon.php';
?>
<div class="column_left">
 <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Informaci�n del servidor');?>
</span></div>
 <br class="clear" />
 <div class="content">
<form action="#" class="validate" method="post">
<table width="100%" cellspacing="0" cellpadding="0" class="data"> 
  <tbody> 
     <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Cliente:');?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('company')->value->getNameCompany();?>
</td>       
     </tr>
     <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Contacto:');?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('company')->value->getNameContac();?>
</td>       
     </tr>
     <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Tel�fono:');?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('company')->value->getTelephone();?>
</td>       
     </tr>
     <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Ip del Servidor:');?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('server')->value->getUrl();?>
</td>       
     </tr>                 
  </tbody>
</table>

</form>
</div>
</div>

<div class="column_right">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Estad�stica de LiveStream');?>
</span></div>
    <br class="clear" />
    <div class="content">
<form action="#" class="validate" method="post">
    <div id="loadStats">
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/js/modules/flash-instance/script.js"></script>    
  <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('result')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
?> 
<table width="100%" cellspacing="0" cellpadding="0" class="data"> 
  <tbody>
     <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Aplicaci�n:');?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('instances')->value[$_smarty_tpl->getVariable('item')->value->getIdInstance()];?>
</td>       
     </tr>  
     <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Fecha y hora:');?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('item')->value->getTimestamp();?>
</td>       
     </tr>
     <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Conectados concurrentes:');?>
<?php echo smarty_function_icon(array('src'=>'information','class'=>'tip','style'=>"padding-left:5px;",'title'=>"Usuarios conectados actualmente"),$_smarty_tpl->smarty,$_smarty_tpl);?>
</th>
                <td class="last">
                <?php echo $_smarty_tpl->getVariable('totalCurrent')->value;?>

                </td>                                       
     </tr>     
     <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Bytes de entrada:');?>
<?php echo smarty_function_icon(array('src'=>'information','class'=>'tip','style'=>"padding-left:5px;",'title'=>"Total de bytes recibidos por el servidor"),$_smarty_tpl->smarty,$_smarty_tpl);?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('item')->value->getBytesIn();?>
</td>       
     </tr>
     <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Bytes de salida:');?>
<?php echo smarty_function_icon(array('src'=>'information','class'=>'tip','style'=>"padding-left:5px;",'title'=>"Total de bytes enviados por el servidor"),$_smarty_tpl->smarty,$_smarty_tpl);?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('item')->value->getBytesOut();?>
</td>       
     </tr>     
     <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Conexiones aceptadas:');?>
<?php echo smarty_function_icon(array('src'=>'information','class'=>'tip','style'=>"padding-left:5px;",'title'=>"Total de conexiones (viewers) aceptadas por el servidor"),$_smarty_tpl->smarty,$_smarty_tpl);?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('item')->value->getAccepted();?>
</td>       
     </tr>
     <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Conexiones Rechazadas:');?>
<?php echo smarty_function_icon(array('src'=>'information','class'=>'tip','style'=>"padding-left:5px;",'title'=>"Total de conexiones (viewers) rechazadas por el servidor"),$_smarty_tpl->smarty,$_smarty_tpl);?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('item')->value->getRejected();?>
</td>       
     </tr>
     <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Inicio de ejecuci�n:');?>
<?php echo smarty_function_icon(array('src'=>'information','class'=>'tip','style'=>"padding-left:5px;",'title'=>"Fecha y hora en que se ejecut� el punto de montaje"),$_smarty_tpl->smarty,$_smarty_tpl);?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('item')->value->getLaunchTime();?>
</td>       
     </tr>
     <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Total conectados:');?>
<?php echo smarty_function_icon(array('src'=>'information','class'=>'tip','style'=>"padding-left:5px;",'title'=>"Total de conectados (viewers) al servidor desde la ejecuci�n del punto de montaje"),$_smarty_tpl->smarty,$_smarty_tpl);?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('item')->value->getTotalConnects();?>
</td>       
     </tr>
     <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Total desconectados:');?>
<?php echo smarty_function_icon(array('src'=>'information','class'=>'tip','style'=>"padding-left:5px;",'title'=>"Total de desconectados (viewers) al servidor desde la ejecuci�n del punto de montaje"),$_smarty_tpl->smarty,$_smarty_tpl);?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('item')->value->getTotalDisconnects();?>
</td>       
     </tr>                                                                        
  </tbody>
</table>
<br/>
   <?php }} else { ?>
                <span>No existe informaci�n por el momento...</span>         
   <?php } ?> 
</div>   
</form>

</div>
</div>    