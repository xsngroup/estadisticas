<?php /* Smarty version 3.0rc1, created on 2013-02-15 19:12:18
         compiled from "application/views\type-server/List.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15211511e88929b3385-12640959%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '085dd229e3af9654ac2bd013868e0d1d3b112dfc' => 
    array (
      0 => 'application/views\\type-server/List.tpl',
      1 => 1307635728,
    ),
  ),
  'nocache_hash' => '15211511e88929b3385-12640959',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_url')) include 'lib/smarty/plugins\function.url.php';
if (!is_callable('smarty_modifier_odd')) include 'lib/smarty/plugins\modifier.odd.php';
if (!is_callable('smarty_function_icon')) include 'lib/smarty/plugins\function.icon.php';
?><div class="column_left">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Tipo de servidores');?>
</span></div>
    <br class="clear" />
    <div class="content">
<form action="<?php echo smarty_function_url(array('action'=>'create'),$_smarty_tpl->smarty,$_smarty_tpl);?>
" method="post" class="validate ajaxForm">
<h4><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Guardar nuevo tipo de servidor');?>
</h4>
<?php $_template = new Smarty_Internal_Template('forms/type-server.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

<br/>
<p> 
<input type="submit" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Guardar');?>
" />
</p>
</form>
	</div>
</div>

<div class="column_right">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Listado de tipos');?>
</span></div>
    <br class="clear" />
    <div class="content">
<table width="100%" cellspacing="0" cellpadding="0" class="data">
    <thead>
        <tr>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Id');?>
</th>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Nombre');?>
</th>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Status');?>
</th>
            <th colspan="2"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Actions');?>
</th>
        </tr>
    </thead>
    <tbody id="ajaxList">
        <?php  $_smarty_tpl->tpl_vars['typeServer'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('typeServers')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['typeServer']->iteration=0;
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['typeServer']->key => $_smarty_tpl->tpl_vars['typeServer']->value){
 $_smarty_tpl->tpl_vars['typeServer']->iteration++;
?>
            <tr class="<?php echo smarty_modifier_odd($_smarty_tpl->tpl_vars['typeServer']->iteration);?>
">
                <td><?php echo $_smarty_tpl->getVariable('typeServer')->value->getIdType();?>
</td>
                <td><?php echo $_smarty_tpl->getVariable('typeServer')->value->getName();?>
</td>
                <td><?php echo $_smarty_tpl->getVariable('status')->value[$_smarty_tpl->getVariable('typeServer')->value->getStatus()];?>
</td>
                <td><a href="<?php echo smarty_function_url(array('action'=>'edit','idServer'=>$_smarty_tpl->getVariable('typeServer')->value->getIdType()),$_smarty_tpl->smarty,$_smarty_tpl);?>
"><?php echo smarty_function_icon(array('src'=>'pencil','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Edit')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
                <td><a href="<?php echo smarty_function_url(array('action'=>'delete','idServer'=>$_smarty_tpl->getVariable('typeServer')->value->getIdType()),$_smarty_tpl->smarty,$_smarty_tpl);?>
" class="confirm"><?php echo smarty_function_icon(array('src'=>'delete','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Delete')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
            </tr>
        <?php }} ?>
    </tbody>
</table>
	</div>
</div>

