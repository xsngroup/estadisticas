<?php /* Smarty version 3.0rc1, created on 2013-06-11 12:53:07
         compiled from "application/views\report/StreamHelixProcess.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2203451b764038cd240-84763173%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f93674cdda24d3483554b151a0bfa13379426e27' => 
    array (
      0 => 'application/views\\report/StreamHelixProcess.tpl',
      1 => 1370973174,
    ),
  ),
  'nocache_hash' => '2203451b764038cd240-84763173',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/js/plugins/dygraph-combined.js"></script>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/css/tab.css" type="text/css" />
<script type="text/javascript">
$(document).ready( function()
{
	var dateStart="<?php echo $_smarty_tpl->getVariable('dateStart')->value;?>
";
	var dateEnd="<?php echo $_smarty_tpl->getVariable('dateEnd')->value;?>
";
	var initHour="<?php echo $_smarty_tpl->getVariable('initHour')->value;?>
";
	var finishHour="<?php echo $_smarty_tpl->getVariable('finishHour')->value;?>
";
	var idStream=<?php echo $_smarty_tpl->getVariable('stream')->value->getIdStream();?>
;
	var groupBy =<?php echo $_smarty_tpl->getVariable('groupBy')->value;?>
;
		
	$('#download').click(function(){	
		//var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, width=508, height=365, top=85, left=140";
		//window.open(baseUrl + "/report/download","",opciones);	 

		location.href=baseUrl + "/report/download-helix-stat/dateStart/"+dateStart+"/dateEnd/"+dateEnd+"/initHour/"+initHour+"/finishHour/"+finishHour+"/idStream/"+idStream+"/groupBy/"+groupBy;
		
	});


	$(".tab_content").hide();
	$("ul.tabs li:first").addClass("active").show();
	$(".tab_content:first").show();

	$("ul.tabs li").click(function()
       {
		$("ul.tabs li").removeClass("active");
		$(this).addClass("active");
		$(".tab_content").hide();

		var activeTab = $(this).find("a").attr("href");
		$(activeTab).fadeIn();
		return false;
	});	
});

</script>
<div class="onecolumn">
 <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Helix hist&oacute;rico live streaming');?>
</span></div>
 <br class="clear" />
 <div class="content">
<form action="#" class="validate" method="post">
<p>
<label>Del: </label><?php echo $_smarty_tpl->getVariable('date_initial')->value;?>

<label> al: </label><?php echo $_smarty_tpl->getVariable('date_final')->value;?>

</p>
<br/><br/>
<p>
  <label>Streaming: </label><?php echo $_smarty_tpl->getVariable('stream')->value->getName();?>

</p>
<br/>
<div id="tabDiv">
<ul class="tabs">
    <li><a href="#tab1">Gr&aacute;fica de datos</a></li>
    <li><a href="#tab2">Info. plataforma</a></li>
</ul>

<div class="tab_container">
    <div id="tab1" class="tab_content">
        <!--Content-->
		<label>Tiempo promedio de visualización:</label><span> <?php echo $_smarty_tpl->getVariable('promedioo')->value;?>
 Seg.</span>
		<br/>
		<br/>
		    <p><b>Series visibles:</b></p>
		
		    <p>
		      <input type=checkbox id="0" checked onClick="change(this)">
		      <label for="0">Concurrentes</label><br/>
		      <input type=checkbox id="1" checked onClick="change(this)">
		      <label for="1">Usuarios nuevos</label><br/>
		      <input type=checkbox id="2" checked onClick="change(this)">
		      <label for="2">Peticiones</label><br/>      
		    </p>
		<br/>
		<br/>
		<?php if ($_smarty_tpl->getVariable('flagData')->value==true){?>
		<div id="graphdiv" style="600px; height: 300px;"><img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/basic/bar-loader.gif" /></div>
		<br/>
		<div id="labelGraph" style="margin-left:35px;"></div>
		<div id="loadInfo"></div>
		<script type="text/javascript">
		
		
		  g = new Dygraph(
		
		    // containing div
		    document.getElementById("graphdiv"),
		
		    // CSV or path to a CSV file.
		    "<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/report/process-table-helix/dateStart/<?php echo $_smarty_tpl->getVariable('dateStart')->value;?>
/dateEnd/<?php echo $_smarty_tpl->getVariable('dateEnd')->value;?>
/initHour/<?php echo $_smarty_tpl->getVariable('initHour')->value;?>
/finishHour/<?php echo $_smarty_tpl->getVariable('finishHour')->value;?>
/idStream/<?php echo $_smarty_tpl->getVariable('stream')->value->getIdStream();?>
/groupBy/<?php echo $_smarty_tpl->getVariable('groupBy')->value;?>
",
			{
		        fillGraph: true,
		    	visibility: [true, true, true, true] ,
		    	<?php if ($_smarty_tpl->getVariable('groupBy')->value==1){?>
		        labels: [ "Date",  "Concurrentes","Usuarios nuevos", "Hits" ],
		        <?php }else{ ?>
		    	labels: [ "Date",  "Concurrentes" ],
		    	<?php }?>
		        labelsDiv: document.getElementById("labelGraph"),  
		        labelsSeparateLines: true     
		        //labelsDivWidth: 300
		   
		    }
		
		
		  );
		
		  function change(el) {
		      g.setVisibility(parseInt(el.id), el.checked);
		    }
		  
		</script>
		<?php }else{ ?>
		<center><h4>No se encontraron resultados para mostrar...</h4></center> 
		<?php }?>	
			<br/>        
    </div>
    <div id="tab2" class="tab_content">
    <style>
	dl {
	  margin:0; 
	  padding:0 0 2px 0; 
	  width:401px; 
	  height:auto; 
	  background:#fff;
	  }
	
	dt {
	  text-align:center; 
	  font-size:1.5em; 
	  border-bottom:3px solid #fff;
	  }
	
	dd {
	  margin:0; 
	  display:block; 
	  width:400px; 
	  height:2em; 
	  background:#3D444C; 
	  border-bottom:1px solid #fff;
	  }
	
	dd b {
	  float:right;
	  display:block; 
	  margin-left:auto; 
	  background:#EBEBEB; 
	  height:2em; 
	  line-height:2em; 
	  text-align:right;
	  }
	  
	.labelProtocol{
		float: right;
		position: absolute;
		text-align: right;
		width: 400px;
		z-index: 100;	
		height: 2em; 
	    margin-top: 4px; 
	    color: #777B7F;  
    }
    </style>
    
       <!--Content-->
       <h3>Detalle de Protocolo</h3>
           
      
       <?php $_smarty_tpl->assign('percentRtmp',explode(".",(round(100-((int)$_smarty_tpl->getVariable('rtmp')->value*100)/$_smarty_tpl->getVariable('sumaProtocol')->value))),null,null);?>
       <?php $_smarty_tpl->assign('percentRtsp',explode(".",(round(100-((int)$_smarty_tpl->getVariable('rtsp')->value*100)/$_smarty_tpl->getVariable('sumaProtocol')->value))),null,null);?>
       <?php $_smarty_tpl->assign('percentHttp',explode(".",(round(100-((int)$_smarty_tpl->getVariable('http')->value*100)/$_smarty_tpl->getVariable('sumaProtocol')->value))),null,null);?> 
                 
         <dl><dd><b style="width: <?php if ((int)$_smarty_tpl->getVariable('rtmp')->value==0){?>100%<?php }else{ ?><?php if ((int)$_smarty_tpl->getVariable('percentRtmp')->value[0]==0){?>100%<?php }else{ ?><?php if ((int)$_smarty_tpl->getVariable('percentRtmp')->value[0]==100){?>0%<?php }else{ ?><?php echo (int)$_smarty_tpl->getVariable('percentRtmp')->value[0];?>
%<?php }?><?php }?><?php }?>;"></b><span class="labelProtocol">RTMP=<?php echo 100-(int)$_smarty_tpl->getVariable('percentRtmp')->value[0];?>
%</span></dd></dl>
         <dl><dd><b style="width: <?php if ((int)$_smarty_tpl->getVariable('rtsp')->value==0){?>100%<?php }else{ ?><?php if ((int)$_smarty_tpl->getVariable('percentRtsp')->value[0]==0){?>100%<?php }else{ ?><?php if ((int)$_smarty_tpl->getVariable('percentRtsp')->value[0]==100){?>0%<?php }else{ ?><?php echo (int)$_smarty_tpl->getVariable('percentRtsp')->value[0];?>
%<?php }?><?php }?><?php }?>;"></b><span class="labelProtocol">RSTP=<?php echo 100-(int)$_smarty_tpl->getVariable('percentRtsp')->value[0];?>
%</span></dd></dl>                
         <dl><dd><b style="width: <?php if ((int)$_smarty_tpl->getVariable('http')->value==0){?>100%<?php }else{ ?><?php if ((int)$_smarty_tpl->getVariable('percentHttp')->value[0]==0){?>100%<?php }else{ ?><?php if ((int)$_smarty_tpl->getVariable('percentHttp')->value[0]==100){?>0%<?php }else{ ?><?php echo (int)$_smarty_tpl->getVariable('percentHttp')->value[0];?>
%<?php }?><?php }?><?php }?>;"></b><span class="labelProtocol">HTTP=<?php echo 100-(int)$_smarty_tpl->getVariable('percentHttp')->value[0];?>
%</span></dd></dl>                
       <br/>
       <h3>Detalle de plataforma</h3>
		 <table width="100%" cellspacing="0" cellpadding="0" class="data">
		    <tbody>
		    <tr>
		            <th>
						<p style="width: 150px;">
						<img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/win.png" title="Windows" /><label style="float: right; margin-top:30px;">Windows: <?php echo $_smarty_tpl->getVariable('windows')->value;?>
</label>
						</p>		            
		            </th>
		            <td>
						<p style="width: 150px;">
						<img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/blackberry.png" title="BlackBerry" /><label style="float: right; margin-top:30px;">BlackBerry: <?php echo $_smarty_tpl->getVariable('bb')->value;?>
</label>
						</p>		            
		            </td>
		    </tr>
		    <tr>          
		            <th>
						<p style="width: 150px;">
						<img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/android.png" title="Android" /><label style="float: right; margin-top:30px;">Android: <?php echo $_smarty_tpl->getVariable('android')->value;?>
</label>
						</p>		            
		            </th>
      
		            <td>
						<p style="width: 150px;">
						<img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/iphone.png" title="Iphone" /><label style="float: right; margin-top:30px;">Iphone: <?php echo $_smarty_tpl->getVariable('iphone')->value;?>
</label>
						</p>		            
		            </td>
		    </tr>
		    <tr>        
		            <th>
						<p style="width: 150px;">
						<img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/ipod.png" title="Ipod" /><label style="float: right; margin-top:30px;">Ipod: <?php echo $_smarty_tpl->getVariable('ipod')->value;?>
</label>
						</p>		            
		            </th>
		            <td>
						<p style="width: 150px;">
						<img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/ipad.png" title="Ipad" /><label style="float: right; margin-top:28px;">Ipad: <?php echo $_smarty_tpl->getVariable('ipad')->value;?>
</label>
						</p>		            
		            </td>
		    </tr>
		    <tr>        
		            <th>
				        <p style="width: 150px;">
						<img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/other.png" title="Otros" /><label style="float: right; margin-top:30px;">Otros: <?php echo $_smarty_tpl->getVariable('other')->value;?>
</label>
						</p>   		            
		            </th>
		            <td></td>
		     </tr>       
		    </tbody>
		</table>
 
    </div>
</div>
</div>

<br/>
<br/>
<p>        
   <input type="button" value="Descargar excel" id="download">
   <input type="button" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Regresar');?>
" class="back" />       
</p>
</form>
</div>
</div>
<div>
</div>
