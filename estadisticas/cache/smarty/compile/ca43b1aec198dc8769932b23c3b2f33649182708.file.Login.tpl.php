<?php /* Smarty version 3.0rc1, created on 2013-02-13 22:35:51
         compiled from "application/views\auth/Login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16403511c15476ae336-83755606%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ca43b1aec198dc8769932b23c3b2f33649182708' => 
    array (
      0 => 'application/views\\auth/Login.tpl',
      1 => 1307635730,
    ),
  ),
  'nocache_hash' => '16403511c15476ae336-83755606',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/js/modules/auth/login.js"></script>
<div id="loginFormContainer">
<div class="onecolumn">
 <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Iniciar Sesi�n');?>
</span></div>
 <br class="clear" />
 <div class="content">
	<?php if ($_smarty_tpl->getVariable('errorMessage')->value){?>
		<div class="error alert_error"><p><?php echo $_smarty_tpl->getVariable('errorMessage')->value;?>
</p></div>
	<?php }?>
	<form action="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/auth/login" method="post" id="loginForm">
	<p> 
	  <label for="username"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Usuario');?>
:</label><br/>
	  <input type="text" name="username" id="username" value="<?php echo $_smarty_tpl->getVariable('post')->value['username'];?>
" style="width: 95%;"/>
	</p>
	<p> 
	  <label for="password"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Contrase�a');?>
:</label><br/>
	  <input type="password" name="password" id="password" value="" style="width: 95%;"/>
	</p>
	<br/>
	<p> 
	  <input type="submit" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Iniciar Sesi�n');?>
"/>
	</p>
	</form>
 </div>	
	</div>
</div>