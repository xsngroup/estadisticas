<?php /* Smarty version 3.0rc1, created on 2013-01-21 13:32:11
         compiled from "application/views/forms/StreamServer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:106709975350fd97bb4dda77-78943343%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '11e6339f13cd5cb74688a74b2dc1be9c730a6c31' => 
    array (
      0 => 'application/views/forms/StreamServer.tpl',
      1 => 1307657328,
    ),
  ),
  'nocache_hash' => '106709975350fd97bb4dda77-78943343',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_html_options')) include '/var/www/html/library/Smarty3/plugins/function.html_options.php';
?><p>
    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Id');?>
:</label><br/>
    # <?php echo $_smarty_tpl->getVariable('post')->value['id_server'];?>
<input type="hidden" name="id_server" id="id_server" value="<?php echo $_smarty_tpl->getVariable('post')->value['id_server'];?>
" />
</p>
<p>
    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Tipo de servidor');?>
:</label><br/>
    <?php echo smarty_function_html_options(array('name'=>'id_server_type','id'=>'id_server_type','options'=>$_smarty_tpl->getVariable('ServerTypes')->value,'selected'=>$_smarty_tpl->getVariable('post')->value['id_server_type']),$_smarty_tpl->smarty,$_smarty_tpl);?>

</p>
<p>
    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Ip');?>
:</label><br/>
    <input type="text" name="url" id="url" value="<?php echo $_smarty_tpl->getVariable('post')->value['url'];?>
" class=" required" />
</p>
<p>
    <label><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Estado');?>
:</label><br/>
    <?php echo smarty_function_html_options(array('name'=>'status','id'=>'status','options'=>$_smarty_tpl->getVariable('status')->value,'selected'=>$_smarty_tpl->getVariable('post')->value['status']),$_smarty_tpl->smarty,$_smarty_tpl);?>

    
</p>

<!--
$idServer = $this->getRequest()->getParam('id_server');
$url = $this->getRequest()->getParam('url');
$status = $this->getRequest()->getParam('status');
$idServerType = $this->getRequest()->getParam('id_server_type');
-->

<!--
$post = array(
    'id_server' => $streamServer->getIdServer(),
    'url' => $streamServer->getUrl(),
    'status' => $streamServer->getStatus(),
    'id_server_type' => $streamServer->getIdServerType(),
);
-->
