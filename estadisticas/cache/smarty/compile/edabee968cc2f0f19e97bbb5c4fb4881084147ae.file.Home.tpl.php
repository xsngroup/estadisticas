<?php /* Smarty version 3.0rc1, created on 2013-02-13 22:48:56
         compiled from "application/views\index/Home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:25777511c1858c80075-04186197%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'edabee968cc2f0f19e97bbb5c4fb4881084147ae' => 
    array (
      0 => 'application/views\\index/Home.tpl',
      1 => 1307635730,
    ),
  ),
  'nocache_hash' => '25777511c1858c80075-04186197',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_icon')) include 'lib/smarty/plugins\function.icon.php';
?>			<!-- <h1>Dashboard</h1> -->
			
			<!-- Begin shortcut menu 
			<ul id="shortcut">
    			<li>
    			  <a href="http://www.gallyapp.com/tf_themes/weadmin/modal_window.html" id="shortcut_home" class="tip" title="Home">
				    <img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/home.png" alt="home"><br>
				    <strong>Home</strong>
				  </a>
				</li>
    			<li>
    			  <a href="http://www.gallyapp.com/tf_themes/weadmin/modal_window.html" class="tip" title="Home">
				    <img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/calendar.png" alt="calendar"><br>
				    <strong>Calendar</strong>
				  </a>
				</li>
    			<li>
    			  <a href="http://www.gallyapp.com/tf_themes/weadmin/modal_window.html" class="tip" title="Home">
				    <img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/stats.png" alt="stats"><br>
				    <strong>Stats</strong>
				  </a>
				</li>
				<li>
    			  <a href="http://www.gallyapp.com/tf_themes/weadmin/modal_window.html" class="tip" title="Home">
				    <img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/setting.png" alt="setting"><br>
				    <strong>Setting</strong>
				  </a>
				</li>
				<li>
    			  <a href="http://www.gallyapp.com/tf_themes/weadmin/modal_window.html" id="shortcut_contacts" class="tip" title="Home">
				    <img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/contacts.png" alt="contacts"><br>
				    <strong>Contacts</strong>
				  </a>
				</li>
				<li>
    			  <a href="http://www.gallyapp.com/tf_themes/weadmin/modal_window.html" id="shortcut_posts" class="tip" title="Home">
				    <img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/posts.png" alt="posts"><br>
				    <strong>Posts</strong>
				  </a>
				</li>
				<li>
    			  <a href="http://www.gallyapp.com/tf_themes/weadmin/modal_window.html" id="shortcut_status" class="tip" title="Home">
				    <img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/status.png" alt="status"><br>
				    <strong>Status</strong>
				  </a>
				</li>				
				<li>
    			  <a href="http://www.gallyapp.com/tf_themes/weadmin/modal_window.html" id="shortcut_company" class="tip" title="Home">
				    <img src="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/images/template/theme/companys.png" alt="companys"><br>
				    <strong>Company</strong>
				  </a>
				</li>								
  			</ul>
  			-->
			<!-- End shortcut menu -->
			
			<!-- Begin shortcut notification 
			<div style="display: block;" id="shortcut_notifications">
				<span style="top: 102px; left: 300px;" class="notification" rel="shortcut_home">10</span>
				<span style="top: 102px; left: 708px;" class="notification" rel="shortcut_contacts">5</span>
				<span style="top: 102px; left: 810px;" class="notification" rel="shortcut_posts">1</span>
			</div>
			 End shortcut noficaton -->
			
			
			<br class="clear">
<div class="onecolumn">
 <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Información del usuario');?>
</span></div>
 <br class="clear" />
 <div class="content">
<form action="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/user/change-password" class="validate" method="post">
    <table width="100%" cellspacing="0" cellpadding="0" class="data">        
        <tbody>
            <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Nombre:');?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('systemUser')->value->getFullName();?>
</td>
            </tr>
            <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Usuario:');?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('systemUser')->value->getUsername();?>
</td>
            </tr>
            <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Perfil de acceso');?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('systemAccessRole')->value->getName();?>
</td>
            </tr>
            <?php if ($_smarty_tpl->getVariable('log')->value){?>
            <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Último acceso:');?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('log')->value->getTimestamp();?>
</td>
            </tr>
            <tr>
                <th align="right"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Ip de acceso:');?>
</th>
                <td class="last"><?php echo $_smarty_tpl->getVariable('log')->value->getIp();?>
</td>
            </tr>
            <?php }?>
            <?php if ($_smarty_tpl->getVariable('changePassword')->value>0){?>
            <tr>
                <td colspan="2" class="tableHeader center"><?php echo smarty_function_icon(array('src'=>'user_suit','alt'=>"icon"),$_smarty_tpl->smarty,$_smarty_tpl);?>
 <?php echo $_smarty_tpl->getVariable('l10n')->value->_('Change Password');?>
</td>
            </tr>
            <tr>
                <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Password');?>
</th>
                <td><input type="password" name="password" id="password" class="required" /></td>
            </tr>
            <tr>
                <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Repeat Password');?>
</th>
                <td><input type="password" name="rpassword" id="rpassword" class="required" /></td>
            </tr>
            
            <?php }?>
        </tbody>
    </table>
</form>
</div>
</div>