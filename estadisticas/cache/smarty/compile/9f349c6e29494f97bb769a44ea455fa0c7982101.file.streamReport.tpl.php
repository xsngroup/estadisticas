<?php /* Smarty version 3.0rc1, created on 2013-01-21 17:28:29
         compiled from "application/views/report/streamReport.tpl" */ ?>
<?php /*%%SmartyHeaderCode:167811459150fdcf1d10c167-59214442%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9f349c6e29494f97bb769a44ea455fa0c7982101' => 
    array (
      0 => 'application/views/report/streamReport.tpl',
      1 => 1342246772,
    ),
  ),
  'nocache_hash' => '167811459150fdcf1d10c167-59214442',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_html_options')) include '/var/www/html/library/Smarty3/plugins/function.html_options.php';
?><script type="text/javascript">
$(document).ready(function(){
	$('#groupBy').change(function(){	
        //alert($(this).val());	
        if($(this).val()==1)
        {
        	$("#firstDate").show();
            $("#secondDate").hide();
            $("#meses").hide();
        }

        if($(this).val()==2)
        {
        	$("#firstDate").show();
       	    $("#secondDate").show();
            $("#meses").hide();
        }

        if($(this).val()==3)
        {
        	$("#firstDate").hide();
       	    $("#secondDate").hide();
        	$("#meses").show();
        }                
	});	
});	

</script>
<div class="column_left">
 <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Hist�rico de Streaming en vivo');?>
</span></div>
 <br class="clear" />
 <div class="content">
<form action="<?php echo $_smarty_tpl->getVariable('baseUrl')->value;?>
/report/process-stream" class="validate" method="post">
<label>Filtro de Fechas:</label><br/><br/>
<p>
     <label>Agrupar por:</label>  
     <select id="groupBy" name="groupBy">
        <option value="1">Horas</option>
        <option value="2">D�as</option>
       <!-- <option value="3">Mes</option> --> 
     </select>
</p>    
<br/>
<p id="firstDate">   

     <label>*Desde:</label>  
     <input type="text" name="date_start" id="date_start" value="<?php echo $_smarty_tpl->getVariable('post')->value['date_start'];?>
<?php echo $_smarty_tpl->getVariable('dateStart')->value;?>
" class="field text medium datePicker dateISO required picker" />                    
<br/>
</p> 

<p id="secondDate" style="display: none;">
<br/>
     <label style="padding-right:3px;">*Hasta: </label> 
     <input type="text" name="date_end" id="date_end" value="<?php echo $_smarty_tpl->getVariable('post')->value['date_end'];?>
<?php echo $_smarty_tpl->getVariable('dateEnd')->value;?>
" class="field text medium datePicker dateISO picker" />                   
<br/>
</p>
<br/>
<br/>
<p>
<label>Filtro de horarios:</label><br/>
<label>De: </label><input type="text" name="initHour" size="10" class="field text small" value="09:00" /><label style="padding-left:15px;">a: </label><input type="text" size="10" class="field text small"  name="finishHour" value="14:00" />
</p>
<br/>
<p>
<label>Se�ales:</label><br/>
<?php echo smarty_function_html_options(array('name'=>'stream','id'=>'stream','options'=>$_smarty_tpl->getVariable('streams')->value),$_smarty_tpl->smarty,$_smarty_tpl);?>

</p>
<br/> 
<br/> 
<p> 
<input type="hidden" name="idCompany" value="<?php echo $_smarty_tpl->getVariable('idCompany')->value;?>
" />
<input type="hidden" name="idServer" value="<?php echo $_smarty_tpl->getVariable('idServer')->value;?>
" />
<input type="submit" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Generar');?>
" />
</p>
</form>
</div>
</div>