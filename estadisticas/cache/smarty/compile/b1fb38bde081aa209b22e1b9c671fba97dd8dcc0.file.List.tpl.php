<?php /* Smarty version 3.0rc1, created on 2013-01-21 13:32:11
         compiled from "application/views/stream-server/List.tpl" */ ?>
<?php /*%%SmartyHeaderCode:50421591250fd97bb3a4839-39412760%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b1fb38bde081aa209b22e1b9c671fba97dd8dcc0' => 
    array (
      0 => 'application/views/stream-server/List.tpl',
      1 => 1307657328,
    ),
  ),
  'nocache_hash' => '50421591250fd97bb3a4839-39412760',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_url')) include 'lib/smarty/plugins/function.url.php';
if (!is_callable('smarty_modifier_odd')) include 'lib/smarty/plugins/modifier.odd.php';
if (!is_callable('smarty_function_icon')) include 'lib/smarty/plugins/function.icon.php';
?><div class="column_left">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Servidores streaming');?>
</span></div>
    <br class="clear" />
    <div class="content">
<form action="<?php echo smarty_function_url(array('action'=>'create'),$_smarty_tpl->smarty,$_smarty_tpl);?>
" method="post" class="validate ajaxForm">
<h4><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Guardar nuevo');?>
</h4>
<?php $_template = new Smarty_Internal_Template('forms/StreamServer.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

<br/>
<p> 
<input type="submit" value="<?php echo $_smarty_tpl->getVariable('l10n')->value->_('Guardar');?>
" />
</p>
</form>
	</div>
</div>

<div class="column_right">
    <div class="header"><span><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Listado de Servidores');?>
</span></div>
    <br class="clear" />
    <div class="content">
<table width="100%" cellspacing="0" cellpadding="0" class="data">
    <thead>
        <tr>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Id');?>
</th>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Ip');?>
</th>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Estado');?>
</th>
            <th><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Tipo de servidor');?>
</th>
            <th colspan="2"><?php echo $_smarty_tpl->getVariable('l10n')->value->_('Acciones');?>
</th>
        </tr>
    </thead>
    <tbody id="ajaxList">
        <?php  $_smarty_tpl->tpl_vars['streamServer'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('streamServers')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['streamServer']->iteration=0;
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['streamServer']->key => $_smarty_tpl->tpl_vars['streamServer']->value){
 $_smarty_tpl->tpl_vars['streamServer']->iteration++;
?>
            <tr class="<?php echo smarty_modifier_odd($_smarty_tpl->tpl_vars['streamServer']->iteration);?>
">
                <td><?php echo $_smarty_tpl->getVariable('streamServer')->value->getIdServer();?>
</td>
                <td><?php echo $_smarty_tpl->getVariable('streamServer')->value->getUrl();?>
</td>
                <td><?php echo $_smarty_tpl->getVariable('status')->value[$_smarty_tpl->getVariable('streamServer')->value->getStatus()];?>
</td>
                <td>
                <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('serverTypeRel')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
?>
                  <?php if ($_smarty_tpl->tpl_vars['item']->value['id_server']==$_smarty_tpl->getVariable('streamServer')->value->getIdServer()){?>
                   <?php echo $_smarty_tpl->getVariable('ServerTypes')->value[$_smarty_tpl->tpl_vars['item']->value['id_type']];?>
,
                  <?php }?> 
                <?php }} ?>                
                </td>
                <td><a href="<?php echo smarty_function_url(array('action'=>'edit','idServer'=>$_smarty_tpl->getVariable('streamServer')->value->getIdServer()),$_smarty_tpl->smarty,$_smarty_tpl);?>
"><?php echo smarty_function_icon(array('src'=>'pencil','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Edit')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
                <td><a href="<?php echo smarty_function_url(array('action'=>'delete','idServer'=>$_smarty_tpl->getVariable('streamServer')->value->getIdServer()),$_smarty_tpl->smarty,$_smarty_tpl);?>
" class="confirm"><?php echo smarty_function_icon(array('src'=>'delete','class'=>'tip','title'=>$_smarty_tpl->getVariable('l10n')->value->_('Delete')),$_smarty_tpl->smarty,$_smarty_tpl);?>
</a></td>
            </tr>
        <?php }} ?>
    </tbody>
</table>
	</div>
</div>
