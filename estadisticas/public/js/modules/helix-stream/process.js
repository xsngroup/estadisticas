/**
 * 
 */
$(document).ready(function(){
		
	    $(".selectLive").change(function(){
		var nameSelect=$(this).attr("name").split("-");
			
		var combos = new Array();
		combos[$(this).attr("name")] = "id_live-"+nameSelect[1];
		var posicion = $(this).attr("name");
		var valor = $(this).val()		
		if(nameSelect[0] == "server" && valor==0){
			$("#id_live-"+nameSelect[1]).html('	<option value="0" selected="selected">Selecciona un servidor...</option>');
		}else{
			$("#"+combos[posicion]).html('<option selected="selected" value="0">Cargando...</option>')
			if(valor!="0" || posicion !="id_live-"+nameSelect[1]){
				$.post(baseUrl +"/helix-stream/combos",{
									combo:nameSelect[0],
									id:$(this).val()
									},function(data){
													$("#"+combos[posicion]).html(data);																																	
													})												
			}
		}
	    });		
	
	 $(".protocol").click(function(){	
		if($(this).is(':checked')){
			$("#loadConf-"+$(this).attr('value')).show();
		}
		else{
			$("#loadConf-"+$(this).attr('value')).hide();
		}		
	 });
	 
	 $(".prefixChange").change(function(){
	 	var nameSelect=$(this).attr("name").split("-");
	 	if($(this).val()>0){
			$("#showPrefix-"+nameSelect[1]).show();
		}
		else{
			$("#showPrefix-"+nameSelect[1]).hide();
		}
	 });
	 
	 $(".ckbxPort").change(function(){
		 	var idSelect=$(this).attr("id").split("-");
            
            if($(this).is(':checked')){
            	$("#port-"+idSelect[1]).removeAttr('disabled');
            }
            else{
            	$("#port-"+idSelect[1]).attr('disabled','disabled');
            }
		 });	 

});	