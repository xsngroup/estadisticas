<?php
/**
 * ##$BRAND_NAME$##
 *
 * ##$DESCRIPTION$##
 *
 * @category   Project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */

require_once "lib/controller/BaseController.php";

/**
 * Clase IndexController que representa el controller para la ruta default
 *
 * @category   Project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 */
class IndexController extends BaseController
{
    /**
     * Pantalla para despues del inicio de sesi�n
     */
    public function indexAction()
    {
        //$this->view->setLayoutFile(false);
        //$this->_helper->viewRenderer->setNoRender();
    	require_once 'application/models/catalogs/UserLogCatalog.php';
        require_once 'application/models/catalogs/UserCatalog.php';
        require_once 'application/models/catalogs/AccessRoleCatalog.php';

        $userLogCatalog = UserLogCatalog::getInstance();
        
        $log= $userLogCatalog->getLastLogin($this->getUser()->getIdUser());                
        $user=UserCatalog::getInstance()->getById($this->getUser()->getIdUser());
        $accessRol=AccessRoleCatalog::getInstance()->getById($user->getIdAccessRole());
        //var_dump($user);
        //var_dump($log);
        // $this->view->changePassword = $lastModified;
        $this->view->systemAccessRole=$accessRol;
        $this->view->systemUser = $user;
        $this->view->log = $log;
    	
        $this->setTitle('Bienvenido al sistema');
        $this->view->setTpl('Home');
    }
}


