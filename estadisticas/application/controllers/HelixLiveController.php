<?php
/**
 * Xsn
 *
 * Xsn
 *
 * @category   Application
 * @package    Application_Controllers
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */

/**
 * Dependences
 */
require_once "lib/controller/CrudController.php";
require_once "application/models/catalogs/HelixLiveCatalog.php";
require_once 'application/models/catalogs/StreamServerCatalog.php';

/**
 * HelixLiveController (CRUD for the HelixLive Objects)
 *
 * @category   Project
 * @package    Project_Controllers
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx) 
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     <zetta> & <chentepixtol>
 * @version    1.0.2 SVN: $Revision$
 */
class HelixLiveController extends CrudController
{
    
    /**
     * alias for the list action
     */
    public function indexAction()
    {
        $this->_forward('list');
    }
    
    /**
     * List the objects HelixLive actives
     */
    public function listAction()
    {
        $this->view->helixLives = HelixLiveCatalog::getInstance()->getByCriteria();
        $this->view->servers=StreamServerCatalog::getInstance()->getByCriteria()->toCombo();
        $this->setTitle('List the HelixLive');
    }
    
    /**
     * delete an HelixLive
     */
    public function deleteAction()
    {
        $helixLiveCatalog = HelixLiveCatalog::getInstance();
        $idLive = $this->getRequest()->getParam('idLive');
        $helixLive = $helixLiveCatalog->getById($idLive);
        $helixLiveCatalog->delete($helixLive);
        $this->setFlash('ok','Successfully removed the HelixLive');
        $this->_redirect('helix-live/list');
    }
    
    /**
     * Form for edit an HelixLive
     */
    public function editAction()
    {
        $helixLiveCatalog = HelixLiveCatalog::getInstance();
        $idLive = $this->getRequest()->getParam('idLive');
        $helixLive = $helixLiveCatalog->getById($idLive);
        $post = array(
            'id_live' => $helixLive->getIdLive(),
            'name' => $helixLive->getName(),
            'id_server' => $helixLive->getIdServer(),
        );
        $this->view->post = $post;
        $this->view->servers=StreamServerCatalog::getInstance()->getByCriteria()->toCombo();
        $this->setTitle('Edit HelixLive');
    }
    
    /**
     * Create an HelixLive
     */
    public function createAction()
    {   
        $helixLiveCatalog = HelixLiveCatalog::getInstance();
        $name = utf8_decode($this->getRequest()->getParam('name'));
        $idServer = utf8_decode($this->getRequest()->getParam('id_server'));
        $helixLive = HelixLiveFactory::create($name, $idServer);
        $helixLiveCatalog->create($helixLive);  
        $this->view->setTpl('_row');
        $this->view->setLayoutFile(false);
        $this->view->helixLive = $helixLive;
        $this->view->servers=StreamServerCatalog::getInstance()->getByCriteria()->toCombo();
    }
    
    /**
     * Update an HelixLive
     */
    public function updateAction()
    {
        $helixLiveCatalog = HelixLiveCatalog::getInstance();
        $idLive = $this->getRequest()->getParam('idLive');
        $helixLive = $helixLiveCatalog->getById($idLive);
        $helixLive->setName($this->getRequest()->getParam('name'));
        $helixLive->setIdServer($this->getRequest()->getParam('id_server'));
        $helixLiveCatalog->update($helixLive);
        $this->setFlash('ok','Successfully edited the HelixLive');
        $this->_redirect('helix-live/list');
    }
    
}
