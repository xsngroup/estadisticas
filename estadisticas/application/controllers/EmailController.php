<?php
/**
 * Xsn
 *
 * Xsn Monitor Stream Servers
 *
 * @category   Application
 * @package    Application_Controllers
 * @copyright  Copyright (c) 2010-2011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */

/**
 * Dependences
 */
require_once "lib/controller/CrudController.php";
require_once "application/models/catalogs/EmailCatalog.php";

/**
 * EmailController (CRUD for the Email Objects)
 *
 * @category   Project
 * @package    Project_Controllers
 * @copyright  Copyright (c) 2010-2011 Xsn Group (http://www.xsn.com.mx) 
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     <zetta> & <chentepixtol>
 * @version    1.0.2 SVN: $Revision$
 */
class EmailController extends CrudController
{
    
    /**
     * alias for the list action
     */
    public function indexAction()
    {
        $this->_forward('list');
    }
    
    /**
     * List the objects Email actives
     */
    public function listAction()
    {
        $this->view->emails = EmailCatalog::getInstance()->getActives();
        $this->setTitle('List the Email');
    }
    
    /**
     * delete an Email
     */
    public function deleteAction()
    {
        $emailCatalog = EmailCatalog::getInstance();
        $idEmail = $this->getRequest()->getParam('idEmail');
        $email = $emailCatalog->getById($idEmail);
        $emailCatalog->deactivate($email);
        $this->setFlash('ok','Successfully removed the Email');
        $this->_redirect('email/list');
    }
    
    /**
     * Form for edit an Email
     */
    public function editAction()
    {
        $emailCatalog = EmailCatalog::getInstance();
        $idEmail = $this->getRequest()->getParam('idEmail');
        $email = $emailCatalog->getById($idEmail);
        $post = array(
            'id_email' => $email->getIdEmail(),
            'email' => $email->getEmail(),
        );
        $this->view->post = $post;
        $this->setTitle('Edit Email');
    }
    
    /**
     * Create an Email
     */
    public function createAction()
    {   
        $emailCatalog = EmailCatalog::getInstance();
        $email = utf8_decode($this->getRequest()->getParam('email'));
        $email = EmailFactory::create($email);
        $emailCatalog->create($email);  
        $this->view->setTpl('_row');
        $this->view->setLayoutFile(false);
        $this->view->email = $email;
    }
    
    /**
     * Update an Email
     */
    public function updateAction()
    {
        $emailCatalog = EmailCatalog::getInstance();
        $idEmail = $this->getRequest()->getParam('idEmail');
        $email = $emailCatalog->getById($idEmail);
        $email->setEmail($this->getRequest()->getParam('email'));
        $emailCatalog->update($email);
        $this->setFlash('ok','Successfully edited the Email');
        $this->_redirect('email/list');
    }
    
}
