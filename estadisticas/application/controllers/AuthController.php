<?php
/**
 * ##$BRAND_NAME$##
 *
 * ##$DESCRIPTION$##
 *
 * @category   Project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 * @author     ##$AUTHOR$##, $LastChangedBy$
 * @version    ##$VERSION$##, SVN:  $Id$
 */

/**
 * UserLogCatalog
 */
require_once 'application/models/catalogs/UserLogCatalog.php';

/**
 * BaseController
 */
require_once 'lib/controller/BaseController.php';

/**
 * AccessRoleCatalog
 */
require_once 'application/models/catalogs/AccessRoleCatalog.php';
/**
 * Clase AuthController que representa el controlador para las acciones de login/logout
 *
 * @category   project
 * @package    Project_Controllers
 * @copyright  ##$COPYRIGHT$##
 */
class AuthController extends BaseController
{
    /**
     * Sobrecargamos el metodo init
     */
    public function init()
    {
        $registry = Zend_Registry::getInstance();
        $this->view->moduleName = $this->getRequest()->getModuleName();
        $this->view->controllerName = $this->getRequest()->getControllerName();
        $this->view->actionName = $this->getRequest()->getActionName();
        $this->view->baseUrl = $this->getRequest()->getBaseUrl();
        $this->view->systemTitle = utf8_decode($registry->config->appSettings->titulo);
        $this->view->l10n = new Zend_Translate('gettext', 'data/locale/base.mo', 'es');
    }
    
    /**
     * 
     */
    public function configAction()
    {
    	require_once 'application/models/catalogs/SecurityActionCatalog.php';
    	require_once 'application/models/catalogs/SecurityControllerCatalog.php';
        parent::init();
        $this->setTitle('Configuraci�n de Facultades');
        $this->view->accessRoles = AccessRoleCatalog::getInstance()->getActives(new Criteria());
        $actionsCriteria = new Criteria();
        $actionsCriteria->add(SecurityAction::SYSTEM,1,Criteria::EQUAL);
        $this->view->actions = SecurityActionCatalog::getInstance()->getByCriteria($actionsCriteria);
        $controllersCriteria = new Criteria();
        $controllersCriteria->add(SecurityController::SYSTEM,1,Criteria::EQUAL);
        $this->view->controllers = SecurityControllerCatalog::getInstance()->getByCriteria($controllersCriteria);
        $this->view->permissions = AccessRoleCatalog::getInstance()->getAllPermissions();
    }
    
    /**
     * Agrega/Elimina un permiso en la base de datos de facultades
     */
    public function setPermissionAction()
    {
        require_once 'lib/security/SecurityInspector.php';
        parent::init();
        $securityInspector = new SecurityInspector();
        $securityInspector->setPermission($this->getRequest()->getParam('value'),$this->getRequest()->getParam('idAction'),$this->getRequest()->getParam('idAccessRole'));
        die($this->getRequest()->getParam('value'));
    }
    
    /**
     * Inspecciona Todos los controllers y las actions, las agrega a la base de datos, y eso
     */
    public function inspectAction()
    {
    	require_once 'lib/security/SecurityInspector.php';
    	parent::init();
    	$securityInspector = new SecurityInspector();
    	$securityInspector->analizeWorkspace();
    	$this->setFlash('ok','Workspace analizado');
    	$this->_redirect('auth/config');
    }
    
    /**
     * Accion para la pantalla de Login
     */
    public function viewLoginAction()
    {
        $this->view->contentTitle = 'Iniciar Sesi�n';
        $this->view->setTpl('Login');
    }

    /*
     * Inicio de Sesi�n
     */
    public function loginAction()
    {
        $username = $this->getRequest()->getParam('username');
        $password = $this->getRequest()->getParam('password');
        try
        {
            if($username == null || $password == null)
                throw new AuthException('Debe especificar Usuario y contrase�a');
           $user = Authentication::authenticate($username, $password);
           $this->getUser()->setBean($user);
           $this->getUser()->setAccessRole(AccessRoleCatalog::getInstance()->getById($user->getIdAccessRole()));
           $this->_redirect('/');
        }
        catch(AuthException $e)
        {
            $this->view->errorMessage = $e->getMessage();
            $this->view->contentTitle = 'Iniciar Sesi�n';
        }
    }

    /**
     * Accion que hace logout.
     */
    public function logoutAction()
    {
    	$this->getUser()->shutdown();
    	$this->_redirect('/');
    }
    
    /**
     * Acci�n para autorizar descuentos
     * @return unknown_type
     */
    public function autorizedAction()
    {
    	$username = $this->getRequest()->getParam('username');
        $password = $this->getRequest()->getParam('password');
        try
        {
           if($username == null || $password == null)
                throw new AuthException('Debe especificar Usuario y contrase�a');
           $user = Authentication::authenticateAs($username, $password, AccessRole::$AccessRoles['Gerente']); 
           $this->getUser()->setAttribute('discounts', true, 'authorized');
        }
        catch(AuthException $e)
        {
            throw new Exception($e->getMessage());
        }
        $this->view->setLayoutFile(false);
        
        die();
    }  
    
    
    public function loadAction()
    {
       require_once 'lib/Csv/Reader.php';
       require_once 'lib/Csv/Dialect/Excel.php';
       require_once 'application/models/catalogs/FlashLiveStatCatalog.php';

       $file = 'C:\\dump.txt';
       
       try {
       		 set_time_limit ( 0 );			
			 $dialect = new Csv_Dialect_Excel();			 
             $dialect->delimiter = ",";	

			 $reader = new Csv_Reader($file,$dialect);			
			
			while ($row = $reader->current()) {
				$flashLive = new FlashLiveStat();
				$flashLive->setIdLiveStat($row[0]);
				$flashLive->setTimestamp($row[1]);
				$flashLive->setType($row[2]);
				$flashLive->setClient($row[3]);
				$flashLive->setPublishTime($row[4]);
				$flashLive->setConnects($row[5]);
				$flashLive->setIdStream($row[6]);
				
				FlashLiveStatCatalog::getInstance()->update($flashLive);
				 //Catalog::clearProfiler ();
				$reader->next();
			}				       	
       		} catch (Csv_Exception_FileNotFound $e){
			echo "<p class='error'>File could not be found</p>";	
		}       	
    }

}
