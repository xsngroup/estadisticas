<?php
/**
 * Xsn
 *
 * Xsn
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */


require_once "lib/utils/Parser.php";

/**
 * Clase ViewersCollection que representa una collección de objetos Viewers
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_collections
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     zetta & chentepixtol
 * @version    1.0.2 SVN: $Revision$
 */
class ViewersCollection extends ArrayIterator
{

    /**
     * @var Parser
     */
    private $parser;
    
    /**
     * Constructor
     * @param array $array
     * @return void
     */
    public function __construct($array = array())
    {
        $this->parser = new Parser('Viewers');
        parent::__construct($array);
    }

    /**
     * Appends the value
     * @param Viewers $viewers
     */
    public function append($viewers)
    {
        parent::offsetSet($viewers->getIdViewer(), $viewers);
        $this->rewind();
    }

    /**
     * Return current array entry
     * @return Viewers
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * Return current array entry and 
     * move to next entry
     * @return Viewers 
     */
    public function read()
    {
        $viewers = $this->current();
        $this->next();
        return $viewers;
    }

    /**
     * Get the first array entry
     * if exists or null if not 
     * @return Viewers|null 
     */
    public function getOne()
    {
        if ($this->count() > 0)
        {
            $this->seek(0);
            return $this->current();
        } else
            return null;
    }
    
    /**
     * Contains one object with $idViewer
     * @param  int $idViewer
     * @return boolean
     */
    public function contains($idViewer)
    {
        return parent::offsetExists($idViewer);
    }
    
    /**
     * Remove one object with $idViewer
     * @param  int $idViewer
     */
    public function remove($idViewer)
    {
        if( $this->contains($idViewer) )
            $this->offsetUnset($idViewer);
    }
    
    /**
     * Merge two Collections
     * @param ViewersCollection $viewersCollection
     * @return void
     */
    public function merge(ViewersCollection $viewersCollection)
    {
        $viewersCollection->rewind();
        while($viewersCollection->valid())
        {
            $viewers = $viewersCollection->read();
            if( !$this->contains( $viewers->getIdViewer() ) )
                $this->append($viewers);
        }
        $viewersCollection->rewind();
    }
    
    /**
     * Diff two Collections
     * @param ViewersCollection $viewersCollection
     * @return void
     */
    public function diff(ViewersCollection $viewersCollection)
    {
        $viewersCollection->rewind();
        while($viewersCollection->valid())
        {
            $viewers = $viewersCollection->read();
            if( $this->contains( $viewers->getIdViewer() ) )
                $this->remove($viewers->getIdViewer());     
        }
        $viewersCollection->rewind();
    }
    
    /**
     * Intersect two Collections
     * @param ViewersCollection $viewersCollection
     * @return ViewersCollection
     */
    public function intersect(ViewersCollection $viewersCollection)
    {
        $newviewersCollection = ViewersCollection();
        $viewersCollection->rewind();
        while($viewersCollection->valid())
        {
            $viewers = $viewersCollection->read();
            if( $this->contains( $viewers->getIdViewer() ) )
                $newviewersCollection->append($viewers);
        }
        $viewersCollection->rewind();
        return $newviewersCollection;
    }
    
    /**
     * Retrieve the array with primary keys 
     * @return array
     */
    public function getPrimaryKeys()
    {
        return array_keys($this->getArrayCopy());
    }
    
    /**
     * Retrieve the Viewers with primary key  
     * @param  int $idViewer
     * @return Viewers
     */
    public function getByPK($idViewer)
    {
        return $this->contains($idViewer) ? $this[$idViewer] : null;
    }
  
    /**
     * Transforma una collection a un array
     * @return array
     */
    public function toArray()
    {
        $array = array();
        while ($this->valid())
        {
            $viewers = $this->read();
            $this->parser->changeBean($viewers);
            $array[$viewers->getIdViewer()] = $this->parser->toArray();
        }
        $this->rewind();
        return $array;
    }
    
    /**
     * Crea un array asociativo de $key => $value a partir de las constantes de un bean
     * @param string $ckey
     * @param string $cvalue
     * @return array
     */
    public function toKeyValueArray($ckey, $cvalue)
    {
        $array = array();
        while ($this->valid())
        {
            $viewers = $this->read();
            $this->parser->changeBean($viewers);
            $array += $this->parser->toKeyValueArray($ckey, $cvalue);
        }
        $this->rewind();
        return $array;
    }
    
    /**
     * Retrieve the parser object
     * @return Parser
     */
    public function getParser()
    {
        return $this->parser;
    }
    
    /**
     * Is Empty
     * @return boolean
     */
    public function isEmpty()
    {
        return $this->count() == 0;
    }
  
  
}

