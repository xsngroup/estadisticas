<?php
/**
 * Xsn
 *
 * Xsn
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */

/**
 * Clase ThemeMobile
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_beans
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx) 
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     <zetta> & <chentepixtol>
 * @version    1.0.2 SVN: $Revision$
 */
class ThemeMobile
{
    /**
     * Constante que contiene el nombre de la tabla 
     * @static TABLENAME
     */
    const TABLENAME = "xsn_core_theme_mobile";

    /**
     * Constantes para los nombres de los campos
     */
    const ID_THEME_MOBILE = "xsn_core_theme_mobile.id_theme_mobile";
    const NAME = "xsn_core_theme_mobile.name";
    const SCREEN_SHOT = "xsn_core_theme_mobile.screen_shot";
    

    /**
     * $idThemeMobile 
     * 
     * @var int $idThemeMobile
     */
    private $idThemeMobile;
    

    /**
     * $name 
     * 
     * @var string $name
     */
    private $name;
    

    /**
     * $screenShot 
     * 
     * @var string $screenShot
     */
    private $screenShot;

    /**
     * Set the idThemeMobile value
     * 
     * @param int idThemeMobile
     * @return ThemeMobile $themeMobile
     */
    public function setIdThemeMobile($idThemeMobile)
    {
        $this->idThemeMobile = $idThemeMobile;
        return $this;
    }

    /**
     * Return the idThemeMobile value
     * 
     * @return int
     */
    public function getIdThemeMobile()
    {
        return $this->idThemeMobile;
    }

    /**
     * Set the name value
     * 
     * @param string name
     * @return ThemeMobile $themeMobile
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Return the name value
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the screenShot value
     * 
     * @param string screenShot
     * @return ThemeMobile $themeMobile
     */
    public function setScreenShot($screenShot)
    {
        $this->screenShot = $screenShot;
        return $this;
    }

    /**
     * Return the screenShot value
     * 
     * @return string
     */
    public function getScreenShot()
    {
        return $this->screenShot;
    }

}
