<?php
/**
 * Xsn
 *
 * Xsn Monitor Stream Servers
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-2011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */

/**
 * Dependences
 */
require_once "lib/db/Catalog.php";
require_once "application/models/beans/Ipviewer.php";
require_once "application/models/exceptions/IpviewerException.php";
require_once "application/models/collections/IpviewerCollection.php";
require_once "application/models/factories/IpviewerFactory.php";

/**
 * Singleton IpviewerCatalog Class
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_catalogs
 * @copyright  Copyright (c) 2010-2011 Xsn Group (http://www.xsn.com.mx)
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     zetta & chentepixtol
 * @version    1.0.2 SVN: $Revision$
 */
class IpviewerCatalog extends Catalog
{

    /**
     * Singleton Instance
     * @var IpviewerCatalog
     */
    static protected $instance = null;


    /**
     * Método para obtener la instancia del catálogo
     * @return IpviewerCatalog
     */
    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
          self::$instance = new self();
        }
        return self::$instance;
    }
    
    /**
     * Constructor de la clase IpviewerCatalog
     * @return IpviewerCatalog
     */
    protected function IpviewerCatalog()
    {
        parent::Catalog();
    }

    /**
     * Metodo para agregar un Ipviewer a la base de datos
     * @param Ipviewer $ipviewer Objeto Ipviewer
     */
    public function create($ipviewer)
    {
        if(!($ipviewer instanceof Ipviewer))
            throw new IpviewerException("passed parameter isn't a Ipviewer instance");
        try
        {
            $data = array(
                'ipviewer' => $ipviewer->getIpviewer(),
            );
            $data = array_filter($data, 'Catalog::notNull');
            $this->db->insert(Ipviewer::TABLENAME, $data);
            $ipviewer->setIdIpviewer($this->db->lastInsertId());
        }
        catch(Exception $e)
        {
            throw new IpviewerException("The Ipviewer can't be saved \n" . $e->getMessage());
        }
    }

    /**
     * Metodo para Obtener los datos de un objeto por su llave primaria
     * @param int $idIpviewer
     * @param boolean $throw
     * @return Ipviewer|null
     */
    public function getById($idIpviewer, $throw = false)
    {
        try
        {
            $criteria = new Criteria();
            $criteria->add(Ipviewer::ID_IPVIEWER, $idIpviewer, Criteria::EQUAL);
            $newIpviewer = $this->getByCriteria($criteria)->getOne();
        }
        catch(Exception $e)
        {
            throw new IpviewerException("Can't obtain the Ipviewer \n" . $e->getMessage());
        }
        if($throw && null == $newIpviewer)
            throw new IpviewerException("The Ipviewer at $idIpviewer not exists ");
        return $newIpviewer;
    }
    
    /**
     * Metodo para Obtener una colección de objetos por varios ids
     * @param array $ids
     * @return IpviewerCollection
     */
    public function getByIds(array $ids)
    {
        if(null == $ids) return new IpviewerCollection();
        try
        {
            $criteria = new Criteria();
            $criteria->add(Ipviewer::ID_IPVIEWER, $ids, Criteria::IN);
            $ipviewerCollection = $this->getByCriteria($criteria);
        }
        catch(Exception $e)
        {
            throw new IpviewerException("IpviewerCollection can't be populated\n" . $e->getMessage());
        }
        return $ipviewerCollection;
    }

    /**
     * Metodo para actualizar un Ipviewer
     * @param Ipviewer $ipviewer 
     */
    public function update($ipviewer)
    {
        if(!($ipviewer instanceof Ipviewer))
            throw new IpviewerException("passed parameter isn't a Ipviewer instance");
        try
        {
            $where[] = "id_ipviewer = '{$ipviewer->getIdIpviewer()}'";
            $data = array(
                'ipviewer' => $ipviewer->getIpviewer(),
            );
            $data = array_filter($data, 'Catalog::notNull');
            $this->db->update(Ipviewer::TABLENAME, $data, $where);
        }
        catch(Exception $e)
        {
            throw new IpviewerException("The Ipviewer can't be updated \n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para guardar un ipviewer
     * @param Ipviewer $ipviewer
     */	
    public function save($ipviewer)
    {
        if(!($ipviewer instanceof Ipviewer))
            throw new IpviewerException("passed parameter isn't a Ipviewer instance");
        if(null != $ipviewer->getIdIpviewer())
            $this->update($ipviewer);
        else
            $this->create($ipviewer);
    }

    /**
     * Metodo para eliminar un ipviewer
     * @param Ipviewer $ipviewer
     */
    public function delete($ipviewer)
    {
        if(!($ipviewer instanceof Ipviewer))
            throw new IpviewerException("passed parameter isn't a Ipviewer instance");
        $this->deleteById($ipviewer->getIdIpviewer());
    }

    /**
     * Metodo para eliminar un Ipviewer a partir de su Id
     * @param int $idIpviewer
     */
    public function deleteById($idIpviewer)
    {
        try
        {
            $where = array($this->db->quoteInto('id_ipviewer = ?', $idIpviewer));
            $this->db->delete(Ipviewer::TABLENAME, $where);
        }
        catch(Exception $e)
        {
            throw new IpviewerException("The Ipviewer can't be deleted\n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para eliminar varios Ipviewer a partir de su Id
     * @param array $ids
     */
    public function deleteByIds(array $ids)
    {
        try
        {
            $criteria = new Criteria();
            $criteria->add(Ipviewer::ID_IPVIEWER, $ids, Criteria::IN);
            $this->db->delete(Ipviewer::TABLENAME, array($criteria->createSql()));
        }
        catch(Exception $e)
        {
            throw new IpviewerException("Can't delete that\n" . $e->getMessage());
        }
    }
    
    /**
     * Metodo para Obtener todos los ids en un arreglo
     * @return array
     */
    public function retrieveAllIds()
    {
        return $this->getIdsByCriteria(new Criteria());
    }

    /**
     * Metodo para obtener todos los id de Ipviewer por criterio
     * @param Criteria $criteria
     * @return array Array con todos los id de Ipviewer que encajen en la busqueda
     */
    public function getIdsByCriteria(Criteria $criteria = null)
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        return $this->getCustomFieldByCriteria(Ipviewer::ID_IPVIEWER, $criteria);
    }

    /**
     * Metodo para obtener un campo en particular de un Ipviewer dado un criterio
     * @param string $field
     * @param Criteria $criteria
     * @param $distinct
     * @return array Array con el campo de los objetos Ipviewer que encajen en la busqueda
     */
    public function getCustomFieldByCriteria($field, Criteria $criteria = null, $distinct = false)
    { 
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $distinct = $distinct ? 'DISTINCT' : '';
        try
        {
            $sql = "SELECT {$distinct} {$field}
                    FROM ".Ipviewer::TABLENAME."
                    WHERE  " . $criteria->createSql();
            $result = $this->db->fetchCol($sql);
        } catch(Zend_Db_Exception $e)
        {
            throw new IpviewerException("No se pudieron obtener los fields de objetos Ipviewer\n" . $e->getMessage());
        }
        return $result;
    }

    /**
     * Metodo que regresa una coleccion de objetos Ipviewer 
     * dependiendo del criterio establecido
     * @param Criteria $criteria
     * @return IpviewerCollection $ipviewerCollection
     */
    public function getByCriteria(Criteria $criteria = null)
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $this->db->setFetchMode(Zend_Db::FETCH_ASSOC);
        try 
        {
            $sql = "SELECT * FROM ".Ipviewer::TABLENAME."
                    WHERE " . $criteria->createSql();
            $ipviewerCollection = new IpviewerCollection();
            foreach ($this->db->fetchAll($sql) as $result){
                $ipviewerCollection->append($this->getIpviewerInstance($result));
            }
        }
        catch(Zend_Db_Exception $e)
        {
            throw new IpviewerException("Cant obtain IpviewerCollection\n" . $e->getMessage());
        }
        return $ipviewerCollection;
    }
    
    /**
     * Metodo que cuenta Ipviewer 
     * dependiendo del criterio establecido
     * @param Criteria $criteria
     * @param string $field
     * @return int $count
     */
    public function countByCriteria(Criteria $criteria = null, $field = 'id_ipviewer')
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        try 
        {
            $sql = "SELECT COUNT( $field ) FROM ".Ipviewer::TABLENAME."
                    WHERE " . $criteria->createSql();   
            $count = $this->db->fetchOne($sql);
        }
        catch(Zend_Db_Exception $e)
        {
            throw new IpviewerException("Cant obtain the count \n" . $e->getMessage());
        }
        return $count;
    }
    
    /**
     * Método que construye un objeto Ipviewer y lo rellena con la información del rowset
     * @param array $result El arreglo que devolvió el objeto Zend_Db despues del fetch
     * @return Ipviewer 
     */
    private function getIpviewerInstance($result)
    {
        return IpviewerFactory::createFromArray($result);
    }

    /**
     * Link a Ipviewer to HelixStream
     * @param int $idIpviewer
     * @param int $idStream
     * @param  $timestamp
     * @param  $status
     */
    public function linkToHelixStream($idIpviewer, $idStream, $timestamp, $status)
    {
        try
        {
            $this->unlinkFromHelixStream($idIpviewer, $idStream);
            $data = array(
                'id_ipviewer' => $idIpviewer,
                'id_stream' => $idStream,
                'timestamp' => $timestamp,
                'status' => $status,
            );
            $this->db->insert(Ipviewer::TABLENAME_IPVIEWER_HELIX_STREAM, $data);
        }
        catch(Exception $e)
        {
            throw new IpviewerException("Can't link Ipviewer to HelixStream\n" . $e->getMessage());
        }
    }

    /**
     * Unlink a Ipviewer from HelixStream
     * @param int $idIpviewer
     * @param int $idStream
     */
    public function unlinkFromHelixStream($idIpviewer, $idStream)
    {
        try
        {
            $where = array(
                $this->db->quoteInto('id_ipviewer = ?', $idIpviewer),
                $this->db->quoteInto('id_stream = ?', $idStream),
            );
            $this->db->delete(Ipviewer::TABLENAME_IPVIEWER_HELIX_STREAM, $where);
        }
        catch(Exception $e)
        {
            throw new IpviewerException("Can't unlink Ipviewer to HelixStream\n" . $e->getMessage());
        }
    }

    /**
     * Unlink all HelixStream relations
     * @param int $idIpviewer
     * @param  $timestamp
     * @param  $status
     */
    public function unlinkAllHelixStreamRelations($idIpviewer, $timestamp = null, $status = null)
    {
        try
        {
            $where = array(
                $this->db->quoteInto('id_ipviewer = ?', $idIpviewer),
            );
            if(null != $timestamp) $where[] = $this->db->quoteInto('timestamp = ?', $timestamp);
            if(null != $status) $where[] = $this->db->quoteInto('status = ?', $status);
            $this->db->delete(Ipviewer::TABLENAME_IPVIEWER_HELIX_STREAM, $where);
        }
        catch(Exception $e)
        {
            throw new IpviewerException("Can't unlink all HelixStream relations \n" . $e->getMessage());
        }
    }

    /**
     * Get Ipviewer - HelixStream relations by Criteria
     * @param Criteria $criteria
     * @return array
     */
    public function getIpviewerHelixStreamRelations(Criteria $criteria = null)
    { 
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $this->db->setFetchMode(Zend_Db::FETCH_ASSOC);
        try
        {
           $sql = "SELECT * FROM ". Ipviewer::TABLENAME_IPVIEWER_HELIX_STREAM ."
                   WHERE  " . $criteria->createSql();
           $result = $this->db->fetchAll($sql);
        } catch(Exception $e)
        {
           throw new IpviewerException("Can't obtain relations by criteria\n" . $e->getMessage());
        }
        return $result;
    }

    /**
     * Get IpviewerCollection by HelixStream
     * @param int $idStream
     * @param [|array] $timestamp
     * @param [|array] $status
     * @return IpviewerCollection
     */
    public function getByHelixStream($idStream, $timestamp = null, $status = null)
    {
        $criteria = new Criteria();
        $criteria->add('id_stream', $idStream, Criteria::EQUAL);
        if(null != $timestamp) $criteria->add('timestamp', $timestamp, is_array($timestamp) ? Criteria::IN : Criteria::EQUAL);
        if(null != $status) $criteria->add('status', $status, is_array($status) ? Criteria::IN : Criteria::EQUAL);
        $ipviewerHelixStream = $this->getIpviewerHelixStreamRelations($criteria);
        $ids = array();
        foreach($ipviewerHelixStream as $rs){
            $ids[] = $rs['id_ipviewer'];
        }
        return $this->getByIds($ids);
    }

    /**
     * Link a Ipviewer to HelixVod
     * @param int $idIpviewer
     * @param int $idVod
     * @param  $timestamp
     * @param  $status
     */
    public function linkToHelixVod($idIpviewer, $idVod, $timestamp, $status)
    {
        try
        {
            $this->unlinkFromHelixVod($idIpviewer, $idVod);
            $data = array(
                'id_ipviewer' => $idIpviewer,
                'id_vod' => $idVod,
                'timestamp' => $timestamp,
                'status' => $status,
            );
            $this->db->insert(Ipviewer::TABLENAME_IPVIEWER_HELIX_VOD, $data);
        }
        catch(Exception $e)
        {
            throw new IpviewerException("Can't link Ipviewer to HelixVod\n" . $e->getMessage());
        }
    }

    /**
     * Unlink a Ipviewer from HelixVod
     * @param int $idIpviewer
     * @param int $idVod
     */
    public function unlinkFromHelixVod($idIpviewer, $idVod)
    {
        try
        {
            $where = array(
                $this->db->quoteInto('id_ipviewer = ?', $idIpviewer),
                $this->db->quoteInto('id_vod = ?', $idVod),
            );
            $this->db->delete(Ipviewer::TABLENAME_IPVIEWER_HELIX_VOD, $where);
        }
        catch(Exception $e)
        {
            throw new IpviewerException("Can't unlink Ipviewer to HelixVod\n" . $e->getMessage());
        }
    }

    /**
     * Unlink all HelixVod relations
     * @param int $idIpviewer
     * @param  $timestamp
     * @param  $status
     */
    public function unlinkAllHelixVodRelations($idIpviewer, $timestamp = null, $status = null)
    {
        try
        {
            $where = array(
                $this->db->quoteInto('id_ipviewer = ?', $idIpviewer),
            );
            if(null != $timestamp) $where[] = $this->db->quoteInto('timestamp = ?', $timestamp);
            if(null != $status) $where[] = $this->db->quoteInto('status = ?', $status);
            $this->db->delete(Ipviewer::TABLENAME_IPVIEWER_HELIX_VOD, $where);
        }
        catch(Exception $e)
        {
            throw new IpviewerException("Can't unlink all HelixVod relations \n" . $e->getMessage());
        }
    }

    /**
     * Get Ipviewer - HelixVod relations by Criteria
     * @param Criteria $criteria
     * @return array
     */
    public function getIpviewerHelixVodRelations(Criteria $criteria = null)
    { 
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $this->db->setFetchMode(Zend_Db::FETCH_ASSOC);
        try
        {
           $sql = "SELECT * FROM ". Ipviewer::TABLENAME_IPVIEWER_HELIX_VOD ."
                   WHERE  " . $criteria->createSql();
           $result = $this->db->fetchAll($sql);
        } catch(Exception $e)
        {
           throw new IpviewerException("Can't obtain relations by criteria\n" . $e->getMessage());
        }
        return $result;
    }

    /**
     * Get IpviewerCollection by HelixVod
     * @param int $idVod
     * @param [|array] $timestamp
     * @param [|array] $status
     * @return IpviewerCollection
     */
    public function getByHelixVod($idVod, $timestamp = null, $status = null)
    {
        $criteria = new Criteria();
        $criteria->add('id_vod', $idVod, Criteria::EQUAL);
        if(null != $timestamp) $criteria->add('timestamp', $timestamp, is_array($timestamp) ? Criteria::IN : Criteria::EQUAL);
        if(null != $status) $criteria->add('status', $status, is_array($status) ? Criteria::IN : Criteria::EQUAL);
        $ipviewerHelixVod = $this->getIpviewerHelixVodRelations($criteria);
        $ids = array();
        foreach($ipviewerHelixVod as $rs){
            $ids[] = $rs['id_ipviewer'];
        }
        return $this->getByIds($ids);
    }

    
    /**
     * Link a FlashIpviewer to FlashStream
     * @param int $idIpviewer
     * @param int $idStream
     * @param  $timestamp
     * @param  $status
     */
    public function linkToFlashStream($idIpviewer, $idStream, $timestamp, $status)
    {
        try
        {
            $this->unlinkFromFlashStream($idIpviewer, $idStream);
            $data = array(
                'id_ipviewer' => $idIpviewer,
                'id_stream' => $idStream,
                'timestamp' => $timestamp,
                'status' => $status,
            );
            $this->db->insert(FlashIpviewer::TABLENAME_FLASH_IPVIEWER_FLASH_STREAM, $data);
        }
        catch(Exception $e)
        {
            throw new FlashIpviewerException("Can't link FlashIpviewer to FlashStream\n" . $e->getMessage());
        }
    }

    /**
     * Unlink a FlashIpviewer from FlashStream
     * @param int $idIpviewer
     * @param int $idStream
     */
    public function unlinkFromFlashStream($idIpviewer, $idStream)
    {
        try
        {
            $where = array(
                $this->db->quoteInto('id_ipviewer = ?', $idIpviewer),
                $this->db->quoteInto('id_stream = ?', $idStream),
            );
            $this->db->delete(FlashIpviewer::TABLENAME_FLASH_IPVIEWER_FLASH_STREAM, $where);
        }
        catch(Exception $e)
        {
            throw new FlashIpviewerException("Can't unlink FlashIpviewer to FlashStream\n" . $e->getMessage());
        }
    }

    /**
     * Unlink all FlashStream relations
     * @param int $idIpviewer
     * @param  $timestamp
     * @param  $status
     */
    public function unlinkAllFlashStreamRelations($idIpviewer, $timestamp = null, $status = null)
    {
        try
        {
            $where = array(
                $this->db->quoteInto('id_ipviewer = ?', $idIpviewer),
            );
            if(null != $timestamp) $where[] = $this->db->quoteInto('timestamp = ?', $timestamp);
            if(null != $status) $where[] = $this->db->quoteInto('status = ?', $status);
            $this->db->delete(FlashIpviewer::TABLENAME_FLASH_IPVIEWER_FLASH_STREAM, $where);
        }
        catch(Exception $e)
        {
            throw new FlashIpviewerException("Can't unlink all FlashStream relations \n" . $e->getMessage());
        }
    }

    /**
     * Get FlashIpviewer - FlashStream relations by Criteria
     * @param Criteria $criteria
     * @return array
     */
    public function getFlashIpviewerFlashStreamRelations(Criteria $criteria = null)
    { 
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $this->db->setFetchMode(Zend_Db::FETCH_ASSOC);
        try
        {
           $sql = "SELECT * FROM ". FlashIpviewer::TABLENAME_FLASH_IPVIEWER_FLASH_STREAM ."
                   WHERE  " . $criteria->createSql();
           $result = $this->db->fetchAll($sql);
        } catch(Exception $e)
        {
           throw new FlashIpviewerException("Can't obtain relations by criteria\n" . $e->getMessage());
        }
        return $result;
    }

    /**
     * Get FlashIpviewerCollection by FlashStream
     * @param int $idStream
     * @param [|array] $timestamp
     * @param [|array] $status
     * @return FlashIpviewerCollection
     */
    public function getByFlashStream($idStream, $timestamp = null, $status = null)
    {
        $criteria = new Criteria();
        $criteria->add('id_stream', $idStream, Criteria::EQUAL);
        if(null != $timestamp) $criteria->add('timestamp', $timestamp, is_array($timestamp) ? Criteria::IN : Criteria::EQUAL);
        if(null != $status) $criteria->add('status', $status, is_array($status) ? Criteria::IN : Criteria::EQUAL);
        $flashIpviewerFlashStream = $this->getFlashIpviewerFlashStreamRelations($criteria);
        $ids = array();
        foreach($flashIpviewerFlashStream as $rs){
            $ids[] = $rs['id_ipviewer'];
        }
        return $this->getByIds($ids);
    }

    /**
     * Link a FlashIpviewer to FlashVod
     * @param int $idIpviewer
     * @param int $idVod
     * @param  $timestamp
     * @param  $status
     */
    public function linkToFlashVod($idIpviewer, $idVod, $timestamp, $status)
    {
        try
        {
            $this->unlinkFromFlashVod($idIpviewer, $idVod);
            $data = array(
                'id_ipviewer' => $idIpviewer,
                'id_vod' => $idVod,
                'timestamp' => $timestamp,
                'status' => $status,
            );
            $this->db->insert(FlashIpviewer::TABLENAME_FLASH_IPVIEWER_FLASH_VOD, $data);
        }
        catch(Exception $e)
        {
            throw new FlashIpviewerException("Can't link FlashIpviewer to FlashVod\n" . $e->getMessage());
        }
    }

    /**
     * Unlink a FlashIpviewer from FlashVod
     * @param int $idIpviewer
     * @param int $idVod
     */
    public function unlinkFromFlashVod($idIpviewer, $idVod)
    {
        try
        {
            $where = array(
                $this->db->quoteInto('id_ipviewer = ?', $idIpviewer),
                $this->db->quoteInto('id_vod = ?', $idVod),
            );
            $this->db->delete(FlashIpviewer::TABLENAME_FLASH_IPVIEWER_FLASH_VOD, $where);
        }
        catch(Exception $e)
        {
            throw new FlashIpviewerException("Can't unlink FlashIpviewer to FlashVod\n" . $e->getMessage());
        }
    }

    /**
     * Unlink all FlashVod relations
     * @param int $idIpviewer
     * @param  $timestamp
     * @param  $status
     */
    public function unlinkAllFlashVodRelations($idIpviewer, $timestamp = null, $status = null)
    {
        try
        {
            $where = array(
                $this->db->quoteInto('id_ipviewer = ?', $idIpviewer),
            );
            if(null != $timestamp) $where[] = $this->db->quoteInto('timestamp = ?', $timestamp);
            if(null != $status) $where[] = $this->db->quoteInto('status = ?', $status);
            $this->db->delete(FlashIpviewer::TABLENAME_FLASH_IPVIEWER_FLASH_VOD, $where);
        }
        catch(Exception $e)
        {
            throw new FlashIpviewerException("Can't unlink all FlashVod relations \n" . $e->getMessage());
        }
    }

    /**
     * Get FlashIpviewer - FlashVod relations by Criteria
     * @param Criteria $criteria
     * @return array
     */
    public function getFlashIpviewerFlashVodRelations(Criteria $criteria = null)
    { 
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $this->db->setFetchMode(Zend_Db::FETCH_ASSOC);
        try
        {
           $sql = "SELECT * FROM ". FlashIpviewer::TABLENAME_FLASH_IPVIEWER_FLASH_VOD ."
                   WHERE  " . $criteria->createSql();
           $result = $this->db->fetchAll($sql);
        } catch(Exception $e)
        {
           throw new FlashIpviewerException("Can't obtain relations by criteria\n" . $e->getMessage());
        }
        return $result;
    }

    /**
     * Get FlashIpviewerCollection by FlashVod
     * @param int $idVod
     * @param [|array] $timestamp
     * @param [|array] $status
     * @return FlashIpviewerCollection
     */
    public function getByFlashVod($idVod, $timestamp = null, $status = null)
    {
        $criteria = new Criteria();
        $criteria->add('id_vod', $idVod, Criteria::EQUAL);
        if(null != $timestamp) $criteria->add('timestamp', $timestamp, is_array($timestamp) ? Criteria::IN : Criteria::EQUAL);
        if(null != $status) $criteria->add('status', $status, is_array($status) ? Criteria::IN : Criteria::EQUAL);
        $flashIpviewerFlashVod = $this->getFlashIpviewerFlashVodRelations($criteria);
        $ids = array();
        foreach($flashIpviewerFlashVod as $rs){
            $ids[] = $rs['id_ipviewer'];
        }
        return $this->getByIds($ids);
    }    

} 
 
