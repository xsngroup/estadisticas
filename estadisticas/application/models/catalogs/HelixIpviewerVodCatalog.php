<?php
/**
 * Xsn
 *
 * Xsn
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */

/**
 * Dependences
 */
require_once "lib/db/Catalog.php";
require_once "application/models/beans/HelixIpviewerVod.php";
require_once "application/models/exceptions/HelixIpviewerVodException.php";
require_once "application/models/collections/HelixIpviewerVodCollection.php";
require_once "application/models/factories/HelixIpviewerVodFactory.php";

/**
 * Singleton HelixIpviewerVodCatalog Class
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_catalogs
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     zetta & chentepixtol
 * @version    1.0.2 SVN: $Revision$
 */
class HelixIpviewerVodCatalog extends Catalog
{

    /**
     * Singleton Instance
     * @var HelixIpviewerVodCatalog
     */
    static protected $instance = null;


    /**
     * Método para obtener la instancia del catálogo
     * @return HelixIpviewerVodCatalog
     */
    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
          self::$instance = new self();
        }
        return self::$instance;
    }
    
    /**
     * Constructor de la clase HelixIpviewerVodCatalog
     * @return HelixIpviewerVodCatalog
     */
    protected function HelixIpviewerVodCatalog()
    {
        parent::Catalog();
    }

    /**
     * Metodo para obtener un campo en particular de un HelixIpviewerVod dado un criterio
     * @param string $field
     * @param Criteria $criteria
     * @param $distinct
     * @return array Array con el campo de los objetos HelixIpviewerVod que encajen en la busqueda
     */
    public function getCustomFieldByCriteria($field, Criteria $criteria = null, $distinct = false)
    { 
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $distinct = $distinct ? 'DISTINCT' : '';
        try
        {
            $sql = "SELECT {$distinct} {$field}
                    FROM ".HelixIpviewerVod::TABLENAME."
                    WHERE  " . $criteria->createSql();
            $result = $this->db->fetchCol($sql);
        } catch(Zend_Db_Exception $e)
        {
            throw new HelixIpviewerVodException("No se pudieron obtener los fields de objetos HelixIpviewerVod\n" . $e->getMessage());
        }
        return $result;
    }

    /**
     * Metodo que regresa una coleccion de objetos HelixIpviewerVod 
     * dependiendo del criterio establecido
     * @param Criteria $criteria
     * @return HelixIpviewerVodCollection $helixIpviewerVodCollection
     */
    public function getByCriteria(Criteria $criteria = null)
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $this->db->setFetchMode(Zend_Db::FETCH_ASSOC);
        try 
        {
            $sql = "SELECT * FROM ".HelixIpviewerVod::TABLENAME."
                    WHERE " . $criteria->createSql();
            $helixIpviewerVodCollection = new HelixIpviewerVodCollection();
            foreach ($this->db->fetchAll($sql) as $result){
                $helixIpviewerVodCollection->append($this->getHelixIpviewerVodInstance($result));
            }
        }
        catch(Zend_Db_Exception $e)
        {
            throw new HelixIpviewerVodException("Cant obtain HelixIpviewerVodCollection\n" . $e->getMessage());
        }
        return $helixIpviewerVodCollection;
    }
    
    /**
     * Metodo que cuenta HelixIpviewerVod 
     * dependiendo del criterio establecido
     * @param Criteria $criteria
     * @param string $field
     * @return int $count
     */
    public function countByCriteria(Criteria $criteria = null, $field = '')
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        try 
        {
            $sql = "SELECT COUNT( $field ) FROM ".HelixIpviewerVod::TABLENAME."
                    WHERE " . $criteria->createSql();   
            $count = $this->db->fetchOne($sql);
        }
        catch(Zend_Db_Exception $e)
        {
            throw new HelixIpviewerVodException("Cant obtain the count \n" . $e->getMessage());
        }
        return $count;
    }
    
    /**
     * Método que construye un objeto HelixIpviewerVod y lo rellena con la información del rowset
     * @param array $result El arreglo que devolvió el objeto Zend_Db despues del fetch
     * @return HelixIpviewerVod 
     */
    private function getHelixIpviewerVodInstance($result)
    {
        return HelixIpviewerVodFactory::createFromArray($result);
    }
  
    /**
     * Obtiene un HelixIpviewerVodCollection  dependiendo del idVod
     * @param int $idVod  
     * @return HelixIpviewerVodCollection 
     */
    public function getByIdVod($idVod)
    {
        $criteria = new Criteria();
        $criteria->add(HelixIpviewerVod::ID_VOD, $idVod, Criteria::EQUAL);
        $helixIpviewerVodCollection = $this->getByCriteria($criteria);
        return $helixIpviewerVodCollection;
    }

    /**
     * Metodo que regresa una coleccion de objetos HelixIpviewerVod con Status 'Active'
     * dependiendo del criterio establecido
     * @param Criteria $criteria
     * @return HelixIpviewerVodCollection $helixIpviewerVodCollection
     */
    public function getActives(Criteria $criteria = null)
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $criteria->add(HelixIpviewerVod::STATUS, HelixIpviewerVod::$Status['Active'], Criteria::EQUAL);
        return $this->getByCriteria($criteria);
    }
    
    /**
     * Metodo que regresa una coleccion de objetos HelixIpviewerVod con Status 'Inactive'
     * dependiendo del criterio establecido
     * @param Criteria $criteria
     * @return HelixIpviewerVodCollection $helixIpviewerVodCollection
     */
    public function getInactives(Criteria $criteria = null)
    {
        $criteria = (null === $criteria) ? new Criteria() : $criteria;
        $criteria->add(HelixIpviewerVod::STATUS, HelixIpviewerVod::$Status['Inactive'], Criteria::EQUAL);
        return $this->getByCriteria($criteria);
    }
    
    /**
     * Activate a helixIpviewerVod
     * @param HelixIpviewerVod $helixIpviewerVod
     */ 
    public function activate($helixIpviewerVod)
    {
        if(!($helixIpviewerVod instanceof HelixIpviewerVod))
            throw new HelixIpviewerVodException("passed parameter isn't a HelixIpviewerVod instance");
        if(HelixIpviewerVod::$Status['Active'] != $helixIpviewerVod->getStatus())
        {
            $helixIpviewerVod->setStatus(HelixIpviewerVod::$Status['Active']);
            $this->save($helixIpviewerVod);
        }
    }
    
    /**
     * Deactivate a helixIpviewerVod
     * @param HelixIpviewerVod $helixIpviewerVod
     */ 
    public function deactivate($helixIpviewerVod)
    {
        if(!($helixIpviewerVod instanceof HelixIpviewerVod))
            throw new HelixIpviewerVodException("passed parameter isn't a HelixIpviewerVod instance");
        if(HelixIpviewerVod::$Status['Inactive'] != $helixIpviewerVod->getStatus())
        {
            $helixIpviewerVod->setStatus(HelixIpviewerVod::$Status['Inactive']);
            $this->save($helixIpviewerVod);
        }
    }


    /**
     * Bender cant implement this methods, make sure you have a primaryField 
     * in your "xsn_core_helix_ipviewer_vod" table
     */
    public function create($helixIpviewerVod){ //throw new Exception('Method not implemented'); 
    }
    public function delete($helixIpviewerVod){ //throw new Exception('Method not implemented'); 
    }
    public function update($helixIpviewerVod){ //throw new Exception('Method not implemented'); 
    }
    public function getByIds($id){ //throw new Exception('Method not implemented'); 
    }
    public function getById($id){ //throw new Exception('Method not implemented'); 
    }
    public function deleteById($id){ //throw new Exception('Method not implemented'); 
    }
    public function retrieveAllIds(){ //throw new Exception('Method not implemented'); 
    }
    public function getIdsByCriteria(Criteria $criteria=null){ //throw new Exception('Method not implemented'); 
    }


} 
 
