<?php
/**
 * Xsn
 *
 * Xsn Monitor Stream Servers
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-2011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */
/**
 * Dependences
 */
require_once "application/models/beans/FlashAppStat.php";

/**
 * Clase FlashAppStatFactory
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_factories
 * @copyright  Copyright (c) 2010-2011 Xsn Group (http://www.xsn.com.mx) 
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     <zetta> & <chentepixtol>
 * @version    1.0.2 SVN: $Revision$
 */
class FlashAppStatFactory
{

   /**
    * Create a new FlashAppStat instance
    * @param string $timestamp
    * @param int $accepted
    * @param int $bytesIn
    * @param int $bytesOut
    * @param int $connected
    * @param int $rejected
    * @param string $launchTime
    * @param int $bwIn
    * @param int $bwOut
    * @param int $totalConnects
    * @param int $totalDisconnects
    * @param int $totalInstancesLoaded
    * @param int $totalInstancesUnloaded
    * @param int $upTime
    * @param int $idApp
    * @return FlashAppStat
    */
   public static function create($timestamp, $accepted, $bytesIn, $bytesOut, $connected, $rejected, $launchTime, $bwIn, $bwOut, $totalConnects, $totalDisconnects, $totalInstancesLoaded, $totalInstancesUnloaded, $upTime, $idApp)
   {
      throw new Exception('Factory Deprecated');
      $newFlashAppStat = new FlashAppStat();
      $newFlashAppStat
          ->setTimestamp($timestamp)
          ->setAccepted($accepted)
          ->setBytesIn($bytesIn)
          ->setBytesOut($bytesOut)
          ->setConnected($connected)
          ->setRejected($rejected)
          ->setLaunchTime($launchTime)
          ->setBwIn($bwIn)
          ->setBwOut($bwOut)
          ->setTotalConnects($totalConnects)
          ->setTotalDisconnects($totalDisconnects)
          ->setTotalInstancesLoaded($totalInstancesLoaded)
          ->setTotalInstancesUnloaded($totalInstancesUnloaded)
          ->setUpTime($upTime)
          ->setIdApp($idApp)
      ;
      return $newFlashAppStat;
   }
   
    /**
     * Método que construye un objeto FlashAppStat y lo rellena con la información del rowset
     * @param array $fields El arreglo que devolvió el objeto Zend_Db despues del fetch
     * @return FlashAppStat 
     */
    public static function createFromArray($fields)
    {
        $newFlashAppStat = new FlashAppStat();
        $newFlashAppStat->setIdAppStat($fields['id_app_stat']);
        $newFlashAppStat->setTimestamp($fields['timestamp']);
        $newFlashAppStat->setAccepted($fields['accepted']);
        $newFlashAppStat->setBytesIn($fields['bytes_in']);
        $newFlashAppStat->setBytesOut($fields['bytes_out']);
        $newFlashAppStat->setConnected($fields['connected']);
        $newFlashAppStat->setRejected($fields['rejected']);
        $newFlashAppStat->setLaunchTime($fields['launch_time']);
        $newFlashAppStat->setBwIn($fields['bw_in']);
        $newFlashAppStat->setBwOut($fields['bw_out']);
        $newFlashAppStat->setTotalConnects($fields['total_connects']);
        $newFlashAppStat->setTotalDisconnects($fields['total_disconnects']);
        $newFlashAppStat->setTotalInstancesLoaded($fields['total_instances_loaded']);
        $newFlashAppStat->setTotalInstancesUnloaded($fields['total_instances_unloaded']);
        $newFlashAppStat->setUpTime($fields['up_time']);
        $newFlashAppStat->setIdApp($fields['id_app']);
        return $newFlashAppStat;
    }
   
}
