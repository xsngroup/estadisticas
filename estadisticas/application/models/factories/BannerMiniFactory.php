<?php
/**
 * Xsn
 *
 * Xsn
 *
 * @category   lib
 * @package    lib_models
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx)
 * @author     <irgg>, $LastChangedBy$
 * @version    1.0.2 SVN: $Id$
 */
/**
 * Dependences
 */
require_once "application/models/beans/BannerMini.php";

/**
 * Clase BannerMiniFactory
 *
 * @category   lib
 * @package    lib_models
 * @subpackage lib_models_factories
 * @copyright  Copyright (c) 2010-1011 Xsn Group (http://www.xsn.com.mx) 
 * @copyright  This File has been proudly generated by Bender (http://code.google.com/p/bender-modeler/). <chentepixtol> <zetta>
 * @author     <zetta> & <chentepixtol>
 * @version    1.0.2 SVN: $Revision$
 */
class BannerMiniFactory
{

   /**
    * Create a new BannerMini instance
    * @param string $name
    * @return BannerMini
    */
   public static function create($name)
   {
      $newBannerMini = new BannerMini();
      $newBannerMini
          ->setName($name)
      ;
      return $newBannerMini;
   }
   
    /**
     * Método que construye un objeto BannerMini y lo rellena con la información del rowset
     * @param array $fields El arreglo que devolvió el objeto Zend_Db despues del fetch
     * @return BannerMini 
     */
    public static function createFromArray($fields)
    {
        $newBannerMini = new BannerMini();
        $newBannerMini->setIdBanner($fields['id_banner']);
        $newBannerMini->setName($fields['name']);
        return $newBannerMini;
    }
   
}
