<script type="text/javascript" src="{$baseUrl}/js/modules/menu/manage.js"></script>
<div class="column_left">
    <div class="header"><span>{$l10n->_('Opciones disponibles')}</span></div>
    <br class="clear" />
    <div class="content"> 
    <table class="wide" width="100%" cellspacing="0" cellpadding="0" class="data">
        <tbody>
            {foreach from=$controllers item=controller}
                <tr>
                    <td><img src="{$baseUrl}/images/template/icons/magnifier.png" alt="toggle" id="parent_{$controller->getIdController()}" class="parentMenu" /></td>
                    <th>{$controller->getName()}</th>
                    <th>(0)</th>
                </tr>
                  {foreach from=$actions item=action}
                    {if $action->getIdController() == $controller->getIdController()}
                  <tr class="child childOf{$controller->getIdController()}" id="action_item_{$action->getIdAction()}">
                    <td></td>
                    <td>{$action->getName()}</td>
                    <td>
                        <img src="{$baseUrl}/images/template/icons/add.png" alt="add" id="add_child_{$action->getIdAction()}" class="moveItem" />
                    </td>
            
                   </tr>
                    {/if}
                  {/foreach}
            {/foreach}
        </tbody>
    </table>               
</div>
</div>
<div class="column_right">
    <div class="header"><span>{$l10n->_('Resultado de Men�')}</span></div>
    <br class="clear" />
    <div class="content">
      <p>
                <label for="name">{$l10n->_('Nombre')}</label>
                <input id="name" name="name" value="">
       </p> 
       <br/>   
    {$menuItems}
	</div>
</div>    