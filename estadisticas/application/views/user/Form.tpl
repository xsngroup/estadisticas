<div class="onecolumn">
 <div class="header"><span>{$l10n->_('Listado de Usuarios')}</span></div>
 <br class="clear" />
 <div class="content">
<form action="{$baseUrl}/user/{if $post.id}update{else}create{/if}/" method="post" class="validate">
<h4>{$l10n->_('Guardar nuevo usuario')}</h4>
<p>
<label for="username">{$l10n->_('Usuario')}:</label><br/>
<input type="text" name="username" id="username" value="{$post.username}" size="40" class="required" {if $isNew==false}readonly="readonly"{/if} />
</p>
<p>
<label for="name">{$l10n->_('Nombre')}:</label><br/>
<input type="text" name="name" id="name" value="{$post.name}" size="40" class="required"/>
</p>
<p>
<label for="middlename">{$l10n->_('Apellido Paterno')}:</label><br/>
<input type="text" name="middlename" id="middlename" value="{$post.middlename}" size="40" class="required"/>
</p>
<p>
<label for="lastname">{$l10n->_('Apellido Materno')}:</label><br/>
<input type="text" name="lastname" id="lastname" value="{$post.lastname}" size="40" />
</p>
<p> 
<label for="password">{$l10n->_('Contraseņa')}:</label><br/>
<input type="password" name="password" id="password" value="" size="40" class="{if $isNew}required{/if}"/>
</p>
<p> 
<label for="passwordConfirm">{$l10n->_('Confirmar Contraseņa')}:</label><br/>
<input type="password" name="passwordConfirm" id="passwordConfirm" value="" size="40"  class="{if $isNew}required{/if}"/>
</p>
<p> 
<label for="group">{$l10n->_('Grupo')}:</label><br/>
{html_options name=accessRole id=accessRole options=$accessRoles selected=$post.accessRole class=required}
</p>
<p> 
<label for="group">{$l10n->_('Estado civil')}:</label><br/>
{html_options name=maritalStatus id=maritalStatus options=$maritalStatus selected=$post.maritalStatus}
</p>
<br/>
<p> 
  <input type="submit" value="{$l10n->_('Guardar')}"/>
  <input type="button" value="{$l10n->_('Cancelar')}"/>
  <input type="hidden" name="id" value="{$post.id}" id="id" />
</p>
</form>
 </div>
</div>