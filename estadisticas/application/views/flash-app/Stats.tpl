
<div class="column_left">
 <div class="header"><span>{$l10n->_('Información del servidor')}</span></div>
 <br class="clear" />
 <div class="content">
<form action="#" class="validate" method="post">
<table width="100%" cellspacing="0" cellpadding="0" class="data"> 
  <tbody> 
     <tr>
                <th align="right">{$l10n->_('Cliente:')}</th>
                <td class="last">{$company->getNameCompany()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Contacto:')}</th>
                <td class="last">{$company->getNameContac()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Teléfono:')}</th>
                <td class="last">{$company->getTelephone()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Ip del Servidor:')}</th>
                <td class="last">{$server->getUrl()}</td>       
     </tr>                 
  </tbody>
</table>

</form>
</div>
</div>

<div class="column_right">
    <div class="header"><span>{$l10n->_('Estadística de aplicaciones')}</span></div>
    <br class="clear" />
    <div class="content">
<form action="#" class="validate" method="post">
    <div id="loadStats">
<script type="text/javascript" src="{$baseUrl}/js/modules/flash-app/script.js"></script>  
 
{foreach $result as $item}
{if $flagLive==true} 
<table width="100%" cellspacing="0" cellpadding="0" class="data"> 
  <tbody>
     <tr>
                <th align="right">{$l10n->_('Aplicación:')}</th>
                <td class="last">{$apps[$item->getIdApp()]}</td>       
     </tr>  
     <tr>
                <th align="right">{$l10n->_('Fecha y hora:')}</th>
                <td class="last">{$item->getTimestamp()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Bytes de entrada:')}{icon src=information class=tip style="padding-left:5px;" title="Total de bytes recibidos por la aplicación"}</th>
                <td class="last">{$item->getBytesIn()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Bytes de salida:')}{icon src=information class=tip style="padding-left:5px;" title="Total de bytes enviados por la aplicación"}</th>
                <td class="last">{$item->getBytesOut()}</td>       
     </tr>
     
     <tr>
                <th align="right">{$l10n->_('Conectados actuales:')}{icon src=information class=tip style="padding-left:5px;" title="Usuarios conectados actualmente"}</th>
                <td class="last">                
                {if $apps[$item->getIdApp()]=='live'}
                {$concurrentLive}
                {else}
                {$item->getConnected()}
                {/if}
                </td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Conexiones aceptadas:')}{icon src=information class=tip style="padding-left:5px;" title="Total de conexiones (viewers) aceptadas por la aplicación"}</th>
                <td class="last">{$item->getAccepted()}</td>       
     </tr>     
     <tr>
                <th align="right">{$l10n->_('Conexiones Rechazadas:')}{icon src=information class=tip style="padding-left:5px;" title="Total de conexiones (viewers) rechazadas por la aplicación"}</th>
                <td class="last">{$item->getRejected()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Inicio de ejecución:')}{icon src=information class=tip style="padding-left:5px;" title="Fecha y hora en que se inició la aplicación"}</th>
                <td class="last">{$item->getLaunchTime()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Ancho de banda de entrada:')}{icon src=information class=tip style="padding-left:5px;" title="Ancho de banda consumido por la aplicación"}</th>
                <td class="last">{$item->getBwIn()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Ancho de banda de salida:')}{icon src=information class=tip style="padding-left:5px;" title="Ancho de banda consumido por la aplicación"}</th>
                <td class="last">{$item->getBwOut()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Total conectados:')}{icon src=information class=tip style="padding-left:5px;" title="Total de conectados (viewers) a la aplicación desde su inició de ejecución"}</th>
                <td class="last">{$item->getTotalConnects()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Total desconectados:')}{icon src=information class=tip style="padding-left:5px;" title="Total de desconectados (viewers) a la aplicación desde su inició de ejecución"}</th>
                <td class="last">{$item->getTotalDisconnects()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Total de instancias cargadas:')}</th>
                <td class="last">{$item->getTotalInstancesLoaded()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Total de instancias cerradas:')}</th>
                <td class="last">{$item->getTotalInstancesUnloaded()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Tiempo transcurrido:')}{icon src=information class=tip style="padding-left:5px;" title="Tiempo transcurrido en segundos desde que se inició la ejecución de la aplicación"}</th>
                <td class="last">{$item->getUpTime()}</td>       
     </tr>
                                                                        
 
  </tbody>
</table>
{else}
 {if $apps[$item->getIdApp()]!='live'}
	<table width="100%" cellspacing="0" cellpadding="0" class="data"> 
	  <tbody>
	     <tr>
	                <th align="right">{$l10n->_('Aplicación:')}</th>
	                <td class="last">{$apps[$item->getIdApp()]}</td>       
	     </tr>  
	     <tr>
	                <th align="right">{$l10n->_('Fecha y hora:')}</th>
	                <td class="last">{$item->getTimestamp()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Bytes de entrada:')}{icon src=information class=tip style="padding-left:5px;" title="Total de bytes recibidos por la aplicación"}</th>
	                <td class="last">{$item->getBytesIn()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Bytes de salida:')}{icon src=information class=tip style="padding-left:5px;" title="Total de bytes enviados por la aplicación"}</th>
	                <td class="last">{$item->getBytesOut()}</td>       
	     </tr>
	     
	     <tr>
	                <th align="right">{$l10n->_('Conectados actuales:')}{icon src=information class=tip style="padding-left:5px;" title="Usuarios conectados actualmente"}</th>
	                <td class="last">
	                {$item->getConnected()}
	                </td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Conexiones aceptadas:')}{icon src=information class=tip style="padding-left:5px;" title="Total de conexiones (viewers) aceptadas por la aplicación"}</th>
	                <td class="last">{$item->getAccepted()}</td>       
	     </tr>     
	     <tr>
	                <th align="right">{$l10n->_('Conexiones Rechazadas:')}{icon src=information class=tip style="padding-left:5px;" title="Total de conexiones (viewers) rechazadas por la aplicación"}</th>
	                <td class="last">{$item->getRejected()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Inicio de ejecución:')}{icon src=information class=tip style="padding-left:5px;" title="Fecha y hora en que se inició la aplicación"}</th>
	                <td class="last">{$item->getLaunchTime()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Ancho de banda de entrada:')}{icon src=information class=tip style="padding-left:5px;" title="Ancho de banda consumido por la aplicación"}</th>
	                <td class="last">{$item->getBwIn()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Ancho de banda de salida:')}{icon src=information class=tip style="padding-left:5px;" title="Ancho de banda consumido por la aplicación"}</th>
	                <td class="last">{$item->getBwOut()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Total conectados:')}{icon src=information class=tip style="padding-left:5px;" title="Total de conectados (viewers) a la aplicación desde su inició de ejecución"}</th>
	                <td class="last">{$item->getTotalConnects()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Total desconectados:')}{icon src=information class=tip style="padding-left:5px;" title="Total de desconectados (viewers) a la aplicación desde su inició de ejecución"}</th>
	                <td class="last">{$item->getTotalDisconnects()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Total de instancias cargadas:')}</th>
	                <td class="last">{$item->getTotalInstancesLoaded()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Total de instancias cerradas:')}</th>
	                <td class="last">{$item->getTotalInstancesUnloaded()}</td>       
	     </tr>
	     <tr>
	                <th align="right">{$l10n->_('Tiempo transcurrido:')}{icon src=information class=tip style="padding-left:5px;" title="Tiempo transcurrido en segundos desde que se inició la ejecución de la aplicación"}</th>
	                <td class="last">{$item->getUpTime()}</td>       
	     </tr>
	                                                                        
	 
	  </tbody>
	</table>
 {/if}
{/if}
<br/>
{foreachelse}
                <span>No existe información por el momento...</span>         
{/foreach} 

</div>   
</form>

</div>
</div>    