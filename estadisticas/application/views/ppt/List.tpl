<form action="{url action=create}" method="post" class="validate ajaxForm">
<table class="center">
    <caption>{$i18n->_('Ppt')}</caption>
    <tfoot>
        <tr>
            <td colspan="2"><input type="submit" value="{$i18n->_('Save')}" /></td>
        </tr>
    </tfoot>
    <tbody>
        {include file='ppt/Form.tpl'}
    </tbody>
</table>
</form>
<hr/>


<table class="center">
    <caption>{$i18n->_('List')}</caption>
    <thead>
        <tr>
            <td>{$i18n->_('IdPpt')}</td>
            <td>{$i18n->_('Name')}</td>
            <td>{$i18n->_('Code')}</td>
            <td colspan="2">{$i18n->_('Actions')}</td>
        </tr>
    </thead>
    <tbody id="ajaxList">
        {foreach $ppts as $ppt}
            <tr class="{$ppt@iteration|odd}">
                <td>{$ppt->getIdPpt()}</td>
                <td>{$ppt->getName()}</td>
                <td>{$ppt->getCode()}</td>
                <td><a href="{url action=edit idPpt=$ppt->getIdPpt()}">{icon src=pencil class=tip title=$i18n->_('Edit')}</a></td>
                <td><a href="{url action=delete idPpt=$ppt->getIdPpt()}" class="confirm">{icon src=delete class=tip title=$i18n->_('Delete')}</a></td>
            </tr>
        {/foreach}
    </tbody>
</table>

