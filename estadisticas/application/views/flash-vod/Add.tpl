<script type="text/javascript" src="{$baseUrl}/js/modules/flash-vod/script.js"></script> 
<div class="onecolumn">
    <div class="header"><span>{$l10n->_('Cat�logo de VOD stream')}</span></div>
    <br class="clear" />
    <div class="content">
<form action="{url action=create}" method="post" class="validate ajaxForm">
{include file='forms/FlashVod.tpl'}
<br/>
<p>
  <input type="submit" value="{$l10n->_('Guardar')}" />
</p>
</form>
	</div>
</div>

<div class="onecolumn">
    <div class="header"><span>{$l10n->_('Listado de VOD stream')}</span></div>
    <br class="clear" />
    <div class="content">
	<p>
	    <label>{$l10n->_('Filtrar por servidor:')}</label><br/>
	    {html_options name=servidor id=servidor options=$servers}
	</p>
	<br/>
<table width="100%" cellspacing="0" cellpadding="0" class="data">
    <caption>{$l10n->_('List')}</caption>
    <thead>
        <tr>
            <th>{$l10n->_('Id')}</th>
            <th>{$l10n->_('Prefijo')}</th>
            <th>{$l10n->_('Nombre')}</th><!--
            <th>{$l10n->_('T�tulo')}</th>
            <th>{$l10n->_('Descripci�n')}</th>
            --><th>{$l10n->_('Servidor')}</th>
            <th colspan="2">{$l10n->_('Editar')}</th>
        </tr>
    </thead>
    <tbody id="ajaxList">
           <tr>
              <td colspan="6" align="center">Elige un servidor para filtrar los datos...</td>
           </tr>    
    </tbody>
</table>
	</div>
</div>

