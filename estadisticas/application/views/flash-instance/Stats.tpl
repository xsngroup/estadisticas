
<div class="column_left">
 <div class="header"><span>{$l10n->_('Informaci�n del servidor')}</span></div>
 <br class="clear" />
 <div class="content">
<form action="#" class="validate" method="post">
<table width="100%" cellspacing="0" cellpadding="0" class="data"> 
  <tbody> 
     <tr>
                <th align="right">{$l10n->_('Cliente:')}</th>
                <td class="last">{$company->getNameCompany()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Contacto:')}</th>
                <td class="last">{$company->getNameContac()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Tel�fono:')}</th>
                <td class="last">{$company->getTelephone()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Ip del Servidor:')}</th>
                <td class="last">{$server->getUrl()}</td>       
     </tr>                 
  </tbody>
</table>

</form>
</div>
</div>

<div class="column_right">
    <div class="header"><span>{$l10n->_('Estad�stica de LiveStream')}</span></div>
    <br class="clear" />
    <div class="content">
<form action="#" class="validate" method="post">
    <div id="loadStats">
<script type="text/javascript" src="{$baseUrl}/js/modules/flash-instance/script.js"></script>    
  {foreach $result as $item} 
<table width="100%" cellspacing="0" cellpadding="0" class="data"> 
  <tbody>
     <tr>
                <th align="right">{$l10n->_('Aplicaci�n:')}</th>
                <td class="last">{$instances[$item->getIdInstance()]}</td>       
     </tr>  
     <tr>
                <th align="right">{$l10n->_('Fecha y hora:')}</th>
                <td class="last">{$item->getTimestamp()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Conectados concurrentes:')}{icon src=information class=tip style="padding-left:5px;" title="Usuarios conectados actualmente"}</th>
                <td class="last">
                {$totalCurrent}
                </td>                                       
     </tr>     
     <tr>
                <th align="right">{$l10n->_('Bytes de entrada:')}{icon src=information class=tip style="padding-left:5px;" title="Total de bytes recibidos por el servidor"}</th>
                <td class="last">{$item->getBytesIn()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Bytes de salida:')}{icon src=information class=tip style="padding-left:5px;" title="Total de bytes enviados por el servidor"}</th>
                <td class="last">{$item->getBytesOut()}</td>       
     </tr>     
     <tr>
                <th align="right">{$l10n->_('Conexiones aceptadas:')}{icon src=information class=tip style="padding-left:5px;" title="Total de conexiones (viewers) aceptadas por el servidor"}</th>
                <td class="last">{$item->getAccepted()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Conexiones Rechazadas:')}{icon src=information class=tip style="padding-left:5px;" title="Total de conexiones (viewers) rechazadas por el servidor"}</th>
                <td class="last">{$item->getRejected()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Inicio de ejecuci�n:')}{icon src=information class=tip style="padding-left:5px;" title="Fecha y hora en que se ejecut� el punto de montaje"}</th>
                <td class="last">{$item->getLaunchTime()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Total conectados:')}{icon src=information class=tip style="padding-left:5px;" title="Total de conectados (viewers) al servidor desde la ejecuci�n del punto de montaje"}</th>
                <td class="last">{$item->getTotalConnects()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Total desconectados:')}{icon src=information class=tip style="padding-left:5px;" title="Total de desconectados (viewers) al servidor desde la ejecuci�n del punto de montaje"}</th>
                <td class="last">{$item->getTotalDisconnects()}</td>       
     </tr>                                                                        
  </tbody>
</table>
<br/>
   {foreachelse}
                <span>No existe informaci�n por el momento...</span>         
   {/foreach} 
</div>   
</form>

</div>
</div>    