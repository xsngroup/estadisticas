<div class="onecolumn">
 <div class="header"><span>{$l10n->_('Informaci�n de se�ales en vivo')}</span></div>
 <br class="clear" />
 <div class="content">
 {assign var="indi" value=0} 
 {foreach $liveArray as $live}
    {if $indi==0}
    {assign var="liveData" value=0}
    {$liveData=$live->getName()} 
     <span>Punto de montaje: <b>{$live->getName()}</b></span>
    {/if}
    {$indi=$indi+1}
 {/foreach}
 
 <center><div id="testPlayer"></div></center>
 
 <div id="loadStats">
<script type="text/javascript" src="{$baseUrl}/js/modules/helix-stream/script.js"></script>
 <style>
	dl {
	  margin:0;  
	  width:100%; 
	  height:45px; 
	  background:#fff;
	  }
	
	dt {
	  text-align:center; 
	  font-size:1.5em; 
	  border-bottom:3px solid #fff;
	  }
	
	dd {
	  margin:0; 
	  display:block; 
	  width:100%; 
	  height:45px; 
	  background:#3D444C; 
	  border-bottom:1px solid #fff;
	  }
	
	dd b {
	  float:right;
	  display:block; 
	  margin-left:auto; 
	  background:#EBEBEB; 
	  height:45px; 
	  line-height:2em; 
	  text-align:right;
	  }
	  
	.labelProtocol{
		float: right;
		position: absolute;
		text-align: right;
		width: 37%;
		z-index: 100;	
		height: 45px; 
	    margin-top: 15px; 
	    color: #777B7F;  
    }
    
    table.data tr th {
       width:50% !important;	
    } 
     
    .last{
   	   vertical-align:middle;
    }
    </style>
 {assign var=incremento value=0}         
 {foreach $liveStats as $item}
 
       {assign var=percentRtmp value="."|explode:(round(100-((int)$rtmp[$incremento]*100)/$sumaProtocol[$incremento]))}
       {assign var=percentRtsp value="."|explode:(round(100-((int)$rtsp[$incremento]*100)/$sumaProtocol[$incremento]))}
       {assign var=percentHttp value="."|explode:(round(100-((int)$http[$incremento]*100)/$sumaProtocol[$incremento]))} 
       
       {assign var=percentWindows value="."|explode:(round(100-((int)$windows[$incremento]*100)/$sumaProtocol[$incremento]))}
       {assign var=percentBb value="."|explode:(round(100-((int)$bb[$incremento]*100)/$sumaProtocol[$incremento]))}
       {assign var=percentAndroid value="."|explode:(round(100-((int)$android[$incremento]*100)/$sumaProtocol[$incremento]))}
       {assign var=percentIphone value="."|explode:(round(100-((int)$iphone[$incremento]*100)/$sumaProtocol[$incremento]))}
       {assign var=percentIpod value="."|explode:(round(100-((int)$ipod[$incremento]*100)/$sumaProtocol[$incremento]))}
       {assign var=percentIpad value="."|explode:(round(100-((int)$ipad[$incremento]*100)/$sumaProtocol[$incremento]))}
       {assign var=percentOther value="."|explode:(round(100-((int)$other[$incremento]*100)/$sumaProtocol[$incremento]))} 
 <br/>
<table width="100%" cellspacing="0" cellpadding="0" class="data"> 
  <tbody>
     <tr>
                <th align="right">{$l10n->_('Stream:')}</th>
                <td class="last">{$stream[$item->getIdStream()]}</td>       
     </tr>  
     <tr>
                <th align="right">{$l10n->_('Fecha y hora:')}</th>
                <td class="last">{$item->getTimestamp()}</td>       
     </tr>  
     <tr>
                <th align="right" style="vertical-align: middle;">{$l10n->_('Protocolo rtmp:')}</th>
                <td class="last">
                 <dl><dd><b style="width: {(int)$percentRtmp[0]}%;"></b><span class="labelProtocol">{100-(int)$percentRtmp[0]}%</span></dd></dl>
                </td>       
     </tr>
     <tr>
                <th align="right" style="vertical-align: middle;">{$l10n->_('Procotolo rtsp:')}</th>
                <td class="last">
                 <dl><dd><b style="width: {(int)$percentRtsp[0]}%;"></b><span class="labelProtocol">{100-(int)$percentRtsp[0]}%</span></dd></dl>                
                </td>         
     </tr>  
     <tr>
                <th align="right" style="vertical-align: middle;">{$l10n->_('Protocolo http:')}</th>
                <td class="last">
                 <dl><dd><b style="width: {(int)$percentHttp[0]}%;"></b><span class="labelProtocol">{100-(int)$percentHttp[0]}%</span></dd></dl>                
                </td>        
     </tr>
     <tr>
                <th align="right"><img src="{$baseUrl}/images/template/theme/win.png" title="Windows" /></th>
                <td class="last">
                 <dl><dd><b style="width: {(int)$percentWindows[0]}%;"></b><span class="labelProtocol">Sistema Operativo Windows={100-(int)$percentWindows[0]}%</span></dd></dl>                
                </td>                       
     </tr>  
     <tr>
                <th align="right"><img src="{$baseUrl}/images/template/theme/blackberry.png" title="BlackBerry" /></th>
                <td class="last">
                 <dl><dd><b style="width: {(int)$percentBb[0]}%;"></b><span class="labelProtocol">Dispositivos BlackBerry={100-(int)$percentBb[0]}%</span></dd></dl>                
                </td>       
     </tr>
     <tr>
                <th align="right"><img src="{$baseUrl}/images/template/theme/android.png" title="Android" /></th>
                <td class="last">
                 <dl><dd><b style="width: {(int)$percentAndroid[0]}%;"></b><span class="labelProtocol">Dispositivos Android={100-(int)$percentAndroid[0]}%</span></dd></dl>                
                </td>        
     </tr>  
     <tr>
                <th align="right"><img src="{$baseUrl}/images/template/theme/iphone.png" title="Iphone" /></th>
                <td class="last">
                 <dl><dd><b style="width: {(int)$percentIphone[0]}%;"></b><span class="labelProtocol">Dispositivos Iphone={100-(int)$percentIphone[0]}%</span></dd></dl>                
                </td>        
     </tr>
     <tr>
                <th align="right"><img src="{$baseUrl}/images/template/theme/ipod.png" title="Ipod" /></th>
                <td class="last">
                 <dl><dd><b style="width: {(int)$percentIpod[0]}%;"></b><span class="labelProtocol">Dispositivos Ipod={100-(int)$percentIpod[0]}%</span></dd></dl>                
                </td>      
     </tr>  
     <tr>
                <th align="right"><img src="{$baseUrl}/images/template/theme/ipad.png" title="Ipad" /></th>
                <td class="last">
                 <dl><dd><b style="width: {(int)$percentIpad[0]}%;"></b><span class="labelProtocol">Dispositivos Ipad={100-(int)$percentIpad[0]}%</span></dd></dl>                
                </td>       
     </tr>   
     <tr>
                <th align="right"><img src="{$baseUrl}/images/template/theme/other.png" title="Otros" /></th>
                <td class="last">
                 <dl><dd><b style="width: {(int)$percentOther[0]}%;"></b><span class="labelProtocol">Otros dispositivos={100-(int)$percentOther[0]}%</span></dd></dl>                
                </td>        
     </tr>                            
     <!--
     <tr>
                <th align="right">{$l10n->_('Estado:')}</th>
                <td class="last">{if $item->getType()=="publishing"}<label style="color: green;">online</label>{else}<label style="color: red;">{$item->getType()}</label>{/if}</td>       
     </tr>      
     <tr>
                <th align="right">{$l10n->_('Id:')}</th>
                <td class="last">{$item->getClient()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Inicio de ejecuci�n:')}</th>
                <td class="last">{$item->getPublishTime()}</td>       
     </tr>
         -->                                                              
     <tr>
               <th align="right">{$l10n->_('N�mero de clientes:')}</th>
               <!-- <td class="last">{$item->getConnects()}</td>-->
               <td class="last">{$sumaProtocol[$incremento]}</td>
     </tr>
      
     <!-- <tr><td colspan="2" align="center"><a href="javascript:void(0)" onclick="testStream('{$server->getUrl()}','{$liveData}','{$stream[$item->getIdStream()]}')">Probar</a></td></tr> -->
  </tbody>
</table>
<br/> 
{$incremento=$incremento+1}
 {/foreach}
</div> 
 </div>
</div> 