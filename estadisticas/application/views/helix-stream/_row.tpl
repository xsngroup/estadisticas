<tr>
    <td>{$helixStream->getIdStream()}</td>
    <td>{$helixStream->getName()}</td>
    <td>{$helixStream->getTitle()}</td>
    <td>{$helixStream->getDescription()}</td>
    <td>{$servers[$helixStream->getIdServer()]}</td>
    <td><a href="{url action=edit idStream=$helixStream->getIdStream()}">{icon src=pencil class=tip title=$l10n->_('Edit')}</a></td>
    <td><a href="{url action=delete idStream=$helixStream->getIdStream()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Delete')}</a></td>
</tr>
