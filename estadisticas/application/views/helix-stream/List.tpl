<script type="text/javascript" src="{$baseUrl}/js/modules/helix-stream/process.js"></script>
<div class="onecolumn">
    <div class="header"><span>{$l10n->_('Helix Server Live Stream')}</span></div>
    <br class="clear" />
    <div class="content">
<form action="{url action=create}" method="post" class="validate ajaxForm">
<h4>{$l10n->_('Guardar nueva se�al en vivo')}</h4>
{include file='forms/HelixStream.tpl'}
<br/>
<p> 
<input type="submit" value="{$l10n->_('Guardar')}" />
</p>
</form>
	</div>
</div>

<div class="onecolumn">
    <div class="header"><span>{$l10n->_('Listado de Live Stream')}</span></div>
    <br class="clear" />
    <div class="content">
<table width="100%" cellspacing="0" cellpadding="0" class="data">
    <thead>
        <tr>
            <th>{$l10n->_('Id')}</th>
            <th>{$l10n->_('Nombre')}</th>
            <th>{$l10n->_('T&iacute;tulo')}</th>
            <th>{$l10n->_('Descripci&oacute;n')}</th>
            <th>{$l10n->_('Servidor')}</th>
            <th colspan="2">{$l10n->_('Actions')}</th>
        </tr>
    </thead>
    <tbody id="ajaxList">
        {foreach $helixStreams as $helixStream}
            <tr class="{$helixStream@iteration|odd}">
                <td>{$helixStream->getIdStream()}</td>
                <td>{$helixStream->getName()}</td>
                <td>{$helixStream->getTitle()}</td>
                <td>{$helixStream->getDescription()}</td>
                <td>{$servers[$helixStream->getIdServer()]}</td>
                <td><a href="{url action=edit idStream=$helixStream->getIdStream()}">{icon src=pencil class=tip title=$l10n->_('Edit')}</a></td>
                <td><a href="{url action=delete idStream=$helixStream->getIdStream()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Delete')}</a></td>
            </tr>
        {foreachelse}
           <tr><td colspan="11" align="center">No existen elementos para mostrar...</td></tr>    
        {/foreach}
    </tbody>
</table>
	</div>
</div>

                 