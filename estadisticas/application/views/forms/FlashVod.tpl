<p>
    <label>{$l10n->_('Id')}: </label>
    # {$post['id_vod']}<input type="hidden" name="id_vod" id="id_vod" value="{$post['id_vod']}"/>
</p>
<br/>
<p>
    <label>{$l10n->_('Servidor')}</label><br/>
    {html_options name=id_server id=id_server options=$servers selected=$post['id_server'] style="width: 310px !important;"}
</p>
<br/>
<p>
    <label>{$l10n->_('Prefijo')}</label><br/>
    <input type="text" name="prefix" id="prefix" value="{$post['prefix']}" class="" style="width: 300px !important;" />
</p>
<br/>
<p>
    <label>{$l10n->_('Nombre')}</label><br/>
    <input type="text" name="name_video" id="name_video" value="{$post['name_video']}" class=" required" style="width: 300px !important;" />
</p>
<br/>
<p>
    <label>{$l10n->_('Titulo')}</label><br/>
    <input type="text" name="title_video" id="title_video" value="{$post['title_video']}" class=""  style="width: 300px !important;" />
</p>
<br/>
<p>
    <label>{$l10n->_('Descripción')}</label><br/>
    <textarea name="description_video" id="description_video" class=""  style="width: 300px !important;">{$post['description_video']}</textarea>
</p>

<!--
$idVod = $this->getRequest()->getParam('id_vod');
$prefix = $this->getRequest()->getParam('prefix');
$nameVideo = $this->getRequest()->getParam('name_video');
$titleVideo = $this->getRequest()->getParam('title_video');
$descriptionVideo = $this->getRequest()->getParam('description_video');
$idServer = $this->getRequest()->getParam('id_server');
-->

<!--
$post = array(
    'id_vod' => $flashVod->getIdVod(),
    'prefix' => $flashVod->getPrefix(),
    'name_video' => $flashVod->getNameVideo(),
    'title_video' => $flashVod->getTitleVideo(),
    'description_video' => $flashVod->getDescriptionVideo(),
    'id_server' => $flashVod->getIdServer(),
);
-->
