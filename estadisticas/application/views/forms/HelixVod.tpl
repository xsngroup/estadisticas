<p>
    <label>{$l10n->_('Id:')}</label>
    # {$post['id_vod']}<input type="hidden" name="id_vod" id="id_vod" value="{$post['id_vod']}" />
</p>
<br/>
<p>
    <label>{$l10n->_('Protocolo:')}</label><br/>
    {html_options name=protocol id=protocol options=$protocols  selected=$post['id_protocol']}
</p>
<br/>
<p>
    <label>{$l10n->_('Servidor:')}</label><br/>
    {html_options name=id_server id=id_server options=$servers selected=$post['id_server']}
</p>
<br/>
<p>
    <label>{$l10n->_('Tipo de identificador:')}</label><br/>
    {html_options name=id_type id=id_type options=$types selected=$post['id_type']}
</p>
<br/>
<p>
    <label>{$l10n->_('Posici�n ante el punto de montaje:')}</label><br/>
    {if !is_null($post['position'])&&!empty($post['position'])}
    <input type="radio" name="position" value="0" {if $post['position']==0}checked="checked"{/if}/><label>Ninguno</label>
    <input type="radio" name="position" value="1" {if $post['position']==1}checked="checked"{/if}/><label>Antes</label>
    <input type="radio" name="position" value="2" {if $post['position']==2}checked="checked"{/if}/><label>Desp&uacute;es</label>    
    {else}
    <input type="radio" name="position" value="0" checked="checked" /><label>Ninguno</label>    
    <input type="radio" name="position" value="1" /><label>Antes</label>
    <input type="radio" name="position" value="2" /><label>Desp&uacute;es</label> 
    {/if}
</p>
<br/>
<p>
    <label>{$l10n->_('Prefijo:')}</label><br/>
    <input type="text" name="prefix" id="prefix" value="{$post['prefix']}" class="" style="width: 270px;" />
</p>
<br/>
<p>
    <label>{$l10n->_('Nombre:')}</label><br/>
    <input type="text" name="name_video" id="name_video" value="{$post['name_video']}" class=" required" style="width: 270px;" />
</p>
<br/>
<p>
    <label>{$l10n->_('Puerto:')}</label><br/>
    <input type="text" name="port" id="port" value="{$post['port']}" class="number" style="width: 270px;" />
</p>
<br/>
<p>
    <label>{$l10n->_('T&iacute;tulo:')}</label><br/>
    <input type="text" name="title_video" id="title_video" value="{$post['title_video']}" class="" style="width: 270px;" />
</p>
<br/>
<p>
    <label>{$l10n->_('Descripci&oacute;n:')}</label><br/>
    <textarea name="description_video" id="description_video" class="" style="width: 270px;">{$post['description_video']}</textarea>
</p>
<br/>
<p>
    <label>{$l10n->_('Ppt Sync:')}</label><br/>
    {if $post['ppt_sync']}
	    <input type="radio" name="ppt_sync"  value="1" {if $post['ppt_sync']==1}checked="checked"{/if} /><label>{$l10n->_('Si')}</label>
	    <input type="radio" name="ppt_sync"  value="2" {if $post['ppt_sync']==2}checked="checked"{/if} /><label>{$l10n->_('No')}</label>    
    {else}
	    <input type="radio" name="ppt_sync"  value="1" /><label>{$l10n->_('Si')}</label>
	    <input type="radio" name="ppt_sync"  value="2" checked="checked" /><label>{$l10n->_('No')}</label>
    {/if}
</p>

<!--
$idVod = $this->getRequest()->getParam('id_vod');
$prefix = $this->getRequest()->getParam('prefix');
$nameVideo = $this->getRequest()->getParam('name_video');
$port = $this->getRequest()->getParam('port');
$titleVideo = $this->getRequest()->getParam('title_video');
$descriptionVideo = $this->getRequest()->getParam('description_video');
$idType = $this->getRequest()->getParam('id_type');
$idServer = $this->getRequest()->getParam('id_server');
$pptSync = $this->getRequest()->getParam('ppt_sync');
-->

<!--
$post = array(
    'id_vod' => $helixVod->getIdVod(),
    'prefix' => $helixVod->getPrefix(),
    'name_video' => $helixVod->getNameVideo(),
    'port' => $helixVod->getPort(),
    'title_video' => $helixVod->getTitleVideo(),
    'description_video' => $helixVod->getDescriptionVideo(),
    'id_type' => $helixVod->getIdType(),
    'id_server' => $helixVod->getIdServer(),
    'ppt_sync' => $helixVod->getPptSync(),
);
-->
