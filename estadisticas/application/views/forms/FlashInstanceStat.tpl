<tr>
    <th>{$i18n->_('id_instance_stat')}</th>
    <td># {$post['id_instance_stat']}<input type="hidden" name="id_instance_stat" id="id_instance_stat" value="{$post['id_instance_stat']}" /></td>
</tr>
<tr>
    <th>{$i18n->_('id_instance')}</th>
    <td>{html_options name=id_instance id=id_instance options=$Instances selected=$post['id_instance'] }</td>
</tr>
<tr>
    <th>{$i18n->_('timestamp')}</th>
    <td><input type="text" name="timestamp" id="timestamp" value="{$post['timestamp']}" class="datePicker dateISO required" /></td>
</tr>
<tr>
    <th>{$i18n->_('bytes_in')}</th>
    <td><input type="text" name="bytes_in" id="bytes_in" value="{$post['bytes_in']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('bytes_out')}</th>
    <td><input type="text" name="bytes_out" id="bytes_out" value="{$post['bytes_out']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('currently_connects')}</th>
    <td><input type="text" name="currently_connects" id="currently_connects" value="{$post['currently_connects']}" class="number required" /></td>
</tr>
<tr>
    <th>{$i18n->_('accepted')}</th>
    <td><input type="text" name="accepted" id="accepted" value="{$post['accepted']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('rejected')}</th>
    <td><input type="text" name="rejected" id="rejected" value="{$post['rejected']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('total_connects')}</th>
    <td><input type="text" name="total_connects" id="total_connects" value="{$post['total_connects']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('total_disconnects')}</th>
    <td><input type="text" name="total_disconnects" id="total_disconnects" value="{$post['total_disconnects']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('launch_time')}</th>
    <td><input type="text" name="launch_time" id="launch_time" value="{$post['launch_time']}" class="datePicker dateISO required" /></td>
</tr>

<!--
$idInstanceStat = $this->getRequest()->getParam('id_instance_stat');
$timestamp = $this->getRequest()->getParam('timestamp');
$bytesIn = $this->getRequest()->getParam('bytes_in');
$bytesOut = $this->getRequest()->getParam('bytes_out');
$currentlyConnects = $this->getRequest()->getParam('currently_connects');
$accepted = $this->getRequest()->getParam('accepted');
$rejected = $this->getRequest()->getParam('rejected');
$totalConnects = $this->getRequest()->getParam('total_connects');
$totalDisconnects = $this->getRequest()->getParam('total_disconnects');
$launchTime = $this->getRequest()->getParam('launch_time');
$idInstance = $this->getRequest()->getParam('id_instance');
-->

<!--
$post = array(
    'id_instance_stat' => $flashInstanceStat->getIdInstanceStat(),
    'timestamp' => $flashInstanceStat->getTimestamp(),
    'bytes_in' => $flashInstanceStat->getBytesIn(),
    'bytes_out' => $flashInstanceStat->getBytesOut(),
    'currently_connects' => $flashInstanceStat->getCurrentlyConnects(),
    'accepted' => $flashInstanceStat->getAccepted(),
    'rejected' => $flashInstanceStat->getRejected(),
    'total_connects' => $flashInstanceStat->getTotalConnects(),
    'total_disconnects' => $flashInstanceStat->getTotalDisconnects(),
    'launch_time' => $flashInstanceStat->getLaunchTime(),
    'id_instance' => $flashInstanceStat->getIdInstance(),
);
-->
