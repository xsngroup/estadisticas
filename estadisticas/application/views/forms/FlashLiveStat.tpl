<tr>
    <th>{$i18n->_('id_live_stat')}</th>
    <td># {$post['id_live_stat']}<input type="hidden" name="id_live_stat" id="id_live_stat" value="{$post['id_live_stat']}" /></td>
</tr>
<tr>
    <th>{$i18n->_('id_live')}</th>
    <td>{html_options name=id_live id=id_live options=$Lives selected=$post['id_live'] }</td>
</tr>
<tr>
    <th>{$i18n->_('timestamp')}</th>
    <td><input type="text" name="timestamp" id="timestamp" value="{$post['timestamp']}" class="datePicker dateISO required" /></td>
</tr>
<tr>
    <th>{$i18n->_('publish_name')}</th>
    <td><input type="text" name="publish_name" id="publish_name" value="{$post['publish_name']}" class="" /></td>
</tr>
<tr>
    <th>{$i18n->_('type')}</th>
    <td><input type="text" name="type" id="type" value="{$post['type']}" class="" /></td>
</tr>
<tr>
    <th>{$i18n->_('client')}</th>
    <td><input type="text" name="client" id="client" value="{$post['client']}" class="" /></td>
</tr>
<tr>
    <th>{$i18n->_('publish_time')}</th>
    <td><input type="text" name="publish_time" id="publish_time" value="{$post['publish_time']}" class="datePicker dateISO required" /></td>
</tr>

<!--
$idLiveStat = $this->getRequest()->getParam('id_live_stat');
$timestamp = $this->getRequest()->getParam('timestamp');
$publishName = $this->getRequest()->getParam('publish_name');
$type = $this->getRequest()->getParam('type');
$client = $this->getRequest()->getParam('client');
$publishTime = $this->getRequest()->getParam('publish_time');
$idLive = $this->getRequest()->getParam('id_live');
-->

<!--
$post = array(
    'id_live_stat' => $flashLiveStat->getIdLiveStat(),
    'timestamp' => $flashLiveStat->getTimestamp(),
    'publish_name' => $flashLiveStat->getPublishName(),
    'type' => $flashLiveStat->getType(),
    'client' => $flashLiveStat->getClient(),
    'publish_time' => $flashLiveStat->getPublishTime(),
    'id_live' => $flashLiveStat->getIdLive(),
);
-->
