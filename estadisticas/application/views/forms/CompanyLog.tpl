<tr>
    <th>{$i18n->_('id_company_log')}</th>
    <td># {$post['id_company_log']}<input type="hidden" name="id_company_log" id="id_company_log" value="{$post['id_company_log']}" /></td>
</tr>
<tr>
    <th>{$i18n->_('id_company')}</th>
    <td>{html_options name=id_company id=id_company options=$Companys selected=$post['id_company'] }</td>
</tr>
<tr>
    <th>{$i18n->_('id_user')}</th>
    <td>{html_options name=id_user id=id_user options=$Users selected=$post['id_user'] }</td>
</tr>
<tr>
    <th>{$i18n->_('event_type')}</th>
    <td><input type="text" name="event_type" id="event_type" value="{$post['event_type']}" class="number required" /></td>
</tr>
<tr>
    <th>{$i18n->_('timestamp')}</th>
    <td><input type="text" name="timestamp" id="timestamp" value="{$post['timestamp']}" class="datePicker dateISO required" /></td>
</tr>

<!--
$idCompanyLog = $this->getRequest()->getParam('id_company_log');
$eventType = $this->getRequest()->getParam('event_type');
$timestamp = $this->getRequest()->getParam('timestamp');
$idCompany = $this->getRequest()->getParam('id_company');
$idUser = $this->getRequest()->getParam('id_user');
-->

<!--
$post = array(
    'id_company_log' => $companyLog->getIdCompanyLog(),
    'event_type' => $companyLog->getEventType(),
    'timestamp' => $companyLog->getTimestamp(),
    'id_company' => $companyLog->getIdCompany(),
    'id_user' => $companyLog->getIdUser(),
);
-->
