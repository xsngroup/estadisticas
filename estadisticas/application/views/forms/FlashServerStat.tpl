<tr>
    <th>{$i18n->_('id_server_stat')}</th>
    <td># {$post['id_server_stat']}<input type="hidden" name="id_server_stat" id="id_server_stat" value="{$post['id_server_stat']}" /></td>
</tr>
<tr>
    <th>{$i18n->_('id_server')}</th>
    <td>{html_options name=id_server id=id_server options=$Servers selected=$post['id_server'] }</td>
</tr>
<tr>
    <th>{$i18n->_('timestamp')}</th>
    <td><input type="text" name="timestamp" id="timestamp" value="{$post['timestamp']}" class="datePicker dateISO required" /></td>
</tr>
<tr>
    <th>{$i18n->_('launch_time')}</th>
    <td><input type="text" name="launch_time" id="launch_time" value="{$post['launch_time']}" class="datePicker dateISO required" /></td>
</tr>
<tr>
    <th>{$i18n->_('up_time')}</th>
    <td><input type="text" name="up_time" id="up_time" value="{$post['up_time']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('cpus')}</th>
    <td><input type="text" name="cpus" id="cpus" value="{$post['cpus']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('cpu_usage')}</th>
    <td><input type="text" name="cpu_usage" id="cpu_usage" value="{$post['cpu_usage']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('memory_usage')}</th>
    <td><input type="text" name="memory_usage" id="memory_usage" value="{$post['memory_usage']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('physical_mem')}</th>
    <td><input type="text" name="physical_mem" id="physical_mem" value="{$post['physical_mem']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('bw_in')}</th>
    <td><input type="text" name="bw_in" id="bw_in" value="{$post['bw_in']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('bw_out')}</th>
    <td><input type="text" name="bw_out" id="bw_out" value="{$post['bw_out']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('bytes_in')}</th>
    <td><input type="text" name="bytes_in" id="bytes_in" value="{$post['bytes_in']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('bytes_out')}</th>
    <td><input type="text" name="bytes_out" id="bytes_out" value="{$post['bytes_out']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('connected')}</th>
    <td><input type="text" name="connected" id="connected" value="{$post['connected']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('total_connects')}</th>
    <td><input type="text" name="total_connects" id="total_connects" value="{$post['total_connects']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('normal_connects')}</th>
    <td><input type="text" name="normal_connects" id="normal_connects" value="{$post['normal_connects']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('service_connects')}</th>
    <td><input type="text" name="service_connects" id="service_connects" value="{$post['service_connects']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('total_disconnects')}</th>
    <td><input type="text" name="total_disconnects" id="total_disconnects" value="{$post['total_disconnects']}" class="number" /></td>
</tr>
<tr>
    <th>{$i18n->_('service_requests')}</th>
    <td><input type="text" name="service_requests" id="service_requests" value="{$post['service_requests']}" class="number" /></td>
</tr>

<!--
$idServerStat = $this->getRequest()->getParam('id_server_stat');
$timestamp = $this->getRequest()->getParam('timestamp');
$launchTime = $this->getRequest()->getParam('launch_time');
$upTime = $this->getRequest()->getParam('up_time');
$cpus = $this->getRequest()->getParam('cpus');
$cpuUsage = $this->getRequest()->getParam('cpu_usage');
$memoryUsage = $this->getRequest()->getParam('memory_usage');
$physicalMem = $this->getRequest()->getParam('physical_mem');
$bwIn = $this->getRequest()->getParam('bw_in');
$bwOut = $this->getRequest()->getParam('bw_out');
$bytesIn = $this->getRequest()->getParam('bytes_in');
$bytesOut = $this->getRequest()->getParam('bytes_out');
$connected = $this->getRequest()->getParam('connected');
$totalConnects = $this->getRequest()->getParam('total_connects');
$normalConnects = $this->getRequest()->getParam('normal_connects');
$serviceConnects = $this->getRequest()->getParam('service_connects');
$totalDisconnects = $this->getRequest()->getParam('total_disconnects');
$serviceRequests = $this->getRequest()->getParam('service_requests');
$idServer = $this->getRequest()->getParam('id_server');
-->

<!--
$post = array(
    'id_server_stat' => $flashServerStat->getIdServerStat(),
    'timestamp' => $flashServerStat->getTimestamp(),
    'launch_time' => $flashServerStat->getLaunchTime(),
    'up_time' => $flashServerStat->getUpTime(),
    'cpus' => $flashServerStat->getCpus(),
    'cpu_usage' => $flashServerStat->getCpuUsage(),
    'memory_usage' => $flashServerStat->getMemoryUsage(),
    'physical_mem' => $flashServerStat->getPhysicalMem(),
    'bw_in' => $flashServerStat->getBwIn(),
    'bw_out' => $flashServerStat->getBwOut(),
    'bytes_in' => $flashServerStat->getBytesIn(),
    'bytes_out' => $flashServerStat->getBytesOut(),
    'connected' => $flashServerStat->getConnected(),
    'total_connects' => $flashServerStat->getTotalConnects(),
    'normal_connects' => $flashServerStat->getNormalConnects(),
    'service_connects' => $flashServerStat->getServiceConnects(),
    'total_disconnects' => $flashServerStat->getTotalDisconnects(),
    'service_requests' => $flashServerStat->getServiceRequests(),
    'id_server' => $flashServerStat->getIdServer(),
);
-->
