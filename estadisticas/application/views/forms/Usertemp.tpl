<tr>
    <th>{$i18n->_('id_usertemp')}</th>
    <td># {$post['id_usertemp']}<input type="hidden" name="id_usertemp" id="id_usertemp" value="{$post['id_usertemp']}" /></td>
</tr>
<tr>
    <th>{$i18n->_('id_access_role')}</th>
    <td>{html_options name=id_access_role id=id_access_role options=$AccessRoles selected=$post['id_access_role'] }</td>
</tr>
<tr>
    <th>{$i18n->_('username')}</th>
    <td><input type="text" name="username" id="username" value="{$post['username']}" class="" /></td>
</tr>
<tr>
    <th>{$i18n->_('password')}</th>
    <td><input type="text" name="password" id="password" value="{$post['password']}" class="" /></td>
</tr>
<tr>
    <th>{$i18n->_('status')}</th>
    <td><input type="text" name="status" id="status" value="{$post['status']}" class="number required" /></td>
</tr>

<!--
$idUsertemp = $this->getRequest()->getParam('id_usertemp');
$username = $this->getRequest()->getParam('username');
$password = $this->getRequest()->getParam('password');
$status = $this->getRequest()->getParam('status');
$idAccessRole = $this->getRequest()->getParam('id_access_role');
-->

<!--
$post = array(
    'id_usertemp' => $usertemp->getIdUsertemp(),
    'username' => $usertemp->getUsername(),
    'password' => $usertemp->getPassword(),
    'status' => $usertemp->getStatus(),
    'id_access_role' => $usertemp->getIdAccessRole(),
);
-->
