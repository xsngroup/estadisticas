<p>
    <label>{$l10n->_('Id')}</label>
    # {$post['id_stream']}<input type="hidden" name="id_stream" id="id_stream" value="{$post['id_stream']}" />
</p>
<br/>
<p>
    <label>{$l10n->_('Activar protocolo:')}</label><br/>
    {foreach $protocols as $protocol}
     {assign var=protX value="0"}
     {assign var=pointMount value="0"}
     {assign var=prefix value="0"}
     {assign var=position value="0"}
     {assign var=port value=""} 
        
     {if count($relHP)>0} 
      {foreach $relHP as $relation}
        {if $relation['id_protocol']==$protocol->getIdProtocol()}
            {$protX=$relation['id_protocol']}
            {$pointMount=$relation['id_live']}
            {$prefix=$relation['id_type']}
            {$position=$relation['position']}
            {$port=$relation['port']}
        {/if}
      {/foreach}     
     {/if} 
      <input type="checkbox" name="protocol[]" class="protocol" value="{$protocol->getIdProtocol()}" {if $protX!=0} checked="checked" {/if} /><label>{$protocol->getName()}</label><br/>           
		<div id="loadConf-{$protocol->getIdProtocol()}" class="confPro" {if $protX!=0}style="display: block;"{/if}>
		<p>
		    <label>{$l10n->_('Servidor:')}</label><br/>
		    {if $post['id_server']}
		       {html_options name="server-{$protocol->getIdProtocol()}" id="server-{$protocol->getIdProtocol()}" class=selectLive options=$servers selected=$post['id_server']}
		    {else}
			    <select name="server-{$protocol->getIdProtocol()}" id="server-{$protocol->getIdProtocol()}" class="selectLive">
			      <option value="0">Selecciona una opci&oacute;n...</option>
			      {foreach $streamServers as $item}
			        <option value="{$item->getIdServer()}">{$item->getUrl()}</option>
			      {/foreach}
			    </select>    
		    {/if}
		</p>
		<br/>
		<p>
		   <input type="checkbox" style="margin:0; margin-right:5px;" id="ckbx-{$protocol->getIdProtocol()}" class="ckbxPort" /><label>{$l10n->_('Puerto:')}</label><br/>
		    <input type="text" name="port-{$protocol->getIdProtocol()}" id="port-{$protocol->getIdProtocol()}" value="{$port}" style="width: 50px;" disabled="disabled" />
		</p>
		<br/>
		<p>
		    <label>{$l10n->_('Punto de montaje:')}</label><br/>
		    {if $pointMount!=0}
		         {html_options name="id_live-{$protocol->getIdProtocol()}" id="id_live-{$protocol->getIdProtocol()}" class=selectPoint options=$lives selected=$pointMount} 
		    {else}
		         {html_options name="id_live-{$protocol->getIdProtocol()}" id="id_live-{$protocol->getIdProtocol()}" class=selectPoint options=$helixLive}
		    {/if}
		    
		</p>
		<!--  
		<br/>
			
			<div id="showPrefix-{$protocol->getIdProtocol()}" {if $position==0}style="display: none;"{/if}>	
			<p>
			    <label>{$l10n->_('Posici�n ante el punto de montaje:')}</label><br/>
			    <input type="radio" name="position-{$protocol->getIdProtocol()}" value="1" {if $position==1}checked="checked"{/if}/><label>Antes</label>
			    <input type="radio" name="position-{$protocol->getIdProtocol()}" value="2" {if $position==2}checked="checked"{/if}/><label>Desp&uacute;es</label>    
			</p>
			
			<br/>
			</div>
			-->
		<br/>
		</div>      
    {/foreach}
</p>
<br/>


<!-- aqui ya se a�aden los datos genericos de la se�al -->
<br/>
<p>
    <label>{$l10n->_('Se�al:')}</label><br/>
    <input type="text" name="name" id="name" value="{$post['name']}" class=" required" style="width: 270px;" />
</p>
<br/>
<p>
    <label>{$l10n->_('Titulo:')}</label><br/>
    <input type="text" name="title" id="title" value="{$post['title']}" class="" style="width: 270px;" />
</p>
<br/>
<p>
    <label>{$l10n->_('Descripci�n:')}</label><br/>
    <textarea name="description" id="description" class="" style="width: 270px;">{$post['description']}</textarea>
</p>

<!--
$idStream = $this->getRequest()->getParam('id_stream');
$name = $this->getRequest()->getParam('name');
$title = $this->getRequest()->getParam('title');
$description = $this->getRequest()->getParam('description');
$idLive = $this->getRequest()->getParam('id_live');
-->

<!--
$post = array(
    'id_stream' => $helixStream->getIdStream(),
    'name' => $helixStream->getName(),
    'title' => $helixStream->getTitle(),
    'description' => $helixStream->getDescription(),
    'id_live' => $helixStream->getIdLive(),
);
-->
