_row.tpl

<tr>
    <td>{$usertemp->getIdUsertemp()}</td>
    <td>{$usertemp->getUsername()}</td>
    <td>{$usertemp->getPassword()}</td>
    <td>{$usertemp->getStatus()}</td>
    <td>{$usertemp->getIdAccessRole()}</td>
    <td><a href="{url action=edit idUsertemp=$usertemp->getIdUsertemp()}">{icon src=pencil class=tip title=$i18n->_('Edit')}</a></td>
    <td><a href="{url action=delete idUsertemp=$usertemp->getIdUsertemp()}" class="confirm">{icon src=delete class=tip title=$i18n->_('Delete')}</a></td>
</tr>
