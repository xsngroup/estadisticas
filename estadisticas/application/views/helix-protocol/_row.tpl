<tr>
    <td>{$helixProtocol->getIdProtocol()}</td>
    <td>{$helixProtocol->getName()}</td>
    <td><a href="{url action=edit idProtocol=$helixProtocol->getIdProtocol()}">{icon src=pencil class=tip title=$l10n->_('Edit')}</a></td>
    <td><a href="{url action=delete idProtocol=$helixProtocol->getIdProtocol()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Delete')}</a></td>
</tr>
