<div class="onecolumn">
    <div class="header"><span>{$l10n->_('Helix Server VOD Stream')}</span></div>
    <br class="clear" />
    <div class="content">
<form action="{url action=create}" method="post" class="validate ajaxForm">
<h4>{$l10n->_('Guardar nueva se�al VOD')}</h4>
{include file='forms/HelixVod.tpl'}
<br/>
<p> 
<input type="submit" value="{$l10n->_('Guardar')}" />
</p>
</form>
	</div>
</div>

<div class="onecolumn">
    <div class="header"><span>{$l10n->_('Listado de VOD Stream')}</span></div>
    <br class="clear" />
    <div class="content">
<table width="100%" cellspacing="0" cellpadding="0" class="data">
    <thead>
        <tr>
            <th>{$l10n->_('Id')}</th>
            <th>{$l10n->_('Prefijo')}</th>
            <th>{$l10n->_('Nombre')}</th>
            <th>{$l10n->_('Puerto')}</th>
            <th>{$l10n->_('T&iacute;tulo')}</th>
            <th>{$l10n->_('Descripci&oacute;n')}</th>
            <th>{$l10n->_('Tipo de identificador')}</th>
            <th>{$l10n->_('Servidor')}</th>
            <th>{$l10n->_('Ppt Sync')}</th>
            <th>{$l10n->_('Protocolo')}</th>
            <th colspan="2">{$l10n->_('Actions')}</th>
        </tr>
    </thead>
    <tbody id="ajaxList">
        {foreach $helixVods as $helixVod}
            <tr class="{$helixVod@iteration|odd}">
                <td>{$helixVod->getIdVod()}</td>
                <td>{$helixVod->getPrefix()}</td>
                <td>{$helixVod->getNameVideo()}</td>
                <td>{$helixVod->getPort()}</td>
                <td>{$helixVod->getTitleVideo()}</td>
                <td>{$helixVod->getDescriptionVideo()}</td>
                <td>{$types[$helixVod->getIdType()]} > {$prefix[$helixVod->getPosition()]}</td>
                <td>{$servers[$helixVod->getIdServer()]}</td>
                <td>{$helixVod->getPptSync()}</td>
                <td>{$protocols[$relations[$helixVod->getIdVod()]]}</td>
                <td><a href="{url action=edit idVod=$helixVod->getIdVod()}">{icon src=pencil class=tip title=$l10n->_('Edit')}</a></td>
                <td><a href="{url action=delete idVod=$helixVod->getIdVod()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Delete')}</a></td>
            </tr>
        {foreachelse}
            <tr>
               <td colspan="12" align="center">No existen elementos para mostrar...</td>
            </tr>    
        {/foreach}
    </tbody>
</table>
	</div>
</div>
