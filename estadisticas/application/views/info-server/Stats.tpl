
<div class="column_left">
 <div class="header"><span>{$l10n->_('Informaci�n del servidor')}</span></div>
 <br class="clear" />
 <div class="content">
<form action="#" class="validate" method="post">
<table width="100%" cellspacing="0" cellpadding="0" class="data"> 
  <tbody> 
     <tr>
                <th align="right">{$l10n->_('Cliente:')}</th>
                <td class="last">{$company->getNameCompany()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Contacto:')}</th>
                <td class="last">{$company->getNameContac()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Tel�fono:')}</th>
                <td class="last">{$company->getTelephone()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Ip del Servidor:')}</th>
                <td class="last">{$server->getUrl()}</td>       
     </tr>                 
  </tbody>
</table>

</form>
</div>
</div>

<div class="column_right">
    <div class="header"><span>{$l10n->_('Estad�stica del servidor')}</span></div>
    <br class="clear" />
    <div class="content">
<form action="#" class="validate" method="post">
    <div id="loadStats">
 <script type="text/javascript" src="{$baseUrl}/js/modules/info-server/script.js"></script>   
  {if count($serverStat)>0} 
<table width="100%" cellspacing="0" cellpadding="0" class="data"> 
  <tbody> 
     <tr>
                <th align="right">{$l10n->_('Fecha y hora:')}</th>
                <td class="last">{$serverStat->getTimestamp()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Procesadores:')}</th>
                <td class="last">{$serverStat->getCpus()}</td>       
     </tr>  
     <tr>
                <th align="right">{$l10n->_('Uso de cpu:')}{icon src=information class=tip style="padding-left:5px;" title="Porcentaje de consumo en base al total de procesadores en donde cada procesador es un 100%"}</th>
                <td class="last">{$serverStat->getCpuUsage()}%</td>       
     </tr>  
     <tr>
                <th align="right">{$l10n->_('Memoria f�sica:')}{icon src=information class=tip style="padding-left:5px;" title="Total de memoria RAM disponible en Mb"}</th>
                <td class="last">{$serverStat->getPhysicalMem()}</td>       
     </tr>                                                                                  
     <tr>
                <th align="right">{$l10n->_('Uso de memoria:')}</th>
                <td class="last">{$serverStat->getMemoryUsage()}%</td>       
     </tr>      
     <tr>
                <th align="right">{$l10n->_('Bytes de entrada:')}{icon src=information class=tip style="padding-left:5px;" title="Total de bytes recibidos por el servidor"}</th>
                <td class="last">{$serverStat->getBytesIn()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Bytes de salida:')}{icon src=information class=tip style="padding-left:5px;" title="Total de bytes enviados por el servidor"}</th>
                <td class="last">{$serverStat->getBytesOut()}</td>       
     </tr>
     <!-- 
     <tr>
                <th align="right">{$l10n->_('Conectados actuales:')}</th>
                <td class="last">{$serverStat->getConnected()}</td>       
     </tr>-->
     <tr>
                <th align="right">{$l10n->_('Inicio de ejecuci�n:')}{icon src=information class=tip style="padding-left:5px;" title="Fecha y hora en que se inici� el servidor de streaming"}</th>
                <td class="last">{$serverStat->getLaunchTime()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Ancho de banda de entrada:')}{icon src=information class=tip style="padding-left:5px;" title="Ancho de banda consumido por el servidor"}</th>
                <td class="last">{$serverStat->getBwIn()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Ancho de banda de salida:')}{icon src=information class=tip style="padding-left:5px;" title="Ancho de banda consumido por el servidor"}</th>
                <td class="last">{$serverStat->getBwOut()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Total conectados:')}</th>
                <td class="last">{$serverStat->getTotalConnects()}</td>       
     </tr>
     <tr>
                <th align="right">{$l10n->_('Total desconectados:')}</th>
                <td class="last">{$serverStat->getTotalDisconnects()}</td>       
     </tr>
     <!-- 
     <tr>
                <th align="right">{$l10n->_('Conexiones normales:')}</th>
                <td class="last">{$serverStat->getNormalConnects()}</td>       
     </tr> 
     <tr>
                <th align="right">{$l10n->_('Servicio de conexi�n:')}</th>
                <td class="last">{$serverStat->getServiceConnects()}</td>       
     </tr>
     
     <tr>
                <th align="right">{$l10n->_('Servicios recibidos:')}</th>
                <td class="last">{$serverStat->getServiceRequests()}</td>       
     </tr>      
     -->
     <tr>
                <th align="right">{$l10n->_('Tiempo transcurrido:')}{icon src=information class=tip style="padding-left:5px;" title="Tiempo transcurrido en segundos desde que se inici� el servidor de streaming"}</th>
                <td class="last">{$serverStat->getUpTime()}</td>       
     </tr>
            
  </tbody>
</table>
<br/>
   {else}
                <span>No existe informaci�n por el momento...</span>         
   {/if} 
</div>   
</form>

</div>
</div>    