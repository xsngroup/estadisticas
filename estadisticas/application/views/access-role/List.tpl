<div class="column_left">
    <div class="header"><span>{$l10n->_('Perfiles de Usuario')}</span></div>
    <br class="clear" />
    <div class="content">
<form action="{url action=create}" method="post" class="validate ajaxForm">
<h4>{$l10n->_('Guardar nuevo Grupo de Usuario')}</h4>
<p>
<label>{$l10n->_('Nombre')}</label><br/>
<input type="text" id="name" name="name" class="required" size="40"/>
</p>
<br/>
<p> 
<input type="submit" value="{$l10n->_('Guardar')}" />
</p>
</form>
	</div>
</div>

<div class="column_right">
    <div class="header"><span>{$l10n->_('Listado de Perfiles')}</span></div>
    <br class="clear" />
    <div class="content">
<table width="100%" cellspacing="0" cellpadding="0" class="data">
	<thead>
		<tr>
			<th>#</th>
			<th>{$l10n->_('Nombre')}</th>
			<th colspan="2">{$l10n->_('Acciones')}</th>
		</tr>
	</thead>
	<tbody id="ajaxList">
		{foreach $accessRoles as $accessRole}
			<tr class="{$accessRole@iteration|odd}">
				<td>{$accessRole->getIdAccessRole()}</td>
				<td>{$accessRole->getName()}</td>
				<td><a href="{url action=edit id=$accessRole->getIdAccessRole()}">{icon src=pencil class=tip title=Editar}</a></td>
				<td><a href="{url action=delete id=$accessRole->getIdAccessRole()}" class="confirm">{icon src=delete class=tip title=Borrar}</a></td>
			</tr>
		{/foreach}
	</tbody>
</table>
	</div>
</div>