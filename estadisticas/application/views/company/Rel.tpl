<script type="text/javascript" src="{$baseUrl}/js/modules/company/process.js"></script>
<div class="onecolumn">
    <div class="header"><span>{$l10n->_('Configurar informaci�n del cliente')}</span></div>
    <br class="clear" />
    <div class="content">
  
<form action="{url action=reladd}" method="post" class="validate">
<p>
<label>{$l10n->_('Plataforma de streaming:')}</label><br/>
<input type="radio" name="typeStreamServer" class="typeStreamServer" value="1" checked="checked"/><label>{$l10n->_('FMS')}</label>
<input type="radio" name="typeStreamServer" class="typeStreamServer" value="2" /><label>{$l10n->_('Helix')}</label>
</p> 
<br/> 
<p>
<label>{$l10n->_('Tipo de se�al:')}</label><br/>
<input type="radio" name="typeRelation" class="typeRelation" value="1" checked="checked"/><label>{$l10n->_('Live stream')}</label>
<input type="radio" name="typeRelation" class="typeRelation" value="2" /><label>{$l10n->_('VOD stream')}</label>
</p>
<br/>
<div id="displayFlashOption">
	<div class="loadLive">
	<p>
	    <label>{$l10n->_('Servidor:')}</label><br/>
	    <select name="id_server" id="id_server" class="selectLive">
	        <option value="0">Seleccione una opci�n...</option>
	    {foreach $Servers as $server}
	        <option value="{$server->getIdServer()}">{$server->getUrl()}</option>
	    {/foreach}
	    </select>
	</p>
	<br/>
	<p>
	    <label>{$l10n->_('Punto de montaje:')}</label><br/>
	    {html_options name=id_live id=id_live class=selectLive options=$Lives selected=$post['id_server']}
	</p>
	<br/>
	<p>
	    <label>{$l10n->_('Stream:')}</label><br/>
	    {html_options name=id_stream id=id_stream options=$Streams selected=$post['id_server']}
	</p>
	</div>
	<div class="loadVod" style="display:none;">
	<p>
	    <label>{$l10n->_('Servidor:')}</label><br/>
	    <select name="id_server2" id="id_server2" class="selectVod">
	        <option value="0">Seleccione una opci�n...</option>
	    {foreach $Servers as $server}
	        <option value="{$server->getIdServer()}">{$server->getUrl()}</option>
	    {/foreach}
	    </select>
	</p>
	<br/>
	<p>
	    <label>{$l10n->_('Video on Demand:')}</label><br/>
	    {html_options name=id_vod id=id_vod options=$Vod}
	</p>
	</div>
</div>
<div id="displayHelixOption" style="display: none;">
<div class="loadLive">
	<p>
	    <label>{$l10n->_('Servidor:')}</label><br/>
	    <select name="id_server_helix1" id="id_server_helix1" class="selectLiveHelix">
	        <option value="0">Seleccione una opci�n...</option>
	    {foreach $Servers as $server}
	        <option value="{$server->getIdServer()}">{$server->getUrl()}</option>
	    {/foreach}
	    </select>
	</p>
	<br/>
	<p>
	    <label>{$l10n->_('Punto de montaje:')}</label><br/>
	    {html_options name=id_live_helix id=id_live_helix class=selectLiveHelix options=$Lives}
	</p>
	<br/>
	<p>
	    <label>{$l10n->_('Stream:')}</label><br/>
	    {html_options name=id_stream_helix id=id_stream_helix options=$Streams}
	</p>
	</div>
	<div class="loadVod" style="display:none;">
	<p>
	    <label>{$l10n->_('Servidor:')}</label><br/>
	    <select name="id_server_helix2" id="id_server_helix2" class="selectVodHelix">
	        <option value="0">Seleccione una opci�n...</option>
	    {foreach $Servers as $server}
	        <option value="{$server->getIdServer()}">{$server->getUrl()}</option>
	    {/foreach}
	    </select>
	</p>
	<br/>
	<p>
	    <label>{$l10n->_('Video on Demand:')}</label><br/>
	    {html_options name=id_vod_helix id=id_vod_helix options=$Vod}
	</p>
	</div>
</div>
<br/>
<p> 
<input type="hidden" name="idCompany" value="{$idCompany}" />
<input type="submit" value="{$l10n->_('Guardar')}" />
<input type="button" value="{$l10n->_('Regresar')}" class="back" />
</p> 
</form>

<br/>
<br/>
<h3 align="center">Lista de se�ales en vivo actualmente asignadas</h3>
<table width="100%" cellspacing="0" cellpadding="0" class="data"> 
  <thead>
      <tr>
                <th align="justify">{$l10n->_('Nombre')}</th>
                <th align="justify">{$l10n->_('Servidor')}</th>                  
                <th align="justify">{$l10n->_('Titulo')}</th> 
                <th align="justify">{$l10n->_('Descripci&oacute;n')}</th>                                                                      
     </tr> 
  </thead>
  <tbody>
   {if count($listStreamFlash)>0}
	   {foreach $listStreamFlash as $streamItem}
	     <tr>
	                <td class="last">{$streamItem->getName()}</td> 
	                <td class="last">{$serversCombo[$streamItem->getIdLive()]}</td> 
	                <td class="last">{$streamItem->getTitle()}</td> 
	                <td class="last">{$streamItem->getDescription()}</td>                                                       
	     </tr>
	   {/foreach}
   {/if}
   {if count($listStreamHelix)>0}
	   {foreach $listStreamHelix as $streamItem}
	     <tr>
	                <td class="last">{$streamItem->getName()}</td> 
	                <td class="last">{$serversCombo[$streamItem->getIdServer()]}</td> 
	                <td class="last">{$streamItem->getTitle()}</td> 
	                <td class="last">{$streamItem->getDescription()}</td>                                                       
	     </tr>
	   {/foreach}   
   {/if}
  {if count($listStreamFlash)<=0 && count($listStreamHelix)<=0}
     <tr>
                <td class="last" colspan="4" align="center">No tiene se�ales en vivo asociadas...</td>       
     </tr>   
  {/if}  
  </tbody>
</table>
<br/>
<br/>
<h3 align="center">Lista de se�ales VOD actualmente asignadas</h3>
<table width="100%" cellspacing="0" cellpadding="0" class="data"> 
  <thead>
      <tr>
                <th align="justify">{$l10n->_('Prefijo')}</th>
                <th align="justify">{$l10n->_('Nombre')}</th>
                <th align="justify">{$l10n->_('Servidor')}</th>                  
                <th align="justify">{$l10n->_('Titulo')}</th> 
                <th align="justify">{$l10n->_('Descripci&oacute;n')}</th>                                                                      
     </tr> 
  </thead>
  <tbody>
  {if count($listVodFlash)>0}
   {foreach $listVodFlash as $vodItem}
     <tr>
                <td class="last">{$vodItem->getPrefix()}</td> 
                <td class="last">{$vodItem->getNameVideo()}</td> 
                <td class="last">{$serversCombo[$vodItem->getIdServer()]}</td> 
                <td class="last">{$vodItem->getTitleVideo()}</td> 
                <td class="last">{$vodItem->getDescriptionVideo()}</td>                                                       
     </tr>  
   {/foreach}
  {/if}
  {if count($listVodHelix)>0} 
   {foreach $listVodHelix as $vodItem}
     <tr>
                <td class="last">{$vodItem->getPrefix()}</td> 
                <td class="last">{$vodItem->getNameVideo()}</td> 
                <td class="last">{$serversCombo[$vodItem->getIdServer()]}</td> 
                <td class="last">{$vodItem->getTitleVideo()}</td> 
                <td class="last">{$vodItem->getDescriptionVideo()}</td>                                                       
     </tr>  
   {/foreach}   
  {/if}
  {if count($listVodFlash)<=0 && count($listVodHelix)<=0}
     <tr>
                <td class="last" colspan="5" align="center">No tiene se�ales en bajo demanda asociadas...</td>       
     </tr>        
  {/if} 
  </tbody>
</table>
	</div>
</div>    