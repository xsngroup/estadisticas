<div class="onecolumn">
    <div class="header"><span>{$l10n->_('Cat�logo de clientes')}</span></div>
    <br class="clear" />
    <div class="content">
<form action="{url action=create}" method="post" class="validate ajaxForm">
<h4>{$l10n->_('Datos del cliente')}</h4>
        {include file='forms/Company.tpl'}
<br/>
<h4>{$l10n->_('Datos de acceso')}</h4>
<p>
<label for="name">{$l10n->_('Nombre')}:</label><br/>
<input type="text" name="name" id="name" value="{$post.name}" size="40" class="required"/>
</p>
<p>
<label for="middlename">{$l10n->_('Apellido Paterno')}:</label><br/>
<input type="text" name="middlename" id="middlename" value="{$post.middlename}" size="40" class="required"/>
</p>
<p>
<label for="username">{$l10n->_('Usuario')}:</label><br/>
<input type="text" name="username" id="username" value="{$post.username}" size="40" class="required" />
</p>
<p> 
<label for="password">{$l10n->_('Contrase�a')}:</label><br/>
<input type="password" name="password" id="password" value="" size="40" class="{if $isNew}required{/if}"/>
</p>
<p> 
<label for="passwordConfirm">{$l10n->_('Confirmar Contrase�a')}:</label><br/>
<input type="password" name="passwordConfirm" id="passwordConfirm" value="" size="40"  class="{if $isNew}required{/if}"/>
</p>
<p> 
<label for="group">{$l10n->_('Grupo')}:</label><br/>
{html_options name=accessRole id=accessRole options=$accessRoles selected=$post.accessRole class=required}
</p>
<br/>
<p> 
<input type="submit" value="{$l10n->_('Guardar')}" />
</p>
</form>
	</div>
</div>

<div class="onecolumn">
    <div class="header"><span>{$l10n->_('Listado de Clientes')}</span></div>
    <br class="clear" />
    <div class="content">
<table width="100%" cellspacing="0" cellpadding="0" class="data">
    <thead>
        <tr>
            <th>{$l10n->_('Id')}</th>
            <th>{$l10n->_('Cliente')}</th>
            <th>{$l10n->_('Contacto')}</th>
            <th>{$l10n->_('Email')}</th>
            <!-- <th>{$l10n->_('Status')}</th> -->
            <th colspan="2">{$l10n->_('Actions')}</th>
        </tr>
    </thead>
    <tbody id="ajaxList">
        {foreach $companys as $company}
            <tr class="{$company@iteration|odd}">
                <td>{$company->getIdCompany()}</td>
                <td>{$company->getNameCompany()}</td>
                <td>{$company->getNameContac()}</td>
                <td>{$emails[$company->getIdCompany()]}</td>
               <!--  <td>{$status[$company->getStatus()]}</td> -->
                <td><a href="{url action=edit idCompany=$company->getIdCompany()}">{icon src=pencil class=tip title=$l10n->_('Edit')}</a></td>
                <!-- <td><a href="{url action=delete idCompany=$company->getIdCompany()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Delete')}</a></td> -->
                <td><a href="{url action=rel idCompany=$company->getIdCompany()}">{icon src=add class=tip title=$l10n->_('Add')}</a></td>
            </tr>
        {/foreach}
    </tbody>
</table>
	</div>
</div>
