_row.tpl

<tr>
    <td>{$logo->getIdLogo()}</td>
    <td>{$logo->getName()}</td>
    <td><a href="{url action=edit idLogo=$logo->getIdLogo()}">{icon src=pencil class=tip title=$i18n->_('Edit')}</a></td>
    <td><a href="{url action=delete idLogo=$logo->getIdLogo()}" class="confirm">{icon src=delete class=tip title=$i18n->_('Delete')}</a></td>
</tr>
