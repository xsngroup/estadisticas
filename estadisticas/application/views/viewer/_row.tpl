_row.tpl

<tr>
    <td>{$viewer->getIdViewer()}</td>
    <td>{$viewer->getName()}</td>
    <td>{$viewer->getTimestamp()}</td>
    <td><a href="{url action=edit idViewer=$viewer->getIdViewer()}">{icon src=pencil class=tip title=$i18n->_('Edit')}</a></td>
    <td><a href="{url action=delete idViewer=$viewer->getIdViewer()}" class="confirm">{icon src=delete class=tip title=$i18n->_('Delete')}</a></td>
</tr>
