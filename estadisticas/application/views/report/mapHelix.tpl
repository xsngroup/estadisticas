<style type="text/css">
  html { height: 100% }
  body { height: 100%; margin: 0px; padding: 0px }
  #map_canvas { height: 100% }
</style>

<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&language=es"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	$('#download').click(function(){	
		//var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, width=508, height=365, top=85, left=140";
		//window.open(baseUrl + "/report/download","",opciones);	 

		location.href=baseUrl + "/report/download-maps-helix";
		
	});	
	var a = {$tamanio};
	{if $tamanio < 57}
    var latlng = new google.maps.LatLng(41.244772343082076, -13.359375);
    var myOptions = {
      zoom: 2,
      center: latlng,
      mapTypeControl: false,
      navigationControl: false,
		streetViewControl: false,
		zoomControl: true,		  
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"),
        myOptions);
    {assign var="indicador" value=1}
    {foreach $ipAccess as $key => $value}
        var latlng2{$indicador} = new google.maps.LatLng({$latlon[$key]});
	    var contentString{$indicador} = '<div id="contentTip">'+
	    '<div id="siteNotice">'+
	    '</div>'+
	    '<h3 id="firstHeading" class="firstHeading">Accesos a se\xF1al en vivo</h3>'+
	    '<div id="bodyContent">'+
	    '<p><b>{$key}</b>, {$value} visitantes   ' +
	    //'<a href="javascript:void(0);">'+
	    //'ver detalle...</a></p>'+
	    '</div>'+
	    '</div>';
	
		var infowindow{$indicador} = new google.maps.InfoWindow({
		    content: contentString{$indicador}
		});
		
		var marker{$indicador} = new google.maps.Marker({
		    position: latlng2{$indicador},
		    map: map,
		    title:"{$key}"
		});
		
		google.maps.event.addListener(marker{$indicador}, 'click', function() {
		  infowindow{$indicador}.open(map,marker{$indicador});
		});	
		{$indicador=$indicador+1}
	{/foreach}
	{/if}
	});	

</script>
<div class="onecolumn">
 <div class="header"><span>{$l10n->_('Helix hist&oacute;rico geolocalizador')}</span></div>
 <br class="clear" />
 <div class="content">
<form action="#" class="validate" method="post">
<p>
<label>Del: </label>{$dates[0]}
<label> al: </label>{$dates[1]}
</p>
<br/>
<p>
<label>Se�al: </label>{$streamName}
</p>
<br/>
{if $tamanio < 57}
<div id="mapcontainer" style="position:relative;width:100%;height:100%;">
  <div id="map_canvas" style="position:absolute;width:100%; height:100%,z-index:1"></div>
</div>
{/if}
{if $tamanio >=57}
<div id="textoError" style="position:relative; padding-left:125px; font-weight:bold; font-size:14px"><p>Las IP's exceden el numero permito para mostrar visualmente, para m�s detalle porfavor descarga el reporte en excel.</p></div>
{/if}
<br/>
<br/> 
<input type="button" value="Descargar excel" id="download"> 
<input type="button" value="{$l10n->_('Regresar')}" class="back" />
</p>
</form>
</div>
</div>