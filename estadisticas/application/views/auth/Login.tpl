<script type="text/javascript" src="{$baseUrl}/js/modules/auth/login.js"></script>
<div id="loginFormContainer">
<div class="onecolumn">
 <div class="header"><span>{$l10n->_('Iniciar Sesi�n')}</span></div>
 <br class="clear" />
 <div class="content">
	{if $errorMessage}
		<div class="error alert_error"><p>{$errorMessage}</p></div>
	{/if}
	<form action="{$baseUrl}/auth/login" method="post" id="loginForm">
	<p> 
	  <label for="username">{$l10n->_('Usuario')}:</label><br/>
	  <input type="text" name="username" id="username" value="{$post.username}" style="width: 95%;"/>
	</p>
	<p> 
	  <label for="password">{$l10n->_('Contrase�a')}:</label><br/>
	  <input type="password" name="password" id="password" value="" style="width: 95%;"/>
	</p>
	<br/>
	<p> 
	  <input type="submit" value="{$l10n->_('Iniciar Sesi�n')}"/>
	</p>
	</form>
 </div>	
	</div>
</div>