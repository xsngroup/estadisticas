<tr>
    <td>{$helixLive->getIdLive()}</td>
    <td>{$helixLive->getName()}</td>
    <td>{$servers[$helixLive->getIdServer()]}</td>
    <td><a href="{url action=edit idLive=$helixLive->getIdLive()}">{icon src=pencil class=tip title=$l10n->_('Edit')}</a></td>
    <td><a href="{url action=delete idLive=$helixLive->getIdLive()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Delete')}</a></td>
</tr>
