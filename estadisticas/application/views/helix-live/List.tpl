<div class="column_left">
    <div class="header"><span>{$l10n->_('Helix Server Puntos de Montaje')}</span></div>
    <br class="clear" />
    <div class="content">
<form action="{url action=create}" method="post" class="validate ajaxForm">
<h4>{$l10n->_('Guardar nuevo punto de montaje')}</h4>
{include file='forms/HelixLive.tpl'}
<br/>
<p> 
<input type="submit" value="{$l10n->_('Guardar')}" />
</p>
</form>
	</div>
</div>

<div class="column_right">
    <div class="header"><span>{$l10n->_('Listado de Puntos de Montaje')}</span></div>
    <br class="clear" />
    <div class="content">
<table width="100%" cellspacing="0" cellpadding="0" class="data">
    <thead>
        <tr>
            <th>{$l10n->_('IdLive')}</th>
            <th>{$l10n->_('Name')}</th>
            <th>{$l10n->_('IdServer')}</th>
            <th colspan="2">{$l10n->_('Actions')}</th>
        </tr>
    </thead>
    <tbody id="ajaxList">
        {foreach $helixLives as $helixLive}
            <tr class="{$helixLive@iteration|odd}">
                <td>{$helixLive->getIdLive()}</td>
                <td>{$helixLive->getName()}</td>
                <td>{$servers[$helixLive->getIdServer()]}</td>
                <td><a href="{url action=edit idLive=$helixLive->getIdLive()}">{icon src=pencil class=tip title=$l10n->_('Edit')}</a></td>
                <td><a href="{url action=delete idLive=$helixLive->getIdLive()}" class="confirm">{icon src=delete class=tip title=$l10n->_('Delete')}</a></td>
            </tr>
        {foreachelse}
           <tr><td colspan="5" align="center">No existen elementos para mostrar...</td></tr>               
        {/foreach}
    </tbody>
</table>
	</div>
</div>

